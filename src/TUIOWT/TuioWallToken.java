/*
	TUIO Java backend - part of the reacTIVision project
	http://reactivision.sourceforge.net/

	Copyright (c) 2005-2009 Martin Kaltenbrunner <mkalten@iua.upf.edu>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
 /*
	Extension to WallTokens by Anonymous authors
 */
    
package TUIOWT;

/**
 * The TuioWallToken class encapsulates /tuio/2Dtok TUIO objects.
 *
 * @author Anonymous Author
 * @version 1.4
 */ 
public class TuioWallToken extends TuioObject {
	
	/* <summary>
    * The individual name ID that is assigned to each TuioWallToken in the template.</summary>
    */
    protected String tokenID;

    /**
     * <summary>
     * tell whether the token is fixed on the wall.</summary>
     */
    protected boolean tfixed;
    /**
    * <summary>
    * Color of the token </summary>
    */
    protected int[] rgba = new int[4];

    protected String shape = "";
    protected float ray = 0;
    protected float inner = 0;
	

    /**
     * <summary>
     * This constructor takes a TuioTime argument and assigns it along with the provided
     * Session ID, Symbol ID, name ID, X and Y coordinate, angle, etc. to the newly created TuioWallToken.</summary>
     *
     * <param name="ttime">the TuioTime to assign</param>
     * <param name="si">the Session ID to assign</param>
     * <param name="sym">the Symbol ID to assign</param>
     * <param name="tid">the name ID to assign</param>
     * <param name="xp">the X coordinate to assign</param>
     * <param name="yp">the Y coordinate to assign</param>
     * <param name="a">the angle to assign</param>
     * <param name="f">the token is attached or not</param>
     * <param name="RGBA">the color to assign</param>
     * <param name="s">the shape name to assign</param>
     * <param name="r">the ray of the token to asign</param>
     */
    public TuioWallToken(TuioTime ttime, long si, int sym, String tid, float xp, float yp, float a, boolean f, int[] RGBA, String s, float r) {
        super(ttime, si, sym, xp, yp, a);
        this.tokenID = tid;
        this.tfixed = f;
        this.rgba[0] = RGBA[0]; this.rgba[1] = RGBA[1]; this.rgba[2] = RGBA[2]; this.rgba[3] = RGBA[3];
        this.shape = s;
        this.ray = r;
    }

    /**
     * <summary>
     * This constructor takes the provided Session ID, Symbol ID, X and Y coordinate
     * and angle, and assigs these values to the newly created TuioObject.</summary>
     *
     * <param name="si">the Session ID to assign</param>
     * <param name="sym">the Symbol ID to assign</param>
     * <param name="tid">the name ID to assign</param>
     * <param name="xp">the X coordinate to assign</param>
     * <param name="yp">the Y coordinate to assign</param>
     * <param name="a">the angle to assign</param>
     * <param name="f">the token is attached or not</param>
     * <param name="RGBA">the color to assign</param>
     * <param name="s">the shape name to assign</param>
     * <param name="r">the ray of the token to asign</param>
     */
    public TuioWallToken(long si, int sym, String tid, float xp, float yp, float a, boolean f, int[] RGBA, String s, float r){
        super(si, sym, xp, yp, a);
       	this.tokenID = tid;
        this.tfixed = f;
        this.rgba[0] = RGBA[0]; this.rgba[1] = RGBA[1]; this.rgba[2] = RGBA[2]; this.rgba[3] = RGBA[3];
        this.shape = s;
        this.ray = r;
    }

   	/**
	 * Takes a TuioTime argument and assigns it along with the provided 
	 * X and Y coordinate and angle to the private TuioWallToken attributes.
	 * The speed and accleration values are calculated accordingly.
	 *
	 * @param	ttime	the TuioTime to assign
	 * @param	xp	the X coordinate to assign
	 * @param	yp	the Y coordinate to assign
	 * @param	a	the angle coordinate to assign
	 * @param   f   the token is attached or not
	 */
	public void update (TuioTime ttime, float xp, float yp, float a, boolean f) {
		super.update(ttime, xp, yp, a);
		this.tfixed = f;
	}

	/**
	 * Takes a TuioTime argument and assigns it along with the provided 
	 * X and Y coordinate, angle, X and Y velocity, motion acceleration,
	 * rotation speed and rotation acceleration to the private TuioWallToken attributes.
	 *
	 * @param	ttime	the TuioTime to assign
	 * @param	xp	the X coordinate to assign
	 * @param	yp	the Y coordinate to assign
	 * @param	a	the angle coordinate to assign
	 * @param	xs	the X velocity to assign
	 * @param	ys	the Y velocity to assign
	 * @param	rs	the rotation velocity to assign
	 * @param	ma	the motion acceleration to assign
	 * @param	ra	the rotation acceleration to assign
	 * @param   f   the token is attached or not
	 */
	public void update (TuioTime ttime, float xp, float yp, float a, float xs, float ys, float rs, float ma, float ra,  boolean f) {
		super.update(ttime,xp,yp, a, xs, ys, rs, ma, ra);
		this.tfixed = f;
	}
    /**
     * <summary>
     * Returns the Token ID  name of this TuioWallToken.</summary>
     * <returns>the Token ID of this TuioWallToken</returns>
     */
    public String getTokenID()
    {
        return tokenID;
    }

    public boolean isFixed()
    {
        return tfixed;
    }

    public void setFixed(boolean value) {
        this.tfixed = value;
    }

    public int[] RGBA()
    {
        return rgba; 
    }

    public void setRGBA(int[] value) {
        this.rgba = value; 
    }

    public String getShape()
    {
        return shape; 
    }
    
    public void setShape(String value) {  
    	this.shape = value; 
    }
    
    public float getRay()
    {
        return ray; 
    }
    public void setRay(float value){
    	this.ray = value; 
    }
    
    public float getInner()
    {
        return inner;
    }

    public void setInner(float value){
        this.inner = value;
    }
}
