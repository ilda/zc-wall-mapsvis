package ilda.zcsample.walls;

public interface  WallCoordinateCalculator {

public double getWallWidth();
public double getWallHeight();
public double getPixInMM();

public double getOneScreenWidth();
public double getOneScreenHeight();

public double getBezelWidth();
public double getBezelHeight();

public int getGridCol();
public int getGridRow();

public String getViconHost();
public double getWallRoomWidth();
public double getWallRoomDepth();

public double[] getCoordinate(double[] pos, double[] dir);

}
