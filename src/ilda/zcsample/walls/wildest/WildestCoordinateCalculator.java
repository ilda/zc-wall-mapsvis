package ilda.zcsample.walls.wildest;

import ilda.zcsample.walls.WallCoordinateCalculator;
import ilda.zcsample.utils.VectUtils;

public class WildestCoordinateCalculator implements WallCoordinateCalculator
{

static public final double WALL_ONE_SCREEN_W = 3840;
static public final double WALL_ONE_SCREEN_H = 2160;
//
static public final double WALL_BEZEL_W = 126;
static public final double WALL_BEZEL_H = 154;
// 
static public final int WALL_COLUMN = 8;
static public final int WALL_RAW = 4;
// no ext bezel
static public final double WALL_SCREEN_W = 31602;  // (3840+2*63)*6 + 2*(3840+63)
static public final double WALL_SCREEN_H = 9102; // (2160+2*77)*2 + 2*(2160+77)
static public final double PIX_IN_MM = 0.1822917;
//
static public final String VICON_HOST = "vicon.wild.lri.fr";
static public final double WALL_ROOM_WIDTH = 8000;
static public final double WALL_ROOM_DEPTH = 3000;

@Override public double getWallWidth() { return WALL_SCREEN_W; }
@Override public double getWallHeight() { return WALL_SCREEN_H; }
@Override public double getPixInMM() { return PIX_IN_MM; }
@Override public double getOneScreenWidth() { return WALL_ONE_SCREEN_W;}
@Override public double getOneScreenHeight() { return WALL_ONE_SCREEN_H;}
@Override public double getBezelWidth() { return WALL_BEZEL_W ;}
@Override public double getBezelHeight() { return WALL_BEZEL_H;}
@Override public int getGridCol() { return WALL_COLUMN;}
@Override public int getGridRow() { return WALL_RAW ;}
@Override public String getViconHost() { return VICON_HOST;}
@Override public double getWallRoomWidth() { return WALL_ROOM_WIDTH;}
@Override public double getWallRoomDepth() {return WALL_ROOM_DEPTH;}

static public class WildestPointedScreen {
	public double wx,wy; // wall coordinate
	public boolean inverted; //
	public WildestPointedScreen(double x, double y)
	{wx = x; wy = y; inverted = false;}
}

static public WildestPointedScreen getWildestPointedScreen(double[] position,  double[] direction)
{
	WildestPointedScreen wps = new WildestPointedScreen(0, 0);
	
	WildestScreen s = WildestScreens.all[0];

	double[] l = new double[3]; 
	l[0] = (position[0] - s.orig[0]); 
	l[1] = (position[1] - s.orig[1]); 
	l[2] = (position[2] - s.orig[2]);
	
	double[] ox2 = VectUtils.cross(s.vectY, direction);
	double[] oy2 = VectUtils.cross(direction, s.vectX);
	double[] oz2 = VectUtils.cross(s.vectX, s.vectY);

	double d1  = (s.vectX[0] * ox2[0])+  (s.vectX[1] * ox2[1]) + (s.vectX[2] * ox2[2]);
	double d2 =  (s.vectY[0] * oy2[0]) + (s.vectY[1] * oy2[1]) + (s.vectY[2] * oy2[2]);
	//double d3 = (direction[0] * oz2[0]) + (direction[1] * oz2[1]) + (direction[2] * oz2[2]);

	// FIXME
	// if (Math.abs(d1) == 0 || Math.abs(d2) == 0) continue;

	double[] p = new double[3];
	p[0] = ((l[0] * ox2[0]) + (l[1] * ox2[1]) + (l[2] * ox2[2]))/d1; 
	//((s.vectX[0] * ox2[0])+  (s.vectX[1] * ox2[1]) + (s.vectX[2] * ox2[2]));
	p[1] = ((l[0] * oy2[0]) + (l[1] * oy2[1]) + (l[2] * oy2[2]))/d2; 
	//((s.vectY[0] * oy2[0]) + (s.vectY[1] * oy2[1]) + (s.vectY[2] * oy2[2]));
	//p[2] = ((l[0] * oz2[0]) + (l[1] * oz2[1]) + (l[2] * oz2[2])) / 
	//	((direction[0] * oz2[0]) + (direction[1] * oz2[1]) + (direction[2] * oz2[2]));

	// indeed we are in pixel here and moreover we swaped z and y !!! 
	double[] c = new double[2];
	c[0] = (WALL_SCREEN_W/2.0);
	c[1] = (WALL_SCREEN_H/2.0);
	
	double d = (c[0] - p[0])*(c[0] - p[0]) + (c[1] - p[1])*(c[1] - p[1]);


	wps.wx = p[0];
	wps.wy = p[1];
	if (direction[1] < 0)
	{
		wps.inverted = true;
	}
	else
	{
		wps.inverted = false;
	}

	return wps;
}

@Override
public double[]  getCoordinate(double[] position,  double[] direction)
{
	WildestPointedScreen wps = getWildestPointedScreen(position, direction);

	//System.out.println(wps.name + " " + wps.wx + " " + wps.sx);
	//if (wps.inverted) wps.wx = wps.wy = -100000;
	return new double[] {wps.wx, wps.wy};
}

}