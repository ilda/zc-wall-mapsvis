package ilda.zcsample.walls.wildest;

public class WildestScreens
{
	
public static final WildestScreen[] all = new WildestScreen[]
{
		//vectX = new Vector3(-0.250734f, 0.00142941f, -0.000593762f)*0.714f,
		//vectY = new Vector3(0.000343606f, -0.0039237f, -0.244973f)*0.714f,
		//vectZ = new Vector3(-0.00573797f, -0.999855f, 0.0160065f)*0.714f
	new WildestScreen(
		       new double[] {2895.0,  -3.5381, 2286.79},  // x,z,y
		       new double[] {-0.1790241, 0.001020599, -0.0004239461},
		       new double[] {0.0002453347, -0.002801522, -0.1749107},
		       new double[] {-0.004096911, -0.7138965,  0.01142864}),
};

}
