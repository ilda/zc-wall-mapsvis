package ilda.zcsample.walls.wild;

import ilda.zcsample.walls.WallCoordinateCalculator;
import ilda.zcsample.utils.VectUtils;

public class WildCoordinateCalculator implements WallCoordinateCalculator
{

static public final double WALL_ONE_SCREEN_W = 2560;
static public final double WALL_ONE_SCREEN_H = 1600;
//
static public final double WALL_BEZEL_W = 92;
static public final double WALL_BEZEL_H = 107;
// 
static public final int WALL_COLUMN = 8;
static public final int WALL_RAW = 4;
// no ext bezel
static public final double WALL_SCREEN_W = 21768;  // (2560+2*92)*6 + 2*(2560+92)
static public final double WALL_SCREEN_H = 7042; // (1600+2*107)*2 + 2*(1600+107)
static public final double PIX_IN_MM = 0.25;
//
static public final String VICON_HOST = "vicon.wild.lri.fr";
static public final double WALL_ROOM_WIDTH = 8000;
static public final double WALL_ROOM_DEPTH = 3000;

@Override public double getWallWidth() { return WALL_SCREEN_W; }
@Override public double getWallHeight() { return WALL_SCREEN_H; }
@Override public double getPixInMM() { return PIX_IN_MM; }
@Override public double getOneScreenWidth() { return WALL_ONE_SCREEN_W;}
@Override public double getOneScreenHeight() { return WALL_ONE_SCREEN_H;}
@Override public double getBezelWidth() { return WALL_BEZEL_W ;}
@Override public double getBezelHeight() { return WALL_BEZEL_H;}
@Override public int getGridCol() { return WALL_COLUMN;}
@Override public int getGridRow() { return WALL_RAW ;}
@Override public String getViconHost() { return VICON_HOST;}
@Override public double getWallRoomWidth() { return WALL_ROOM_WIDTH;}
@Override public double getWallRoomDepth() {return WALL_ROOM_DEPTH;}

static public double[] name2coord(String name)
{
	double[] ret = new double[2];
	
	double x = 0, y = 0, l = 0, c = 0, m = 0;

	if (name.length() >= 3)
	{
		int n = new Integer(name.substring(1,2));
		// 4 -> 0 ; 1 -> 3 
		//l = -n + 4;
		l = n -1;
		y = (l) * (WALL_ONE_SCREEN_H + 2*WALL_BEZEL_H) - WALL_BEZEL_H;
		if (name.substring(0,1).equals("A") && name.substring(2,3).equals("L"))
		{
			c = 0;
		}
		else if (name.substring(0,1).equals("A") && name.substring(2,3).equals("R"))
		{
			c = 1;
		}
		else if (name.substring(0,1).equals("B") && name.substring(2,3).equals("L"))
		{
			c = 2;
		}
		else if (name.substring(0,1).equals("B") && name.substring(2,3).equals("R"))
		{
			c = 3;
		}
		else if (name.substring(0,1).equals("C") && name.substring(2,3).equals("L"))
		{
			c = 4;
		}
		else if (name.substring(0,1).equals("C") && name.substring(2,3).equals("R"))
		{
			c = 5;
		}
		else if (name.substring(0,1).equals("D") && name.substring(2,3).equals("L"))
		{
			c = 6;
		}
		else if (name.substring(0,1).equals("D") && name.substring(2,3).equals("R"))
		{
			c = 7;
		}
		x = (c) * (WALL_ONE_SCREEN_W + 2*WALL_BEZEL_W) - WALL_BEZEL_W;
	}
	ret[0] = x;
	ret[1] = y;

	return ret;
}

static public class wildPointedScreen {
	public String name;  // picked (indeed closest screen)
	public double wx,wy; // wall coordinate
	public double sx,sy; // picked screen coordinate
	public boolean inverted; //
	public wildPointedScreen(String n, double x, double y, double a, double b)
	{ name = n; wx = x; wy = y;  sx = a; sy = b; inverted = false;}
}

static public wildPointedScreen getWildPointedScreen(double[] position,  double[] direction)
{
	String name = null;
	double mindist = -1;
	wildPointedScreen wps = new wildPointedScreen(null, 0, 0, 0, 0);

	for(int i = 0; i < 32; i++)
	{
		WildScreen s = WildScreens.all[i];

		double[] l = new double[3]; 
		l[0] = (position[0] - s.orig[0]); 
		l[1] = (position[1] - s.orig[1]); 
		l[2] = (position[2] - s.orig[2]);
		
		double[] ox2 = VectUtils.cross(s.vectY, direction);
		double[] oy2 = VectUtils.cross(direction, s.vectX);
		double[] oz2 = VectUtils.cross(s.vectX, s.vectY);

		double d1  = (s.vectX[0] * ox2[0])+  (s.vectX[1] * ox2[1]) + (s.vectX[2] * ox2[2]);
		double d2 =  (s.vectY[0] * oy2[0]) + (s.vectY[1] * oy2[1]) + (s.vectY[2] * oy2[2]);
		//double d3 = (direction[0] * oz2[0]) + (direction[1] * oz2[1]) + (direction[2] * oz2[2]);

		if (Math.abs(d1) == 0 || Math.abs(d2) == 0) continue;

		double[] p = new double[3];
		p[0] = ((l[0] * ox2[0]) + (l[1] * ox2[1]) + (l[2] * ox2[2]))/d1; 
		//((s.vectX[0] * ox2[0])+  (s.vectX[1] * ox2[1]) + (s.vectX[2] * ox2[2]));
		p[1] = ((l[0] * oy2[0]) + (l[1] * oy2[1]) + (l[2] * oy2[2]))/d2; 
		//((s.vectY[0] * oy2[0]) + (s.vectY[1] * oy2[1]) + (s.vectY[2] * oy2[2]));
		//p[2] = ((l[0] * oz2[0]) + (l[1] * oz2[1]) + (l[2] * oz2[2])) / 
		//	((direction[0] * oz2[0]) + (direction[1] * oz2[1]) + (direction[2] * oz2[2]));

		// indeed we are in pixel here and moreover we swaped z and y !!! 
		double[] c = new double[2];
		c[0] = (WALL_ONE_SCREEN_W/2.0);
		c[1] = (WALL_ONE_SCREEN_H/2.0);
		
		double d = (c[0] - p[0])*(c[0] - p[0]) + (c[1] - p[1])*(c[1] - p[1]);

		if (mindist < 0 || d < mindist)
		{
			mindist = d;

			wps.name = s.name;
			wps.sx = p[0];
			wps.sy = p[1];
			if (direction[1] < 0)
			{
				wps.inverted = true;
			}
			else
			{
				wps.inverted = false;
			}
			// could not work ...
			//wps.wx =  p[0] + (s.orig[0] /s.vectX.x);
			//wps.wy =  p[1] + (s.orig[2] /s.vectY.z);
			
		}
	}

	double[] t = name2coord(wps.name);
	wps.wx = wps.sx + t[0];
	wps.wy = wps.sy + t[1];

	return wps;
}

public double[]  getCoordinate(double[] position,  double[] direction)
{
	wildPointedScreen wps = getWildPointedScreen(position, direction);

	//System.out.println(wps.name + " " + wps.wx + " " + wps.sx);
	if (wps.inverted) wps.wx = wps.wy = -100000;
	return new double[] {wps.wx, wps.wy};
}

}