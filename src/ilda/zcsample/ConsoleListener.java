package ilda.zcsample;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.Dimension;

import fr.inria.zvtm.cluster.ClusterGeometry;
import fr.inria.zvtm.cluster.ClusteredView;
import fr.inria.zvtm.cluster.ClusteredViewAdapter;

public class ConsoleListener extends ClusteredViewAdapter
{
	//static final double WHEEL_ZOOMIN_FACTOR = 1.1;
	//static final double WHEEL_ZOOMOUT_FACTOR = 1/1.1;
	double WHEEL_ZOOMIN_FACTOR = 1.1;
	double WHEEL_ZOOMOUT_FACTOR = 1/1.1;
	ClusteredView cv;
	int vId;

	ClusterDisplay _display;
	double _width, _height;

	private int lastJPX;
	private int lastJPY;
	private double prevPressX = 0, prevPressY = 0;
	private int pressedButton = 0;
	private boolean indrag = false;

	public ConsoleListener(ClusterDisplay cd, ClusteredView cv, int vid)
	{
		_display = cd;
		this.cv = cv;
		this.vId = vid;
		Dimension d = cv.getSize();
		_width = d.getWidth();
		_height = d.getHeight();
		_display.registerDevice(this,"Console "+vid);
		ClusterDisplay.zcsEvent e =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, 1);
		e.x = 1.0; e.y = 1.0;
		e.vId = vId;
		_display.handleEvent(e);
	}

	@Override
	public void press1(int blockNum, int mod, int jpx, int jpy){
		prevPressX =  jpx/_width;
		prevPressY = jpy/_height;
		pressedButton = 1;
	}
	@Override
	public void release1(int blockNum, int mod, int jpx, int jpy){
		if (indrag && pressedButton == 1){
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, this, 1);
			zcse.x = jpx/_width; zcse.y =  jpy/_height;
			_display.handleEvent(zcse);		
			pressedButton = 0;
			indrag = false;
		}
	}
	@Override
	public void click1(int blockNum, int mod, int jpx, int jpy, int clickNumber){
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CURSOR_CLICK, this, 1);
		zcse.x = jpx/_width; zcse.y =  jpy/_height;
		zcse.button = 1;
		_display.handleEvent(zcse);
		if (pressedButton == 1){
			pressedButton = 0;
		}
	}
	@Override
	public void press2(int blockNum, int mod, int jpx, int jpy){
		prevPressX =  jpx/_width;
		prevPressY = jpy/_height;
		pressedButton = 2;
	}
	@Override
	public void release2(int blockNum, int mod, int jpx, int jpy){
		if (indrag && pressedButton == 2){
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, this, 1);
			zcse.x = jpx/_width; zcse.y =  jpy/_height;
			_display.handleEvent(zcse);		
			pressedButton = 0;
			indrag = false;
		}
	}
	@Override
	public void click2(int blockNum, int mod, int jpx, int jpy, int clickNumber){

	}
	@Override
	public void press3(int blockNum, int mod, int jpx, int jpy){
		prevPressX =  jpx/_width;
		prevPressY = jpy/_height;
		pressedButton = 3;
	}
	@Override
	public void release3(int blockNum, int mod, int jpx, int jpy){
		if (indrag && pressedButton == 3){
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, this, 1);
			zcse.x = jpx/_width; zcse.y =  jpy/_height;
			_display.handleEvent(zcse);		
			pressedButton = 0;
			indrag = false;
		}
	}
	@Override
	public void click3(int blockNum, int mod, int jpx, int jpy, int clickNumber){

	}
	@Override
	public void mouseMoved(int blockNum, int jpx, int jpy){
		//System.out.println("MouseMoved " + jpx+ " " + jpy);
		double x = jpx/_width;
		double y = jpy/_height;
		double dx = (x - lastJPX/_width);
		double dy = (y - lastJPY/_height);
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, this, 1);
		zcse.x = x; zcse.y = y;
		zcse.dx = dx; zcse.dy = dy;
		_display.handleEvent(zcse);
		lastJPX = jpx; lastJPY = jpy; 
	}

	@Override
	public void mouseDragged(int blockNum, int mod, int buttonNumber,int jpx,int jpy)
	{
		double x = jpx/_width;
		double y = jpy/_height;
		double dx = (x - prevPressX);
		double dy = (y - prevPressY);
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, this, 1);
		zcse.x = x; zcse.y = y;
		zcse.dx = dx; zcse.dy = dy; //*_height/_display.getHeight();
		zcse.button = pressedButton;
		_display.handleEvent(zcse);
		if(!indrag){
			zcse.type = ClusterDisplay.zcsEvent.ZCS_START_DRAG;
			_display.handleEvent(zcse);
			indrag = true;
		}
		//System.out.println("mouseDragged " +zcse.dx*_width+","+ zcse.dy*_height);
		zcse.type = ClusterDisplay.zcsEvent.ZCS_DRAG;
		_display.handleEvent(zcse);

		prevPressX = x; prevPressY = y;
		lastJPX = jpx; lastJPY = jpy; 
	}

	@Override
	public void mouseWheelMoved(
		int blockNum, short wheelDirection, int jpx, int jpy)
	{
		double x = jpx/_width;
		double y = jpy/_height;
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, this, 1);
		zcse.x = x; zcse.y = y;
		if (wheelDirection  == 1){
			zcse.f = WHEEL_ZOOMIN_FACTOR;
		}
		else {
			zcse.f = WHEEL_ZOOMOUT_FACTOR;
		}
		_display.handleEvent(zcse);
	}

	@Override
	public void Kpress(int blockNumber, char c,int code,int mod)
	{
		if (code == KeyEvent.VK_Q) {
		    _display.stop();
			System.exit(0);
		}
	}
}