package ilda.zcsample.cursors;

import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.glyphs.VImage;
import fr.inria.zvtm.glyphs.VRectangle;
import fr.inria.zvtm.glyphs.SIRectangle;

import java.awt.*;

public class SimpleCursor extends AbstractCursor {
    private double thickness;
    private double halfLength;
    private static final double X_START_POSITION = 0;
    private static final double Y_START_POSITION = 0;
    private Color color;

    private SIRectangle hRect;
    private SIRectangle vRect;

    private Object pickedObj = null;
    private boolean isActive = true;

    SimpleCursor(VirtualSpace target) {
        this(target, 10, 80, Color.RED);
    }

    SimpleCursor(VirtualSpace target, long thickness, long halfLength) {
        this(target, thickness, halfLength, Color.RED);
    }

    public SimpleCursor(VirtualSpace target, double thickness, double halfLength, Color color)
    {
        super(target);
        this.thickness = thickness;
        this.halfLength = halfLength;
        this.color = color;
        double bsWidth = 4;

	    if (bsWidth >= thickness) bsWidth =  thickness/2;

	    BasicStroke bs = new BasicStroke((int)bsWidth);

	    hRect = new SIRectangle(X_START_POSITION, Y_START_POSITION, (int)1000, this.halfLength, this.thickness, this.color, Color.BLACK);
	    hRect.setTranslucencyValue(0.7f);
	    hRect.setDrawBorder(true);
	    hRect.setStroke(bs);
	    // hRect.orientTo(Math.PI/4);
	    vRect = new SIRectangle(X_START_POSITION, Y_START_POSITION, (int)1000, this.thickness, this.halfLength, this.color, Color.BLACK);
	    vRect.setTranslucencyValue(0.7f);
	    vRect.setDrawBorder(true);
	    vRect.setStroke(bs);

        targetSpace.addGlyph(hRect);
        targetSpace.addGlyph(vRect);
        //vRect.orientTo(Math.PI/4);
    }

    @Override
    public void dispose() {
        targetSpace.removeGlyph(hRect);
        targetSpace.removeGlyph(vRect);
    }

    @Override
    public void moveTo(double x, double y) {
        if (isActive) {
            super.moveTo(x,y);
            hRect.moveTo(x, y);
            vRect.moveTo(x, y);
	   }
    }

    @Override
    public void move(double x, double y){
        if (isActive) {
            super.move(x,y);
            hRect.moveTo(x, y);
            vRect.moveTo(x, y);
        }
    }

    @Override
    public void setVisible(boolean v) {
        super.setVisible(v);
        hRect.setVisible(v);
        vRect.setVisible(v);
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public double[] getBounds(){
        double[] bounds = new double[4];
        bounds[0] = -halfLength;
        bounds[1] = +halfLength;
        bounds[2] = +halfLength;
        bounds[3] = -halfLength;
        return bounds;
    }
}
