package ilda.zcsample;

import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.glyphs.VImage;
import fr.inria.zvtm.glyphs.VRectangle;
import fr.inria.zvtm.glyphs.SIRectangle;

import java.awt.*;

public class WallCursor {
    private double thickness;
    private double halfLength;
    private static final double X_START_POSITION = 0;
    private static final double Y_START_POSITION = 0;
    private Color color;
    private final VirtualSpace target;

    private SIRectangle hRect;
    private SIRectangle vRect;
    private long cursorX = 0;
    private long cursorY = 0;

    private Object pickedObj = null;
    private boolean isActive = true;

    public String getOwnerId() {
        return ownerId;
    }

    String ownerId = "";

    WallCursor(VirtualSpace target) {
        this(null, target, 10, 80, Color.RED);
    }

    WallCursor(VirtualSpace target, long thickness, long halfLength) {
        this(null, target, thickness, halfLength, Color.RED);
    }

    WallCursor(VirtualSpace target, long thickness, long halfLength, Color c) {
	    this(null, target, thickness, halfLength, c);
    }

    public WallCursor(String ownerId, VirtualSpace target, double thickness, double halfLength, Color color)
    {
        this.ownerId = ownerId;
        this.target = target;
        this.thickness = thickness;
        this.halfLength = halfLength;
        this.color = color;
        double bsWidth = 4;

	    if (bsWidth >= thickness) bsWidth =  thickness/2;

	    BasicStroke bs = new BasicStroke((int)bsWidth);

	    hRect = new SIRectangle(X_START_POSITION, Y_START_POSITION, (int)1000, this.halfLength, this.thickness, this.color, Color.BLACK);
	    hRect.setTranslucencyValue(0.7f);
	    hRect.setDrawBorder(true);
	    hRect.setStroke(bs);
	    // hRect.orientTo(Math.PI/4);
	    vRect = new SIRectangle(X_START_POSITION, Y_START_POSITION, (int)1000, this.thickness, this.halfLength, this.color, Color.BLACK);
	    vRect.setTranslucencyValue(0.7f);
	    vRect.setDrawBorder(true);
	    vRect.setStroke(bs);

        target.addGlyph(hRect);
        target.addGlyph(vRect);
        //vRect.orientTo(Math.PI/4);
    }

    public void dispose() {
        target.removeGlyph(hRect);
        target.removeGlyph(vRect);
    }

    public void moveTo(long x, long y) {
        if (isActive) {
            hRect.moveTo(x, y);
            vRect.moveTo(x, y);
            cursorX = x;
            cursorY = y;
	}
    }

    public long getCursorX() {
        return cursorX;
    }

    public long getCursorY() {
        return cursorY;
    }

    public void setVisible(boolean v) {
        hRect.setVisible(v);
        vRect.setVisible(v);
    }

    public double getThickness() {
        return thickness;
    }

    public double getHalfLength() {
        return halfLength;
    }

    public Color getColor() {
        return color;
    }

    public Object getPickedObject() {
        return pickedObj;
    }

    public void setPickedObj(Object obj) {
        this.pickedObj = obj;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }
}
