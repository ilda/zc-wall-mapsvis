package ilda.zcsample;

import java.io.File;
import java.io.IOException;
import java.io.FilenameFilter;

import java.util.HashMap;

import java.awt.Color;

import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.animation.EndAction;
import fr.inria.zvtm.animation.Animation;
import fr.inria.zvtm.animation.interpolation.SlowInSlowOutInterpolator;

import fr.inria.zvtm.engine.VirtualSpaceManager;

import fr.inria.zuist.engine.SceneManager;
import fr.inria.zuist.engine.PseudoView;

public class ZuistRenderer extends Renderer
{

protected SceneManager[] _zsms;
protected double[] _sceneBounds = null;
protected double _sceneWidth = 0, _sceneHeight= 0;
protected int _mainIndex = 0;
protected String[] _sceneNames;

protected SceneManager[] _aux_zsms;
protected double[][] _aux_sceneBounds = null;
protected double[] _aux_sceneWidth = null, _aux_sceneHeight = null;
protected String[] _aux_sceneNames;


private VirtualSpaceManager _vsm = VirtualSpaceManager.INSTANCE;

public ZuistRenderer(ClusterDisplay cd, String zuistFileNames, String zuistAuxFileNames)
{
	super(cd);
	System.out.println("ZuistRenderer "+zuistFileNames);
	String[] splitFileNames = zuistFileNames.split(":");

	// first get the scene names...
	_sceneNames = new String[splitFileNames.length];
	for (int idx = 0; idx < splitFileNames.length; idx++)
    {
    	String[] ss = splitFileNames[idx].split("/"); // UNIX !
    	if (ss.length <= 1){
    		_sceneNames[idx] = ""+idx;
    	}
    	else{
    		_sceneNames[idx] = ss[ss.length-2];
    	}
    	// FIXME: remove the common part in the names ...
    }

	_zsms = new SceneManager[splitFileNames.length];

    for (int idx = 0; idx < splitFileNames.length; idx++)
    {
    	String zuistFileName = splitFileNames[idx];
    	VirtualSpace zvs = null;
		PseudoView pv = null;
		Camera cam = null;
		if (idx == 0) {
			zvs = _vs;
			cam = _camera;
			pv = new PseudoView(zvs, cam, 0, 0); 
		} else {
			zvs =  _vsm.addVirtualSpace("dm scene " + idx);
			cam = zvs.addCamera();
			pv = new PseudoView(zvs, cam, 0, 0); 
		}
		_zsms[idx] = new SceneManager(pv, new HashMap(2,1));
		//_zsms[idx].setDebugMode(true);

		_zsms[idx].enableRegionUpdater(false);
		File xmlSceneFile = new File(zuistFileName);
		try {
			System.out.println("Loading dm Scene ("+idx+") "  +xmlSceneFile.getCanonicalPath());		
		}
		catch (IOException ex){}
		//gp.setValue(0);
		//gp.setVisible(true);
		File SCENE_FILE = xmlSceneFile;;
		File SCENE_FILE_DIR = SCENE_FILE.getParentFile();
		_zsms[idx].loadScene(SceneManager.parseXML(SCENE_FILE), SCENE_FILE_DIR, true, null); // ,gp);
		HashMap sceneAttributes = _zsms[idx].getSceneAttributes();
		
		if (idx == 0){
			setupBounds(0);
		}
		else {
			cam.moveTo(
				(_sceneBounds[0] +  _sceneBounds[2])/2, (_sceneBounds[1]  + _sceneBounds[3])/2);
			_zsms[idx].enableRegionUpdater(true);
			_zsms[idx].setUpdateLevel(true);
		}

		if (idx == 0 && sceneAttributes.containsKey(SceneManager._background)){
			//_display.setBackgroundColor((Color)sceneAttributes.get(SceneManager._background));
		}
    }

    if (splitFileNames.length > 0){
    	_zsms[0].enableRegionUpdater(false);
    	EndAction ea  = new EndAction(){
		    public void execute(Object subject, Animation.Dimension dimension){
			    _zsms[0].setUpdateLevel(true);
			    _zsms[0].enableRegionUpdater(true);
		    }
	    };
	    getGlobalView(ea);
	}

	// aux stuff !
	if (zuistAuxFileNames == null){
		return;
	}

	System.out.println("ZuistRenderer AUX "+zuistAuxFileNames);
	splitFileNames = zuistAuxFileNames.split(":");

	// first get the scene names...
	_aux_sceneNames = new String[splitFileNames.length];
	for (int idx = 0; idx < splitFileNames.length; idx++){
		String[] ss = splitFileNames[idx].split("/"); // UNIX !
    	if (ss.length <= 1){
    		_aux_sceneNames[idx] = ""+idx;
    	}
    	else{
    		_aux_sceneNames[idx] = ss[ss.length-2];
    	}
    	// FIXME: remove the common part in the names ...
    }
	

	_aux_zsms = new SceneManager[splitFileNames.length];
	_aux_sceneBounds = new double[splitFileNames.length][];
	_aux_sceneWidth = new double[splitFileNames.length];
	_aux_sceneHeight = new double[splitFileNames.length];

	for (int idx = 0; idx < splitFileNames.length; idx++)
    {
    	String zuistFileName = splitFileNames[idx];
    	VirtualSpace zvs = null;
		//PseudoView pv = null;
		Camera cam = null;
		zvs =  _vsm.addVirtualSpace("aux dm scene " + idx);
		cam = zvs.addCamera();
		PseudoView pv = new PseudoView(zvs, cam, 100, 100);
		_aux_zsms[idx] = new SceneManager(pv, new HashMap(2,1));
		//_aux_zsms[idx].setDebugMode(true);

		_aux_zsms[idx].enableRegionUpdater(false);
		File xmlSceneFile = new File(zuistFileName);
		try {
			System.out.println("Loading aux dm Scene "+xmlSceneFile.getCanonicalPath());		
		}
		catch (IOException ex){}
		//gp.setValue(0);
		//gp.setVisible(true);
		File SCENE_FILE = xmlSceneFile;;
		File SCENE_FILE_DIR = SCENE_FILE.getParentFile();
		_aux_zsms[idx].loadScene(SceneManager.parseXML(SCENE_FILE), SCENE_FILE_DIR, true, null); // ,gp);
		HashMap sceneAttributes = _aux_zsms[idx].getSceneAttributes();
		
		setupAuxBounds(idx);
		cam.moveTo(
				(_aux_sceneBounds[idx][0] +  _aux_sceneBounds[idx][2])/2, (_aux_sceneBounds[idx][1]  + _aux_sceneBounds[idx][3])/2);
		_aux_zsms[idx].enableRegionUpdater(true);
		_aux_zsms[idx].setUpdateLevel(true);
    }
}

@Override
public boolean setMainLayerIndex(int idx)
{
	if (_mainIndex == idx) return false;
	if (idx < 0 || idx >= _zsms.length) return false;


	// update the camera && vs in _zsms[_mainIndex]
	//PseudoView pvMain = _zsms[_mainIndex].getPseudoViewByCamera(_camera);
	PseudoView pvMain = _zsms[_mainIndex].getPseudoView(0);
	PseudoView pv = _zsms[idx].getPseudoView(0);
	System.out.println("setMainLayerIndex pvMain: " + pvMain.getWidth());
	System.out.println("setMainLayerIndex pv: " + pv.getWidth());
	_zsms[_mainIndex].removePseudoView(0);//_zsms[_mainIndex].removePseudoView(pvMain);
	_zsms[_mainIndex].addPseudoView(0, pv);
	_zsms[idx].removePseudoView(0); //_zsms[idx].removePseudoView(pv);
	_zsms[idx].addPseudoView(0, pvMain);

	// invoke the grande gauffre
	System.out.println("invoke the grande gauffre");
	_zsms[idx].updateVisibleRegions();
	_display.refresh();
	_vsm.repaint();

	_mainIndex = idx;
	return true;
}

@Override
public boolean nextMainLayerIndex(){
	int idx = _mainIndex + 1;
	if (idx >= _zsms.length) idx = 0;
	return setMainLayerIndex(idx);
}

@Override
public String[] getLayerNames()
{
	return _sceneNames;
}

public SceneManager getZuistSceneManager()
{ 
	return _zsms[0];
}

public SceneManager getZuistSceneManagerAtIndex(int idx)
{
	if (idx >= 0 && idx < _zsms.length) return _zsms[idx];
	return null;
}

public SceneManager[] getZuistSceneManagers()
{
	return _zsms;
}

@Override
public String[] getAuxLayerNames()
{
	return _aux_sceneNames;
}

public SceneManager getAuxZuistSceneManagerAtIndex(int idx)
{
	if (idx >= 0 && idx < _aux_zsms.length) return _aux_zsms[idx];
	return null;
}

public SceneManager[] getAuxZuistSceneManagers()
{
	return _aux_zsms;
}



// FIXME _zsms[0] !!!!
void setupBounds(int idx)
{
	_sceneWidth = 0; _sceneHeight= 0;
	_sceneBounds = null;
	int l = 0;
	while (_zsms[idx].getRegionsAtLevel(l) == null){
		l++;
		if (l > _zsms[idx].getLevelCount()){
			l = -1;
			break;
		}
	}
	if (l > -1){
		_sceneBounds = _zsms[idx].getLevel(l).getBounds();
		System.out.println(
			"Bounds ("+idx+","+ l+ ") WNES: " 
			+ _sceneBounds[0] +" "+ _sceneBounds[1] +" "+  _sceneBounds[2] +" "+ _sceneBounds[3]);
		_sceneWidth = - _sceneBounds[0] +  _sceneBounds[2];
		_sceneHeight = _sceneBounds[1]  - _sceneBounds[3];
	}

	System.out.println("Scene Loaded: "+ _sceneWidth +" "+ _sceneHeight);

	if (_sceneBounds != null)
	{
		_camera.moveTo(
			(_sceneBounds[0] +  _sceneBounds[2])/2, (_sceneBounds[1]  + _sceneBounds[3])/2);
	}
	//_camera.setAltitude(0.0);
}

void setupAuxBounds(int idx)
{
	_aux_sceneWidth[idx] = 0; _aux_sceneHeight[idx]= 0;
	_aux_sceneBounds[idx] = null;
	int l = 0;
	while (_aux_zsms[idx].getRegionsAtLevel(l) == null){
		l++;
		if (l > _aux_zsms[idx].getLevelCount()){
			l = -1;
			break;
		}
	}
	if (l > -1){
		_aux_sceneBounds[idx] = _aux_zsms[idx].getLevel(l).getBounds();
		System.out.println(
			"aux bounds ("+idx+") Bounds ("+ l+ ") WNES: " 
			+ _aux_sceneBounds[idx][0] +" "+ _aux_sceneBounds[idx][1] +" "+  _aux_sceneBounds[idx][2] +" "+ _aux_sceneBounds[idx][3]);
		_aux_sceneWidth[idx] = - _aux_sceneBounds[idx][0] +  _aux_sceneBounds[idx][2];
		_aux_sceneHeight[idx] = _aux_sceneBounds[idx][1]  - _aux_sceneBounds[idx][3];
	}

	System.out.println("aux Scene Loaded ("+idx+"): "+ _aux_sceneWidth[idx] +" "+ _aux_sceneHeight[idx]);
}

public void globalView(){
	getGlobalView(null);
}

void getGlobalView(EndAction ea)
{
    if (_sceneBounds == null) { return; }

    _camera.moveTo(
	    (_sceneBounds[0] +  _sceneBounds[2])/2, (_sceneBounds[1] + _sceneBounds[3])/2);
    double fw = (double) _sceneWidth / (double) _displayWidth;
    double fh = (double) _sceneHeight / (double) _displayHeight;
    double f = fw;
    //System.out.println("fw: " + fw + ", fh: " + fh);
    if (fh > fw) f = fh;
    _camera.setAltitude(0.0);
    double a = (_camera.focal + _camera.altitude) / _camera.focal;
    double newz = _camera.focal * a * f - _camera.focal;
    _camera.setAltitude(newz);
    if (ea != null)
    {
	    ea.execute(null,null);
    }

}

void getGlobalViewAtCam(Camera cam, double w, double h, boolean onlycenter)
{
    if (_sceneBounds == null) { return; }

    cam.moveTo(
	    (_sceneBounds[0] +  _sceneBounds[2])/2, (_sceneBounds[1] + _sceneBounds[3])/2);
    if (onlycenter) { return; }
    double fw = (double) _sceneWidth / (double) w;
    double fh = (double) _sceneHeight / (double) h;
    double f = fw;
    //System.out.println("fw: " + fw + ", fh: " + fh);
    if (fh > fw) f = fh;
    cam.setAltitude(0.0);
    double a = (cam.focal + cam.altitude) / cam.focal;
    double newz = cam.focal * a * f - cam.focal;
    cam.setAltitude(newz);
}

void auxGlobalView(Camera cam, int idx, double width, double height)
{
    if (idx < 0 || idx >= _aux_sceneBounds.length  || _aux_sceneBounds[idx] == null) { return; }

    cam.moveTo(
	    (_aux_sceneBounds[idx][0] +  _aux_sceneBounds[idx][2])/2, (_aux_sceneBounds[idx][1] + _aux_sceneBounds[idx][3])/2);
    double fw = (double) _aux_sceneWidth[idx] / (double) width;
    double fh = (double) _aux_sceneHeight[idx] / (double) height;
    double f = fw;
    //System.out.println("fw: " + fw + ", fh: " + fh);
    if (fh > fw) f = fh;
    cam.setAltitude(0.0);
    double a = (cam.focal + cam.altitude) / cam.focal;
    double newz = cam.focal * a * f - cam.focal;
    cam.setAltitude(newz);
}

// static HashMap<String,String> parseSceneOptions(ViewerOptions options)
// {
//         HashMap<String,String> res = new HashMap(2,1);
//         if (options.httpUser != null){
//             res.put(SceneManager.HTTP_AUTH_USER, options.httpUser);
//         }
//         if (options.httpPassword != null){
//             res.put(SceneManager.HTTP_AUTH_PASSWORD, options.httpPassword);
//         }
//         return res;
// }

@Override
public double[] getBounds()
{
	return _sceneBounds;
}

public double[] getAuxBounds(int idx){
	if (_aux_sceneBounds != null && idx >= 0 && idx < _aux_sceneBounds.length){
		return _aux_sceneBounds[idx];
	}
	return null;
}
@Override
public boolean click(double x, double y)
{
	return false;
}

}
