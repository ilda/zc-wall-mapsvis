package ilda.zcsample;

import java.util.Vector;
import java.util.HashMap;
import java.util.List;

import java.util.TimerTask;
import java.util.Timer;
import java.util.Date;

import java.awt.Color;

import java.io.File;
import java.io.IOException;
import java.io.FilenameFilter;

import fr.inria.zvtm.cluster.ClusterGeometry;
import fr.inria.zvtm.cluster.ClusteredView;
import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.engine.Location;

import fr.inria.zvtm.glyphs.Glyph;
import fr.inria.zvtm.glyphs.VCircle;
import fr.inria.zvtm.glyphs.VRectangle;
import fr.inria.zvtm.glyphs.VRing;
import fr.inria.zvtm.glyphs.VRectangleOr;
import fr.inria.zvtm.glyphs.SICircle;
import fr.inria.zvtm.glyphs.VPolygon;
import fr.inria.zvtm.glyphs.DPath;

import fr.inria.zuist.engine.SceneManager;
import fr.inria.zuist.engine.Level;
import fr.inria.zuist.engine.Region;

// GeoTools
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.feature.FeatureCollection;
// import org.geotools.feature.simple.SimpleFeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
// import org.geotools.factory.GeoTools;
import org.geotools.util.factory.GeoTools;
import org.geotools.data.shapefile.shp.JTSUtilities;
import org.geotools.geometry.jts.LiteShape;
//
// import com.vividsolutions.jts.geom.Geometry;
// import com.vividsolutions.jts.geom.Coordinate;
// import com.vividsolutions.jts.geom.Point;
// import com.vividsolutions.jts.geom.Polygon;
// import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;
// import com.vividsolutions.jts.geom.util.PolygonExtracter;
// import com.vividsolutions.jts.geom.util.LinearComponentExtracter;
// import com.vividsolutions.jts.geom.LineString;
// import com.vividsolutions.jts.linearref.LinearIterator;
//
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.simplify.DouglasPeuckerSimplifier;
import org.locationtech.jts.geom.util.PolygonExtracter;
import org.locationtech.jts.geom.util.LinearComponentExtracter;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.linearref.LinearIterator;
//
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.Map;
import java.net.MalformedURLException;

import fr.inria.zvtm.glyphs.BoatInfoG;

public class TsunamiOverlay {

protected double [] _bounds;
protected ClusterDisplay _display;
protected Renderer _renderer;
protected VirtualSpace _vs;
protected Camera _camera;

//protected VRing _ring = null;
//protected double _radius;
//protected double _grow;
//protected Timer _timer;
//protected ScheduledTask _sheduledTask;
//protected boolean _started = false;


// Evacuations
private Vector<VPolygon> _evacPolys = null; 
private boolean _evacDisplayed = false;
private Vector<SICircle> _evacPoints = null;
private boolean _evacPointsDisplayed = false;

// Tsunami
private Vector<DPath> _tsunamiLines = null;
private SICircle _epicentre;
private boolean _tsunamiDisplayed = false;
private Vector<VPolygon> _mapPolys = null; 
private boolean _mapDisplayed = false;

// Boats
private Vector<Boat> _boats = null;
private boolean _boatsDisplayed = false;
private Vector<VPolygon> _securePolys = null; 
private boolean _secureDisplayed = false;

private String[] _dataSets = {"Evacuation", "Tsunami", "Boats"};


public TsunamiOverlay(ClusterDisplay cd, Renderer r, VirtualSpace vs, Camera cam)
{
	_display = cd;
	_renderer = r;
	_bounds = r.getBounds();
	_vs = vs;
	_camera = cam;
	Camera mcam = _display.getMainCamera();
	//Location cgl = mcam.getLocation();
	_camera.setLocation(mcam.getLocation());
	mcam.stick(_camera, true);
	
	//initRendering();
	//_started = false;
	//_timer = new Timer();
	//_sheduledTask = new ScheduledTask();

	System.out.println(
		"TsunamiOverlay is Running ");
}

public String[] getDataSetsNames() { return _dataSets; }

// 0 remove, 1 display, 2 toogle, -1 no change  
public void displayShapes(int evac, int tsunami, int boats)
{
    boolean display = false;
    boolean remove = false;

    // ------------------------
    // evac
    if (_evacPolys == null && evac <= 0){
        // ok !
    }
    else if ((evac == 1 && !_evacDisplayed) || (evac == 2 && !_evacDisplayed)){
        display = true;
    }
    else if ((evac == 0 &&  _evacDisplayed) || (evac == 2 && _evacDisplayed)) {
        remove = true;
    }

    if (display){
        if (_evacPolys == null){
            _loadEvac(new File("evac/zone_evacuer_sainteAnne.shp"), Color.RED, true);
        }
        else {
            for(VPolygon p : _evacPolys){
                _vs.addGlyph(p);
            }
            _evacDisplayed = true;
        }
        if (_evacPoints == null){
            _loadEvacPoints(new File("Martinique-data/1_Evacuation/1_Evacuation_Ile/PEZR_ac_Pop.shp"), Color.GREEN, true);
        }
        else {
            for(VPolygon p : _evacPolys){
                _vs.addGlyph(p);
            }
            _evacDisplayed = true;
            for(SICircle c : _evacPoints){
                _vs.addGlyph(c);
            }
            _evacPointsDisplayed = true;
        }
    }

    if (remove && _evacPolys != null){
        for(VPolygon p : _evacPolys){
             _vs.removeGlyph(p);
        }
        _evacDisplayed = false;
    }
    if (remove && _evacPoints != null){
        for(SICircle p : _evacPoints){
             _vs.removeGlyph(p);
        }
        _evacPointsDisplayed = false;
    }


    // ------------------------
    // tsunami
    
    display = false;
    remove = false;
    
    if (_tsunamiLines == null && tsunami <= 0){
        // ok !
    }
    else if ((tsunami == 1 && !_tsunamiDisplayed) || (tsunami == 2 && !_tsunamiDisplayed)){
        display = true;
    }
    else if ((tsunami == 0 &&  _tsunamiDisplayed) || (evac == 2 && _tsunamiDisplayed)) {
        remove = true;
    }

    if (display){
        if (_tsunamiLines == null){
            _loadTsunami(new File("Caribe_Wave_2015/2_Caribe_Wave_2015/TTT_CW15_PolyL.shp"), new File("Caribe_Wave_2015/2_Caribe_Wave_2015/Epicentre_CW15.shp"), Color.RED, true);
        }
        else {
            for(DPath d : _tsunamiLines){
                _vs.addGlyph(d);
            }
            _tsunamiDisplayed = true;
        }
        if (_mapPolys == null){
            _loadMap(new File("Caribe_Wave_2015/1_Referentiel_Gde_Caraibe/Pays_Gde_Caraibe.shp"), new Color(76,153,0), true);
        }
        else if (!_mapDisplayed) {
            for(VPolygon p : _mapPolys){
                _vs.addGlyph(p);
            }
            _mapDisplayed = true;
        }
    }
    if (remove && _tsunamiLines != null){
        for(DPath d : _tsunamiLines){
             _vs.removeGlyph(d);
        }
        if(_epicentre != null){
            _vs.removeGlyph(_epicentre);
        }
        _tsunamiDisplayed = false;
    }
    // map see below

    // ------------------------
    // Boats

    display = false;
    remove = false;

    if (_boats == null && boats <= 0){
        // ok !
    }
    else if ((boats == 1 && !_boatsDisplayed) || (boats == 2 && !_boatsDisplayed)){
        display = true;
    }
    else if ((boats == 0 &&  _boatsDisplayed) || (boats == 2 && _boatsDisplayed)) {
        remove = true;
    }

    if (display){
        if (_boats == null){
            _loadBoats(new File("Caribe_Wave_2015/4_Donnees_Navires/Navire_CW_15.shp"), Color.BLACK, true);
        }
        else {
            for(Boat b : _boats){
                _vs.addGlyph(b.glyph);
            }
            _boatsDisplayed = true;
        }
        if(_securePolys == null){
            _loadSecure(new File("Caribe_Wave_2015/3_Securite_Navire/Zone_exclusion_05NM.shp"), new Color(153,0,153), true);
        }
        else if(!_secureDisplayed){
            for(VPolygon p : _securePolys){
                _vs.addGlyph(p);
            }
            _secureDisplayed = true;
        }
        if (_mapPolys == null){
            _loadMap(new File("Caribe_Wave_2015/1_Referentiel_Gde_Caraibe/Pays_Gde_Caraibe.shp"), new Color(76,153,0), true);
        }
        else if (!_mapDisplayed) {
            for(VPolygon p : _mapPolys){
                _vs.addGlyph(p);
            }
            _mapDisplayed = true;
        }
    }
    if (remove && _boats != null){
        for(Boat b : _boats){
             _vs.removeGlyph(b.glyph);
        }
        _boatsDisplayed = false;
    }
    if (remove && _securePolys != null && _secureDisplayed){
        for(VPolygon p : _securePolys){
                _vs.removeGlyph(p);
        }
        _secureDisplayed = false;
    }

    // remove maps ??
    if (!_boatsDisplayed && !_tsunamiDisplayed)
    {
        if (_mapPolys != null && _mapDisplayed){
            for(VPolygon p : _mapPolys){
                _vs.removeGlyph(p);
            }
            _mapDisplayed = false;
        }
    }
}

private void _loadBoats(File shapeFile, Color shapeColor, boolean show)
{
    if (_boats == null){
        _boats = new Vector<Boat>();
    }
    else{
        return;
    }
    _boatsDisplayed = show;

    Map connect = new HashMap();
    try {
        connect.put("url", shapeFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            //SimpleFeatureCollection featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            double dx = 500.0*0.96;
            double dy = 600.0*0.95;
            double deltax = 0;
             double deltay = 0;  // plus haut...
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Point p = (Point)f.getAttribute(0);
                //Point2D.Double vp = fromLLToPixel(p.getY(), p.getX());
                double x = p.getX() * 42203.53 + 2584540  + deltax;
                double y = p.getY() * 44451.54 - 661805.3  + deltay;
                boolean tooClose = false;
                for (Boat boat:_boats){
                    if (boat.distanceTo(x,y) < 100){
                        tooClose = true;
                        break;
                    }
                }
                if (!tooClose){
                    Boat boat = new Boat(x,y, f.getAttributes());
                    //System.out.println("Boat "+f.getAttributes().get(2)+"-- vx: "+vx+", vy: "+vy+", x:"+p.getX()+", y:"+p.getY());
                    if (show) _vs.addGlyph(boat.glyph);
                    _boats.add(boat);
                }
            }
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}

// bof bof ....
private void _loadAmpMsg(File shapeFile){
    Map connect = new HashMap();
    try {
        connect.put("url", shapeFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            double dx = 500.0*0.96;
            double dy = 600.0*0.95;
            double deltax = 0;
             double deltay = 0;  // plus haut...
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Point p = (Point)f.getAttribute(0);
                //Point2D.Double vp = fromLLToPixel(p.getY(), p.getX());
                double x = p.getX() * 42203.53 + 2584540  + deltax;
                double y = p.getY() * 44451.54 - 661805.3  + deltay;
                boolean tooClose = false;
                SICircle amp = new SICircle(x,y, 0, 10, Color.GREEN);
                _vs.addGlyph(amp);
            }
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}

private void _loadMap(File shapeFile, Color shapeColor, boolean show)
{
    if (_mapPolys == null){
        _mapPolys = new Vector<VPolygon>();
    }
    else{
        return;
    }
    _mapDisplayed = show; 
    Map connect = new HashMap();
    try {
        connect.put("url", shapeFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            Vector<Polygon> awtPolygons = new Vector<Polygon>();
            Vector points = new Vector();
            Point2D.Double[] zvtmCoords;
            int i = 0;
            int num_points = 0;
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Geometry geometry = (Geometry)f.getDefaultGeometry();
                Object[] polygons = PolygonExtracter.getPolygons(geometry).toArray();
                //System.out.println("map num poly "+ polygons.length);
                for (int k=0;k<polygons.length;k++){
                    Geometry simplifiedPolygon = DouglasPeuckerSimplifier.simplify((Geometry)polygons[k], 0.001);
                    PathIterator pi = (new LiteShape(simplifiedPolygon, null, false)).getPathIterator(null);
                    double[] coords = new double[6];
                    int type;
                    Vector shapes = new Vector();
                    boolean  skip = false;
                    while (!pi.isDone()){
                        type = pi.currentSegment(coords);
                        double deltax = 0;
                        double deltay = 0; 
                        //Point2D.Double vp = fromLLToPixel(coords[1], coords[0]);
                        if (type == PathIterator.SEG_LINETO){
                            double x =  coords[0] * 42203.53 + 2584540  + deltax;
                            double y =  coords[1] * 44451.54 - 661805.3  + deltay;
                            //System.out.println(x+" "+y);
                            num_points++;
                            //System.out.print(".");
                            points.add(new Point2D.Double(x, y));
                            if (x > 0 && x < 19000 && y < 0 && y > -23000) {skip = true;}
                            //points.add(new Point2D.Double(vp.getX(), vp.getY()));
                        }
                        else if (type == PathIterator.SEG_MOVETO){
                            //System.out.print("s");
                            points.clear();
                            double x =  coords[0] * 42203.53 + 2584540  + deltax;
                            double y =  coords[1] * 44451.54 - 661805.3  + deltay;
                            points.add(new Point2D.Double(x, y));
                            if (x > 0 && x < 19000 && y < 0 && y > -23000) {skip = true;}
                            //System.out.println(x+" "+y);
                            num_points++;
                        }
                        else if (type == PathIterator.SEG_CLOSE && points.size() > 2){
                            //System.out.print("c");
                            zvtmCoords = new Point2D.Double[points.size()];
                            for (int j=0;j<zvtmCoords.length;j++){
                                zvtmCoords[j] = (Point2D.Double)points.elementAt(j);
                            }
                            if (!skip){
                                VPolygon polygon = new VPolygon(zvtmCoords, 5, shapeColor,Color.BLACK);
                                _mapPolys.add(polygon);
                                // polygon.setFilled(false);
                                if (show) _vs.addGlyph(polygon);
                                //application.sm.createClosedShapeDescription(polygon, "B"+Integer.toString(polygonID++),
                                //    polygon.getZindex(),
                                //    region, false);
                            }
                            points.clear();
                            //skip = false;
                        }
                        else if (type == PathIterator.SEG_CLOSE){
                             //System.err.println("Error: not enough points");
                             points.clear();
                        }
                        else {
                            System.err.println("Error: GeoToolsManager.loadShape: Unsupported path iterator element type:" + type);
                        }
                        pi.next();
                    }
                }
            }
            System.out.println("\n map loaded with "+num_points+" points");
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}

private void _loadSecure(File shapeFile, Color shapeColor, boolean show)
{
    if (_securePolys == null){
        _securePolys = new Vector<VPolygon>();
    }
    else{
        return;
    }
    _secureDisplayed = show; 
    Map connect = new HashMap();
    try {
        connect.put("url", shapeFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            Vector<Polygon> awtPolygons = new Vector<Polygon>();
            Vector points = new Vector();
            Point2D.Double[] zvtmCoords;
            int i = 0;
            int num_points = 0;
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Geometry geometry = (Geometry)f.getDefaultGeometry();
                Object[] polygons = PolygonExtracter.getPolygons(geometry).toArray();
                for (int k=0;k<polygons.length;k++){
                    Geometry simplifiedPolygon = DouglasPeuckerSimplifier.simplify((Geometry)polygons[k], 0.001);
                    PathIterator pi = (new LiteShape(simplifiedPolygon, null, false)).getPathIterator(null);
                    double[] coords = new double[6];
                    int type;
                    Vector shapes = new Vector();
                    boolean  skip = false;
                    while (!pi.isDone()){
                        type = pi.currentSegment(coords);
                        double deltax = 0;
                        double deltay = 0; 
                        //Point2D.Double vp = fromLLToPixel(coords[1], coords[0]);
                        if (type == PathIterator.SEG_LINETO){
                            double x =  coords[0] * 42203.53 + 2584540  + deltax;
                            double y =  coords[1] * 44451.54 - 661805.3  + deltay;
                            //System.out.println(x+" "+y);
                            num_points++;
                            // System.out.print(".");
                            points.add(new Point2D.Double(x, y));
                            //if (x > 0 && x < 19000 && y < 0 && y > -23000) {skip = true;}
                            //points.add(new Point2D.Double(vp.getX(), vp.getY()));
                        }
                        else if (type == PathIterator.SEG_MOVETO){
                            points.clear();
                        }
                        else if (type == PathIterator.SEG_CLOSE && points.size() > 0){
                            zvtmCoords = new Point2D.Double[points.size()];
                            for (int j=0;j<zvtmCoords.length;j++){
                                zvtmCoords[j] = (Point2D.Double)points.elementAt(j);
                            }
                            if (!skip){
                                VPolygon polygon = new VPolygon(zvtmCoords, 8, shapeColor,shapeColor);
                                polygon.setFilled(false);
                                _securePolys.add(polygon);
                                polygon.setFilled(false);
                                if (show) _vs.addGlyph(polygon);
                                //application.sm.createClosedShapeDescription(polygon, "B"+Integer.toString(polygonID++),
                                //    polygon.getZindex(),
                                //    region, false);
                            }
                            skip = false;
                        }
                        else if (type == PathIterator.SEG_CLOSE){
                             System.err.println("Error: not enough points");

                        }
                        else {
                            System.err.println("Error: GeoToolsManager.loadShape: Unsupported path iterator element type:" + type);
                        }
                        pi.next();
                    }
                }
            }
            System.out.println("\n secure loaded with "+num_points+" points");
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}

private void _loadTsunami(File lineFile, File epicentreFile, Color shapeColor, boolean show)
{
    if (_tsunamiLines == null){
        _tsunamiLines = new Vector<DPath>();
    }
    else{
        return;
    }
    _tsunamiDisplayed = show; 
    _loadEpicentre(epicentreFile, shapeColor, show);
    Map connect = new HashMap();
    try {
        connect.put("url", lineFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            int num_lines= 0;
            double deltax = 0;
            double deltay = 0; 
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Geometry geometry = (Geometry)f.getDefaultGeometry();
                Object[] lines = LinearComponentExtracter.getLines(geometry).toArray();
                //System.out.print("num lines: " + lines.length);
                for (int k=0;k<lines.length;k++){
                    LinearIterator it = new LinearIterator((Geometry)lines[k]);
                    while (it.hasNext()){
                        LineString l =  it.getLine();
                        int np = l.getNumPoints();
                        //System.out.print(" "+np);
                        Coordinate[] coords = l.getCoordinates();
                        DPath path = null;
                        for (int u=0; u < coords.length; u++){
                            //Point2D.Double vp = fromLLToPixel(coords[u].y, coords[u].x);
                            double x =  coords[u].x * 42203.53 + 2584540  + deltax;
                            double y =  coords[u].y * 44451.54 - 661805.3  + deltay;
                            if (path == null){
                                path = new DPath(x,y, 10, shapeColor);
                            }
                            else {
                                path.addSegment(x,y, true);
                            } 
                        }
                        if (show) _vs.addGlyph(path);
                        _tsunamiLines.add(path);
                        it.next();
                    }
                }
                //System.out.println("");
                num_lines++;
            }
            System.out.println("all num lines: " + num_lines);
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}

private void _loadEpicentre(File shapeFile, Color shapeColor, boolean show){
    if (_epicentre != null) return;
    Map connect = new HashMap();
    try {
        connect.put("url", shapeFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            double deltax = 0;
            double deltay = 0; 
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Point p = (Point)f.getAttribute(0);
                //Point2D.Double vp = fromLLToPixel(p.getY(), p.getX());
                //System.out.println("Epicentre -- vx: "+vx+", vy: "+vy+", x:"+p.getX()+", y:"+p.getY());
                double x =  p.getX() * 42203.53 + 2584540  + deltax;
                double y =  p.getY() * 44451.54 - 661805.3  + deltay;
                _epicentre = new SICircle(x,y, 0, 10, Color.RED);
                if (show) _vs.addGlyph(_epicentre);
            }
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}

private void _loadEvacPoints(File shapeFile, Color shapeColor, boolean show){
    if (_evacPoints == null){
        _evacPoints = new Vector<SICircle>();
    }
    else{
        return;
    }
    _evacPointsDisplayed = show;
    int num_points = 0;
    Map connect = new HashMap();
    try {
        connect.put("url", shapeFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            double dx = 500.0*0.96;
            double dy = 600.0*0.95;
            double deltax = 3;
            double deltay = +1200;  // plus haut...
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Point p = (Point)f.getAttribute(0);
                //Point2D.Double vp = fromLLToPixel(p.getY(), p.getX());
                double x = (p.getX() * 192.0/dx - 192.0*690000.0/dx)  + deltax;
                double y = (p.getY() * 228.0/dy - 228.0*1650000.0/dy)  + deltay;
                //System.out.println(x+" "+y);
                SICircle c = new SICircle(x,y, 10, 8, shapeColor);
                _evacPoints.add(c);
                if (show) _vs.addGlyph(c);
                num_points++;
            }
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
            System.out.println("\n evac points loaded with "+num_points+" points");
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}
private void _loadEvac(File shapeFile, Color shapeColor, boolean show)
{
    if (_evacPolys == null){
        _evacPolys = new Vector<VPolygon>();
    }
    else{
        return;
    }
    _evacDisplayed = show; 
    Map connect = new HashMap();
    try {
        connect.put("url", shapeFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            Vector<Polygon> awtPolygons = new Vector<Polygon>();
            Vector points = new Vector();
            Point2D.Double[] zvtmCoords;
            int i = 0;
            int num_points = 0;
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Geometry geometry = (Geometry)f.getDefaultGeometry();
                Object[] polygons = PolygonExtracter.getPolygons(geometry).toArray();
                for (int k=0;k<polygons.length;k++){
                    Geometry simplifiedPolygon = DouglasPeuckerSimplifier.simplify((Geometry)polygons[k], 10.01);
                    PathIterator pi = (new LiteShape(simplifiedPolygon, null, false)).getPathIterator(null);
                    double[] coords = new double[6];
                    int type;
                    Vector shapes = new Vector();
                    while (!pi.isDone()){
                        type = pi.currentSegment(coords);
                        double dx = 500.0*0.96;
                        double dy = 600.0*0.95;
                        double deltax = 3;
                        double deltay = +1200;  // plus haut...
                        // 692.6863600000506 -1592.326160000288
                        //Point2D.Double vp = fromLLToPixel(coords[1], coords[0]);
                        if (type == PathIterator.SEG_LINETO){

                            double x = (coords[0] * 192.0/dx - 192.0*690000.0/dx)  + deltax;
                            double y = (coords[1] * 228.0/dy - 228.0*1650000.0/dy)  + deltay;
                            //System.out.println(x+" "+y);
                            num_points++;
                            // System.out.print(".");
                            points.add(new Point2D.Double(x, y));
                            //points.add(new Point2D.Double(vp.getX(), vp.getY()));
                        }
                        else if (type == PathIterator.SEG_MOVETO){
                            points.clear();
                        }
                        else if (type == PathIterator.SEG_CLOSE){
                            zvtmCoords = new Point2D.Double[points.size()];
                            for (int j=0;j<zvtmCoords.length;j++){
                                zvtmCoords[j] = (Point2D.Double)points.elementAt(j);
                            }
                            VPolygon polygon = new VPolygon(zvtmCoords, 9, shapeColor, shapeColor, .4f);
                            _evacPolys.add(polygon);

                            // polygon.setFilled(false);
                            if (show) _vs.addGlyph(polygon);
                            //application.sm.createClosedShapeDescription(polygon, "B"+Integer.toString(polygonID++),
                            //    polygon.getZindex(),
                            //    region, false);
                        }
                        else {
                            System.err.println("Error: GeoToolsManager.loadShape: Unsupported path iterator element type:" + type);
                        }
                        pi.next();
                    }
                }
            }
            System.out.println("\n evac zone loaded with "+num_points+" points");
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}


// --------------------------------------------------------------
// debug stuff..
double yy = 0;
double xx = 0;

void tmpT(double x, double y){
    yy = yy + 100*y;
    xx = xx + 100*x;
    System.out.println("tmpT "+ xx +" "+ yy);
    double a = (_camera.focal+Math.abs(_camera.altitude)) / _camera.focal;

    Location l = _camera.getLocation();
    double newx = l.getX()+a*x;
    double newy = l.getY()+a*y;

    _camera.setLocation(new Location(newx, newy, l.getAltitude()));
    l = _camera.getLocation();
    System.out.println("Loc came tmpT "+  l.getX()+" "+ l.getY());
}


// --------------------------------------------------------------
// old tsunami emulation

// public void start()
// {
//     if (_started){
//         // restart !
//         initRendering();
//         _ring.sizeTo(_radius);
//         //_timer.cancel();
//         //_timer.purge();
//         //_timer = new Timer();
//         return;
//     }
//     else {
//         _vs.addGlyph(_ring);
//         //loadShapes(new File("evac/zone_evacuer_sainteAnne.shp"), Color.RED);

//     }
//     _started = true;
//     _timer.schedule(_sheduledTask, 0, 250); // Create Repetitively task for every 1 secs

// }

// public class ScheduledTask extends TimerTask {

//     // Add your task here
//     public void run() {
//         _ring.sizeTo(_radius+_grow);
//         _radius = _radius+_grow;
//     }
// }

// public void initRendering()
// {
//     // _bounds WNES
//     double cx = _bounds[0];
//     double cy = _bounds[3];
//     //cx = cy = 0.0;
//     _radius = (_bounds[2] - _bounds[0])/(2*100);
//     _grow = (_bounds[2] - _bounds[0])/(2*400);
//     float p = 0;
//     if (_ring == null) {
//         _ring = new VRing(cx, cy, 0, _radius, Math.PI/2, p, Math.PI/4, Color.RED, Color.BLACK, 0.5f); //0.5f);
//         _vs.addGlyph(_ring);
//     }
// }

}
