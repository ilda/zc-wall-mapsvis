/*
 *   AUTHOR:    Olivier Chapuis <chapuis@lri.fr>
 *   Copyright (c) CNRS, 2015. All Rights Reserved
 *   Licensed under the GNU GPL.
 *
 */
package ilda.zcsample;

import ilda.zcsample.vicon.VRPNViconReceiver;
import ilda.zcsample.vicon.ViconTrackerInfo;
import ilda.zcsample.walls.WallCoordinateCalculator;
import ilda.zcsample.walls.wild.WildCoordinateCalculator;
import ilda.zcsample.walls.wilder.WilderCoordinateCalculator;
import ilda.zcsample.walls.wildest.WildestCoordinateCalculator;

import ilda.zcsample.filters.*;

//import ilda.zcsample.Dashboard;
//import ilda.zcsample.DashboardVicon;

import java.util.HashMap;
import java.util.Iterator;

import java.util.Observable;
import java.util.Observer; 

import java.awt.Color;
import java.awt.geom.Point2D;

class ViconManager  implements Observer {

private SmartiesManager _smartiesManager;
private ClusterDisplay _display;
private WallCoordinateCalculator _wcc = null;
private boolean _data_viz;
private HashMap<Object, OneTracker> _pointerObjects;
private HashMap<Object, OneTracker> _trackerObjects;
private int _count_pointer = 0;



public class OneTracker {
	String name;
	int id;
	boolean is_a_pointer;
	PointerFilter pfilter;
	ViconTrackerInfo vti = null;
	SmartiesButtons sb = null;
	DashboardVicon dbVicon = null;
	//
	Point2D.Double prevCur = new Point2D.Double(0, 0);
	int pressedButton = 0;
	boolean indrag = false;

	OneTracker(String n, int id_, boolean iap, String f, String param)
	{
		name = n;
		id = id_;
		is_a_pointer = iap;
		if (f==null && is_a_pointer){
			f = "oneeuro";
		}
		if (f != null)
		{
			if (f.toLowerCase().equals("oneeuro"))
			{
				// freq, mincutoff (0.7?), cutoff slope(beta), cutoff derivative (ok)
				double freq = 60.0;
				double mincutoff = 1.0; //(0.7?)
				double beta = 0.007;
				if (param != null)
				{
					String[] params = param.split("/");
					double t = 0;
					if (params.length > 0)
					{
						try { t = Double.parseDouble(params[0]); } catch(Exception e) {}
						finally { mincutoff = t; }
					}
					if (params.length > 1)
					{
						try { t = Double.parseDouble(params[1]); } catch(Exception e) {}
						finally { beta = t; }
					}
					if (params.length > 2)
					{
						try { t = Double.parseDouble(params[2]); } catch(Exception e) {}
						finally { freq = t; }
					}
				}
				pfilter = new OneEuroPointerFilter(freq, mincutoff, beta, 1.0);
			}
			else
			{
				int num_smooth = 60;
				double radius = 0.06;
				if (param != null)
				{
					String[] params = param.split("/");
					int i = 0; double t = 0;
					if (params.length > 0)
					{
						try { i = Integer.parseInt(params[0]); } catch(Exception e) {}
						finally { num_smooth = i; }
					}
					if (params.length > 1)
					{
						try { t = Double.parseDouble(params[1]); } catch(Exception e) {}
						finally { radius= t; }
					}
				}
				pfilter = new MeanPointerFilter(num_smooth, radius);
			}
		}
	}

	public void changeFilter(String fn)
	{

	}

	public void updateFilterParams(String params)
	{

	}
}



ViconManager(
	ClusterDisplay cd, SmartiesManager sm, String objects, String host, int port,
	String wall, boolean data_viz, boolean force)
{
	_smartiesManager = sm;
	_display = cd;

	_pointerObjects = new HashMap();
	_trackerObjects = new HashMap();

	if (wall != null && wall.toLowerCase().equals("wild"))
	{
		_wcc = new WildCoordinateCalculator();
	}
	else if (wall != null && wall.toLowerCase().equals("wilder"))
	{
		_wcc = new WilderCoordinateCalculator();
	}
	else if (wall != null && wall.toLowerCase().equals("wildest"))
	{
		_wcc = new WildestCoordinateCalculator();
	}

	if (_wcc != null && host == null)
	{
		host = _wcc.getViconHost();
	}

	int pointer_count = 0;
	int tracker_count = 0;
	int smarties_port_inc = 1;
	String[] aobj = objects.split(":");
	for(int i = 0; i < aobj.length; i++)
	{
		String[] obj = aobj[i].split(",");
		boolean is_a_pointer = (obj.length > 1 && obj[1].toLowerCase().equals("pointer"));
		String filter_name = (obj.length > 2)? obj[2]:null;
		String params = (obj.length > 3)? obj[3]:null;
		System.out.println("ViconManager: try to connect to \""+ obj[0] + "\". a pointer? "+ is_a_pointer);
		VRPNViconReceiver vvr = null;
		try {
			vvr = new VRPNViconReceiver(
				obj[0], host, port, data_viz, force, _wcc, is_a_pointer);
		}
		catch(RuntimeException e)
		{
   			System.out.println("" +e);
		}
		if (vvr != null)
		{
			vvr.addObserver(this);
			if (is_a_pointer)
			{
				OneTracker ot =
					new OneTracker(obj[0], pointer_count, true, filter_name, params);
				_pointerObjects.put(vvr.hashCode(), ot);
				if (false){ // for now use one dashboard... see below
					ot.sb = new SmartiesButtons(
								smarties_port_inc, pointer_count, _display, (Object)this);
					smarties_port_inc++;
				}
				pointer_count++;
			}
			else
			{
				// a tracker only track ...
				OneTracker ot =
					new OneTracker(obj[0], tracker_count, false, filter_name, params);
				_trackerObjects.put(vvr.hashCode(), ot);
				tracker_count++;
				// could add build a special smarties client for logging ...
			}
		}
	}

	if (pointer_count > 0)
	{
		// register pointers
		_display.registerDevice(this,"ViconPointer");
		for (OneTracker ot : _pointerObjects.values())
		{
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(
					ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, ot.id);
			System.out.println("Register Pointer name:" + ot.name +":" );
			if(ot.name.equals("AirTokenFootA")){
				zcse.color = Color.GREEN;
			}
			else if (ot.name.equals("AirTokenFootB")){
				zcse.color = Color.RED;
			}
			else {
				zcse.color = Color.MAGENTA;
			}
			_display.handleEvent(zcse);
		}
		// for now link to a simple dashboard for the first pointer 
		for (OneTracker ot : _pointerObjects.values())
		{
			ot.dbVicon = new DashboardVicon(_display, null, this,  ot);
			break;
		}
	}
}

public Point2D.Double getLastFirstPointerPosition()
{
	Point2D.Double ret = null;
	for (OneTracker ot : _pointerObjects.values())
	{
		if (ot.vti != null) { ret = new Point2D.Double(ot.vti.wp[0], ot.vti.wp[1]); }
		break;
	}
	return ret;
}

public void update(Observable obj, Object arg)
{
	if (!(arg instanceof ViconTrackerInfo)) { return; }

	ViconTrackerInfo e = (ViconTrackerInfo)arg;
	//_smartiesManager.showObjectPosition(e.name, e.pos);
	if (false)
	{
		System.out.println(
			"VICON\t"+ e.timestamp+ "\t"+ e.name +"\n"
			+"\tpos: "+ e.pos[0] +"\t"+ e.pos[1] +"\t"+ e.pos[2] +"\n"
			+"\tdir: "+ e.dir[0] +"\t"+ e.dir[1] +"\t"+ e.dir[2] +"\n");
	}

	OneTracker ot = _trackerObjects.get((obj).hashCode());
	if (ot != null)
	{
		// just a tracker ... maybe log something ?

	}

	if (_wcc == null)  { return; }

	// we can compute the position of a cursor on the wall

	ot = _pointerObjects.get((obj).hashCode());
	if (ot != null && ot.is_a_pointer)
	{
		// we have a pointer
		ot.vti = e;
		int button = 0;
		int type = ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR;
		ClusterDisplay.zcsEvent zcse = _display.new zcsEvent(0, this, ot.id);
		Point2D.Double cur = ot.pfilter.filter(
			new Point2D.Double(e.wp[0], e.wp[1]), 
			new Point3D(e.pos[0], e.pos[1],e.pos[2]),
			(long)e.timestamp);
		zcse.x = cur.x/_wcc.getWallWidth();
		zcse.y = cur.y/_wcc.getWallHeight();
		zcse.dx = (cur.x - ot.prevCur.x)/_wcc.getWallWidth();
		zcse.dy = (cur.y - ot.prevCur.y)/_wcc.getWallWidth();
		zcse.button = 0;
		zcse.type = ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR;
		_display.handleEvent(zcse);
		if (ot.dbVicon != null){
			// FIXME CLICK VS DRAG !!!  for now we drag with a 0 threshold !!! FIXME
			button = ot.dbVicon.getPressedButton();
			if (button > 0 && ot.pressedButton == 0){
				zcse.button = button;
				zcse.type = ClusterDisplay.zcsEvent.ZCS_START_DRAG;
				_display.handleEvent(zcse);
				ot.pressedButton = button;
			}
			else if (ot.pressedButton > button && button == 0){
				zcse.button = button;
				zcse.type = ClusterDisplay.zcsEvent.ZCS_END_DRAG;
				_display.handleEvent(zcse);
				ot.pressedButton = button;
			}
			else if (ot.pressedButton > 0) {
				zcse.button = ot.pressedButton;
				zcse.type = ClusterDisplay.zcsEvent.ZCS_DRAG;
				_display.handleEvent(zcse);
			}
		}
		else if (ot.sb != null) {
				// TODO ...
		}
		ot.prevCur = cur;

		if (false)
		{
			ot.sb.showObjectPosition(e.name, e.pos);
		}
		if (false)
		{
			System.out.println(
				"VICON\t"+ e.timestamp+ "\t"+ e.name +"\n"
				+"\twall coor: "+ e.wp[0] +"\t"+ e.wp[1] +"\n");
		}
	}

}

}