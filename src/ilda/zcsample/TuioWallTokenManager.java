package ilda.zcsample;

import java.util.Vector;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import java.util.Observable;
import java.util.Observer; 

import java.awt.geom.Point2D;
import java.awt.Color;

import java.text.DecimalFormat;

import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;
import ilda.zcsample.portals.DragMag;

import TUIOWT.*;


class TuioWallTokenManager extends BasicDevice implements TuioWallTokenListener 
{

	private static double displayWidthMM = 0;
	private static double displayHeightMM = 0;

	TuioWTClient client;
	private int verbose = 1;

	private ClusterDisplay _display;
	private BasicDevice _SSobject;

	private double tooCloseDistAfterMM = 100; // mm
	private double dragTh = 15; // mm
	private double maxDistPinchCursors = 1200; // mm

	private long longPressTime = 1000; // ms

	TuioWallTokenManager(ClusterDisplay cd, int port) {
		_display = cd;
		_SSobject = this;

		try {
			client = new TuioWTClient(port);
		} catch (Exception e1) {
			System.out.println("Tuio client error" + e1);
			//System.exit(0);
		}

		if (client!=null) {
			client.addTuioWallTokenListener(this);
			client.connect();
		} else {
			System.out.println("Tuio client error");
		}

		displayWidthMM = _display.getWidthMM();
		displayHeightMM = _display.getHeightMM();

		_display.registerDevice(this,"TuioWallToken");
	}

	// -----------------------------------------------------------------------------
	// basic device
	public void moveCursor(int id, double x, double y) {
		//x = (x>1)? 1:x; x = (x<0)? 0:x;
	    //y = (y>1)? 1:y; y = (y<0)? 0:y;
	}

	public void setAttached(int id, boolean b) {
		// System.out.println("Set attached "+ id +" "+ b); 
	}

	public void setLinked(int id, boolean b) {	
	}

	// ---------------------------------------------------------------------------
	// local stuff
	class MyToken
	{
		public TuioWallToken tok;
		public int id;
		public double x, y;
		public double downx, downy;
		public double px, py;  // previous x,y
		public double dx, dy;  // previous x,y
		public double angle; // angle
		public double prevAngle;
		public boolean zoomallowed = false;
		public boolean fixed = false;
		public boolean attached = false;
		public boolean linked = false;
		public boolean inDrag = false;
		public long downTime;
		public boolean inLongPress = false;

		public MyToken(TuioWallToken t, int id, double x, double y, double a)
		{
			this.tok = t;
			this.id = id;
			this.x = this.downx = x;
			this.y = this.downy = y;
			this.px = x;
			this.py = y;
			angle = prevAngle = a;
			dx = dy = 0;
			downTime = t.getTuioTime().getTotalMilliseconds();
		}

		public void update(TuioWallToken t)
		{
			tok = t;
			this.x = t.getX(); this.y = t.getY();
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, _SSobject, (int)id);
			zcse.x = x; 
			zcse.y = y; 
			zcse.dx = dx = x-px; 
			zcse.dy = dy = y-py;
			this.px = x;
			this.py = y;
			_display.handleEvent(zcse);
		}

		public long timeFromDown(){
			return (tok.getTuioTime().getTotalMilliseconds() - downTime);
		}
	} // class myToken

	class MyCursor {
		public TuioCursor tc;
		public double x,y;
		public double downx, downy;
		public double px, py; 
		public long downTime;
		public boolean inLongPress;

		public MyCursor(TuioCursor cc) { 
			tc = cc; 
			x = downx = px = tc.getX(); y= downy = py = tc.getY();
			downTime = tc.getTuioTime().getTotalMilliseconds();
		}
		public void update(TuioCursor cc){
			px = x; py = y;
			x = tc.getX(); y = tc.getY();
			tc = cc;
			if (inLongPress){
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, _SSobject, (int)tc.getSessionID());
				zcse.x = tc.getX(); zcse.y = tc.getY();;
				_display.handleEvent(zcse);
			}
		}
		public void click(TuioCursor tcur){

		}
		public double deltaToDownMM(){
			double dist = Math.pow(
					Math.pow((tc.getX() - downx)*displayWidthMM, 2)+
					Math.pow((tc.getY() - downy)*displayHeightMM, 2),
					0.5);
			return dist;
		}
		public double distMM(MyCursor c){
			double dist = Math.pow(
					Math.pow((tc.getX() - c.tc.getX())*displayWidthMM, 2)+
					Math.pow((tc.getY() - c.tc.getY())*displayHeightMM, 2),
					0.5);
			return dist;
		}
		public double downDistMM(MyCursor c){
			double dist = Math.pow(
					Math.pow((downx - c.downx)*displayWidthMM, 2)+
					Math.pow((downy - c.downy)*displayHeightMM, 2),
					0.5);
			return dist;
		}
		public long timeFromDown(){
			return (tc.getTuioTime().getTotalMilliseconds() - downTime);
		}
		public void startLongPress(){
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, _SSobject, (int)tc.getSessionID());
			zcse.x = tc.getX(); zcse.y = tc.getY(); //zcse.hide = true;
			zcse.color = Color.YELLOW;
			zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_CREATE_PIEMENU;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_START_MOVE_CURSOR;
			_display.handleEvent(zcse);
			inLongPress = true;
		}
		public void up(){
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_MOVE_CURSOR, _SSobject, (int)tc.getSessionID());
			zcse.x = tc.getX(); zcse.y = tc.getY();
			zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DELETE_CURSOR, _SSobject, (int)tc.getSessionID());
			zcse.x = tc.getX(); zcse.y = tc.getY();
			zcse.is_touch = true;
			_display.handleEvent(zcse);
		}
	}

	class Dragger {
		public MyCursor mc;

		public Dragger(MyCursor c){
			mc = c;
		}

		public double distMM(MyCursor c){
			return mc.distMM(c);
		}

		public void startDrag(TuioCursor tcur){
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, _SSobject, (int)mc.tc.getSessionID());
			zcse.x = mc.downx; zcse.y = mc.downy; // zcse.hide = true;
			zcse.color = Color.YELLOW;
			zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_START_DRAG;
			_display.handleEvent(zcse);
			mc.update(tcur);
			double dx = (mc.tc.getX() - mc.downx); 
			double dy = (mc.tc.getY() - mc.downy); 
			zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DRAG, _SSobject, (int)mc.tc.getSessionID());
			zcse.x = mc.tc.getX(); zcse.y = mc.tc.getY();
			zcse.dx = dx; zcse.dy = dy; //*_height/_display.getHeight();
			zcse.button = 0;
			zcse.contacts =  1;
			zcse.is_touch = true;
			//System.out.println("mouseDragged " +zcse.dx*_width+","+ zcse.dy*_height);
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR;
			_display.handleEvent(zcse);
		}

		public void drag(TuioCursor tcur){
			mc.update(tcur);
			double dx = (mc.x - mc.px); 
			double dy = (mc.y - mc.py); 
			//System.out.println("DRAG N "+ (-dx) + " " + dy);
			//FIXME viewer.directTranslate(-dx, dy);
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DRAG, _SSobject, (int)mc.tc.getSessionID());
			zcse.x = mc.tc.getX(); zcse.y = mc.tc.getY();
			zcse.dx = dx; zcse.dy = dy; //*_height/_display.getHeight();
			zcse.button = 0;
			zcse.contacts = 1;
			zcse.is_touch = true;
			//System.out.println("mouseDragged " +zcse.dx*_width+","+ zcse.dy*_height);
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR;
			_display.handleEvent(zcse);
		}

		public void endDrag(TuioCursor tcur){
			mc.update(tcur);
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, _SSobject, (int)mc.tc.getSessionID());
			zcse.x = mc.tc.getX(); zcse.y = mc.tc.getY();
			zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_END_MOVE_CURSOR;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_DELETE_CURSOR;
			_display.handleEvent(zcse);
		}

	}

	class Pincher {
		public MyCursor mc1;
		public MyCursor mc2;
		public double prevD;

		public Pincher (MyCursor cc1, MyCursor cc2){
			mc1 = cc1; mc2 = cc2;
			prevD = mc1.distMM(mc2);
		}

		public double minDistMM(MyCursor c){
			return Math.min (mc1.distMM(c), mc2.distMM(c));
		}

		private void update(TuioCursor tcur){
			if (mc1.tc.getSessionID() == tcur.getSessionID()){
				mc1.update(tcur);
			}
			else if (mc2.tc.getSessionID() == tcur.getSessionID()){
				mc2.update(tcur);
			}
			prevD = mc1.distMM(mc2);
		}

		public void startPinch(TuioCursor tcur){
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, _SSobject, (int)mc1.tc.getSessionID());
			zcse.x = (mc1.downx + mc2.downx)/2; 
			zcse.y = (mc1.downy + mc2.downy)/2; 
			zcse.f = 1; // zcse.hide = true;
			zcse.color = Color.YELLOW; 
			zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_START_ZOOM;
			_display.handleEvent(zcse);
			//
			double prevd = Math.pow(
				Math.pow((mc1.downx - mc2.downx)*_display.getWidthMM(), 2) +
				Math.pow((mc1.downy - mc2.downy)*_display.getHeightMM(), 2),
				0.5);
			update(tcur);
			double d = mc1.distMM(mc2);
			double f = prevd/d; 
			zcse.type = ClusterDisplay.zcsEvent.ZCS_ZOOM;
			zcse.x = (mc1.tc.getX() + mc2.tc.getX())/2; 
			zcse.y = (mc1.tc.getY() + mc2.tc.getY())/2; 
			zcse.f = f; zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_START_MOVE_CURSOR;
			_display.handleEvent(zcse);
			//System.out.println("StartPinch " + f);
		}

		public void pinch(TuioCursor tcur){
			double pd = prevD;
			//double prevd = mc1.distMM(mc2);
			update(tcur);
			double d = mc1.distMM(mc2);
			double f = pd/d;  // FIXME aspect ratio ???
			//System.out.println("Pinch factor " + f);
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, _SSobject, (int)mc1.tc.getSessionID());
			zcse.x = (mc1.tc.getX() + mc2.tc.getX())/2; 
			zcse.y = (mc1.tc.getY() + mc2.tc.getY())/2; 
			zcse.f = f; zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR;
			_display.handleEvent(zcse);
		}

		public void endPinch(TuioCursor tcur){
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_ZOOM, _SSobject, (int)mc1.tc.getSessionID());
			zcse.x = (mc1.tc.getX() + mc2.tc.getX())/2; 
			zcse.y = (mc1.tc.getY() + mc2.tc.getY())/2; 
			zcse.f = 1; zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_END_MOVE_CURSOR;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_DELETE_CURSOR;
			_display.handleEvent(zcse);
			//System.out.println("EndPinch ");
		}
	}

	// ----------------------------------------------------------------------------
	// tuio

	// --------------------------------
	// tokens
	private HashMap<Integer, MyToken> mytokens = new HashMap<Integer, MyToken>();	
	
	public void addTuioWallToken(TuioWallToken ttok) {
		if (verbose >= 1){
			System.out.println(
			"addTuioWallToken "+ttok.getTokenID() +" "+ttok.getSymbolID()+" ("+ttok.getSessionID()+") "
			+ttok.getX()+" "+ttok.getY()+" "+ttok.getAngle()*180/Math.PI);
		}
		MyToken tt = mytokens.get(ttok.getSymbolID());
		if (tt == null){
			tt = new MyToken(ttok, ttok.getSymbolID(), ttok.getX(), ttok.getY(), ttok.getAngle()*180/Math.PI);
			mytokens.put(ttok.getSymbolID(), tt);
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, _SSobject, ttok.getSymbolID());
			int[] rgba = ttok.RGBA();
			zcse.color = new Color(rgba[0], rgba[1], rgba[2], rgba[3]);
			zcse.x = tt.x; 
			zcse.y = tt.y;
			zcse.is_token = true;
			zcse.ray = ttok.getRay();
			_display.handleEvent(zcse);
		} else {
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(
					ClusterDisplay.zcsEvent.ZCS_SHOW_CURSOR, _SSobject, ttok.getSymbolID());
			_display.handleEvent(zcse);
			// move this in MyToken
			tt.x = ttok.getX(); tt.y = ttok.getY();
			tt.angle = tt.prevAngle = ttok.getAngle()*180/Math.PI;
			tt.downx = tt.px = tt.x; tt.downy = tt.py = tt.y;
			tt.dx = tt.dy = 0;
			tt.downTime = ttok.getTuioTime().getTotalMilliseconds();
			tt.update(ttok);
		}
		tt.inDrag = false;
		tt.zoomallowed = false;
		tt.inLongPress = false;
		tt.fixed = ttok.isFixed(); // a priori false ... but FIXME who knows!
	}
	
	public void updateTuioWallToken(TuioWallToken ttok) { 
		if (verbose >= 2){
			System.out.println(
			"updateTuioWallToken "+ttok.getTokenID() +" "+ttok.getSymbolID()+" ("+ttok.getSessionID()+") "
			+ttok.getX()+" "+ttok.getY()+" "+ttok.getAngle()+" "+ttok.isFixed());
		}
		MyToken tt = mytokens.get(ttok.getSymbolID());
		if (tt == null) { 
			return; // should not happen
		}
		if (!tt.fixed && ttok.isFixed()){
			if (verbose >= 1){
				System.out.println(
					"addTuioWallToken ATTACHED "+ttok.getTokenID() +" "+ttok.getSymbolID()+" ("+ttok.getSessionID()+") "
					+ttok.getX()+" "+ttok.getY()+" "+ttok.getAngle()+" "+ttok.isFixed());
			}
			tt.fixed = true;
			if (tt.inDrag){
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, _SSobject, ttok.getSymbolID());
				zcse.x = ttok.getX(); zcse.y = ttok.getY();	
				zcse.button = 0;
				_display.handleEvent(zcse);
				tt.inDrag = false;
			}
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_LOCK, _SSobject, ttok.getSymbolID());
				zcse.x = ttok.getX(); zcse.y = ttok.getY();
				_display.handleEvent(zcse);
		}
		double prevx = tt.px;
		double prevy = tt.py;
		if (!tt.fixed){
			tt.update(ttok);
		}
		if (tt.inLongPress){
			// update is enough
		}
		else if (tt.inDrag) {
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DRAG, _SSobject, ttok.getSymbolID());
				zcse.x = tt.x; zcse.y = tt.y;	
				zcse.dx = tt.dx; zcse.dy = tt.dy;
				zcse.button = 0;
				//System.out.println("DRAG: " + tt.dx + " " + tt.dy);
				_display.handleEvent(zcse);
		}
		else if (!tt.fixed) {
			// checking starting drag
			double delta = Math.abs(tt.x - tt.downx)*_display.getWidthMM() + Math.abs(tt.y - tt.downy)*_display.getHeightMM();
			//System.out.println("TOKEN CHECK DRAG: " + delta);
			if (delta > dragTh){
				//System.out.println("TOKEN DRAG: " + delta);
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_START_DRAG, _SSobject, ttok.getSymbolID());
				zcse.x = tt.downx; zcse.y = tt.downy;	
				zcse.button = 0;
				_display.handleEvent(zcse);
				zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DRAG, _SSobject, ttok.getSymbolID());
				zcse.x = tt.x; zcse.y = tt.y;	
				zcse.dx = tt.x - tt.downx; zcse.dy = tt.y - tt.downy;
				zcse.button = 0;
				_display.handleEvent(zcse);
				tt.inDrag = true;
			}
			else if (!tt.zoomallowed) {
				// System.out.println("TOKEN CHECK LONG PRESS " + tt.timeFromDown());
				if (tt.timeFromDown() > longPressTime){
					tt.inLongPress = true;
					tt.zoomallowed = false;
					//System.out.println("TOKEN LONG PRESS ");
					ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_PIEMENU, _SSobject, (int)tt.id);
					zcse.x = tt.x; zcse.y = tt.y; 
					_display.handleEvent(zcse);
					tt.inLongPress = true;
				}
			}
		}
		tt.angle = ttok.getAngle()*180/Math.PI;	
		//System.out.println("TOKEN CHECK Angle: " + Math.abs(tt.angle - tt.prevAngle) + " "+tt.angle + " prev: "+ tt.prevAngle);
		if (tt.zoomallowed || (!tt.inLongPress && Math.abs(tt.angle - tt.prevAngle) > 6))
		{
			//System.out.println("TOKEN Angle: "+tt.zoomallowed);
			if (!tt.zoomallowed){
				tt.zoomallowed = true;
				tt.prevAngle = tt.angle;
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_START_ZOOM, _SSobject, ttok.getSymbolID());
				zcse.x = tt.x; zcse.y = tt.y;
				_display.handleEvent(zcse);
			}
			if (Math.abs(tt.angle - tt.prevAngle) > 3) {
				double f = 1.03;
				if (tt.angle - tt.prevAngle < 0) f = 1/1.03; 
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, _SSobject, ttok.getSymbolID());
				zcse.x = tt.x; zcse.y = tt.y; zcse.f = f;
				_display.handleEvent(zcse);
				tt.prevAngle = tt.angle;
			}
		}
	}
	
	public void removeTuioWallToken(TuioWallToken ttok) {
		if (verbose >= 1){
			System.out.println(
			"removeTuioWallToken "+ttok.getTokenID() +" "+ttok.getSymbolID()+" ("+ttok.getSessionID()+") "
			+ttok.getX()+" "+ttok.getY()+" "+ttok.getAngle()+" "+ttok.isFixed());
		}
		MyToken tt = mytokens.get(ttok.getSymbolID());
		if (tt == null) { 
			return; // should not happen
		}
		// end the curent operation
		// TODO
		tt.zoomallowed = false;
		if (tt.inDrag){
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, _SSobject, ttok.getSymbolID());
			zcse.x = tt.x; zcse.y = tt.y;	
			zcse.button = 0;
			_display.handleEvent(zcse);
		}
		if (tt.zoomallowed){
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_ZOOM, _SSobject, ttok.getSymbolID());
			zcse.x = tt.x; zcse.y = tt.y;	
			zcse.button = 0;
			_display.handleEvent(zcse);
		}
		if (!tt.inDrag && !tt.zoomallowed && !tt.fixed && !tt.inLongPress &&
			(Math.abs(tt.x - tt.downx)*_display.getWidthMM() + Math.abs(tt.y - tt.downy)*_display.getHeightMM()) < dragTh)
		{
			// tape
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_PICK_OR_DROP, _SSobject, ttok.getSymbolID());
			zcse.x = tt.x; zcse.y = tt.y;	
			zcse.button = 0;
			_display.handleEvent(zcse);
		}
		tt.fixed = false;
		tt.zoomallowed = false;
		tt.inDrag = false;
		tt.inLongPress = false;

		// hide
		ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(
					ClusterDisplay.zcsEvent.ZCS_HIDE_CURSOR, _SSobject, ttok.getSymbolID());
		_display.handleEvent(zcse);

		// mytokens.remove(ttok.getSessionID());
	}

	public void addTuioObject(TuioObject tobj) { }
	public void updateTuioObject(TuioObject tobj) { }
	public void removeTuioObject(TuioObject tobj) { }
	

	// ---------------------------------
	// cursors
	private ArrayList<MyCursor> waitCursors = new ArrayList<MyCursor>();  
	private ArrayList<Dragger> draggers = new ArrayList<Dragger>();
	private ArrayList<Pincher> pinchers = new ArrayList<Pincher>();  
	private ArrayList<MyCursor> cursors = new ArrayList<MyCursor>();  

	public void addTuioCursor(TuioCursor tcur) {
		if (verbose >= 1){
			System.out.println(
			"addTuioCursor "+tcur.getSessionID() +" "+ tcur.getTuioTime().getTotalMilliseconds() +" "+
			tcur.getX()+" "+tcur.getY());
		}
		MyCursor mc = new MyCursor(tcur);
		// check if its too close to a pincher or dragger
		// if yes ignore
		for(Dragger d : draggers){
			if (d.distMM(mc) < tooCloseDistAfterMM) { 
				// ignore
				return;
			}
		}
		for(Pincher p : pinchers){
			if (p.minDistMM(mc) < tooCloseDistAfterMM) { 
				// ignore
				return;
			}
		}
		for(MyCursor c : cursors){
			if (c.distMM(mc) < tooCloseDistAfterMM) { 
				// ignore
				return;
			}
		}

		// if no add to waitCursors to see what we will do with it !
		waitCursors.add(new MyCursor(tcur));
	}

	public void updateTuioCursor(TuioCursor tcur) { 
		// a dragger ?
		for (Dragger d : draggers){
			if (tcur.getSessionID() == d.mc.tc.getSessionID()){
				d.drag(tcur);
				return;
			}
		}
		for (Pincher p : pinchers){
			if (tcur.getSessionID() == p.mc1.tc.getSessionID()){
				p.pinch(tcur);
				return;
			}
			else if (tcur.getSessionID() == p.mc2.tc.getSessionID()){
				p.pinch(tcur);
				return;
			}
		}
		for(MyCursor c : cursors){
			if (tcur.getSessionID() == c.tc.getSessionID()){
				c.update(tcur);
				return;
			}
		}

		// now handle the wait cursor
		MyCursor pinchCur = null;
		for(MyCursor c : waitCursors){
			if (tcur.getSessionID() == c.tc.getSessionID()){
				c.update(tcur);
				if (c.deltaToDownMM() > dragTh){
					// drag !!!
					double minDist = -1;
					for(MyCursor otherc : waitCursors){
						if (otherc.tc.getSessionID() == c.tc.getSessionID()) continue;
						double dist = c.distMM(otherc);
						if (dist < minDist || minDist < 0){
							pinchCur = otherc;
							minDist = dist;
						}
					}
					if (pinchCur != null && minDist < maxDistPinchCursors){
						// pinch ?
						double downdist = c.downDistMM(pinchCur);
						double diffPinch = Math.abs(minDist - downdist);
						if (diffPinch > dragTh/2){
							Pincher p = new Pincher(c, pinchCur);
							pinchers.add(p);
							waitCursors.remove(c);
							waitCursors.remove(pinchCur);
							p.startPinch(tcur);
						}
						else{
							if (minDist < tooCloseDistAfterMM){
								waitCursors.remove(pinchCur);
							}
							pinchCur = null;
						}
					}
					if (pinchCur == null)
					{
						// drag !!!
						Dragger d = new Dragger(c);
						draggers.add(d);
						waitCursors.remove(c);
						d.startDrag(tcur);
					}
				}
				else {
					// move a little only
					if (c.timeFromDown() > longPressTime){
						c.startLongPress();
						cursors.add(c);
						waitCursors.remove(c);
					}

				}
				return;
			}
		}
	}

	public void removeTuioCursor(TuioCursor tcur) {
		if (verbose >= 1){
			System.out.println(
			"removeTuioCursor "+tcur.getSessionID() +" "+ tcur.getTuioTime().getTotalMilliseconds() +" "+
			tcur.getX()+" "+tcur.getY());
		}
		for (Dragger d : draggers){
			if (tcur.getSessionID() == d.mc.tc.getSessionID()){
				d.endDrag(tcur);
				draggers.remove(d);
				return;
			}
		}
		for (Pincher p : pinchers){
			if (tcur.getSessionID() == p.mc1.tc.getSessionID()){
				p.endPinch(tcur);
				pinchers.remove(p);
				return;
			}
			else if (tcur.getSessionID() == p.mc2.tc.getSessionID()){
				p.endPinch(tcur);
				pinchers.remove(p);
				return;
			}
		}
		for(MyCursor c : cursors){
			if (tcur.getSessionID() == c.tc.getSessionID()){
				c.up();
				cursors.remove(c);
				return;
			}
		}

		for(MyCursor c : waitCursors){
			if (tcur.getSessionID() == c.tc.getSessionID()){
				c.click(tcur);
				waitCursors.remove(c);
				return;
			}
		}
	}
	
	public void refresh(TuioTime ftime) { }

}



