package ilda.zcsample;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import java.io.File;
import java.io.IOException;
import java.io.FilenameFilter;

import java.awt.Color;
import java.awt.GraphicsEnvironment;
import java.awt.GraphicsDevice;
import java.awt.Rectangle;
import java.awt.Cursor;
import java.awt.geom.Point2D;
import javax.swing.JFrame;

import fr.inria.zvtm.cluster.ClusterGeometry;
import fr.inria.zvtm.cluster.ClusteredView;
import fr.inria.zvtm.cluster.ClusteredViewStartListener;
import fr.inria.zvtm.cluster.ClusteredViewStartAdapter;
import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.engine.Location;
import fr.inria.zvtm.event.PickerListener;
import fr.inria.zvtm.engine.PickerVS;

import fr.inria.zvtm.engine.portals.Portal;
import fr.inria.zvtm.engine.portals.CameraPortal;
import fr.inria.zvtm.engine.portals.OverviewPortal;

import fr.inria.zvtm.glyphs.Glyph;
import fr.inria.zvtm.glyphs.VCircle;
import fr.inria.zvtm.glyphs.VRectangle;
import fr.inria.zvtm.widgets.PieMenu;

import fr.inria.zuist.engine.SceneManager;
import fr.inria.zuist.engine.PseudoView;

import ilda.zcsample.portals.DragMag;
import ilda.zcsample.cursors.*;

public class ClusterDisplay {

private ClusterGeometry _geometry;
private ClusteredView _clusteredView = null;
private View _mView;

private VirtualSpace _space, _dmVisSpace, _dataSpace, _cursorSpace;
private Vector<Camera> _vCameras;
private Camera _camera, _dmVisCamera, _dataCamera, _cursorCamera;

private VirtualSpace[] _aux_dataSpaces;
private Camera[] _aux_dataCameras;

private DragMagsManager _dmms;


// consoles ...
private ClusterGeometry[] _cGeometries;
private ClusteredView[] _cClusteredViews = null;
private VirtualSpace[] _cCursorSpaces;
private Vector<Camera>[] _cVCameras;
private Camera[] _cCameras, _cDMVisCameras, _cDataCameras, _cCursorCameras;
private OverviewPortal[] _cOPs;
private VirtualSpace[] _cZvss;
private PseudoView[] _cPVs;


private boolean _lockedPolicy = false;


// one screen width and height
private double _osWidth;
private double _osHeight;
// nbt of columns and rows
private int _col;
private int _row;
// bezels
private double _bezWidth;
private double _bezHeight;

private double _width, _height;
private double _widthMM, _heightMM;
private double _pixInMM;

private boolean _zuist;
private boolean _desktop;
private Renderer _renderer;
private TsunamiOverlay _tsunamiOver;
private GisLayer _gisLayer;

private PieMenuBuilder _pieMenuBuiler;
private HashMap<Object, ZcsDevice> _devices;

private Color _defaultBgColor = Color.DARK_GRAY;

private double _borderPickTolerence = 90; // pixel !!! FIXME in mm 

VirtualSpaceManager _vsm = VirtualSpaceManager.INSTANCE;

public ClusterDisplay(
	double w, double h, int c, int r, double bw, double bh, double pim)
{
	_osWidth = w;
	_osHeight = h;
	_col = c;
	_row = r;
	_bezWidth = bw;
	_bezHeight = bh;
	_desktop = Sample.desktop;
	_zuist = Sample.zuist;
	_pixInMM = pim;

	_space = _vsm.addVirtualSpace("mainSpace");
    _camera = _space.addCamera();
    _vCameras = new Vector<Camera>();
    _vCameras.add(_camera);
    _dataSpace = null;

    if (Sample.tsunami){
		_dataSpace = _vsm.addVirtualSpace("overlaySpace");
    	_dataCamera = _dataSpace.addCamera();
    	_vCameras.add(_dataCamera);
    	if (Sample.boat && !_desktop){
    		_aux_dataSpaces = new VirtualSpace[1];
    		_aux_dataSpaces[0] = _vsm.addVirtualSpace("aux_overlaySpace 0");
    		_aux_dataCameras = new Camera[1];
    		_aux_dataCameras[0] = _aux_dataSpaces[0].addCamera();
    		//_vCameras.add(_dataCamera);
    	}
	}

	_dmVisSpace = _vsm.addVirtualSpace("dmVisSpace");
	// NO USE A PORTAL !!!
    //_dmVisCamera = _dmVisSpace.addCamera();
    //_vCameras.add(_dmVisCamera);

    _cursorSpace = _vsm.addVirtualSpace("cursorSpace");
    _cursorCamera = _cursorSpace.addCamera();
    // no add this camera to the overlay camera...
    //_vCameras.add(_cursorCamera);

    if (!_desktop) { // cluster
		_geometry = new ClusterGeometry((int)w, (int)h, c, r);
		_geometry = _geometry.addBezels((int)_bezWidth, (int)_bezHeight);
    	_clusteredView = new ClusteredView(_geometry, r-1, c, r, _vCameras, 0);
    	if (Sample.sync && Sample.numSlaves > 0){
    		ClusterListenersWarpers.setSynchronousAtStart( _clusteredView, Sample.numSlaves);
    	}
		_width = _geometry.getWidth();
    	_height = _geometry.getHeight();
    	_widthMM = _geometry.getWidth()*_pixInMM;
    	_heightMM = _geometry.getHeight()*_pixInMM;
    	_clusteredView.setBackgroundColor(_defaultBgColor);
    	_clusteredView.setDrawPortalsOffScreen(true);
		_vsm.addClusteredView(_clusteredView);
		_vsm.setClusteredOverlayCamera(_cursorCamera, _clusteredView);
	}
	else{ // desktop
		GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice dev = genv.getDefaultScreenDevice();
        Rectangle rec = dev.getDefaultConfiguration().getBounds();
        _width = w;
    	_height = h;
    	_widthMM = w * _pixInMM;
    	_heightMM = h * _pixInMM;
    	if (Sample.fullscreen){
    		_width =(int) rec.getBounds().getWidth();
            _height= (int) rec.getBounds().getHeight();
    	}
		_mView = _vsm.addFrameView(
			_vCameras, "desktop", (Sample.opengl) ? View.OPENGL_VIEW : View.STD_VIEW, (int)_width, (int)_height,
			 false, false, !Sample.fullscreen, null);
		_mView.setBackgroundColor(_defaultBgColor);
        _mView.setCursorIcon(Cursor.DEFAULT_CURSOR);
        //_mView.setCursorIcon(Cursor.CUSTOM_CURSOR);
        _mView.getCursor().setVisibility(false);
        if (Sample.refreshRate > 0) { _mView.setRefreshRate(Sample.refreshRate); }
        _mView.setAntialiasing(Sample.antialiasing);
        //_mView.setDrawPortalsOffScreen(true);
        if (Sample.fullscreen){
            genv.getDefaultScreenDevice().setFullScreenWindow((JFrame)_mView.getFrame());
        }
        else {
            _mView.setVisible(true);
        }
        _vsm.setOverlayCamera(_cursorCamera, _mView);
	}
	// default...
	_camera.moveTo(0, 0);
    _camera.setAltitude(0.0f);
    _cursorCamera.moveTo(0, 0);

    // init the renderer
    SceneManager[] zsms = null; 
    SceneManager[] aux_zsms = null; 
    if (_zuist) {
    	_renderer = new ZuistRenderer(this, Sample.zuist_file_names, Sample.zuist_aux_file_names);
    	zsms = ((ZuistRenderer)_renderer).getZuistSceneManagers();
    	aux_zsms = ((ZuistRenderer)_renderer).getAuxZuistSceneManagers();
    	if (!_desktop) {
    		_clusteredView.setLatePaintSync(100);
    	}
    }
    else {
		_renderer = new CirclesRenderer(this);
	}
	_renderer.initRendering();

	if (!_zuist) {
		_renderer.globalView();
	}

    // init the console
    if (Sample.consoles != null){
		int n = Sample.consoles.length;
		System.out.println("ClusterDisplay starts "+n+" consoles");
		_cClusteredViews = new  ClusteredView[n];
		_cGeometries = new ClusterGeometry[n];
		_cCursorSpaces = new VirtualSpace[n]; 
		_cVCameras = new Vector[n];
		_cCameras = new Camera[n];
		_cCursorCameras = new Camera[n];
		_cOPs = new OverviewPortal[n];
		if (_zuist){
			_cZvss = new VirtualSpace[n];
			_cPVs = new  PseudoView[n];
		}
		int cwidth = 100; int cheight = 100; // FIXME 

		for (int i = 0; i < n; i++)
	    {
	    	String[] opts = Sample.consoles[i].split(",");
	    	try{
	    		cwidth = Integer.parseInt(opts[0]);
	    		cheight =  Integer.parseInt(opts[1]);
	    	} catch(Exception ex){
	    		System.out.println("Bad Option to the consoles.... " + ex);
	    	}
	    	_cGeometries[i] = new ClusterGeometry(cwidth, cheight, 1, 1);
	    	_cVCameras[i] = new Vector<Camera>();
	    	if (_zuist){
	    		_cZvss[i] = _vsm.addVirtualSpace("Console"+i);
	    		_cCameras[i] = _cZvss[i].addCamera();
	    		_cPVs[i] = new PseudoView(_cZvss[i], _cCameras[i], cwidth, cheight);
	    		zsms[0].addPseudoView(_cPVs[i]);
	    	}
	    	else {
	    		_cCameras[i] = _space.addCamera();
	    	}
	    	_cVCameras[i].add(_cCameras[i]);
	    	
	    	if (_dataSpace!=null){
	    		// FIXME: make this idependant...
	    		Camera dcam = _dataSpace.addCamera();
				dcam.setLocation(_cCameras[i].getLocation());
				_cCameras[i].stick(dcam, true);
				_cVCameras[i].add(dcam);
			}
	    	_cClusteredViews[i] = new ClusteredView(
	    		_cGeometries[i], 0,  1, 1, _cVCameras[i], i+1);
	    	_cClusteredViews[i].setBackgroundColor(_defaultBgColor);
    		_cClusteredViews[i].setDrawPortalsOffScreen(true);
	    	_vsm.addClusteredView(_cClusteredViews[i]);

	    	_cClusteredViews[i].enableEventForwarding(true);
	    	_cCursorSpaces[i] = _vsm.addVirtualSpace("cursorSpace "+ i);
    		_cCursorCameras[i] = _cCursorSpaces[i].addCamera();
			_vsm.setClusteredOverlayCamera(_cCursorCameras[i], _cClusteredViews[i]);
	    }
	    Vector obsCamVect = new Vector<Camera>();
	    for (int i = 0; i < n; i++) {
	    	obsCamVect.add(_cCameras[i]);
	    }
	    for (int i = 0; i < n; i++) {
	    	Camera cam;
	    	double ovx = 0.8*_cGeometries[i].getWidth();
	    	double ovy = 0.8*_cGeometries[i].getHeight();
	    	double ovw = 0.2*_cGeometries[i].getWidth();
	    	double ovh = 0.2*_cGeometries[i].getHeight();
	    	if (_zuist){
	    		cam = _cZvss[i].addCamera();
	    		PseudoView pv = new PseudoView(_cZvss[i], cam, (int)ovw, (int)ovh);
	    		zsms[0].addPseudoView(pv);
	    	}
	    	else {
	    		cam = _space.addCamera();
	    	}
	    	//System.out.println("Add OP Console "+i+" "+_cGeometries[i].getWidth()+" "+_cGeometries[i].getBlockHeight() + "  " + _cGeometries[i].getHeight());
	    	_cOPs[i] = new OverviewPortal(
	    		(int)(ovx), (int)(ovy), (int)(ovw), (int)(ovh), cam, obsCamVect);
			_vsm.addClusteredPortal(_cOPs[i], _cClusteredViews[i]);

			_cOPs[i].setBorder(Color.BLUE);
			_cOPs[i].setBorderWidth(2);
			if (_zuist){_cOPs[i].setBackgroundColor(new Color(164, 207, 252)); }
			else {_cOPs[i].setBackgroundColor(Color.DARK_GRAY); }
			_cOPs[i].setObservedRegionColor(i, Color.RED);
			_cOPs[i].setObservedRegionTranslucency(0.2f);
			//cp.setObservedRegionBorderWidth(3);
			_cOPs[i].drawObservedRegionLocator(true);
			//_cOPs[i].setObservedViewLocationAndSize(0,0,0,0);
			double [] b = _renderer.getBounds();
			Location loc = _cOPs[i].centerOnRegion(0, b[0], b[1], b[2], b[3]);
			// FIXME zuits change that when we add the caraibian area...
	    }
	}

	//_vsm.setClusteredOverlayCamera(_cursorCamera, _clusteredView);

	if (Sample.tsunami){
		// a4cffc  164, 207, 252
    	setBackgroundColor(new Color(164, 207, 252));
		if (zsms != null && zsms[0] != null){
    		_tsunamiOver = new TsunamiOverlay(this, _renderer, _dataSpace, _dataCamera);
    		//_tsunamiOver.computeSlippyMapTransform(zsms[0], 256);
    	}
    	if (aux_zsms != null && aux_zsms[0] != null && Sample.boat){
    		double[] bounds = ((ZuistRenderer)_renderer).getAuxBounds(0);
    		if (bounds != null){
    			PseudoView pv = aux_zsms[0].getPseudoView(0);
    			_aux_dataCameras[0].setLocation(pv.c.getLocation());
				//pv.c.stick(_aux_dataCameras[0], true);
    			_gisLayer = new GisLayer(_aux_dataSpaces[0]);
    			_gisLayer.computeSlippyMapTransform(aux_zsms[0], 256);
    			File f;
    			if (!Sample.desktop){
    				//f = new File("Caribe_Wave_2015/4_Donnees_Navires/Navire_CW_15.shp");
    				f = new File("Caribe_Wave_2015/4_Donnees_Navires/Navire_CW_15.shp");
    				_gisLayer.loadBoats(f);
    				// f = new File("3_Securite_Navire/Zone_exclusion_05NM.shp");
    				//_gisLayer.loadSecure(f, new Color(153,0,153), true);
    			}
    			f = new File("Caribe_Wave_2015/2_Caribe_Wave_2015/TTT_CW15_PolyL.shp");
    			_gisLayer.loadTsunamiLines(f);
    			f = new File("Caribe_Wave_2015/2_Caribe_Wave_2015/Epicentre_CW15.shp");
    			_gisLayer.loadEpicentre(f);
    		}
    	}
	}

	_devices = new HashMap();

	_dmms = new DragMagsManager(this, _dmVisSpace, _space, zsms, _dataSpace, aux_zsms, _aux_dataSpaces);

	if (!_zuist) {
		//addOverviewPortal(0.8, 0.8, 0.1, 0.1);
	}
	else{

	}

	_pieMenuBuiler = new PieMenuBuilder(_cursorSpace, this);

	System.out.println(
		"ClusterDisplay is running " + _width + " " + _height);
}

public void initConsoleListener() {
	if (_desktop || _cClusteredViews == null) { return; }
	for(int i= 0; i < _cClusteredViews.length; i++){
	 	ClusterListenersWarpers.setConsoleListener(this, _cClusteredViews[i], i+1);
	}
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
//

public String[] getLayerNames(){
	return _renderer.getLayerNames();
}

public String[] getAuxLayerNames(){
	return _renderer.getAuxLayerNames();
}

public boolean setMainLayerIndex(int idx) {
	return _renderer.setMainLayerIndex(idx);
}

public boolean nextMainLayerIndex() {
	return _renderer.nextMainLayerIndex();
}

public void tmpT(double x, double y){
	if (_tsunamiOver != null) _tsunamiOver.tmpT(x, y);
}

public void displayTsunamiData(int how){
	if (_tsunamiOver != null){
		_tsunamiOver.displayShapes(how, how, how);
	}
}

public void displayTsunamiData(int evac, int tsunami, int boats){
	if (_tsunamiOver != null){
		_tsunamiOver.displayShapes(evac, tsunami, boats);
	}
}

public String[] getTsunamiDataSetsNames() { 
	if (_tsunamiOver != null){
		return _tsunamiOver.getDataSetsNames();
	}
	return null;
}

public void startTsunami() {
	if (_tsunamiOver != null){
		//re TODO _tsunamiOver.start();
	}
}

public void globalView(){
	_renderer.globalView();
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

public void setSyncronous(int b)
{
	if (_desktop){
		return;
	}
	boolean v = _clusteredView.isSynchronous();
	if (b == 0){
		v = false;
	}
	else if (b == 1){
		v = true;
	}
	else{
		v = !v;
	}
	_clusteredView.setSynchronous(v);
}

public void setBackgroundColor(Color c)
{
	_defaultBgColor = c;
	if (_desktop){
		_mView.setBackgroundColor(c);
	}
	else {
		_clusteredView.setBackgroundColor(c);
		if (_cClusteredViews !=null && _cClusteredViews.length > 0){
			for (int i = 0; i < _cClusteredViews.length; i++){
				_cClusteredViews[i].setBackgroundColor(c);
			}
		}
	}
}

public Color getBackgroundColor() { return _defaultBgColor; }

public void addPortal(Portal p){
	if (_desktop){
		_vsm.addPortal(p, _mView);
	}
	else {
		_vsm.addClusteredPortal(p, _clusteredView);
	}
}

public void stackPortalFront(Portal p){
	if (_desktop){
		_vsm.stackPortalFront(p);
	}
	else {
		_vsm.stackClusteredPortalFront(p);
	}
}

public void stackPortalFront(DragMag dm){
	stackPortalFront((Portal)dm);
	Vector<CameraPortal> cps = dm.getMagicPortals();
	for(CameraPortal ip : cps) { stackPortalFront((Portal)ip); }
}

public void destroyPortal(Portal p){
	if (_desktop){
		_vsm.destroyPortal(p);
	}
	else {
		_vsm.destroyClusteredPortal(p);
	}
}

public void stop(){
	if (!_desktop){
		_vsm.stop();
	}
}
// --------------------------------------------------------------------------------
// gets...

public ClusteredView getClusteredView() { return _clusteredView; }

public View getView() { return _mView; }

public double getHeight() { return _height; }

public double getWidth() { return _width; }

public double getHeightMM() { return _heightMM; }

public double getWidthMM() { return _widthMM; }

public int getNumCol() { return _col; }

public int getNumRow() { return _row; }

public double getBezHeight() { return _bezHeight; }

public double getBezWidth() { return _bezWidth; }

public VirtualSpace getSpace() { return _space; }

public Camera getMainCamera() { return _camera; }

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// to be moved into events ?

private void fixDMMapping(DragMag dm)
{
	if (dm.getType() == DragMag.DM_TYPE_DRAGMAG){
		double xx,yy;
		double cx = dm.x+dm.w/2;
		double cy = dm.y+dm.h/2;
		double fac = 1.5;
		if (cx > _width/2){
			// right
			cx = cx + fac*dm.w/2; // fixme: add vis width
			if (cx+dm.w/2 >= _width-10) { 
				cx = _width - 2*1.1*dm.w/2; 
			} // fixme: use mm
		}
		else {
			// left
			cx = cx - fac*dm.w/2; // idem
			if (cx-dm.w/2 <= -10) { cx = cx + 2*fac*dm.w/2; } // fixme: use mm
		}
		// a priori up
		cy = cy - fac*dm.h/2;
		if (cy-dm.h/2 < 0.01*_height){
			// but not to high (should use user height :)
			cy = dm.h/2 + 0.01*_height;
		}
		cx = cx - dm.w/2;
		cy = cy - dm.h/2;
		System.out.println("************ fixDMMapping "+ (dm.x-dm.w/2) +" "+ (dm.y-dm.h/2) +" "+ cx +" "+ cy);
		dm.moveTo(cx, cy);
	}
}
public DragMag addDragMag(int type){
	DragMag dm = _dmms.addDragMag(type);
	fixDMMapping(dm);
	return dm;
}
public DragMag addDragMag(int type, double x, double y){
	DragMag dm = _dmms.addDragMag(type, (int)(x*_width), (int)(y*_height));
	fixDMMapping(dm);
	return dm;
}

public DragMag addDragMagAtIndex(int idx, int type, double x, double y){
	DragMag dm = _dmms.addDragMagAtIndex(idx, type, (int)(x*_width), (int)(y*_height));
	fixDMMapping(dm);
	return dm;
}

public DragMag addMagic(Object s, int id){
	ZcsDevice dev = _devices.get(s);
	if (dev == null) return null;
	ZcsCursor cur = dev.cursors.get(id);
	if (cur == null) return null;
	if (cur.attachedDragMag != null){
		return _dmms.addMagic(cur.attachedDragMag);
	}
	else if (cur.currentDragMag != null){
		return _dmms.addMagic(cur.currentDragMag);
	}
	return null;
}

public DragMag addMagic(double x, double y){
	return _dmms.addMagic((int)(x*_width), (int)(y*_height));
}

public DragMag addMagic(DragMag dm){
	if (dm == null) { return null; }
	return _dmms.addMagic(dm);
}

public void removeOrAddMagic(DragMag dm){
	if (dm == null) { return; }
	if (_dmms.numOfMagic(dm) > 0){
		_dmms.removeMagic(dm);
	}
	else{
		_dmms.addMagic(dm);
	}
}

public void removeDragMag(double x, double y){
	DragMag dm = _dmms.checkDragMag((int)(x*_width), (int)(y*_height));
	if (dm != null) {
		_detachDragMag(dm);
	}
	_dmms.removeDragMag((int)(x*_width), (int)(y*_height));
}
public void removeDragMag(DragMag dm){
	if (dm == null) { return; }
	_detachDragMag(dm);
	_dmms.removeDragMag(dm);
}

public DragMag addAuxDragMagView(int idx, double x, double y)
{
	/* int idx, int type, int x, int y, double scale, boolean aux */
	DragMag dm = _dmms.addDragMagAtIndex(
		idx, DragMag.DM_TYPE_INDEPENDANT,(int)(x*_width), (int)(y*_height), 1, true);
	return dm;
}

public OverviewPortal addOverviewPortal(double x, double y, double w, double h)
{
	Camera cam = _space.addCamera();
    //_vCameras.add(cam);

	OverviewPortal cp = new OverviewPortal(
		(int)(x*_width), (int)(y*_height),
		(int)(w*_width), (int)(h*_height), cam, _camera);
	
	addPortal(cp);

	cp.setBorder(Color.BLUE);
	cp.setBorderWidth(5);
	cp.setBackgroundColor(Color.DARK_GRAY);
	cp.setObservedRegionColor(Color.RED);
	cp.setObservedRegionTranslucency(0.2f);
	//cp.setObservedRegionBorderWidth(3);
	cp.drawObservedRegionLocator(true);

	double [] b = _renderer.getBounds();
	Location loc = cp.centerOnRegion(0, b[0], b[1], b[2], b[3]);
	
	//cp.setVisible(false);
	return cp;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
//

// -------------------------
//
public class ZcsCursor implements PickerListener 
{
public int id;
public int vId;
public double x, y;
public double viewWidth, viewHeight;
public double ray; // in mm
public Color color;
public OlivierCursor wc;
public boolean is_touch = false;
public boolean is_token = false;
public DragMag currentDragMag = null;
public DragMag currentVisDragMag = null;
public DragMag attachedDragMag = null;
public DragMag attachedVisDragMag = null;
public DragMag linkedDragMag = null;
public DragMag selectedDragMag = null;
public DragMag selectedVisDragMag = null;
public double deltax =0,deltay = 0;
public boolean grabed = false;
public boolean zoomIsResize = false;
public boolean zoomIsWidthResize = false;
public boolean zoomIsHeightResize = false;
public boolean zoomForbiddenDuringDrag = false;
public boolean dragIsMove = false;
public boolean dragIsResize = false;

//
public boolean doLockBg = false;
public DragMag lockedDm = null;
//
public OverviewPortal currentOP = null;
//
public PieMenu pm = null;
public DragMag pmdm = null; 
public double pmx, pmy;
protected PickerVS mnSpacePicker;
protected Point2D.Double vsCoords = new Point2D.Double();

public void move(double x, double y)
{
	this.x = x; this.y = y;
	double xx = x*viewWidth - viewWidth/2.0;
	double yy =  viewHeight/2.0 - y*viewHeight;
	wc.moveTo((long)(xx), (long)(yy));
	if(mnSpacePicker != null) {
		mnSpacePicker.setVSCoordinates(xx, yy);
	}
	if(pm != null) {
		mnSpacePicker.computePickedGlyphList(_cursorCamera, false);
		Glyph g = mnSpacePicker.lastGlyphEntered();
	}
}

public void moveTo(double x, double y)
{
	this.x = x; this.y = y;
	//System.out.println("move "+ x+" "+y);
	double w = viewWidth;
	double h = viewHeight;
	wc.moveTo(x*w - w/2.0, h/2.0 - y*h);

	if(mnSpacePicker != null) {
		vsCoords.x = x*w - w/2.0;
		vsCoords.y = h/2.0 - y*h;
		mnSpacePicker.setVSCoordinates(vsCoords.x, vsCoords.y);
	}
	if(pm != null) {
		mnSpacePicker.computePickedGlyphList(_cursorCamera, false);
	}
}

public void triggerPieMenuCommand(){
	Glyph g = mnSpacePicker.lastGlyphEntered();
	System.out.println("triggerPieMenuCommand " + g);
	if (g != null && g.getType() != null && g.getType().equals(PieMenuBuilder.T_PM)){
		int index = pm.getItemIndex(g);
		if (index != -1){
			String label = pm.getLabels()[index].getText();
			System.out.println("triggerPieMenuCommand: " + label);
			pm.destroy(0);
    		pm = null;
			if (label == PieMenuBuilder.BGM_GLOBAL_VIEW){
				globalView();
			}
			else if (label == PieMenuBuilder.BGM_DRAGMAG){
				addDragMag(DragMag.DM_TYPE_DRAGMAG, pmx, pmy);
			}
			else if (label == PieMenuBuilder.BGM_DATA){
				displayTsunamiData(2); // Toggle
			}
			else if (label == PieMenuBuilder.BGM_LAYER){
				nextMainLayerIndex();
			}
			else if (label == PieMenuBuilder.BGM_OVERVIEW){
				addAuxDragMagView(0, pmx, pmy);
				//addOverviewPortal(x, y, _width*0.1, _height*0.1);
			}
			else if (label == PieMenuBuilder.DMM_MAGIC){
				removeOrAddMagic(pmdm); // FIXME Toggel
			}
			else if (label == PieMenuBuilder.DMM_DELETE){
				removeDragMag(pmdm);
			}
			else if (label == PieMenuBuilder.DMM_ATTACH){
				if (pmdm != null && !isDmLocked(pmdm)) {
					if (is_token){
						if (selectedDragMag != null){
							DragMag dm =selectedDragMag;
							selectedDragMag =  null;
							//if (e.obj instanceof BasicDevice) {
								// should add setSelected
    							//BasicDevice bDevice = (BasicDevice)e.obj;
    							//bDevice.setAttached(cur.id, false);
    						//}
							ZcsCursor acur = _getCursorAttachedToDragMag(dm, false);
							Color c = DragMagsManager.DDM_BORDER_COLOR;
							if (acur != null) { c = acur.color; }
							dm.setBorder(c);
						}
						else{
							selectedDragMag = pmdm;
							//if (e.obj instanceof BasicDevice) {
								// should add setSelected
    							//BasicDevice bDevice = (BasicDevice)e.obj;
    							//bDevice.setAttached(cur.id, true);
    						//}
							selectedDragMag.setBorder(color);
						}

					}
					else if (attachedDragMag == null){
						currentDragMag = attachedDragMag = pmdm;
						//if (source != null) source.setAttached(cid, true);
						attachedDragMag.setBorder(color);
						if (pmdm.getType() != DragMag.DM_TYPE_INDEPENDANT){
							hide();
						}
					}
					else {
						attachedDragMag.setBorder(DragMagsManager.DDM_BORDER_COLOR);
						attachedDragMag = null;
						// if (source != null) source.setAttached(cid, false);
						show();
					}
				}
			}
		}
	}
	// if (g != null && g.getType() == null) { return; }
	if (pm != null){
		pm.destroy(0);
    	pm = null;
    }
}

public void hide() { wc.setVisible(false); }
public void show() { wc.setVisible(true); }
public boolean isVisible() { return wc.isVisible(); }

public ZcsCursor(int id, double x, double y, Color c, int vid)
{
	this.id = id;
	this.vId = vid;
	this.x = x;
	this.y = y;
	this.color = c;
	
	viewWidth = _width;
	viewHeight = _height;
	VirtualSpace s = _cursorSpace;
	if (vid > 0 && _cGeometries !=null && _cGeometries.length > vid-1){
		viewWidth =  _cGeometries[vid-1].getWidth();
		viewHeight = _cGeometries[vid-1].getBlockHeight(); // FUCKING getHeight();
		s = _cCursorSpaces[vid-1];
	}
	grabed = false;

	mnSpacePicker = new PickerVS();
	_cursorSpace.registerPicker(mnSpacePicker);
	mnSpacePicker.setListener(this);

	double t = (viewWidth > 3000)? 16:2;
	double w = (viewHeight > 3000)? 100:8;
	//if (istouch) { t = t/2; w = w/2; }
	wc = new OlivierCursor(s,t,w,this.color);
	move(x, y);
}

public ZcsCursor(int id, double x, double y, Color c)
{
	this(id, x, y, c, 0);
}

public void setRay(double r){
	ray = r;
}

@Override
public void enterGlyph(Glyph g) {
	// System.out.println("glyph entred " + g.getType());
	if (g.getType() != null){
		if (g.getType().equals(PieMenuBuilder.T_PM)){
			g.highlight(true, null);
		}
		// else if (g.getType().startsWith(Config.T_SPMI)){
		// 	//g.highlight(true, null);
		// 	if (g.getType() == Config.T_SPMISc){
		// 		subPieMenuEvent(g);
		// 	}
		// }
		// else if (g.getType().equals(Config.T_CLT_BTN)){
		// 	app.getMenuEventHandler().selectCLT((String)g.getOwner());
		// }
	}
	else {
		if (pm != null && g == pm.getBoundary()){
			pm.setSensitivity(true);
		}
	}
}

@Override
public void exitGlyph(Glyph g) {
	//System.out.println("glyph exited " + g.getType());
	if (g.getType() != null){
		if (g.getType().equals(PieMenuBuilder.T_PM)) { //} || g.getType().startsWith(Config.T_SPMI)){
			// exiting a pie menu item
			g.highlight(false, null);
		}
		// else if (g.getType().equals(Config.T_CLT_BTN)){
		// 	g.highlight(false, null);
		// }
	}
	else {
		if (pm != null && g == pm.getBoundary()){
			// crossing the main pie menu's trigger
			// Glyph lge = mnSpacePicker.lastGlyphEntered();
			// if (lge != null && lge.getType() == PieMenuBuilder.T_PM){
			// 	if (app.getMenuEventHandler().displaySubPieMenu(lge, new Point2D.Double(vsCoords.x, vsCoords.y))){
			// 		app.getMenuEventHandler().mainPieMenu.setSensitivity(false);
			// 	}
			// }
		}
		// else if (app.getMenuEventHandler().subPieMenu != null && g == app.getMenuEventHandler().subPieMenu.getBoundary()){
		// 	// crossing a sub pie menu's trigger
		// 	// (takes back to main pie menu)
		// 	hideSubPieMenu();
		// 	app.getMenuEventHandler().mainPieMenu.setSensitivity(true);
		// }
	}
}
}

/// --------------------------
//
private boolean isBgLocked(){
	for (Map.Entry<Object, ZcsDevice> de : _devices.entrySet()) {
    	ZcsDevice dev = de.getValue();
    	for (Map.Entry<Integer, ZcsCursor> ce : dev.cursors.entrySet()){
    		ZcsCursor cur = ce.getValue();
    		if (cur.doLockBg) return true;
    	}
	}
	return false;
}

private boolean isDmLocked(DragMag dm){
	if (dm == null) { return false; }
	for (Map.Entry<Object, ZcsDevice> de : _devices.entrySet()) {
    	ZcsDevice dev = de.getValue();
    	for (Map.Entry<Integer, ZcsCursor> ce : dev.cursors.entrySet()){
    		ZcsCursor cur = ce.getValue();
    		if (cur.lockedDm == dm) return true;
    	}
	}
	return false;
}

// --------------------------
//
public  class zcsEvent
{

final static int ZCS_CREATE_CURSOR=1;
final static int ZCS_DELETE_CURSOR=2;
final static int ZCS_HIDE_CURSOR=3;
final static int ZCS_SHOW_CURSOR=4;

final static int ZCS_MOVE_CURSOR=10;
final static int ZCS_START_MOVE_CURSOR=11;
final static int ZCS_END_MOVE_CURSOR=12;

final static int ZCS_CURSOR_CLICK=15;

final static int ZCS_PICK_OR_DROP=50;


final static int ZCS_START_DRAG=100;
final static int ZCS_DRAG=101;
final static int ZCS_END_DRAG=102;

final static int ZCS_START_ZOOM=105;
final static int ZCS_ZOOM=106;
final static int ZCS_END_ZOOM=107;

final static int ZCS_ATTACH=200;
final static int ZCS_LINK=201;

final static int ZCS_CREATE_PIEMENU=300;

final static int ZCS_LOCK=333;

BasicDevice source;
int type;
int vId;
double ray;
double x,y;
double dx, dy;
double f;
Object obj;
int cid;
int button;
Color color;
boolean onoff;
boolean hide;
DragMag dm;
boolean visor;
boolean is_touch;
boolean is_token;
int contacts;

public zcsEvent(int t, Object o, int cid_)
{
	type = t;
	vId = 0;
	ray = 10; // mm
	obj = o;
	if (o instanceof BasicDevice) source = (BasicDevice)o;
	cid = cid_;
	x=y=0;
	dm = null;
	visor = false;
	is_touch = false;
	is_token = false;
	contacts = 0;
}

public zcsEvent(BasicDevice s, int t, Object o, int cid_)
{
	source = s;
	type = t;
	vId = 0;
	ray = 10; // mm
	obj = o;
	cid = cid_;
	x=y=0;
	dm = null;
	visor = false;
	is_touch = false;
	is_token = false;
	contacts = 0;
}
	
}

// --------------------------
//
public  class ZcsDevice
{
	
public HashMap<Integer, ZcsCursor> cursors;

public ZcsDevice(String name)
{
	cursors = new HashMap();
}

public void createCursor(int id, double x, double y, Color c, boolean hide, int vid)
{
	if (c == null) c = Color.RED;
		
	ZcsCursor cur = new ZcsCursor(id, x,y,c,vid);
	cursors.put(id, cur);
	if (hide) { cur.hide(); }
}

public void createCursor(int id, double x, double y, Color c, boolean hide)
{
	createCursor(id, x, y, c, false, 0);
}

public void createCursor(int id, double x, double y, Color c)
{
	createCursor(id, x, y, c, false, 0);
}

public void createCursor(int id, double x, double y)
{
	createCursor(id, x, y, null, false, 0);
}

public void deleteCursor(int id)
{
	cursors.remove(id);
}

}

// --------------------------
//
public void registerDevice(Object obj, String name)
{
	_devices.put(obj, new ZcsDevice(name)); 
	System.out.println("registerDevice: "+obj);
}


// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------

public Object getContext(double x, double y){
	DragMag dm = _dmms.checkDragMag((int)(x*_width+0.5), (int)(y*_height+0.5));
	// visdm = _dmms.checkVis((int)(e.x*_width), (int)(e.y*_height));
	return (Object)dm;
}
// --------------------------
//

private ZcsCursor _getCursorAttachedToDragMag(DragMag dm, boolean vis)
{
	for (Map.Entry<Object, ZcsDevice> de : _devices.entrySet()) {
    	ZcsDevice dev = de.getValue();
    	for (Map.Entry<Integer, ZcsCursor> ce : dev.cursors.entrySet()){
    		ZcsCursor cur = ce.getValue();
    		if (!vis && cur.attachedDragMag == dm){
    			return cur;
    		}
    		if (vis && cur.attachedVisDragMag == dm){
    			return cur;
    		}
    	}
	}
	return null;
}

private ZcsCursor _getCursorSelectedByDragMag(DragMag dm, boolean vis)
{
	for (Map.Entry<Object, ZcsDevice> de : _devices.entrySet()) {
    	ZcsDevice dev = de.getValue();
    	for (Map.Entry<Integer, ZcsCursor> ce : dev.cursors.entrySet()){
    		ZcsCursor cur = ce.getValue();
    		if (!vis && cur.selectedDragMag == dm){
    			return cur;
    		}
    		if (vis && cur.selectedVisDragMag == dm){
    			return cur;
    		}
    	}
	}
	return null;

}

private void _detachDragMag(DragMag dm){
	for (Map.Entry<Object, ZcsDevice> de : _devices.entrySet()) {
    	ZcsDevice dev = de.getValue();
    	Object obj = de.getKey();
    	BasicDevice bDevice = null;
    	if (obj instanceof BasicDevice) {
    		bDevice = (BasicDevice)obj;
    	}
    	for (Map.Entry<Integer, ZcsCursor> ce : dev.cursors.entrySet()){
    		ZcsCursor cur = ce.getValue();
    		if (cur.currentDragMag == dm){
    			cur.currentDragMag = null;
    		}
    		if (cur.currentVisDragMag == dm){
    			cur.currentVisDragMag = null;
    		}
    		if (cur.attachedDragMag == dm){
    			cur.attachedDragMag = null;
    			if(bDevice != null) {
    				System.out.println("_detachDragMag " + cur.id);
    				bDevice.setAttached(cur.id, false); }
    			cur.show();
    		}
    		if (cur.attachedVisDragMag == dm){
    			cur.attachedVisDragMag = null;
    			if(bDevice != null) { 
    				System.out.println("_detachDragMag vis " + cur.id);
    				bDevice.setAttached(cur.id, false); }
    			cur.show();
    		}
    		if (cur.linkedDragMag == dm){
    			cur.linkedDragMag = null;
    			if(bDevice != null) { bDevice.setLinked(cur.id, false); }
    		}
    	}
	}
}

private void _checkDragMag(ZcsCursor cur, zcsEvent e)
{
	if (cur.vId > 0) {
		//for now only some portal
		if (cur.vId > _cOPs.length) { return; }
		int i = cur.vId-1;
		OverviewPortal op = _cOPs[i];
		boolean inside = op.coordInside((int)(e.x*_cGeometries[i].getWidth()),
			(int)(e.y*_cGeometries[i].getHeight()));
		if (inside && cur.currentOP==null) {
			//System.out.println("enter OP "+cur.vId);
			cur.currentOP=op;
		}
		else if (!inside && cur.currentOP != null){
			cur.currentOP=null;
			//System.out.println("leave OP "+cur.vId);
		}
		return;
	}

	if (cur == null || _dmms == null || cur.attachedDragMag != null ||
		cur.attachedVisDragMag != null || cur.grabed || cur.selectedDragMag != null ||
		cur.selectedVisDragMag != null || cur.pm != null) return;

	DragMag dm = _dmms.checkDragMag((int)(e.x*_width+0.5), (int)(e.y*_height+0.5));
	DragMag visdm = null;
	ZcsCursor scur = null;
	ZcsCursor acur = null;
	ZcsCursor pcur = null;


	if (dm != null){
	 	acur = _getCursorAttachedToDragMag(dm, false);
	 	scur = _getCursorSelectedByDragMag(dm, false);
	}
	if (cur.currentDragMag != null){
		pcur = _getCursorAttachedToDragMag(cur.currentDragMag, false);
	}
	if (dm != null && (acur == null || !_lockedPolicy) &&  !isDmLocked(dm)){
		if (dm == cur.currentDragMag) { // ok
		}
		else{
			if (cur.currentDragMag == null){
				// entring dm
				dm.setBorder(cur.color);
			}
			else{
				// leaving cur.currentDragMag, entring dm
				Color c = DragMagsManager.DDM_BORDER_COLOR;
				if (scur != null) { c = scur.color; }
				else if (acur != null) { c = acur.color; }
				else if (pcur != null) { c = pcur.color; }
				cur.currentDragMag.setBorder(c);
				dm.setBorder(cur.color);
			}
			cur.currentDragMag = dm;
		}
	}
	else {
		if (cur.currentDragMag != null && (acur == null || !_lockedPolicy)){
			// leaving  cur.currentDragMag
			Color c = DragMagsManager.DDM_BORDER_COLOR;
			if (scur != null) { c = scur.color; }
			else if (acur != null) { c = acur.color; }
			else if (pcur != null) { c = pcur.color; }
			cur.currentDragMag.setBorder(c);
			cur.currentDragMag = null;
		}
		visdm = _dmms.checkVis((int)(e.x*_width), (int)(e.y*_height));
		acur = null;
		pcur = null;
		if (visdm != null){
			acur = _getCursorAttachedToDragMag(visdm, true);
		}
		if (cur.currentVisDragMag != null){
			pcur =  _getCursorAttachedToDragMag(cur.currentVisDragMag, true);
		}
		if (visdm != null && (acur == null || !_lockedPolicy) &&  !isDmLocked(visdm)) {
			if (visdm == cur.currentVisDragMag) { // ok
			}
			else{
				if (cur.currentVisDragMag == null){
					// entring visdm
					visdm.setVisLinesLightColor(cur.color);
				}
				else{
					// leaving cur.currentVisDragMag, entring visdm
					Color c = DragMag.DM_VIS_COLORS[1];
					if (scur != null) { c = scur.color; }
					else if (acur != null) { c = acur.color; }
					else if (pcur != null) { c = pcur.color; }
					cur.currentVisDragMag.setVisLinesLightColor(c);
				}
				cur.currentVisDragMag = visdm;
			}
		}
		else{
			if (cur.currentVisDragMag != null && acur == null){
				// leaving  visdm
				Color c = DragMag.DM_VIS_COLORS[1];
				if (scur != null) { c = scur.color; }
				else if (acur != null) { c = acur.color; }
				else if (pcur != null) { c = pcur.color; }
				cur.currentVisDragMag.setVisLinesLightColor(c);
				cur.currentVisDragMag = null;
			}
		}
	}
}

private void _moveAttachedCursors(ZcsCursor activeCur, DragMag movedDM, DragMag movedVisDM){
	for (Map.Entry<Object, ZcsDevice> de : _devices.entrySet()) {
    	ZcsDevice dev = de.getValue();
    	Object obj = de.getKey();
    	if (!(obj instanceof BasicDevice)) { continue; }
    	BasicDevice bDevice = (BasicDevice)obj;
    	for (Map.Entry<Integer, ZcsCursor> ce : dev.cursors.entrySet()){
    		ZcsCursor cur = ce.getValue();
    		int id = (Integer)ce.getKey();
    		if (activeCur != null && activeCur == cur) { continue; }
    		if (movedDM != null && cur.attachedDragMag == movedDM){
    			// System.out.println(" _moveAttachedCursors move cursor of attached dm");
    			bDevice.moveCursor(
    				id, (float)(movedDM.x + movedDM.w/2)/(float)getWidth(),
    				(float)(movedDM.y + movedDM.h/2)/(float)getHeight());
    			cur.moveTo(
    				(float)(movedDM.x + movedDM.w/2)/(float)getWidth(),
    				(float)(movedDM.y + movedDM.h/2)/(float)getHeight());
    		}
    		if (movedVisDM != null && cur.attachedVisDragMag == movedVisDM){
    			bDevice.moveCursor(
    				id, movedVisDM.getVisX()/getWidth(),
    				movedVisDM.getVisY()/getHeight());
    			cur.moveTo(movedVisDM.getVisX()/getWidth(),
    				movedVisDM.getVisY()/getHeight());
    		}
    	}
    }
}


private void _moveAllVisAttachedCursor(){
		Set<DragMag> dms = _dmms.getDragMags();
		for(DragMag dm : dms){
			 _moveAttachedCursors(null, null, dm);
		}
	}


private void _moveNoHiddenAttachedCursor(
	ZcsCursor cur, DragMag dm, double x, double y, double dx, double dy)
{
	//for now
	if (cur.vId > 0) return;

	if (dm == null) return;
	if (dm.coordInside((int)(x*_width+0.5), (int)(y*_height+0.5))){
		cur.move(x, y);
	}
	else{
		dm.move((dx+dx*0.01)*_width, (dy+dy*0.01)*_height);
		cur.move(x, y);
	}
}

// --------------------------
//
public void handleEvent(zcsEvent e)
{
	ZcsDevice dev = _devices.get(e.obj);
	if (dev == null)
	{
		// WARN!
		System.out.println("handleEvent Device not found!! " + e.obj);
		return;
	}
	
	//System.out.println(" handleEvent " + e.type);
	switch (e.type)
	{
	case zcsEvent.ZCS_CREATE_CURSOR:
	{
		System.out.println("ZCS_CREATE_CURSOR "+dev+" "+ e.cid);
		dev.createCursor(e.cid,e.x,e.y,e.color,e.hide,e.vId);
		ZcsCursor cur = dev.cursors.get(e.cid);
		cur.is_touch = e.is_touch;
		cur.is_token = e.is_token;
		cur.setRay(e.ray);
		if (cur != null){
			_checkDragMag(cur, e);
		}
		break;	
	}
	case zcsEvent.ZCS_DELETE_CURSOR:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("DELETE_CURSOR handleEvent cursor not found!!");
			break;
		}
		if (cur.pm != null){
			cur.triggerPieMenuCommand();
		}
		cur.hide();
		// FIXME: selected color
		if (cur.currentDragMag != null){
			ZcsCursor scur = _getCursorSelectedByDragMag(cur.currentDragMag, false);
			ZcsCursor pcur = _getCursorAttachedToDragMag(cur.currentDragMag, false);
			Color c = DragMagsManager.DDM_BORDER_COLOR;
			if (scur != null) { c = scur.color; }
			else if (pcur != null) { c = pcur.color; }
			cur.currentDragMag.setBorder(c);
			cur.currentDragMag = null;
		}
		if (cur.currentVisDragMag != null){
			ZcsCursor scur = _getCursorSelectedByDragMag(cur.currentVisDragMag, true);
			ZcsCursor pcur =  _getCursorAttachedToDragMag(cur.currentVisDragMag, true);
			Color c = DragMag.DM_VIS_COLORS[1];
			if (scur != null) { c = scur.color; }
			else if (pcur != null) { c = pcur.color; }
			cur.currentVisDragMag.setVisLinesLightColor(c);
			cur.currentVisDragMag = null;
		}
		if (cur.linkedDragMag != null){
			cur.linkedDragMag = null;
		}
		// FIXME: attached & selected !
		if (e.obj instanceof BasicDevice) {
			// not very useful...
    		BasicDevice bDevice = (BasicDevice)e.obj;
    		bDevice.setAttached(cur.id, false);
    		bDevice.setLinked(cur.id, false);
    	}
		dev.deleteCursor(e.cid);
		break;
	}
	case zcsEvent.ZCS_HIDE_CURSOR: // UP keeping identity
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("HIDE_CURSOR handleEvent cursor not found!!");
			break;
		}
		if (cur.pm != null){
			cur.triggerPieMenuCommand();
		}
		cur.hide();
		if (cur.currentDragMag != null && cur.selectedDragMag == null){
			ZcsCursor pcur =  _getCursorAttachedToDragMag(cur.currentDragMag, false);
			Color c = DragMagsManager.DDM_BORDER_COLOR;
			if (pcur != null) { c = pcur.color; }
			cur.currentDragMag.setBorder(c);
			cur.currentDragMag = null;
		}
		if (cur.currentVisDragMag != null && cur.selectedVisDragMag == null) { 
			ZcsCursor pcur = null;
			pcur = _getCursorAttachedToDragMag(cur.currentVisDragMag, true);
			Color c = DragMag.DM_VIS_COLORS[1];
			if (pcur != null) { c = pcur.color; }
			cur.currentVisDragMag.setVisLinesLightColor(c);
		}
		cur.currentDragMag = null;
		cur.currentVisDragMag = null;
		cur.lockedDm = null;
		cur.doLockBg = false;
		break;
	}
	case zcsEvent.ZCS_SHOW_CURSOR:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("SHOW_CURSOR handleEvent cursor not found!!");
			break;
		}
		cur.show();
		break;
	}
	case zcsEvent.ZCS_LOCK:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("LOCK handleEvent cursor not found!!");
			break;
		}
		DragMag dm= null ;
		if (cur.selectedDragMag != null) dm = cur.selectedDragMag;
		else if (cur.selectedDragMag != null) dm = cur.selectedDragMag;
		else if (cur.currentDragMag != null) dm = cur.currentDragMag;
		if (dm != null) {
			// select ?
			cur.lockedDm = dm;
			break;
		}
		if (cur.selectedVisDragMag != null) dm = cur.selectedVisDragMag;
		else if (cur.selectedVisDragMag != null) dm = cur.selectedVisDragMag;
		else if (cur.currentVisDragMag != null) dm = cur.currentVisDragMag;
		if (dm != null) {
			// select ?
			cur.lockedDm = dm;
			break;
		}
		cur.doLockBg = true;
		break;
	}
	case zcsEvent.ZCS_START_MOVE_CURSOR:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) break;
		_checkDragMag(cur, e);
		if (cur.pm != null) break;
		if (cur.attachedDragMag != null){
		}
		else if (cur.attachedVisDragMag != null){
			_dmms.enableRegionUpdater(cur.attachedVisDragMag, false);
		}
		else if (cur.selectedVisDragMag != null){
			_dmms.enableRegionUpdater(cur.selectedVisDragMag, false);
		}
		if(cur.selectedDragMag == null && cur.attachedDragMag != null){
			//System.out.println("Stack Portal Front at START_MOVE_CURSOR");
			stackPortalFront(cur.attachedDragMag);
		}
		break;
	}
	case zcsEvent.ZCS_MOVE_CURSOR:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		//System.out.println("MOVE_CURSOR " + e.x +" "+ e.y);
		if (cur == null) {
			// WARN
			System.out.println("MOVE_CURSOR handleEvent cursor not found!!");
			break;
		}
		cur.move(e.x, e.y);
		if (isDmLocked(cur.attachedDragMag) ||  isDmLocked(cur.attachedVisDragMag) || cur.pm != null){
			break;
		}
		if (cur.attachedDragMag != null && cur.isVisible()) {
			_moveNoHiddenAttachedCursor(cur, cur.attachedDragMag, e.x, e.y, e.dx, e.dy);
			break;
		}
		if(e.button != 0){
			// FIXME really ?
			break;
		}
		if (cur.attachedDragMag != null){
			cur.attachedDragMag.move(e.dx*_width, e.dy*_height);
			break;
		}
		else if (cur.attachedVisDragMag != null){
			// cur.attachedVisDragMag.moveVis(e.dx*_width, -e.dy*_height);
			cur.attachedVisDragMag.moveToVis(e.x*_width, e.y*_height);
			if (cur.linkedDragMag != null){
				cur.linkedDragMag.move(e.dx*_width, e.dy*_height);
			}
			break;
		}
		else {
			_checkDragMag(cur, e);
		}
		break;	
	}
	case zcsEvent.ZCS_END_MOVE_CURSOR:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			System.out.println("END_MOVE_CURSOR handleEvent cursor not found!!");
			break;
		}
		if (cur.pm != null){
		}
		else if (cur.attachedDragMag != null){
		}
		else  if (cur.attachedVisDragMag != null){
			_dmms.enableRegionUpdater(cur.attachedVisDragMag, true);
		}
		else {
			_checkDragMag(cur, e);
		}
		break;
	}
	case zcsEvent.ZCS_CURSOR_CLICK:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("CURSOR_CLICK handleEvent cursor not found!!");
			break;
		}
		System.out.println("ZCS_CURSOR_CLICK " + e.button + " @ "+cur.x +","+ cur.y);
		if (cur.pm == null && cur.currentDragMag != null && !isDmLocked(cur.currentDragMag)){
			stackPortalFront(cur.currentDragMag);
		}
		if (cur.pm != null){
			cur.triggerPieMenuCommand();
			System.out.println("ZCS_CURSOR_CLICK trigger PM");
		}
		else if (e.button == 1)
		{
			// convert to scene space
			if (cur.currentDragMag != null){
				DragMag dm = cur.currentDragMag;
				double[] r = _windowToViewCoordinate(cur.vId,
					dm.getCamera(), dm.w, dm.h, cur.x, cur.y, dm.x, dm.y);
				System.out.println("ZCS_CURSOR_CLICK in DM SPACE" + r[0] +" "+ r[1]);
			}
			else if (cur.currentOP != null){
				OverviewPortal op = cur.currentOP;
				int i = cur.vId-1;
				double[] r = _windowToViewCoordinate(cur.vId,
					op.getCamera(), op.w, op.h, cur.x, cur.y, op.x, op.y);
				System.out.println("ZCS_CURSOR_CLICK in OP " + r[0] +" "+ r[1]);
				Camera c = _cCameras[i];
				Location l = c.getLocation();
    			c.setLocation(new Location(r[0], r[1], l.getAltitude()));
			}
			else {
				double[] r = _windowToViewCoordinate(cur.vId, cur.x, cur.y);
				System.out.println("ZCS_CURSOR_CLICK " + r[0] +" "+ r[1]);
				_renderer.click(r[0], r[1]);
			}
		}
		break;	
	}
	// FIXME LONG PRESS ...
	case zcsEvent.ZCS_START_DRAG:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) break;

		_checkDragMag(cur,e);
		cur.grabed = true;
		DragMag dm = null;
		cur.dragIsMove = false;
		cur.dragIsResize = false;
		cur.zoomForbiddenDuringDrag = false;
		if (cur.currentDragMag == null && cur.currentVisDragMag != null && e.button == 1){
			_dmms.enableRegionUpdater(cur.currentVisDragMag, false);
		}
		if (cur.selectedDragMag == null && cur.selectedVisDragMag != null && e.button == 1){
			_dmms.enableRegionUpdater(cur.selectedVisDragMag, false);
		}
		if (cur.selectedDragMag != null) {
			dm = cur.selectedDragMag;
		}
		else if (cur.currentDragMag != null){
			dm = cur.currentDragMag;
		}
		if (dm != null && !isDmLocked(cur.currentDragMag)){
			cur.deltax = (e.x - dm.x/_width);
			cur.deltay = (e.y - dm.y/_height);
			//System.out.println("Stack Portal Front");
			stackPortalFront(dm);
			
			double bptW = _borderPickTolerence; 
			double bptH = _borderPickTolerence; 
			if (bptW > (double)dm.w/4) { bptW = (double)dm.w/4; }
			if (bptH > (double)dm.h/4) { bptH = (double)dm.h/4; }

			double dxr = Math.abs(cur.x*_width - (dm.x + dm.w));
			double dxl = Math.abs(cur.x*_width - (dm.x));
			double dyb = Math.abs(cur.y*_height - (dm.y + dm.h));
			double dyt = Math.abs(cur.y*_height - (dm.y));
			// bottom right corner
			if (dxr < bptW && dyb < bptH){
				cur.dragIsResize = true;
				cur.zoomForbiddenDuringDrag = true;
			}
			else if (dxl < bptW && cur.y*_height >  dm.y - bptH && cur.y*_height <  dm.y+dm.h + bptH){
				// left border
				cur.dragIsMove = true;
				cur.zoomForbiddenDuringDrag = true;
			}
			else if (dxr < bptW && cur.y*_height >  dm.y - bptH && cur.y*_height <  dm.y+dm.h + bptH){
				// right border
				cur.dragIsMove = true;
				cur.zoomForbiddenDuringDrag = true;
			}
			else if (dyt < bptH && cur.x*_width >  dm.x - bptW && cur.x*_width <  dm.x+dm.w + bptW){
				// top border
				cur.dragIsMove = true;
				cur.zoomForbiddenDuringDrag = true;
			}
			else if (dyb < bptH && cur.x*_width >  dm.x -  bptW	&& cur.x*_width <  dm.x+dm.w + bptW){
				// bottom border
				cur.dragIsMove = true;
				cur.zoomForbiddenDuringDrag = true;
			}
			if (e.button == 1 && !cur.dragIsResize){
				cur.dragIsMove = true;
				cur.zoomForbiddenDuringDrag = true;
			}
			else if (e.button == 2){
				// ok
				cur.dragIsMove = false;
				cur.dragIsResize = false;
			}
			else if (e.button >= 3){
				//
				cur.dragIsResize = true;
				cur.zoomForbiddenDuringDrag = true;
			}
		}
		break;
	}
	case zcsEvent.ZCS_DRAG:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		int vid = 0;
		if (cur == null) {
			// WARN
			System.out.println("PAN handleEvent cursor not found!!");
			// break; // maybe ok here ...
		}
		else{
			vid = cur.vId;
		}

		if (cur != null && cur.selectedDragMag != null){
			if (!isDmLocked(cur.selectedDragMag)){
				if (cur.dragIsMove){
					cur.selectedDragMag.moveTo((e.x-cur.deltax)*_width, (e.y-cur.deltay)*_height);
					_moveAttachedCursors(cur, cur.selectedDragMag, null);
				}
				else if (cur.dragIsResize){
					cur.selectedDragMag.resize((int)(e.dx*_width), (int)(e.dy*_height));
					_dmms.dragMagResized(cur.selectedDragMag);
					_moveAttachedCursors(cur, null, cur.selectedDragMag);
				}
				else{
					cur.selectedDragMag.moveCam(e.dx*_width, -e.dy*_height);
				}
			}
		}
		else if (cur != null && cur.selectedVisDragMag != null){
			cur.zoomForbiddenDuringDrag = true;
			if (!isDmLocked(cur.selectedVisDragMag)){
				cur.selectedVisDragMag.moveVis(e.dx*_width, -e.dy*_height);
				_moveAttachedCursors(cur, null, cur.selectedVisDragMag);
			}
		}
		else if (cur != null && cur.currentDragMag != null){
			//cur.currentDragMag.moveTo(e.x*_width, e.y*_height);
			if (isDmLocked(cur.currentDragMag)){
				break;
			}
			if (cur.dragIsMove){
				cur.currentDragMag.moveTo((e.x-cur.deltax)*_width, (e.y-cur.deltay)*_height);
				_moveAttachedCursors(cur, cur.currentDragMag, null);
			}
			else if (cur.dragIsResize){
				cur.currentDragMag.resize((int)(e.dx*_width), (int)(e.dy*_height));
				_dmms.dragMagResized(cur.currentDragMag);
				_moveAttachedCursors(cur, null, cur.currentDragMag);
			}
			else{
				cur.currentDragMag.moveCam(e.dx*_width, -e.dy*_height);
			}
			//cur.currentDragMag.move((int)e.ix, (int)e.iy);
		}
		else if (cur != null && cur.currentVisDragMag != null)
		{
			if (!isDmLocked(cur.currentVisDragMag))
			{
				cur.zoomForbiddenDuringDrag = true;
				cur.currentVisDragMag.moveVis(e.dx*_width, -e.dy*_height);
				_moveAttachedCursors(cur, null, cur.currentVisDragMag);
			}
		}
		else if (cur != null && cur.currentOP != null){
			//if (e.button == 1){
				_directTranslate(vid, cur.currentOP.getCamera(), e.dx, -e.dy);
			//}
		}
		else
		{
			if (isBgLocked()){
				break;
			}
			if (e.button <= 1) {
				_directTranslate(vid, -e.dx, e.dy);
				if (vid == 0) // for now
					_moveAllVisAttachedCursor();
			}
			else if (e.button >= 2) {
				_directTranslate(vid,-(e.button+e.contacts)*e.dx, (e.button+e.contacts)*e.dy);
				if (vid == 0) // for now
					_moveAllVisAttachedCursor();
			}
		}
		break;	
	}
	case zcsEvent.ZCS_END_DRAG:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) break;
		cur.grabed=false;
		if (cur.currentDragMag == null && cur.currentVisDragMag != null){
			_dmms.enableRegionUpdater(cur.currentVisDragMag, true);
		}
		if (cur.selectedDragMag == null && cur.selectedVisDragMag != null && e.button == 1){
			_dmms.enableRegionUpdater(cur.selectedVisDragMag, true);
		}
		_checkDragMag(cur, e);
		cur.zoomForbiddenDuringDrag = false;
		break;
	}
	case zcsEvent.ZCS_START_ZOOM:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) break;
		if (cur.currentDragMag == null && cur.currentVisDragMag == null){
			// why this test ?
			_checkDragMag(cur, e);
		}
		if (cur.currentDragMag == null && cur.currentVisDragMag != null){
			_dmms.enableRegionUpdater(cur.currentVisDragMag, false);
		}
		if (cur.selectedDragMag == null && cur.selectedVisDragMag != null){
			_dmms.enableRegionUpdater(cur.selectedVisDragMag, false);
		}
		cur.zoomIsResize = cur.zoomIsWidthResize = cur.zoomIsHeightResize = false;
		if (cur.selectedDragMag != null && !isDmLocked(cur.selectedDragMag)){
			stackPortalFront(cur.selectedDragMag);
			// check for resizing
			// if cur intersect with the border of the dragmag

			DragMag dm = cur.selectedDragMag;
			double bptW = _borderPickTolerence; 
			double bptH = _borderPickTolerence; 
			if (bptW > (double)dm.w/4) { bptW = (double)dm.w/4; }
			if (bptH > (double)dm.h/4) { bptH = (double)dm.h/4; }

			double dxr = Math.abs(cur.x*_width - (dm.x + dm.w));
			double dxl = Math.abs(cur.x*_width - (dm.x));
			double dyb = Math.abs(cur.y*_height - (dm.y + dm.h));
			double dyt = Math.abs(cur.y*_height - (dm.y));
			if ((dxr < bptW && dyb < bptH) || (dxr < bptW && dyt < bptH) ||
				(dxl < bptW && dyb < bptH) || (dxl < bptW && dyt < bptH))
			{
				// corners
				cur.zoomIsResize = true;
			}
			else if ((dxl < bptW && cur.y*_height >  dm.y - bptH && cur.y*_height <  dm.y+dm.h + bptH) ||
					 (dxr < bptW && cur.y*_height >  dm.y - bptH && cur.y*_height <  dm.y+dm.h + bptH)){
				// left right borders
				cur.zoomIsWidthResize = true;
			}
			else if ((dyt < bptH && cur.x*_width >  dm.x - bptW && cur.x*_width <  dm.x+dm.w + bptW) ||
					 (dyb < bptH && cur.x*_width >  dm.x - bptW && cur.x*_width <  dm.x+dm.w + bptW))
			{
				// top bottom borders
				cur.zoomIsHeightResize = true;
			}
		}
		if(cur.attachedDragMag != null && !isDmLocked(cur.attachedDragMag)){
			stackPortalFront(cur.attachedDragMag);
		}
		else if(cur.currentDragMag != null && !isDmLocked(cur.currentDragMag)){
			stackPortalFront(cur.currentDragMag);
		}
		break;
	}
	
	case zcsEvent.ZCS_ZOOM:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		int vid = 0;
		if (cur == null) {
			// WARN
			System.out.println("ZCS_ZOOM handleEvent cursor not found!!");
			// break; //maybe ok here ...
		}
		else{
			vid = cur.vId;
		}
		if (cur.zoomForbiddenDuringDrag){
			break;
		}
		//System.out.println("ZCS_ZOOM " +e.x +","+ e.y +","+e.f );
		if (cur != null && cur.selectedDragMag != null)
		{
			if (isDmLocked(cur.selectedDragMag)) { break; }
			if (cur.zoomIsResize || cur.zoomIsWidthResize || cur.zoomIsHeightResize){
				//System.out.println("Resize dragmag " + e.f+" "+cur.selectedDragMag.w+" " +cur.selectedDragMag.h);

				double w = (cur.zoomIsResize || cur.zoomIsWidthResize)? -e.f*cur.selectedDragMag.w + cur.selectedDragMag.w : 0;
				double h = (cur.zoomIsResize || cur.zoomIsHeightResize)? -e.f*cur.selectedDragMag.h + cur.selectedDragMag.h : 0;
				double ww = w+cur.selectedDragMag.w;
				double hh = h+cur.selectedDragMag.h;
				if (ww > _width/4) { w = 0; }
				if (hh > _height/2.5) { h = 0; }
				//System.out.println("Resize dragmag " + w+" "+h);
				cur.selectedDragMag.resize((int)w, (int)h);
				//cur.selectedDragMag.w = (int)w; //ize((int)w, (int)h);
				//cur.selectedDragMag.h = (int)h;
				_dmms.dragMagResized(cur.selectedDragMag);
				_moveAttachedCursors(cur, null, cur.selectedDragMag);
			}
			else{
				cur.selectedDragMag.zoom(e.f);
			}
		}
		else if (cur != null && cur.currentDragMag != null){
			if (isDmLocked(cur.currentDragMag)) { break; }
			if (cur.currentDragMag.isManatthan()){
                // zoom at the center of the dragmag
                double x = cur.currentDragMag.x + cur.currentDragMag.w/2;
                double y = cur.currentDragMag.y + cur.currentDragMag.h/2;
                _centredZoom(e.f, x, y);
            }
            else if (cur.attachedDragMag == null && (cur.isVisible() || e.is_touch)){
            	// FIXMEax center zoom vs the cursor pos in the dragmag view
            	cur.currentDragMag.centredZoom(e.f, cur.x*_width, cur.y*_height);
				//_moveAttachedCursors(cur, null, cur.currentDragMag);
            }
            else {
            	cur.currentDragMag.zoom(e.f);
            }
		}
		else if (cur.selectedDragMag != null){

		}
		else if (!isBgLocked()) {
			_centredZoom(vid, e.f, e.x, e.y);
			if (vid == 0) // for now
				_moveAllVisAttachedCursor();
		}
		break;	
	}

	case zcsEvent.ZCS_END_ZOOM:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) break;
		if (cur.currentDragMag == null && cur.currentVisDragMag != null){
			_dmms.enableRegionUpdater(cur.currentVisDragMag, true);
		}
		if (cur.selectedDragMag == null && cur.selectedVisDragMag != null){
			_dmms.enableRegionUpdater(cur.selectedVisDragMag, true);
		}
		_checkDragMag(cur, e);
		break;
	}

	case zcsEvent.ZCS_PICK_OR_DROP:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("ZCS_PICK_OR_DROP handleEvent cursor not found!!");
			break;
		}
		DragMag dm = _dmms.checkDragMag((int)(e.x*_width+0.5), (int)(e.y*_height+0.5));
		DragMag visdm = _dmms.checkVis((int)(e.x*_width), (int)(e.y*_height));
		boolean dopick = true;
		if (cur.selectedDragMag != null){
			if (dm == null){
				// drop !!
				if (!isDmLocked(cur.selectedDragMag)){
					cur.selectedDragMag.moveTo(e.x*_width - cur.selectedDragMag.w/2, e.y*_height - cur.selectedDragMag.h/2);
					_moveAttachedCursors(cur, cur.selectedDragMag, null);
				}
				dopick = false;
			}
			else {
				// unselect
				ZcsCursor acur = null;
				if (dm == cur.selectedDragMag){
					dopick = false;
				}
				DragMag pdm = cur.selectedDragMag;
				cur.selectedDragMag =  null;
				if (e.obj instanceof BasicDevice) {
					// should add setSelected
    				//BasicDevice bDevice = (BasicDevice)e.obj;
    				//bDevice.setAttached(cur.id, false);
    			}
				acur = _getCursorAttachedToDragMag(pdm, false);
				Color c = DragMagsManager.DDM_BORDER_COLOR;
				if (acur != null) { c = acur.color; }
				pdm.setBorder(c);
			}
		}
		else if (cur.selectedVisDragMag != null){
			if (visdm == null){
				// drop !!
				if (!isDmLocked(cur.selectedVisDragMag)){
					cur.selectedVisDragMag.moveToVis(e.x*_width, e.y*_height);
					_moveAttachedCursors(cur, null, cur.selectedVisDragMag);
				}
				dopick = false;
			}
			else {
				// unselect
				ZcsCursor acur = null;
				if (visdm == cur.selectedVisDragMag){
					dopick = false;
				}
				DragMag pdm = cur.selectedVisDragMag;
				cur.selectedVisDragMag =  null;
				if (e.obj instanceof BasicDevice) {
					// should add setSelected
    				//BasicDevice bDevice = (BasicDevice)e.obj;
    				//bDevice.setAttached(cur.id, false);
    			}
				acur = _getCursorAttachedToDragMag(pdm, true);
				Color c = DragMag.DM_VIS_COLORS[1];
				if (acur != null) { c = acur.color; }
				pdm.setVisLinesLightColor(c);
			}
		}
		if (dopick) {
			// pick
			if (dm != null) { 
				if (!isDmLocked(dm)) {
					cur.selectedDragMag = dm;
					if (e.obj instanceof BasicDevice) {
						// should add setSelected
    					//BasicDevice bDevice = (BasicDevice)e.obj;
    					//bDevice.setAttached(cur.id, true);
    				}
					cur.selectedDragMag.setBorder(cur.color);
				}
			}
			else if (visdm != null){
				if (!isDmLocked(dm)) {
					cur.selectedVisDragMag = visdm;
					if (e.obj instanceof BasicDevice) {
						// should add setSelected
    					//BasicDevice bDevice = (BasicDevice)e.obj;
    					//bDevice.setAttached(cur.id, true);
    				}
					cur.selectedVisDragMag.setVisLinesLightColor(cur.color);
				}
			}
		}
		break;
	}

	case zcsEvent.ZCS_ATTACH:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("ZCS_ATTACH handleEvent cursor not found!!");
			break;
		}
		if (cur.is_token){
			break;
		}
		if (e.onoff){
			DragMag dm = e.dm;
			boolean visor = e.visor;
			if (dm == null){
				if (cur.currentDragMag != null){
					dm = cur.currentDragMag;
					visor = false;
				}
				else if (cur.currentVisDragMag != null){
					dm = cur.currentVisDragMag;
					visor = true;
				}
			}
			if (dm != null && !visor){
				ZcsCursor tmpc = _getCursorAttachedToDragMag(dm, false);
				if (tmpc != null){
					System.out.println("ZCS_ATTACH DragMag already attcahed");
					break;
				}
				System.out.println("ZCS_ATTACH attach DragMag");
				if (!isDmLocked(dm)){
					cur.currentDragMag = cur.attachedDragMag = dm;
					if (e.source != null) e.source.setAttached(e.cid, true);
					cur.attachedDragMag.setBorder(cur.color);
					if (dm.getType() != DragMag.DM_TYPE_INDEPENDANT){
						cur.hide();
					}
				}
			}
			else if (dm != null && visor){
				ZcsCursor tmpc = _getCursorAttachedToDragMag(dm, true);
				if (tmpc != null){
					System.out.println("ZCS_ATTACH VisDragMag already attcahed");
					break;
				}
				System.out.println("ZCS_ATTACH attach VisDragMag " + cur.currentDragMag);
				if (!isDmLocked(dm)){
					cur.currentVisDragMag = cur.attachedVisDragMag = dm;
					if (e.source != null) e.source.setAttached(e.cid, true);
					cur.attachedVisDragMag.setVisLinesLightColor(cur.color);
					cur.hide();
				}
			}
			else{
				System.out.println("ZCS_ATTACH attach to source...");
				if (e.source != null) e.source.setAttached(e.cid, true);
				cur.hide();
			}
		}
		else {
			System.out.println("ZCS_ATTACH detach");
			if (cur.attachedDragMag != null && !isDmLocked(cur.attachedDragMag)){
				cur.attachedDragMag.setBorder(DragMagsManager.DDM_BORDER_COLOR);
				cur.attachedDragMag = null;
				if (e.source != null) e.source.setAttached(e.cid, false);
				cur.show();
			}
			if (cur.attachedVisDragMag != null && !isDmLocked(cur.attachedDragMag)){
				cur.attachedVisDragMag.setVisLinesLightColor(DragMag.DM_VIS_COLORS[1]);
				cur.attachedVisDragMag = null;
				if (e.source != null) e.source.setAttached(e.cid, false);
				cur.show();
			}
			
		}
		break;
	}
	case zcsEvent.ZCS_LINK:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("ZCS_LINK handleEvent cursor not found!!");
			break;
		}
		if (e.onoff){
			if (cur.attachedVisDragMag != null && !isDmLocked(cur.attachedVisDragMag)){
				cur.linkedDragMag = cur.attachedVisDragMag;
			}
		}
		else{
			if (!isDmLocked(cur.linkedDragMag)) {
				cur.linkedDragMag = null;
			}
		}
		break;
	}
	case zcsEvent.ZCS_CREATE_PIEMENU:
	{
		ZcsCursor cur = dev.cursors.get(e.cid);
		if (cur == null) {
			// WARN
			System.out.println("ZCS_CREATE_PIEMENU handleEvent cursor not found!!");
			break;
		}
		if (isBgLocked()){
			break;
		}
		DragMag dm = null;
		if (cur.selectedDragMag != null){
			dm = cur.selectedDragMag;
		}
		else if (cur.currentDragMag != null){
			dm = cur.currentDragMag;
		}

		System.out.println("CREATE_PIEMENU " +e.x*_width +" "+ e.y*_height + " dm "+dm);
		if (cur.pm != null){
			cur.pm.destroy(0);
        	cur.pm = null;
		}
		cur.pmdm = dm;
		cur.pmx = e.x;
		cur.pmy = e.y;
		if (dm != null){
			cur.pm = _pieMenuBuiler.createDMPieMenu(new Point2D.Double(e.x*_width - _width/2, -e.y*_height + _height/2), (float)(1.05*cur.ray*(1f/_pixInMM))); // add 5%
		}
		else {
			cur.pm = _pieMenuBuiler.createBgPieMenu(new Point2D.Double(e.x*_width - _width/2, -e.y*_height + _height/2), (float)(1.05*cur.ray*(1f/_pixInMM))); // add 5%
		}
		break;
	}
	default:
	{
		break;
	}
	} // end switch
}

// --------------------------------------------------------------------------------
// 
private double[] _windowToViewCoordinate(int vid, double x, double y)
{
	Camera cam = _camera;
	double width = _width;
	double height = _height;
	if (vid > 0 && _cCameras != null && _cCameras.length > vid-1){
		cam = _cCameras[vid-1];
		width = _cGeometries[vid-1].getWidth();
		height = _cGeometries[vid-1].getBlockHeight(); // FUCK
	}
	Location cgl = cam.getLocation();
	double a = (cam.focal + cam.getAltitude()) / cam.focal;
	// 
	double xx = (long)((double)x*width - ((double)width/2.0));
	double yy = (long)(-(double)y*height + ((double)height/2.0));
	//
	xx = cgl.getX()+ a*xx;
	yy = cgl.getY()+ a*yy;
	
	double[] r = new double[2];
	r[0] = xx;
	r[1] = yy;

	return r;
}

private double[] _windowToViewCoordinate(int vid, Camera cam, double w, double h, double x, double y, double dmx, double dmy)
{
	double width = _width;
	double height = _height;
	if (vid > 0 && _cCameras!=null && _cCameras.length > vid-1){
		width = _cGeometries[vid-1].getWidth();
		height = _cGeometries[vid-1].getHeight();
	}
	Location cgl = cam.getLocation();
	double a = (cam.focal + cam.getAltitude()) / cam.focal;
	// 
	double xx = (long)((double)x*width - dmx - ((double)w/2.0));
	double yy = (long)(-(double)y*height + dmy + ((double)h/2.0));
	//
	xx = cgl.getX()+ a*xx;
	yy = cgl.getY()+ a*yy;
	
	double[] r = new double[2];
	r[0] = xx;
	r[1] = yy;

	return r;
}

private double[] _windowToViewCoordinate(double x, double y)
{
	Location cgl = _camera.getLocation();
	double a = (_camera.focal + _camera.getAltitude()) / _camera.focal;
	// 
	double xx = (long)((double)x - ((double)_width/2.0));
	double yy = (long)(-(double)y + ((double)_height/2.0));
	//
	xx = cgl.getX()+ a*xx;
	yy = cgl.getY()+ a*yy;
	
	double[] r = new double[2];
	r[0] = xx;
	r[1] = yy;

	return r;
}

private double[] _windowToViewCoordinate(Camera cam, double w, double h, double x, double y)
{
	Location cgl = cam.getLocation();
	double a = (cam.focal + cam.getAltitude()) / cam.focal;
	// 
	double xx = (long)((double)x - ((double)w/2.0));
	double yy = (long)(-(double)y + ((double)h/2.0));
	//
	xx = cgl.getX()+ a*xx;
	yy = cgl.getY()+ a*yy;
	
	double[] r = new double[2];
	r[0] = xx;
	r[1] = yy;

	return r;
}

public void refresh(){
	_directTranslate(1, 1);
	_directTranslate(-1, -1);
}

/* x,y in (X Window) screen coordinate */
private void _directTranslate(int vid, Camera mc, double x, double y)
{
	Camera cam = _camera;
	double width = _width;
	double height = _height;
	if (vid > 0 && _cCameras!=null && _cCameras.length > vid-1){
		cam = _cCameras[vid-1];
		width = _cGeometries[vid-1].getWidth();
		height = _cGeometries[vid-1].getHeight(); // FUCK
	}
	
    double a = (cam.focal+Math.abs(cam.altitude)) / cam.focal;
    double b = (mc.focal+Math.abs(mc.altitude)) / mc.focal;
    double f = b/a;

    Location l = cam.getLocation();
    x = x*width; y = y*height;
    double newx = l.getX()+f*a*x;
    double newy = l.getY()+f*a*y;
    
    cam.setLocation(new Location(newx, newy, l.getAltitude()));
}

/* x,y in (X Window) screen coordinate */
private void _directTranslate(int vid, double x, double y)
{
	Camera cam = _camera;
	double width = _width;
	double height = _height;
	if (vid > 0 && _cCameras!=null && _cCameras.length > vid-1){
		cam = _cCameras[vid-1];
		width = _cGeometries[vid-1].getWidth();
		height = _cGeometries[vid-1].getHeight(); // FUCK
	}
	
    double a = (cam.focal+Math.abs(cam.altitude)) / cam.focal;

    Location l = cam.getLocation();
    x = x*width; y = y*height;
    double newx = l.getX()+a*x;
    double newy = l.getY()+a*y;
    
    cam.setLocation(new Location(newx, newy, l.getAltitude()));
}

/* x,y in (X Window) screen coordinate */
private void _directTranslate(double x, double y)
{
    double a = (_camera.focal+Math.abs(_camera.altitude)) / _camera.focal;

    Location l = _camera.getLocation();
    double newx = l.getX()+a*x;
    double newy = l.getY()+a*y;
    
    _camera.setLocation(new Location(newx, newy, l.getAltitude()));
}

/* x,y in (X Window) screen coordinate */
private void _centredZoom(int vid, double f, double x, double y)
{
	Camera cam = _camera;
	double width = _width;
	double height = _height;
	if (vid > 0 && _cCameras != null && _cCameras.length > vid-1){
		cam = _cCameras[vid-1];
		width = _cGeometries[vid-1].getWidth();
		height = _cGeometries[vid-1].getHeight(); // FUCK
	}

    Location cgl = cam.getLocation();
    double a = (cam.focal + cam.altitude) / cam.focal;

    //System.out.println("centredZoom: " + a + " " + _camera.altitude + " " + f);

    double newz = cam.focal * a * f - cam.focal;
    //System.out.println("newz: " + newz);

    if (newz <= 0.0)
    {	
	    newz = 0;
	    f = cam.focal/ (a * cam.focal);
	    //System.out.println("newz is <= 0, fixing f: " + f);
    }

    // could be optimized...
    double[] r = _windowToViewCoordinate(vid, x, y);

    double dx = cgl.getX() - r[0];
    double dy = cgl.getY() - r[1];
    double newx, newy;
    newx = cgl.getX() + (f*dx - dx); // *a/(_camera.altitude+ _camera.focal));
    newy = cgl.getY() + (f*dy - dy);
    cam.setLocation(new Location(newx, newy, newz));
    cam.setLocation(new Location(newx, newy, newz));
    // this double call fix a bug that I guess come from Camera.stick !!
}

/* x,y in (X Window) screen coordinate */
private void _centredZoom(double f, double x, double y)
{
    Location cgl = _camera.getLocation();
    double a = (_camera.focal + _camera.altitude) / _camera.focal;

    //System.out.println("centredZoom: " + a + " " + _camera.altitude + " " + f);

    double newz = _camera.focal * a * f - _camera.focal;
    //System.out.println("newz: " + newz);

    if (newz <= 0.0)
    {	
	    newz = 0;
	    f = _camera.focal/ (a * _camera.focal);
	    //System.out.println("newz is <= 0, fixing f: " + f);
    }

    double[] r = _windowToViewCoordinate(x, y);

    double dx = cgl.getX() - r[0];
    double dy = cgl.getY() - r[1];
    double newx, newy;
    newx = cgl.getX() + (f*dx - dx); // *a/(_camera.altitude+ _camera.focal));
    newy = cgl.getY() + (f*dy - dy);
    _camera.setLocation(new Location(newx, newy, newz));
    _camera.setLocation(new Location(newx, newy, newz));
    // this double call fix a bug that I gues come from Camera.stick !!
}

}
