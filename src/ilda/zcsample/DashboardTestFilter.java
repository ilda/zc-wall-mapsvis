package ilda.zcsample;

import ilda.zcsample.filters.*;

import java.util.Vector;
import java.util.Random;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import java.awt.Color;
import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;

import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.event.ViewAdapter;
import fr.inria.zvtm.event.ViewListener;
import fr.inria.zvtm.engine.ViewPanel;
import fr.inria.zvtm.glyphs.Glyph;

public class DashboardTestFilter extends Dashboard
{

PointerFilter pfilter;
// mean filter...
int numSmooth = 40;
double radius = 0.06;

public DashboardTestFilter(ClusterDisplay cd, VirtualSpace vs)
{
	super(cd);

	// create an other "filtred" cursors ...
	//pfilter = new MeanPointerFilter(numSmooth, radius);
	pfilter = new OneEuroPointerFilter(60.0, 1.0, 0.007, 1.0);
	
	registerFiltredPointer(2);
	System.out.println("DashboardTestFilter is running ");
	Dobject = this;
}

@Override
public void setViewListener(View view)
{
	view.setListener(new DashboardTestFilterEventHandler());
}

@Override
public void registerDevice()
{
	System.out.println("registerDevice: DashboardTestFilter  "+this);
	_display.registerDevice(this,"DashboardTestFilter");
}

@Override
public void registerPointer(int id)
{
	ClusterDisplay.zcsEvent e =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, id);
	e.x = 0.5; e.y = 0.5;
	_display.handleEvent(e);
	System.out.println("DashboardTest register a pointer "+id);
}

public void registerFiltredPointer(int id)
{
	ClusterDisplay.zcsEvent e =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, id);
	Point2D.Double cur = pfilter.filter(new Point2D.Double(0.5, 0.5), null, System.currentTimeMillis());
	//pfilter.printPoints();
	e.x = cur.x; e.y = cur.y;
	e.color = Color.WHITE;
	_display.handleEvent(e);
	System.out.println("DashboardTest register a pointer "+id);
}

public class DashboardTestFilterEventHandler extends ViewAdapter
{
	
private int lastJPX;
private int lastJPY;

double lastMouseX;
double lastMouseY;

Random rand = new Random();
Timer timer = new Timer();
TimerTask timerT = new TimerT();
int timerCalled = 0;
double hertz = 60;

public double randInt(int min, int max) {

    int randomNum = rand.nextInt((max - min) + 1) + min;

    return (double)randomNum;
}

public void myMouseMove()
{
	ClusterDisplay.zcsEvent zcse =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, Dobject, 1);
	zcse.x = lastMouseX; // + randInt(-100,100)/10000.0;
	zcse.y = lastMouseY; // + randInt(-100,100)/10000.0;
	_display.handleEvent(zcse);
	zcse.x = lastMouseX + randInt(-100,100)/10000.0;
	zcse.y = lastMouseY + randInt(-100,100)/10000.0;
	//_display.handleEvent(zcse);
	ClusterDisplay.zcsEvent ee =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, Dobject, 2);
	Point2D.Double cur = pfilter.filter(new Point2D.Double(zcse.x, zcse.y), null, System.currentTimeMillis());
	//pfilter.printPoints();
	ee.x = cur.x; ee.y = cur.y;
	_display.handleEvent(ee);
	timerT = new TimerT();
	timer.schedule(timerT, (long)(1000/hertz));
}

public class TimerT extends TimerTask
{	 
	@Override
	public void run() {
	    myMouseMove();
	}
}

@Override	
public void press1(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){}

@Override public void release1(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){}

@Override public void click1(ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e)
{
	ClusterDisplay.zcsEvent zcse =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CURSOR_CLICK, Dobject, 1);
	zcse.button = 1;
	_display.handleEvent(zcse);
}

@Override public void press2(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){}

@Override public void release2(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){}

@Override public void click2(ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e){}

@Override public void press3(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e)
{}

@Override public void release3(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e)
{

}

@Override public void click3(ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e){}

@Override public void mouseMoved(ViewPanel v,int jpx,int jpy, MouseEvent e)
{
	//System.out.println("MouseMoved " + jpx+ " " + jpy);
	lastMouseX = jpx/_width;
	lastMouseY= jpy/_height;
	timerT.cancel();
	if (false)
	{
		timer.cancel();
    	timer.purge();
    }
	myMouseMove();
}

@Override public void mouseDragged(ViewPanel v,int mod,int buttonNumber,int jpx,int jpy, MouseEvent e)
{
}

@Override public void mouseWheelMoved(ViewPanel v,short wheelDirection,int jpx,int jpy, MouseWheelEvent e){}

//@Override public void enterGlyph(Glyph g){}

//@Override public void exitGlyph(Glyph g){}

@Override public void Ktype(ViewPanel v,char c,int code,int mod, KeyEvent e){
}

@Override public void Kpress(ViewPanel v,char c,int code,int mod, KeyEvent e){
	if (c == 'a') numSmooth++;
	else if (c == 'q') numSmooth--;
	System.out.println("setSmothingNumPoints "+numSmooth);
	pfilter.setSmothingNumPoints(numSmooth);
}

@Override public void Krelease(ViewPanel v,char c,int code,int mod, KeyEvent e){}

@Override public void viewActivated(View v){
	Dimension dim = v.getPanelSize();
	_width = dim.width;
	_height = dim.height;
	//System.out.println("Dashboard is running " + _width + " " + _height);
}

@Override public void viewDeactivated(View v){}

@Override public void viewIconified(View v){}

@Override public void viewDeiconified(View v){}

@Override public void viewClosing(View v){
	vsm.stop();
	System.exit(0);
	// kill the cluster...
}

} //

}
