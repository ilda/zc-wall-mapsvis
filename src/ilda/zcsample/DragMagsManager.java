package ilda.zcsample;

import java.util.Vector;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.BasicStroke;

import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.Location;
import fr.inria.zvtm.engine.portals.CameraPortal;
import fr.inria.zvtm.engine.portals.DraggableCameraPortal;
import fr.inria.zvtm.engine.portals.RoundCameraPortal;
import fr.inria.zvtm.event.CameraListener;

import fr.inria.zuist.engine.Region;
import fr.inria.zuist.engine.PseudoView;
import fr.inria.zuist.engine.SceneManager;

import fr.inria.zvtm.cluster.ClusteredView;

import ilda.zcsample.portals.DragMag;

class DragMagsManager  {

	class DragMagExtInfo{
		PseudoView _pv;
		SceneManager _sm;
		int _idx = 0;
		Vector<PseudoView> _pvs;
		Vector<SceneManager> _sms;
		Vector<Integer> _idxs;
		DragMagExtInfo(PseudoView pv, SceneManager sm, int idx){
			this._pv = pv; this._sm = sm; this._idx = idx;
			_pvs = new Vector<PseudoView>();
			_sms = new Vector<SceneManager>();
			_idxs = new Vector<Integer>();
		}
		void addPseudoViewAndSM(PseudoView pv, SceneManager sm, int idx){
			_pvs.add(pv); _sms.add(sm); _idxs.add(idx);
		}
		void ClearMagic(){
			_pvs.clear(); _sms.clear(); _idxs.clear();
		}
		//void removePseudoViewAndSM(PseudoView pv, SceneManager sm, int idx){
		//	_pvs.remove(pv); _sms.remove(sm); _idxs.remove(idx);
		//}
	}

    public static double DDM_SCALE_FAC = 4.0;
    public static Color  DDM_BAR_COLOR = Color.GRAY;
    public static Color  DDM_INSIDE_BAR_COLOR = Color.RED;
    public static Color  DDM_BORDER_COLOR = Color.BLUE;
    public static Color  DDM_INSIDE_BORDER_COLOR = Color.RED;

	ClusterDisplay _display;
	Camera _camera;
	VirtualSpace _space = null;
	VirtualSpace _visSpace,  _dataSpace;
	VirtualSpace[]  _aux_dataSpaces;
	SceneManager[] _zsms = null;
	SceneManager[] _aux_zsms = null;
    VirtualSpaceManager _vsm;
    //HashMap<DragMag, PseudoView> dmset = new HashMap<DragMag, PseudoView>();
    //HashMap<DragMag, SceneManager> smdmset = new HashMap<DragMag, SceneManager>();
    HashMap<DragMag, DragMagExtInfo> dminfoset = new HashMap<DragMag, DragMagExtInfo>();
    private float _borderWidth = 1.0f;
    private float _visLineWidth = 1.0f;

    // FIXME change the update refresh rate...
    private boolean _allowZuistUpdateControl = false; //false; //true;

	DragMagsManager(ClusterDisplay cd, VirtualSpace viss, VirtualSpace vs, SceneManager[] zsms, VirtualSpace ds, SceneManager[] aux_zsms, VirtualSpace[] aux_ds)
	{
        _display = cd;
        _visSpace = viss;
        _space = vs;
        _zsms = zsms;
        _aux_zsms = aux_zsms;
        _dataSpace = ds;
        _aux_dataSpaces = aux_ds;
        _vsm = VirtualSpaceManager.INSTANCE;
        _camera = cd.getMainCamera();
        if (_display.getWidth() > 10000){
        	_borderWidth = 20.0f;
        	_visLineWidth = 5.0f;
        	DragMag.MIN_SIZE = 2*DragMag.MIN_SIZE;
        }
        //_borderWidth=11;
        Camera c = _visSpace.addCamera();
        CameraPortal cp = new CameraPortal(0,0, (int)_display.getWidth(), (int)_display.getHeight(), c);
        _display.addPortal(cp);
	}

	int count = 0;

	DragMag addDragMagAtIndex(int idx, int type, int x, int y, double scale, boolean aux)
	{
		count++;
		double fac = (aux)? 0.15:0.08;

		int width = (aux)? (int)Math.floor(_display.getWidth()*fac) : (int)Math.floor(_display.getWidth()*(fac/1.8)*_display.getWidth()/_display.getHeight());
        int height = (aux)? (int)Math.floor(_display.getWidth()*fac*2*(_display.getHeight()/_display.getWidth())) : (int)Math.floor(_display.getWidth()*fac);
		VirtualSpace zvs = null;
		PseudoView pv = null;
		Camera cam = null;
		SceneManager zsm = null;
		boolean aux_add_pv = false; // 
		boolean aux_sync = false;
		if (_zsms != null && !aux && idx >= 0 && idx < _zsms.length && _zsms[idx] != null) {
			zsm = _zsms[idx];
			zvs =  _vsm.addVirtualSpace("dragmag "+count);
			cam = zvs.addCamera();
			pv = new PseudoView(zvs, cam, width, height);
		}
		else if (_aux_zsms != null && idx >= 0 && idx < _aux_zsms.length && _aux_zsms[idx] != null){
			zsm = _aux_zsms[idx];
			if (!aux_add_pv){
				pv = zsm.getPseudoView(0);
				zvs =  pv.vs;
				cam = pv.c;
				pv.sizeTo(width, height);
				// see if we have already a dg using this aux view
				// aux views are synchronized !
				for(DragMagExtInfo dmei: dminfoset.values()) {
					if (zsm == dmei._sm){
						width = dmei._pv.getWidth();
						height = dmei._pv.getHeight();
						aux_sync = true;
						break;
					}
				}
			}
			else {
				zvs =  _vsm.addVirtualSpace("dragmag "+count);
				cam = zvs.addCamera();
				pv = new PseudoView(zvs, cam, width, height);
			}
		}
		else if (aux){
			count--;
			return null;
		}
		else {
			cam = _space.addCamera(); 
		}
		Vector<Camera> cvect = new Vector<Camera>();
		cvect.add(cam);
		if (_dataSpace != null && !aux){
			Camera dcam = _dataSpace.addCamera();
			dcam.setLocation(cam.getLocation());
			cam.stick(dcam, true);
			cvect.add(dcam);
		}
		else if (aux && _aux_dataSpaces != null && idx >= 0 && idx < _aux_dataSpaces.length){
			Camera dcam = _aux_dataSpaces[idx].addCamera();
			dcam.setLocation(cam.getLocation());
			cam.stick(dcam, true);
			cvect.add(dcam);
		}
		DragMag dg = new DragMag(
			_display.getWidth(), _display.getHeight(),
			x - width/2, y - height/2, width, height, cvect,
			_camera, _visSpace, scale, type);
		//_vsm.addClusteredPortal(dg, _display.getClusteredView());
		_display.addPortal(dg);
		CameraPortal cp = (CameraPortal)dg;
		// this should be done after _display.addPortal(dg);
		dg.setBackgroundColor(_display.getBackgroundColor());
		//dg.setTranslucencyValue(1.0f); // testing
		//dg.setBackgroundColor(null); // for testing
		//dg.setBackgroundColor(new Color(125,125,125,196));
		//dg.setDragBarColor(DDM_BAR_COLOR);
        dg.setBorder(DDM_BORDER_COLOR);
        dg.setBorderWidth(_borderWidth);
        //if (dg.isManatthan()) dg.setDragBarHeight(0);
       	dg.setVisLinesWidth(_visLineWidth);
       	//dg.setVisRectAttributes(null,0.3f);
       	dg.setMaxBufferSizeRatios(1,3); // WILDER...

        if (zsm != null  && pv != null && (!aux || aux_add_pv)) {
        	// with aux 
        	zsm.addPseudoView(-1, pv);
        }

        if (aux && aux_sync){
        	// nothing ?
        	cam.setAltitude(cam.altitude+1);
        	cam.setAltitude(cam.altitude-1);
        }
        else if (aux && zsm != null) {
        	//dg.setBackgroundColor(Color.WHITE);
        	double[] wnes = zsm.findFarmostRegionCoords();
        	cam.moveTo((wnes[0] +  wnes[2])/2, (wnes[1] + wnes[3])/2);
    		double fw = (double)(-wnes[0] +  wnes[2])  / (double) width;
    		double fh = (double)(-wnes[1] + wnes[3]) / (double) height;
    		System.out.println("dm aux bounds: " + wnes[0] +" "+ wnes[1] +" "+  wnes[2] +" "+ wnes[3]);
    		double f = fw;
    		System.out.println("fw: " + fw + ", fh: " + fh);
    		if (fh > fw) f = fh;
    		cam.setAltitude(0.0);
    		double a = (cam.focal + cam.altitude) / cam.focal;
    		double newz = cam.focal * a * f - cam.focal;
    		cam.setAltitude(newz);
    		//zsm.updateVisibleRegions();
        }
        else{
        	dg.setupCamera();
        }
        dminfoset.put(dg, new DragMagExtInfo(pv, zsm, idx));
        return dg;
	}

	DragMag addDragMagAtIndex(int idx, int type, int x, int y, double scale){
		return addDragMagAtIndex(idx, type, x, y, scale, false);
	}

	DragMag addDragMag(int type, int x, int y, double scale){
		return addDragMagAtIndex(0, type, x, y, scale);
	}

	DragMag addDragMag(int type, int x, int y){
		return addDragMag(type, x, y, DDM_SCALE_FAC);
	}

	DragMag addDragMagAtIndex(int idx, int type, int x, int y){
		return addDragMagAtIndex(idx, type, x, y, DDM_SCALE_FAC);
	}

	DragMag addDragMag(int type){
		return addDragMag(
			type, (int)_display.getWidth()/2, (int)_display.getHeight()/2,
			DDM_SCALE_FAC);
	}

	DragMag addDragMag(){
		return addDragMag(DragMag.DM_TYPE_DRAGMAG);
	}

	public boolean removeDragMag(DragMag dm){
		if (dm == null) return false;
		dm.dispose();
		// FIXME do someting with the pv ?
		// remove stuff created here (zcs spaces, os cam ?)
		Vector<CameraPortal> cps = dm.getMagicPortals();
		for(CameraPortal cp : cps) { _display.destroyPortal(cp); }
		_display.destroyPortal(dm);
		dminfoset.remove(dm);
		return true;
		//_vsm.repaint();
	}

	public boolean removeDragMag(int jpx, int jpy){
		DragMag ret = null;
		for(Map.Entry<DragMag, DragMagExtInfo> entry: dminfoset.entrySet()) {
			DragMag dm = entry.getKey();
			if (dm.coordInside(jpx, jpy)) { ret = dm; }
		}
		if (ret != null){
			return removeDragMag(ret);
		}
		return false;
	}

	public Set<DragMag> getDragMags(){
		return dminfoset.keySet();
	}

	DragMag addMagic(int idx, DragMag dm)
	{
		VirtualSpace zvs = null;
		PseudoView pv = null;
		Camera cam = null;
		SceneManager zsm = null;
		if (idx >= 0 && idx < _zsms.length && _zsms[idx] != null) {
			zsm = _zsms[idx];
			count++;
			zvs =  _vsm.addVirtualSpace("dragmag "+count);
			cam = zvs.addCamera();
			pv = new PseudoView(zvs, cam, dm.w, dm.h);
		}
		else {
			return null; 
		}
		Vector<Camera> cvect = new Vector<Camera>();
		cvect.add(cam);
		if (_dataSpace != null){
			Camera dcam = _dataSpace.addCamera();
			dcam.setLocation(cam.getLocation());
			cam.stick(dcam, true);
			cvect.add(dcam);
		}
		zsm.addPseudoView(pv);
		CameraPortal cp = dm.addMagic(cvect);
		_display.addPortal(cp);
		cp.setBorder(dm.getBorder());
        cp.setBorderWidth(dm.getBorderWidth());
        cp.setBackgroundColor(_display.getBackgroundColor());
		DragMagExtInfo dminfo = dminfoset.get(dm);
		dminfo.addPseudoViewAndSM(pv,zsm,idx);
		return dm;
	}

	DragMag addMagic(DragMag dm){
		DragMagExtInfo dminfo = dminfoset.get(dm);
		int nidx = -1;
		int i = 0;
		while(i < _zsms.length) {
			if (i == dminfo._idx) { i++; continue; }
			Vector<Integer> idxs = dminfo._idxs;
			for(int j = 0; j < idxs.size(); j++){
				if (i == idxs.elementAt(j)) { i++; continue; }
			}
			nidx = i; break;
		}
		if (nidx != -1) {
			return addMagic(nidx, dm);
		}
		return null;
	}

	DragMag addMagic(int jpx, int jpy){
		DragMag dm = null;
		for(Map.Entry<DragMag, DragMagExtInfo> entry: dminfoset.entrySet()) {
			DragMag r = entry.getKey();
			if (r.coordInside(jpx, jpy)) { dm = r; }
		}
		if (dm != null){
			return addMagic(dm);
		}
		return null;
	}

	public int numOfMagic(DragMag dm){
		if (dm == null) return 0;
		DragMagExtInfo dminfo = dminfoset.get(dm);
		if (dminfo == null) { return 0; }
		return dminfo._pvs.size();
	}

	public boolean removeMagic(DragMag dm){
		if (dm == null) return false;
		DragMagExtInfo dminfo = dminfoset.get(dm);
		for (int i = 0; i < dminfo._pvs.size(); i++){
			PseudoView pv = dminfo._pvs.elementAt(i);
			SceneManager sm = dminfo._sms.elementAt(i);
			sm.removePseudoView(pv);
		}
		dminfo.ClearMagic();
		// see the FIXME in rempoveDragMag
		Vector<CameraPortal> cps = dm.getMagicPortals();
		for(CameraPortal cp : cps) { 
			_display.destroyPortal(cp);
		}
		dm.clearMagic();

		return true;
	}
	
	public void dragMagResized(DragMag dm){
		// FIXME for aux take the bigger pv !!!
		DragMagExtInfo dminfo = dminfoset.get(dm);
		PseudoView pv = dminfo._pv;
		if (pv != null){
			pv.sizeTo(dm.w, dm.h);
		}
		Vector<PseudoView> pvs = dminfo._pvs;
		for(PseudoView pvi : pvs) {
			if (pvi != null) pvi.sizeTo(dm.w, dm.h);
		}
	}
	
	public DragMag checkDragMag(int jpx, int jpy){
		DragMag ret = null;
		for(Map.Entry<DragMag, DragMagExtInfo> entry: dminfoset.entrySet()) {
			DragMag dm = entry.getKey();
			if (dm.coordInside(jpx, jpy)) { ret = dm; }
		}
		return ret;
	}

	public DragMag checkVis(int jpx, int jpy){
		DragMag ret = null;
		for(Map.Entry<DragMag, DragMagExtInfo> entry: dminfoset.entrySet()) {
			DragMag dm = entry.getKey();
			if (dm.getType() != DragMag.DM_TYPE_DRAGMAG) continue;
			if (dm.coordInsideVis(jpx, jpy)) { ret = dm; }
		}
		return ret;
	}

	// public void enableRegionUpdater(boolean v)
	// {
	// 	if (!_allowZuistUpdateControl || _zsm == null) return;

	// 	if (!v){
	// 		_zsm.enableRegionUpdater(false);
	// 	}
	// 	else{
	// 		_zsm.setUpdateLevel(true);
	// 		_zsm.enableRegionUpdater(true);
	// 	}
	// }

	public void enableRegionUpdater(DragMag dm, boolean v)
	{
		if (!_allowZuistUpdateControl) return; // || _zsm == null) return;

		DragMagExtInfo dminfo = dminfoset.get(dm);
		PseudoView pv = dminfo._pv;
		SceneManager zsm =  dminfo._sm;
		Vector<PseudoView> pvs = dminfo._pvs;
		Vector<SceneManager> zsms = dminfo._sms;
		// FIXME dragmagic ??

		if (pv == null || zsm == null){
			return;
		}
		
		//System.out.println("enableRegionUpdater "+ dm + " "+ v);
		if (!v){
			zsm.enableRegionUpdater(pv, false);

			for(int i = 0; i < pvs.size(); i++) {
				PseudoView p = pvs.elementAt(i);
				SceneManager s =  zsms.elementAt(i);
				s.enableRegionUpdater(p, false);
			}
		}
		else{
			zsm.enableRegionUpdater(pv, true);
			zsm.setUpdateLevel(pv, true);
			for(int i = 0; i < pvs.size(); i++) {
				PseudoView p = pvs.get(i);
				SceneManager s =  zsms.get(i);
				s.enableRegionUpdater(p, true);
				s.setUpdateLevel(p, true);
			}
		}
	}

}