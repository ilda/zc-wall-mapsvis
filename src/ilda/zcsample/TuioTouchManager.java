/*
 *   AUTHOR:    Olivier Chapuis <chapuis@lri.fr>
 *   Copyright (c) CNRS, 2015. All Rights Reserved
 *   Licensed under the GNU GPL.
 *
 */
package ilda.zcsample;

import java.awt.Color;

import java.util.Observable;
import java.util.Observer; 

import ilda.zcsample.tuiotouch.TuioTouch;
import ilda.zcsample.ildaevent.IldaEvent;

class TuioTouchManager implements Observer
{

double _displayWidth, _displayHeight;
ClusterDisplay _display;
TuioTouch _tuioTouch;

TuioTouchManager(ClusterDisplay cd, int port)
{
	_display = cd;
	_displayWidth = _display.getWidth();
	_displayHeight = _display.getHeight();

	_tuioTouch = new TuioTouch((double)_displayWidth/(double)_displayHeight, 0.003, 0.001, 0.01, port);
	_tuioTouch.addObserver(this);

	_display.registerDevice(this,"TuioTouch");
}

double prevMoveX = 0, prevMoveY = 0;
double prevPinchD = 0;
double prevAngle = 0;
int cursorCount = 0;
int contacts_lim1 = 1;
int contacts_lim2 = 2;
int contacts_lim3 = 4;
boolean longPressed = false;

// private int computeMM(int contacts){
// 	int mm = InputManager.IM_DRAG_DM_MODE_DRAG_VIEW;
// 	if ((contacts > contacts_lim1 && contacts <= contacts_lim2) || longPressed) {
// 		mm = InputManager.IM_DRAG_DM_MODE_MOVE_DM;
// 	}
// 	else if (contacts >= contacts_lim3){
// 		mm = InputManager.IM_DRAG_DM_MODE_RESIZE_DM;
// 	}
// 	return mm;
// } 

int current_contacts = 0;

public void update(Observable obj, Object arg)
{
	//System.out.println("TuioTouchManager");
    if (!(arg instanceof IldaEvent.Base)) return;

	IldaEvent.Base e = (IldaEvent.Base)arg;

	switch(e.type)
	{
		case IldaEvent.START_MOVE:
		{
			IldaEvent.StartMove ee = (IldaEvent.StartMove)e;
			System.out.println("StartMove " + ee.x +" "+ ee.y);
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, 0);
			zcse.x = ee.x; zcse.y = ee.y; zcse.hide = true;
			zcse.color = Color.YELLOW;
			zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_START_DRAG;
			_display.handleEvent(zcse);
			prevMoveX = ee.x; prevMoveY = ee.y;
			current_contacts = ee.contacts;
			break;
		}
		case IldaEvent.MOVE:
		{
			IldaEvent.Move ee = (IldaEvent.Move)e;
			double dx = (ee.x - prevMoveX); //*_displayWidth;
			double dy = (ee.y - prevMoveY); //*_displayHeight;
			//System.out.println("Move "+ (-dx) + " " + dy + " " + ee.x + " "+ ee.y);
			//FIXME viewer.directTranslate(-dx, dy);
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DRAG, this, 0);
			zcse.x = ee.x; zcse.y = ee.y;
			zcse.dx = dx; zcse.dy = dy; //*_height/_display.getHeight();
			zcse.button = 0;
			zcse.contacts = current_contacts;
			zcse.is_touch = true;
			//System.out.println("mouseDragged " +zcse.dx*_width+","+ zcse.dy*_height);
			_display.handleEvent(zcse);
			prevMoveX = ee.x; prevMoveY = ee.y;
			break;
		}
		case IldaEvent.END_MOVE:
		{
			IldaEvent.EndMove ee = (IldaEvent.EndMove)e;
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, this, 0);
			zcse.x = ee.x; zcse.y = ee.y; zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_DELETE_CURSOR;
			_display.handleEvent(zcse);
			//System.out.println("EndMove " + ee.x +" "+ ee.y);
			break;
		}
		case IldaEvent.START_PINCH:
		{
			IldaEvent.StartPinch ee = (IldaEvent.StartPinch)e;
			prevPinchD = ee.d; // prevPinchA = ee.a;
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, 0);
			zcse.x = ee.cx; zcse.y = ee.cy; zcse.f = 1; zcse.hide = true;
			zcse.color = Color.YELLOW; zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_START_ZOOM;
			_display.handleEvent(zcse);
			System.out.println("StartPinch " + ee.d);
			break;
		}
		case IldaEvent.PINCH:
		{
			IldaEvent.Pinch ee = (IldaEvent.Pinch)e;
			//System.out.println("SPinch " + ee.d);
			if (ee.d != 0)
			{
				double f = prevPinchD/ee.d;  // FIXME aspect ratio ???
				//System.out.println("Pinch factor " + f);
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, this, 0);
				zcse.x = ee.cx; zcse.y = ee.cy; zcse.f = f; zcse.is_touch = true;
				_display.handleEvent(zcse);
			}
			prevPinchD = ee.d;
			break;
		}
		case IldaEvent.END_PINCH:
		{
			IldaEvent.EndPinch ee = (IldaEvent.EndPinch)e;
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_ZOOM, this, 0);
			zcse.x = ee.cx; zcse.y = ee.cy; zcse.f = 1; zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_DELETE_CURSOR;
			_display.handleEvent(zcse);
			System.out.println("EndPinch ");
			break;
		}
		case IldaEvent.START_ROTATE:
		{
			IldaEvent.StartRotate ee = (IldaEvent.StartRotate)e;
			prevAngle = ee.a;
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, 0);
			zcse.x = ee.cx; zcse.y = ee.cy; zcse.f = 1; zcse.hide = true;
			zcse.color = Color.YELLOW; zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_START_ZOOM;
			_display.handleEvent(zcse);
			System.out.println("StartRotate " + ee.a);
			break;
		}
		case IldaEvent.ROTATE:
		{
			IldaEvent.Rotate ee = (IldaEvent.Rotate)e;
			//System.out.println("SPinch " + ee.d);
			System.out.println("Rotate " + ee.a);
			if (Math.abs(ee.a - prevAngle) > 3) {
				double f = 1.03;
				if (ee.a - prevAngle < 0) f = 1/1.03; 
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, this, 0);
					zcse.x = ee.cx; zcse.y = ee.cy; zcse.f = f; zcse.is_touch = true;
					_display.handleEvent(zcse);
				prevAngle = ee.a;
			}
			break;
		}
		case IldaEvent.END_ROTATE:
		{
			IldaEvent.EndRotate ee = (IldaEvent.EndRotate)e;
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_ZOOM, this, 0);
			zcse.x = ee.cx; zcse.y = ee.cy; zcse.f = 1; zcse.is_touch = true;
			_display.handleEvent(zcse);
			zcse.type = ClusterDisplay.zcsEvent.ZCS_DELETE_CURSOR;
			_display.handleEvent(zcse);
			System.out.println("EndRotate ");
			break;
		}
		case IldaEvent.SIMPLE_TAP:
		{
			IldaEvent.SimpleTap ee = (IldaEvent.SimpleTap)e;
			System.out.println("SimpleTap ");
			break;
		}
		case IldaEvent.LONG_PRESS:
		{
			IldaEvent.SimpleTap ee = (IldaEvent.SimpleTap)e;
			System.out.println("LongPress ");
			break;
		}
		case IldaEvent.VOID_END:
		{
			IldaEvent.VoidEnd ee = (IldaEvent.VoidEnd)e;
			System.out.println("VoidEnd ");
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DELETE_CURSOR, this, 0);
			zcse.is_touch = true;	
			_display.handleEvent(zcse);
			break;
		}
	}
}

}