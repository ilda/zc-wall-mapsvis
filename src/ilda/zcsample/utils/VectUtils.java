package ilda.zcsample.utils;

public class VectUtils
{

static public double norm3(double[] a)
{
    return Math.sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
}

static public double norm2(double[] a)
{
    return Math.sqrt(a[0]*a[0] + a[1]*a[1]);
}
    
static public double norm3(double a,double b,double c)
{
    return Math.sqrt(a*a + b*b + c*c);
}

static public double norm2(double a,double b)
{
    return Math.sqrt(a*a + b*b);
}

static public double dist3(double[] a,  double[] b)
{
    return norm3(a[0]-b[0], a[1]- b[1], a[2]-b[2]);
}

static public double dist2(double[] a,  double[] b)
{
    return norm2(a[0]-b[0], a[1]- b[1]);
}

static public double dot(double[] a,  double[] b)
{
    return (a[0]*b[0] + a[1]*b[1] + a[2]*b[2]);
}

static public double dot2(double[] a,  double[] b)
{
    return (a[0]*b[0] + a[1]*b[1]);
}

static public double[] cross(double[] a,  double[] b)
{
	double [] ret = new double[3];

	ret[0] = a[1]*b[2] - a[2]*b[1];
	ret[1] = a[2]*b[0] - a[0]*b[2];
	ret[2] = a[0]*b[1] - a[1]*b[0];

	return ret;
}


}