package ilda.zcsample;

import java.util.Vector;

import java.awt.Color;
import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;

import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.event.ViewAdapter;
import fr.inria.zvtm.event.ViewListener;
import fr.inria.zvtm.engine.ViewPanel;
import fr.inria.zvtm.glyphs.Glyph;

import ilda.zcsample.portals.DragMag;

public class Dashboard extends BasicDevice
{

static final double WHEEL_ZOOMIN_FACTOR = 1.1;
static final double WHEEL_ZOOMOUT_FACTOR = 1/1.1;

static final int WIDTH = 1000;
static final int HEIGHT = 300;

ClusterDisplay _display;
VirtualSpaceManager vsm = VirtualSpaceManager.INSTANCE;
double _width, _height;
Object Dobject;
View _view;
boolean _desktop = false;

public Dashboard(ClusterDisplay cd)
{
	_display = cd;
	if (cd.getView() != null){ // desktop !!
		_view = cd.getView();
		_width = cd.getWidth();
		_height = cd.getHeight();
		_desktop = true;
	}
	else{
		VirtualSpace vs = vsm.addVirtualSpace("dashboardSpace");
		Camera cam = vs.addCamera();
		Vector<Camera> cameras = new Vector<Camera>();
		cameras.add(cam);
		_view = vsm.addFrameView(
	    	cameras, "Dashboard", View.STD_VIEW, WIDTH, HEIGHT, true);  //false, true, true, null);
		_view.setBackgroundColor(Color.GRAY);
		_view.setCursorIcon(Cursor.DEFAULT_CURSOR);
        _view.setCursorIcon(Cursor.CUSTOM_CURSOR);
        //_view.getCursor().setVisibility(false);
		//
		GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
        	GraphicsDevice dev = genv.getDefaultScreenDevice();
        	Rectangle r = dev.getDefaultConfiguration().getBounds();

		_view.setLocation((int)(r.getBounds().getWidth()-WIDTH-50),
			(int)(r.getBounds().getHeight()-HEIGHT-50));
		
		Dimension dim = _view.getPanelSize();
		_width = dim.width;
		_height = dim.height;
	}

	setViewListener(_view);

	registerDevice();
	registerPointer(1);
	System.out.println("Dashboard is running "+ _width +" "+_height );
	Dobject = this;
}

public void setViewListener(View view)
{
	System.out.println("add DashboardEventHandler...");
	DashboardEventHandler deh = new DashboardEventHandler();
	view.setListener(deh);
	view.getPanel().getComponent().addComponentListener(deh);
}

public void registerDevice()
{
	_display.registerDevice(this,"Dashboard");
}

public void registerPointer(int id)
{
	ClusterDisplay.zcsEvent e =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, id);
	e.x = 1.0; e.y = 1.0;
	e.color = Color.GRAY;
	_display.handleEvent(e);
	System.out.println("Dashboard register a pointer "+id);
}

public void moveCursor(int id, double x, double y) {}
public void setAttached(int id, boolean b) {
	System.out.println("setAttached: " + id +" "+ b);
}
public void setLinked(int id, boolean b) {}

public class DashboardEventHandler extends ViewAdapter implements ComponentListener
{
	
private int lastJPX;
private int lastJPY;
private double downX = 0, downY = 0;
private double prevPressX = 0, prevPressY = 0;
private int pressedButton = 0;
boolean indrag = false;

@Override	
public void press1(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	downX = prevPressX =  jpx/_width;
	downY = prevPressY = jpy/_height;
	pressedButton = 1;
}

@Override public void release1(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	if (indrag && pressedButton == 1){
		ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, Dobject, 1);
		zcse.x = jpx/_width; zcse.y =  jpy/_height;
		_display.handleEvent(zcse);		
		pressedButton = 0;
		indrag = false;
	}
}

@Override public void click1(ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e)
{
	ClusterDisplay.zcsEvent zcse =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CURSOR_CLICK, Dobject, 1);
	zcse.x = jpx/_width; zcse.y =  jpy/_height;
	zcse.button = 1;
	_display.handleEvent(zcse);
	if (pressedButton == 1){
		pressedButton = 0;
	}
}

@Override public void press2(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	downX = prevPressX =  jpx/_width;
	downY = prevPressY = jpy/_height;
	pressedButton = 2;
}

@Override public void release2(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	if (indrag && pressedButton == 2){	
		pressedButton = 0;
		indrag = false;
	}
}

@Override public void click2(ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e){
}

@Override public void press3(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	downX = prevPressX =  jpx/_width;
	downY = prevPressY = jpy/_height;
	pressedButton = 3;
	System.out.println("press3");
	ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_PIEMENU, Dobject, 1);
	zcse.x = jpx/_width; zcse.y =  jpy/_height;
	_display.handleEvent(zcse);	
	//ClusterDisplay.zcsEvent zcse =
	//				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, Dobject, 1);
	//zcse.x = jpx/_width; zcse.y =  jpy/_height;
	//_display.handleEvent(zcse);
}

@Override public void release3(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	if (indrag && pressedButton == 3){
		ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, Dobject, 1);
		zcse.x = jpx/_width; zcse.y =  jpy/_height;
		_display.handleEvent(zcse);		
		pressedButton = 0;
		indrag = false;
	}
}

@Override public void click3(ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e){}

@Override public void mouseMoved(ViewPanel v,int jpx,int jpy, MouseEvent e)
{
	//System.out.println("MouseMoved " + jpx+ " " + jpy);
	double x = jpx/_width;
	double y = jpy/_height;
	double dx = (x - lastJPX/_width);
	double dy = (y - lastJPY/_height);
	ClusterDisplay.zcsEvent zcse =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, Dobject, 1);
	zcse.x = x; zcse.y = y;
	zcse.dx = dx; zcse.dy = dy;
	_display.handleEvent(zcse);
	lastJPX = jpx; lastJPY = jpy; 
}

@Override public void mouseDragged(ViewPanel v,int mod,int buttonNumber,int jpx,int jpy, MouseEvent e)
{
	double x = jpx/_width;
	double y = jpy/_height;
	double dx = (x - prevPressX);
	double dy = (y - prevPressY);
	ClusterDisplay.zcsEvent zcse =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, Dobject, 1);
	zcse.x = x; zcse.y = y;
	zcse.dx = dx; zcse.dy = dy; //*_height/_display.getHeight();
	zcse.button = pressedButton;
	_display.handleEvent(zcse);
	if(!indrag){
		zcse.x = downX; zcse.y = downY;
		zcse.dx = 0; zcse.dy = 0;
		zcse.type = ClusterDisplay.zcsEvent.ZCS_START_DRAG;
		_display.handleEvent(zcse);
		indrag = true;
		zcse.x = x; zcse.y = y;
		zcse.dx = (x - downX); zcse.dy = (y - downY);
		zcse.type = ClusterDisplay.zcsEvent.ZCS_DRAG;
		_display.handleEvent(zcse);
	}
	else{
		//System.out.println("mouseDragged " +zcse.dx*_width+","+ zcse.dy*_height);
		zcse.type = ClusterDisplay.zcsEvent.ZCS_DRAG;
		_display.handleEvent(zcse);
	}
	
	prevPressX = x; prevPressY = y;
	lastJPX = jpx; lastJPY = jpy; 
}

@Override public void mouseWheelMoved(
	ViewPanel v,short wheelDirection,int jpx,int jpy, MouseWheelEvent e)
{
	double x = jpx/_width;
	double y = jpy/_height;
	ClusterDisplay.zcsEvent zcse =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, Dobject, 1);
	zcse.x = x; zcse.y = y;
	if (wheelDirection  == WHEEL_UP){
		zcse.f = WHEEL_ZOOMIN_FACTOR;
	}
	else {
		zcse.f = WHEEL_ZOOMOUT_FACTOR;
	}
	_display.handleEvent(zcse);
}

// @Override public void enterGlyph(Glyph g){}

//@Override public void exitGlyph(Glyph g){}

@Override
public void Ktype(ViewPanel v,char c,int code,int mod, KeyEvent e)
{
}

@Override public void Kpress(ViewPanel v,char c,int code,int mod, KeyEvent e)
{
	code = e.getKeyCode();
	//System.out.println("Kpress "+ code + " "+ e.getKeyChar());
	if  (code == KeyEvent.VK_S &&  e.isShiftDown()) {
	      _display.setSyncronous(2); 
	}
	else if (code == KeyEvent.VK_D &&  e.isShiftDown()) {
	        _display.addDragMagAtIndex(1, DragMag.DM_TYPE_DRAGMAG, lastJPX/_width, lastJPY/_height);
	}
	else if (code == KeyEvent.VK_D) {
	        _display.addDragMagAtIndex(0, DragMag.DM_TYPE_DRAGMAG, lastJPX/_width, lastJPY/_height);
	}
	else if (code == KeyEvent.VK_U) {
	        _display.addMagic(lastJPX/_width, lastJPY/_height);
	}
	else if (code == KeyEvent.VK_M) {
	        _display.addDragMag(DragMag.DM_TYPE_MANATTHAN, lastJPX/_width, lastJPY/_height);
	}
	else if (code == KeyEvent.VK_I) {
			_display.addDragMag(DragMag.DM_TYPE_INDEPENDANT, lastJPX/_width, lastJPY/_height);
	}
	else if (code == KeyEvent.VK_R) {
			_display.removeDragMag(lastJPX/_width, lastJPY/_height);
	}
	else if (code == KeyEvent.VK_A) {
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ATTACH, Dobject, 1);
		zcse.onoff = true;
		_display.handleEvent(zcse);
	}
	else if (code == KeyEvent.VK_Z) {
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ATTACH, Dobject, 1);
		zcse.onoff = false;
		_display.handleEvent(zcse);
	}
	else if (code == KeyEvent.VK_UP) {
		_display.tmpT(0,-1);
	}
	else if (code == KeyEvent.VK_DOWN) {
		_display.tmpT(0,1);
	}
	else if (code == KeyEvent.VK_LEFT) {
		_display.tmpT(-1,0);
	}
	else if (code == KeyEvent.VK_RIGHT) {
		_display.tmpT(1,0);
	}
	else if (e.getKeyChar() == '1') {
		System.out.println("Change main idx 1");
		_display.setMainLayerIndex(1);
	}
	else if (e.getKeyChar() == '0') {
		System.out.println("Change main idx 0");
		_display.setMainLayerIndex(0);	
	}
	else if (e.getKeyChar() == 's') {
		_display.startTsunami();	
	}
	else if (code == KeyEvent.VK_L) {
		//_display.loadTsunamiData();
		_display.displayTsunamiData(2);
	}
	else if (code == KeyEvent.VK_X) {
		_display.addAuxDragMagView(0, lastJPX/_width, lastJPY/_height);
	}
	else if (code == KeyEvent.VK_G) {
		_display.globalView();
	}
	else if (code == KeyEvent.VK_Q) {
	    _display.stop();
		System.exit(0);
	}
}

@Override public void Krelease(ViewPanel v,char c,int code,int mod, KeyEvent e){}

@Override public void viewActivated(View v){
	Dimension dim = v.getPanelSize();
	_width = dim.width;
	_height = dim.height;
	System.out.println("Dashboard activated " + _width + " " + _height);
}

@Override public void viewDeactivated(View v){}

@Override public void viewIconified(View v){}

@Override public void viewDeiconified(View v){}

@Override public void viewClosing(View v){
	_display.stop();
	System.exit(0);
	// kill the cluster...
}

/*ComponentListener*/
    public void componentHidden(ComponentEvent e){}
    public void componentMoved(ComponentEvent e){}
    public void componentResized(ComponentEvent e){
       	Dimension dim = _view.getPanelSize();
		_width = dim.width;
		_height = dim.height;
		System.out.println("Dashboard resized " + _width + " " + _height);
    }
    public void componentShown(ComponentEvent e){}

} //

}
