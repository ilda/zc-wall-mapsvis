package ilda.zcsample;

import ilda.zcsample.vicon.VRPNViconReceiver;
import ilda.zcsample.vicon.ViconTrackerInfo;
import ilda.zcsample.walls.WallCoordinateCalculator;
import ilda.zcsample.walls.wild.WildCoordinateCalculator;
import ilda.zcsample.walls.wilder.WilderCoordinateCalculator;

//import ilda.zcsample.Dashboard;
//import ilda.zcsample.DashboardVicon;

import java.util.HashMap;
import java.util.Iterator;

import java.util.Observable;
import java.util.Observer; 

import java.awt.Color;
import java.awt.geom.Point2D;

class SimpleViconManager  implements Observer {

private WallCoordinateCalculator _wcc = null;
private boolean _data_viz;

SimpleViconManager(
	String objects, String host, int port, String wall, boolean data_viz, boolean force)
{

	if (wall != null && wall.toLowerCase().equals("wild"))
	{
		_wcc = new WildCoordinateCalculator();
	}
	else if (wall != null && wall.toLowerCase().equals("wilder"))
	{
		_wcc = new WilderCoordinateCalculator();
	}

	if (_wcc != null && host == null)
	{
		host = _wcc.getViconHost();
	}


	String[] aobj = objects.split(":");
	for(int i = 0; i < aobj.length; i++)
	{
		String[] obj = aobj[i].split(",");
		boolean is_a_pointer = (obj.length > 1 && obj[1].toLowerCase().equals("pointer"));
		System.out.println("ViconManager: try to connect to \""+ obj[0] + "\". a pointer? "+ is_a_pointer);
		VRPNViconReceiver vvr = null;
		try {
			vvr = new VRPNViconReceiver(
				obj[0], host, port, data_viz, force, _wcc, is_a_pointer);
		}
		catch(RuntimeException e)
		{
   			System.out.println("" +e);
		}
		if (vvr != null)
		{
			vvr.addObserver(this);
		}
	}
}


public void update(Observable obj, Object arg)
{
	if (!(arg instanceof ViconTrackerInfo)) { return; }

	ViconTrackerInfo e = (ViconTrackerInfo)arg;
	if (true)
	{
		System.out.println(
			"VICON\t"+ e.timestamp+ "\t"+ e.name +"\n"
			+"\tpos: "+ e.pos[0] +"\t"+ e.pos[1] +"\t"+ e.pos[2] +"\n"
			+"\tdir: "+ e.dir[0] +"\t"+ e.dir[1] +"\t"+ e.dir[2] +"\n");
		if (e.wp != null)
		{
			System.out.println(
					"VICON\t"+ e.timestamp+ "\t"+ e.name +"\n"
					+"\twall coor: "+ e.wp[0] +"\t"+ e.wp[1] +"\n");
		}
	}

}

}