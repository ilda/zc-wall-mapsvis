package ilda.zcsample;

import java.util.Vector;

import java.util.Observable;
import java.util.Observer; 

import java.awt.geom.Point2D;
import java.awt.Color;

import java.text.DecimalFormat;

import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;

// see:
// http://smarties.lri.fr/
// http://smarties.lri.fr/java-tutorial.html
// http://smarties.lri.fr/javadoc/index.html

import fr.lri.smarties.libserver.Smarties;
import fr.lri.smarties.libserver.SmartiesColors;
import fr.lri.smarties.libserver.SmartiesEvent;
import fr.lri.smarties.libserver.SmartiesPuck;
import fr.lri.smarties.libserver.SmartiesDevice;
import fr.lri.smarties.libserver.SmartiesWidget;
import fr.lri.smarties.libserver.SmartiesWidgetHandler;
import fr.lri.smarties.libserver.SmartiesWidgetHandler;

class SmartiesButtons implements Observer
{

private double _displayWidth, _displayHeight;
private ClusterDisplay _display;
private Object _SSobject;
private int _pointer_id;

Smarties m_smarties;

SmartiesWidget _log_widget = null;

SmartiesButtons(int portInc, int pointer_id, ClusterDisplay cd, Object ssobj)
{
	_display = cd;
	_displayWidth = cd.getWidth();
	_displayHeight = cd.getHeight();
	_pointer_id = pointer_id;
	_SSobject = (Object)ssobj;
	m_smarties = new Smarties(
		Smarties.DEFAULT_PORT_SMARTIES + portInc,
		(int)_displayWidth, (int)_displayHeight, cd.getNumCol(), cd.getNumRow());

	m_smarties.setPureWidgetArea(true);


	// add some Smarties widgets into a 3 x 3 grid
	m_smarties.initWidgets(5,5);

	SmartiesWidget w = m_smarties.addWidget(
		SmartiesWidget.SMARTIES_WIDGET_TYPE_BUTTON, "the button", 1, 1, 5, 4);
	w.handler = new buttonHandler();
	w.font_size = 42;

	_log_widget = m_smarties.addWidget(
		SmartiesWidget.SMARTIES_WIDGET_TYPE_TEXTVIEW, "Vicon Log", 1, 5, 5, 1);
	_log_widget.font_size = 10;

	m_smarties.setDeviceOrientation(Smarties.SMARTIES_DEVICE_ORIENTATION_PORTRAIT);

	m_smarties.addObserver(this);
	m_smarties.Run();
	_display.registerDevice(this,"SmartiesButtons");
	System.out.println("SmartiesButtons running " + _displayWidth + " " + _displayHeight);
}

// ------------------------------------------------------------------------
// local "data" attached to a puck 


// ------------------------------------------------------------------------
// events handleing

public void update(Observable obj, Object arg)
{

	if (!(arg instanceof SmartiesEvent)) 
	{
		return;
	}

	SmartiesEvent e = (SmartiesEvent)arg;

	switch (e.type) {

	case SmartiesEvent.SMARTIE_EVENTS_TYPE_KEYUP:
	{
	    System.out.println("SMARTIE_EVENTS_TYPE_KEYUP:  keycode " + e.keycode + " pid " + e.id);
	    break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_KEYDOWN:
	{
	    System.out.println("SMARTIE_EVENTS_TYPE_KEYDOWN: keycode " + e.keycode + " pid " + e.id);
	    break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_STRING_EDIT:
	{
	    System.out.println("SMARTIE_EVENTS_TYPE_STRING_EDIT: string " + e.text);
	    break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_WIDGET:
	{
		//System.out.println("SMARTIE_EVENTS_TYPE_WIDGET: " + e.widget.uid);
		if (e.widget.handler != null)
		{
			e.widget.handler.callback(e.widget, e, this);
		}
		break;
	}
	default:
	{
		break;
	}
	} // ens switch

}


// ------------------------------------------------------------------------
// widgets handler ...

public class buttonHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("button 1 clicked");
		ClusterDisplay.zcsEvent zcse = _display.new zcsEvent(
					ClusterDisplay.zcsEvent.ZCS_CURSOR_CLICK, _SSobject, _pointer_id);
		zcse.button = 1;
		_display.handleEvent(zcse);
		return true;
	}
}


// -----------------------------------------------------------
// widget logger

private double mypos[] = new double[3];
private String log_name = null;
private long prev_ct = 0;

public void showObjectPosition(String name, double[] pos)
{
	if (_log_widget == null)
	{
		return;
	}
	
	//if (log_name == null) { log_name = name; }
	//if (!log_name.equals(name)) { return; }

	DecimalFormat df = new DecimalFormat("0.0000");
	String str = name;
	for(int i = 0; i < pos.length && i < 3; i++)
	{
		str = str +" "+ df.format(pos[i]);
		mypos[i] = pos[i];
	}
	// not too often ... 

	//System.out.println(str);
	long ct = System.currentTimeMillis();
	if (ct - prev_ct > 500)
	{	
		prev_ct = ct;
		m_smarties.sendWidgetLabel(_log_widget.uid, str);
	}
}
}
