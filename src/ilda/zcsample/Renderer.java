package ilda.zcsample;


import java.util.Vector;

import java.awt.Color;

import fr.inria.zvtm.cluster.ClusterGeometry;
import fr.inria.zvtm.cluster.ClusteredView;
import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.engine.Location;

import fr.inria.zvtm.glyphs.Glyph;
import fr.inria.zvtm.glyphs.VCircle;
import fr.inria.zvtm.glyphs.VRectangle;
import fr.inria.zvtm.glyphs.VRing;

public class Renderer {

	
protected ClusterDisplay _display;
protected double _displayWidth, _displayHeight;
protected VirtualSpace _vs;
protected Camera _camera; 
protected double _bezWidth;
protected double _bezHeight;

public Renderer(ClusterDisplay cd)
{
	_display = cd;
	_displayWidth = _display.getWidth();
	_displayHeight = _display.getHeight();
	_vs = _display.getSpace();
	_camera = _display.getMainCamera();
	_bezWidth = _display.getBezWidth();
	_bezHeight = _display.getBezHeight();
}

public void initRendering()
{
	// draw some stuff...
}

public String[] getLayerNames() {
	return null;
}

public String[] getAuxLayerNames() {
	return null;
}

public boolean setMainLayerIndex(int idx)
{
	return false;
}

public boolean nextMainLayerIndex(){
	return false;
}

public boolean click(double x, double y)
{
	return false;
}

public void globalView(){
	double[] b = getBounds();
	double west = b[0];
    double north = b[3];
    double east = b[2];
    double south = b[1];
    double newX = (west + east) / 2;
    double newY = (north + south) / 2;
	double nah = (east-west)*_camera.getFocal() / _displayWidth - _camera.getFocal();
    //new altitude to fit vertically
    double nav = (north-south)*_camera.getFocal()/ _displayHeight - _camera.getFocal();
    double newAlt = Math.max(Math.abs(nah), Math.abs(nav));

    System.out.println("globalView: "+b[0]+" "+b[1]+" "+b[2]+" "+b[3]+" : "+newAlt);
    _camera.setLocation(new Location(newX, newY, newAlt));

}

public double[] getBounds()
{
		// west north east south
	double[] ret = {-_displayWidth/2, _displayHeight/2, _displayWidth/2, -_displayHeight/2,};
	return ret;
}

}
