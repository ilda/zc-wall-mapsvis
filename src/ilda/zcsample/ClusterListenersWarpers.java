package ilda.zcsample;

import fr.inria.zvtm.cluster.ClusteredView;
import fr.inria.zvtm.cluster.ClusteredViewStartListener;
import fr.inria.zvtm.cluster.ClusteredViewStartAdapter;
import fr.inria.zvtm.cluster.ClusteredViewAdapter;

public class ClusterListenersWarpers {

	public static void setSynchronousAtStart(ClusteredView cv, int numSlaves)
	{
		cv.setStartListener(numSlaves,
		new ClusteredViewStartAdapter(){
	    		public void started(ClusteredView cv){
	    				cv.setSynchronous(true);
	    		}
	    	}
	    );
	}

	public static void setClusteredViewListener(ClusteredView cv, ClusteredViewAdapter cl)
	{
		cv.setListener(cl);
	}

	public static void setConsoleListener(ClusterDisplay cd, ClusteredView cv, int index){
		ConsoleListener cl = new ConsoleListener(cd, cv, index);
	 	cv.setListener(cl);
	}
}
