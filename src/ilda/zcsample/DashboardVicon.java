package ilda.zcsample;

import java.awt.Color;
import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;

import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.event.ViewAdapter;
import fr.inria.zvtm.event.ViewListener;
import fr.inria.zvtm.engine.ViewPanel;
import fr.inria.zvtm.glyphs.Glyph;

import ilda.zcsample.portals.DragMag;

public class DashboardVicon extends Dashboard
{

ViconManager _vm;
int _cid;
DashboardViconEventHandler _eh;
ViconManager.OneTracker _ot;

public DashboardVicon(ClusterDisplay cd, VirtualSpace vs, ViconManager vm, ViconManager.OneTracker ot)
{
	super(cd);
	_vm = vm;
	_ot = ot;
	_cid = ot.id;

	_width = cd.getWidth();
	_height = cd.getHeight();

	_view.setTitle("Vicon Dashboard");
	System.out.println("DashboardVicon is running ");
	Dobject = (Object)vm;
}

public int getPressedButton(){
	return _eh.pressedButton;
}

@Override
public void setViewListener(View view)
{
	System.out.println("add DashboardViconEventHandler");
	_eh = new DashboardViconEventHandler();
	_view.setListener(_eh);
}

@Override
public void registerDevice()
{
	// the device has been registered by the ViconManager... 
}

@Override
public void registerPointer(int id)
{
	// a pointer (at least) has been regitered by the ViconManager... 
}

public class DashboardViconEventHandler extends ViewAdapter
{

int pressedButton = 0;
boolean indrag = false;

@Override	
public void press1(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	if (pressedButton != 0){ return; }
	System.out.println("DashboardViconEventHandler press1");
	pressedButton = 1;
}

@Override
public void release1(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	if (pressedButton == 1){
		pressedButton = 0;
	}
}

@Override public void click1(
	ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e){}

@Override public void press2(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	if (pressedButton != 0){ return; }
	pressedButton = 2;
}

@Override public void release2(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	if (pressedButton == 2){
		pressedButton = 0;
	}
}

@Override public void click2(ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e){}

@Override public void press3(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e)
{
	if (pressedButton != 0){ return; }
	pressedButton = 3;
}

@Override public void release3(ViewPanel v,int mod,int jpx,int jpy, MouseEvent e){
	if (pressedButton == 3){
		pressedButton = 0;
	}
}

@Override public void click3(ViewPanel v,int mod,int jpx,int jpy,int clickNumber, MouseEvent e){}

@Override public void mouseMoved(ViewPanel v,int jpx,int jpy, MouseEvent e){}

@Override public void mouseDragged(ViewPanel v,int mod,int buttonNumber,int jpx,int jpy, MouseEvent e){}

@Override public void mouseWheelMoved(ViewPanel v,short wheelDirection,int jpx,int jpy, MouseWheelEvent e)
{
	Point2D.Double p = _vm.getLastFirstPointerPosition();
	double x = p.x/_width;
	double y = p.y/_height;
	ClusterDisplay.zcsEvent zcse =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, Dobject, _cid);
	zcse.x = x; zcse.y = y;
	if (wheelDirection  == WHEEL_UP){
		zcse.f = WHEEL_ZOOMIN_FACTOR;
	}
	else {
		zcse.f = WHEEL_ZOOMOUT_FACTOR;
	}
	_display.handleEvent(zcse);
}

//@Override public void enterGlyph(Glyph g){}

//@Override public void exitGlyph(Glyph g){}

@Override public void Ktype(ViewPanel v,char c,int code,int mod, KeyEvent e){
}

@Override public void Kpress(ViewPanel v,char c,int code,int mod, KeyEvent e){
}

@Override public void Krelease(ViewPanel v,char c,int code,int mod, KeyEvent e){}

@Override public void viewActivated(View v){
}

@Override public void viewDeactivated(View v){}

@Override public void viewIconified(View v){}

@Override public void viewDeiconified(View v){}

@Override public void viewClosing(View v){
	vsm.stop();
	System.exit(0);
	// kill the cluster...
}

} //

}
