package ilda.zcsample.vicon;

public class ViconUtils
{

static public double[] qmult(double[] qLeft, double[] qRight)
{
	int X=0,Y=1,Z=2,W=3;
	double[] tempDest = new double[4];


	tempDest[W] = qLeft[W]*qRight[W] - qLeft[X]*qRight[X] - 
		qLeft[Y]*qRight[Y] - qLeft[Z]*qRight[Z];

	tempDest[X] = qLeft[W]*qRight[X] + qLeft[X]*qRight[W] + 
		qLeft[Y]*qRight[Z] - qLeft[Z]*qRight[Y];
	
	tempDest[Y] = qLeft[W]*qRight[Y] + qLeft[Y]*qRight[W] + 
		qLeft[Z]*qRight[X] - qLeft[X]*qRight[Z];
	
	tempDest[Z] = qLeft[W]*qRight[Z] + qLeft[Z]*qRight[W] + 
		qLeft[X]*qRight[Y] - qLeft[Y]*qRight[X];
	
	return tempDest;
	
}   /* q_mult  */

static public double[] xform(double[] q)
{
	int X=0,Y=1,Z=2,W=3;

	double[] qinv = new double[4];
	double[] dir = new double[3];

	// 
	double[] vect = new double[4];
	vect[X] = vect[Y] = vect[W] = 0;
	vect[Z] = 1;

	// inverse q:
	double qNorm = 1.0 / (q[X]*q[X] + q[Y]*q[Y] + q[Z]*q[Z] + q[W]*q[W]);
	qinv[X] = -q[X] * qNorm;
	qinv[Y] = -q[Y] * qNorm;
	qinv[Z] = -q[Z] * qNorm;
	qinv[W] =  q[W] * qNorm;

	double[] tmp = qmult(qmult(q, vect), qinv);
	
	dir[X] = tmp[X]; dir[Y] = tmp[Y];  dir[Z] = tmp[Z]; 
	return dir;
}

}