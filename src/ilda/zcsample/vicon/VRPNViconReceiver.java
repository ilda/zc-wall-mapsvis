package ilda.zcsample.vicon;

import ilda.zcsample.walls.*;

import java.io.*;
import vrpn.*;

import java.util.Observable;  

 
public class VRPNViconReceiver extends Observable
	implements vrpn.TrackerRemote.PositionChangeListener, 
		vrpn.TrackerRemote.VelocityChangeListener,
		vrpn.TrackerRemote.AccelerationChangeListener
{

String name;
boolean debug = false;
ViconDataViewer dataViewer = null;
WallCoordinateCalculator _wcc = null;
boolean _is_a_pointer;

public void trackerPositionUpdate(TrackerRemote.TrackerUpdate u, TrackerRemote tracker)
{
	// position are in m, convert in mm
	u.pos[0] = u.pos[0]*1000; u.pos[1] = u.pos[1]*1000; u.pos[2] = u.pos[2]*1000;
	double[] dir = ViconUtils.xform(u.quat);

	double[] wp = null;
	if (_wcc != null && _is_a_pointer)
	{
		wp = _wcc.getCoordinate(u.pos, dir);
	}

	if (debug)
	{
		//if (name == "HeadBlack")  {
        System.out.println(
			"Tracker position message from vrpn for " + name + ": \n" +
			"\ttime:  " + u.msg_time.getTime( ) + "  sensor:  " + u.sensor + "\n" +
			"\tposition:  " + u.pos[0] + " " + u.pos[1] + " " + u.pos[2] + "\n" +
			"\torientation:  " + u.quat[0] + " " + u.quat[1] + " " + u.quat[2] + " " + u.quat[3] +  "\n" +
			"\tdirection:  " + dir[0]    + " " + dir[1] + " " + dir[2] +  "\n");
        if (wp != null)
			System.out.println("\twallcoor:  " + wp[0]    + " " + wp[1]);
    }

	setChanged();
	notifyObservers(
		new ViconTrackerInfo(name, u.pos, dir, wp, u.msg_time.getTime(), _is_a_pointer));
}

    
private void logData(TrackerRemote.TrackerUpdate u) {

}

public void trackerVelocityUpdate(TrackerRemote.VelocityUpdate v, TrackerRemote tracker )
{
	System.out.println(
		"Tracker velocity message from vrpn: \n" +
		"\ttime:  " + v.msg_time.getTime( ) + "  sensor:  " + v.sensor + "\n" +
		"\tvelocity:  " + v.vel[0] + " " + v.vel[1] + " " + v.vel[2] + "\n" +
		"\torientation:  " + v.vel_quat[0] + " " + v.vel_quat[1] + " " +
		v.vel_quat[2] + " " + v.vel_quat[3] + "\n" + "\t quat dt:  " + v.vel_quat_dt);
}
	
public void trackerAccelerationUpdate(TrackerRemote.AccelerationUpdate a, TrackerRemote tracker)
{
	System.out.println(
		"Tracker acceleration message from vrpn: \n" +
		"\ttime:  " + a.msg_time.getTime( ) + "  sensor:  " + a.sensor + "\n" +
		"\tposition:  " + a.acc[0] + " " + a.acc[1] + " " + a.acc[2] + "\n" +
		"\torientation:  " + a.acc_quat[0] + " " + a.acc_quat[1] + " " + a.acc_quat[2] + " " + a.acc_quat[3] + "\n" +
		"\t quat dt:  " + a.acc_quat_dt );
}

public void setDebug(boolean v) {debug = v; }

public VRPNViconReceiver(
	String n, String host, int port, boolean viewer, boolean force, 
	WallCoordinateCalculator wcc, boolean is_a_pointer)
{
	super(); // Observable

	_wcc = wcc;
	_is_a_pointer = is_a_pointer;
	name = n;
	String trackerName = name + "@" + host;
	if (port > 0) trackerName = trackerName + ":" + port;
	System.out.println("new VRPNVICONTracker " + trackerName);
	TrackerRemote tracker = null;
	try
	{
		tracker = new TrackerRemote( trackerName, null, null, null, null );
	}
	catch( InstantiationException e )
	{
		// do something b/c you couldn't create the tracker
		System.out.println( "We couldn't connect to tracker " + trackerName + "." );
		System.out.println( e.getMessage( ) );
		throw new RuntimeException("Fail to create a vicon traker for obj." + name);
	}
	
	try {
	    Thread.sleep(100);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}

	int subcount = 0;
	while(subcount < 3)
	{
			
		if (tracker.isConnected() && tracker.isLive() && tracker.doingOkay())
		{
			System.out.println("The tracker " + trackerName + " is connected and alive");
			tracker.addPositionChangeListener( this );
			break;
		} else {
			//tracker.stopRunning();
			//tracker.shutdownTracker( );
		}
		subcount++;
		try {
		    Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	if (subcount >= 3)
	{
		System.out.println(
			"The tracker " + trackerName + " IS NOT CONNECTED | NOT ALIIIIIIVE !!!! " +
			tracker.isConnected() + " " + tracker.isLive() + " " + tracker.doingOkay());
		if (!force)
		{
			tracker.stopRunning();
			//tracker.shutdownTracker( );
			throw new RuntimeException("Fail to connect to Vicon object " + name);
		}
	}

	if (viewer)
	{
		dataViewer = ViconDataViewer.getInstance(_wcc);
		addObserver(dataViewer);
		//System.out.println("created dataViewer: " + dataViewer.toString());
	}
}
	
	
}
