package ilda.zcsample.vicon;

import ilda.zcsample.walls.*;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.geom.*;
import java.util.Hashtable;
import java.util.Observable;

/**
 * Created by can on 19/02/2014.
 */
public class MultiUserTrack2D extends JPanel {
    static double ORIGIN_X = ViconDataViewer.DEFAULT_WINDOW_SIZE.getWidth()/2;
    static double ORIGIN_Y = 35; // corresponding to the wall position and thickness
    static double VICON_DATA_RANGE_Y = 4000; // mm
    static double VICON_DATA_RANGE_X = 8000; // mm
    final static Color bg = Color.white;
    final static Color fg = Color.black;
    final static Color red = Color.red;
    final static Color white = Color.white;

    final static BasicStroke stroke = new BasicStroke(2.0f);
    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = new BasicStroke(1.0f,
            BasicStroke.CAP_BUTT,
            BasicStroke.JOIN_MITER,
            10.0f, dash1, 0.0f);
    Point2D origin;
    Hashtable<String, PersonPos> allPersons;
    Hashtable<String, PointerPos> allPointers;

    public MultiUserTrack2D(WallCoordinateCalculator wcc) {
        //Initialize drawing colors
        setBackground(bg);
        setForeground(fg);
        origin = new Point2D.Double(ORIGIN_X, ORIGIN_Y);

        allPersons= new Hashtable<String, PersonPos>();
        allPointers = new Hashtable<String, PointerPos>();

        if (wcc != null)
        {
            VICON_DATA_RANGE_Y = wcc.getWallRoomDepth();
            VICON_DATA_RANGE_X = wcc.getWallRoomWidth();
        }
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(getBackground());
        g2.fillRect(0,0,getWidth(),getHeight());
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(fg);
        g2.setStroke(stroke);

        drawWall(g2);

        for (PersonPos p:allPersons.values()) {
            p.drawHead(g2);
            p.drawOrientation(g2);
        }
        for (PointerPos pointer: allPointers.values()) {
            pointer.drawDot(g2);
            pointer.drawOrientation(g2);

            //checkIntersection(pointer);
        }

    }

    private void drawWall(Graphics2D g) {
        g.drawRect(5, 5, (int) ViconDataViewer.DEFAULT_WINDOW_SIZE.getWidth() - 10, 30);
        g.drawString("WALL", (int) ViconDataViewer.DEFAULT_WINDOW_SIZE.getWidth() / 2, 25);
    }


    private void checkIntersection(PointerPos pointerPos) {
            for (PersonPos personPos : allPersons.values()) {
                pointerPos.checkIntersection(personPos);
            }
    }


    public void positionUpdated(
        String name, double[] pos, double[] dir, boolean is_a_pointer)
    {
        if (!is_a_pointer) {
            peoplePositionUpdated(name, pos, dir);
        } else {
            pointerPositionUpdated(name, pos, dir);
        }
        repaint();
    }

    private void peoplePositionUpdated(String name, double[] pos, double[] dir) {
         if (!allPersons.containsKey(name)) {
             PersonPos newPerson = new PersonPos(name);
             allPersons.put(name, newPerson);
         }
        PersonPos thePerson = allPersons.get(name);
        thePerson.updatePosition(pos, dir);
    }

    private void pointerPositionUpdated(String name, double[] pos, double[] dir) {
        if (!allPointers.containsKey(name)) {
            PointerPos newPointer = new PointerPos(name);
            allPointers.put(name, newPointer);
        }
        PointerPos thePointer = allPointers.get(name);
        thePointer.updatePosition(pos, dir);
    }




    class PersonPos {
        private double x = origin.getX();
        private double y = origin.getY();
        private double dx;
        private double dy;
        String name;
        Ellipse2D circle;

        PersonPos(String name) {
            this.name = name;

       }

        void updatePosition(double[] pos, double[] dir) {
           // System.out.println("receive head dir data from vicon: dx " + dir[0] + " dy: " + dir[1] + "dz: " + dir[2]);
            x = scaleXToWindow(pos[0]);
            y = scaleYToWindow(pos[1]);

            dx = dir[0];
            dy = dir[1];
        }

        public void drawHead(Graphics2D g) {
            circle = new Ellipse2D.Double(x, y, 30, 30);
            g.draw(circle);
            g.drawString(name, (float)x - 15, (float)y-10);
        }

        public void drawOrientation(Graphics2D g) {

           // System.out.println("drawing line with points: " + (x + 15) + ", " + (y + 15) + ";   " + (x + dx*30) + ", " + (y - Math.sin(Math.acos(dx)))*30);

           // System.out.println("Mach.sin(Math.acos(dx))");
            if (dy >= 0) {
           // g.drawLine((int)x + 15, (int)y + 15 , (int)(x + dx*30), (int)(y - Math.sin(Math.toRadians(Math.acos(dx))))*30);
                g.drawLine((int)x + 15, (int)y + 15 , (int)(x + dx*30), (int)(y - Math.sin(Math.acos(dx))*30));
            } else {
                g.drawLine((int)x + 15, (int)y + 15 , (int)(x + dx*30), (int)(y + Math.sin(Math.acos(dx))*30));

            }

        }

        private double scaleXToWindow(double x) {
            double xAfter = - (x * ViconDataViewer.DEFAULT_WINDOW_SIZE.getWidth())/ (VICON_DATA_RANGE_X / 2) + origin.getX();
            return xAfter;
        }

        private double scaleYToWindow(double y) {
            double yAfter = (y * (ViconDataViewer.DEFAULT_WINDOW_SIZE.getHeight() - origin.getY()))/ (VICON_DATA_RANGE_Y) + origin.getY();
            return yAfter;
        }

        public Rectangle2D getArea() {
            Rectangle2D rect = circle.getBounds2D();
            return rect;
        }
    }

    private class PointerPos {
        private double x = origin.getX();
        private double y = origin.getY();
        private double dx = 0;
        private double dy = 0;
        String name;
        Line2D line;

        PointerPos(String name) {
            this.name = name;

        }

        void updatePosition(double[] pos, double[] dir) {
           // System.out.println("receive dir data from vicon: dx " + dir[0] + " y: " + dir[1] + "z: " + dir[2]);
            x = scaleXToWindow(pos[0]);
            y = scaleYToWindow(pos[1]);

            dx = dir[0];
            dy = dir[1];
        }

        public void drawDot(Graphics2D g) {
            Ellipse2D dot = new Ellipse2D.Double(x, y, 6, 6);
            g.fill(dot);
            g.drawString(name, (float)x - 15, (float)y-10);
        }

        public void drawOrientation(Graphics2D g) {
            if (dy >= 0) {
                line = new Line2D.Double((int)x + 3, (int)y + 3 , (int)(x + dx*1000), (int)(y - Math.sin(Math.acos(dx))*1000));
            }else{
                line = new Line2D.Double((int)x + 3, (int)y + 3 , (int)(x + dx*1000), (int)(y + Math.sin(Math.acos(dx))*1000));
            }
            g.draw(line);
        }

        private double scaleXToWindow(double x) {
            double xAfter = - (x * ViconDataViewer.DEFAULT_WINDOW_SIZE.getWidth())/ (VICON_DATA_RANGE_X / 2) + origin.getX();
            return xAfter;
        }

        private double scaleYToWindow(double y) {
            double yAfter = (y * (ViconDataViewer.DEFAULT_WINDOW_SIZE.getHeight() - origin.getY()))/ (VICON_DATA_RANGE_Y) + origin.getY();
            return yAfter;
        }

        public void checkIntersection(PersonPos personPos) {
            Rectangle2D rect = personPos.getArea();
            if (line.intersects(rect)) {
            }else{
            }
        }
    }
}
