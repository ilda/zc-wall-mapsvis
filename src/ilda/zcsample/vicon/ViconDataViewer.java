package ilda.zcsample.vicon;

/**
 * Created by can on 17/02/2014.
 * Small modif by olivier (observer !)
 */

import ilda.zcsample.walls.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.util.Observable;
import java.util.Observer; 

public class ViconDataViewer implements Observer
{
    public static ViconDataViewer viconDataViewer = null;
    public static ViconDataViewer getInstance(WallCoordinateCalculator wcc)
    {
        if (viconDataViewer == null) {
             viconDataViewer = new ViconDataViewer(wcc);
        }
        return viconDataViewer;
    };

    public final static Dimension DEFAULT_WINDOW_SIZE = new Dimension(1200, 650);
    MultiUserTrack2D dataView2D;

    private ViconDataViewer(WallCoordinateCalculator wcc)
    {
        JFrame frame = new JFrame("MultiUserTrack 2D");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        dataView2D = new MultiUserTrack2D(wcc);
        frame.getContentPane().add("Center", dataView2D);
        frame.pack();
        frame.setSize(DEFAULT_WINDOW_SIZE);
        frame.setVisible(true);


    }
    
    public void update(Observable obj, Object arg)
	{
		if (!(arg instanceof ViconTrackerInfo)) { return; }

		ViconTrackerInfo e = (ViconTrackerInfo)arg;
        dataView2D.positionUpdated(e.name, e.pos, e.dir, e.is_a_pointer);
    }



}

