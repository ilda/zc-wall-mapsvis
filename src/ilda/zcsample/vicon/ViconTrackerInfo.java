package ilda.zcsample.vicon;

public class ViconTrackerInfo
{
	public String name;
	public double[] pos;
	public double[] dir;
	public double[] wp;
	public long timestamp;
	public boolean is_a_pointer;
	public ViconTrackerInfo(
		String n, double[] p, double[] d, double[] w, long t, boolean iap)
	{ name = n; pos = p; dir = d; wp = w; timestamp = t; is_a_pointer = iap; }
};