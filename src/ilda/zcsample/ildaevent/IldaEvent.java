package ilda.zcsample.ildaevent;


public class IldaEvent {

public static final int MOTION=0;  // typically for cursors (not segementable)

public static final int DOWN = 1;
public static final int UP = 2;

public static final int PRESS = 3;
public static final int RELEASE = 4;

public static final int SIMPLE_TAP = 10;
public static final int TAP = 10;
public static final int CLICK = 11;

public static final int LONG_PRESS=15;

public static final int MULTI_TAP = 20;
public static final int MULTI_CLICK = 21;

public static final int RYTHMIC_TAP = 30;
public static final int RYTHMIC_CLICK = 30;

public static final int WHEEL = 90;

public static final int SEMENTIC = 99;

public static final int START_MOVE=100;
public static final int MOVE=101;
public static final int END_MOVE=102;

public static final int START_DRAG=200;
public static final int DRAG=201;
public static final int END_DRAG=202;

public static final int START_PINCH=300;
public static final int PINCH=301;
public static final int END_PINCH=302;

public static final int START_ROTATE=400;
public static final int ROTATE=401;
public static final int END_ROTATE=402;

public static final int KEY_PRESS = 1001;
public static final int KEY_LONGPRESS = 1002;
public static final int KEY_RELEASE = 1003;

public static final int VOID_END=10000;

public IldaEvent() {}

public class Base {
	public int type = 0;
	public int vId = 0;
	public int id = -1;
	public Base() {}
	public Base(int id) {this.id = id;}
	public Base(int id, int vid) {this.id = id; this.vId = vid; }
}

public class KeyPress extends Base {
	public int keyCode;
	public String keyName;

	public KeyPress(int id,  int kc, String kn, int vid) {
		super(id, vid);
		keyCode = kc;
		keyName = kn;
		type = KEY_PRESS;
	}
	public KeyPress(int id,  int kc, String kn) {
		this(id, kc, kn, 0);
	}
	public KeyPress(int kc, String kn) { 
		this(-1, kc, kn);
	}
}

public class KeyRelease extends KeyPress {
	public KeyRelease(int id,  int kc, String kn, int vid) {
		super(id, kc, kn, vid);
		type = KEY_RELEASE;		
	}
	public KeyRelease(int id,  int kc, String kn) {
		this(id, kc, kn, 0);
	}
	public KeyRelease(int kc, String kn) { 
		this(-1, kc, kn);
	}
}

class XY extends Base {
	public double x,y;
	public XY(int id, double xx, double yy, int vid) { 
		super(id,vid);
		x = xx; y = yy;
	}
	public XY(int id, double xx, double yy) { 
		super(id);
		x = xx; y = yy;
	}
	public XY(double xx, double yy) { 
		super();
		x = xx; y = yy;
	}
}

public class Motion extends XY {
	public Motion(int id, double xx, double yy, int vid) {
		super(id, xx, yy, vid);
		type = MOTION;
	}
	public Motion(int id, double xx, double yy) {
		super(id, xx, yy);
		type = MOTION;
	}
	public Motion(double xx, double yy) {
		this(-1, xx, yy);
	}
}

public class Wheel extends Motion {
	public double z;
	public Wheel(int id, double xx, double yy, double zz, int vid) { 
		super(id,xx,yy,vid);
		z = zz;
		type = WHEEL;
	}
	public Wheel(int id, double xx, double yy, double zz) { 
		this(id,xx,yy,zz,0);
	}
	public Wheel(double xx, double yy, double zz) { 
		this(-1, xx, yy, zz);
	}
}

public class SimpleTap extends XY {
	public int contacts = 1;
	public SimpleTap(int id, double xx, double yy, int c, int vid) {
		super(id, xx, yy, vid);
		contacts = c;
		type = SIMPLE_TAP;
	}
	public SimpleTap(int id, double xx, double yy, int c) {
		this(id, xx, yy, c, 0);
	}
	public SimpleTap(double xx, double yy, int c) {
		this(-1, xx, yy, c, 0);
	}
	public SimpleTap(int id, double xx, double yy) {
		this(id, xx, yy, 1, 0);
	}
	public SimpleTap(double xx, double yy) {
		this(-1, xx, yy, 1, 0);
	}
}
public class Click extends XY {
	public int button = 1;
	public Click(int id, double xx, double yy, int b, int vid) {
		super(id, xx, yy, vid);
		button= b;
		type = CLICK;
	}
	public Click(int id, double xx, double yy, int b) {
		this(id, xx, yy, b, 0);
	}
	public Click(double xx, double yy, int b) {
		this(-1, xx, yy, b, 0);
	}
	public Click(int id, double xx, double yy) {
		this(id, xx, yy, 1, 0);
	}
	public Click(double xx, double yy) {
		this(-1, xx, yy, 1, 0);
	}
}

public class LongPress extends SimpleTap {
	public LongPress(int id, double xx, double yy, int c, int vid) {
		super(id, xx, yy, c, vid);
		type = LONG_PRESS;
	}
	public LongPress(int id, double xx, double yy, int c) {
		this(id, xx, yy, c, 0);
	}
	public LongPress(double xx, double yy, int c) {
		this(-1, xx, yy, c, 0);
	}
	public LongPress(int id, double xx, double yy) {
		this(id, xx, yy, 1, 0);
	}
	public LongPress(double xx, double yy) {
		this(-1, xx, yy, 1, 0);
	}
}
public class VoidEnd extends SimpleTap {
	public VoidEnd(int id, double xx, double yy, int c, int vid) {
		super(id, xx, yy, c, vid);
		type = VOID_END;
	}
	public VoidEnd(int id, double xx, double yy, int c) {
		this(id, xx, yy, c, 0);
	}
	public VoidEnd(double xx, double yy, int c) {
		this(-1, xx, yy, c, 0);
	}
	public VoidEnd(int id, double xx, double yy) {
		this(id, xx, yy, 1, 0);
	}
	public VoidEnd(double xx, double yy) {
		this(-1, xx, yy, 1, 0);
	}
}

public class StartMove extends SimpleTap {
	public StartMove(int id, double xx, double yy, int c, int vid) {
		super(id, xx, yy, c, vid);
		type = START_MOVE;
	}
	public StartMove(int id, double xx, double yy, int c) {
		this(id, xx, yy, c, 0);
	}
	public StartMove(double xx, double yy, int c) {
		this(-1, xx, yy, c, 0);
	}
	public StartMove(int id, double xx, double yy) {
		this(id, xx, yy, 1, 0);
	}
	public StartMove(double xx, double yy) {
		this(-1, xx, yy, 1, 0);
	}
}
public class Move extends SimpleTap {
	public Move(int id, double xx, double yy, int c, int vid) {
		super(id, xx, yy, c, vid);
		type = MOVE;
	}
	public Move(int id, double xx, double yy, int c) {
		this(id, xx, yy, c, 0);
	}
	public Move(double xx, double yy, int c) {
		this(-1, xx, yy, c, 0);
	}
	public Move(int id, double xx, double yy) {
		this(id, xx, yy, 1, 0);
	}
	public Move(double xx, double yy) {
		this(-1, xx, yy, 1, 0);
	}
}
public class EndMove extends SimpleTap {
	public EndMove(int id, double xx, double yy, int c, int vid) {
		super(id, xx, yy, c, vid);
		type = END_MOVE;
	}
	public EndMove(int id, double xx, double yy, int c) {
		this(id, xx, yy, c, 0);
	}
	public EndMove(double xx, double yy, int c) {
		this(-1, xx, yy, c, 0);
	}
	public EndMove(int id, double xx, double yy) {
		super(id, xx, yy, 1, 0);
	}
	public EndMove(double xx, double yy) {
		super(-1, xx, yy, 1, 0);
	}
}


public class StartDrag extends Click {
	public StartDrag(int id, double xx, double yy, int b, int vid) {
		super(id, xx, yy, b, vid);
		type = START_DRAG;
	}
	public StartDrag(int id, double xx, double yy, int b) {
		this(id, xx, yy, b, 0);
	}
	public StartDrag(double xx, double yy, int b) {
		this(-1, xx, yy, b, 0);
	}
	public StartDrag(int id, double xx, double yy) {
		this(id, xx, yy, 1, 0);
	}
	public StartDrag(double xx, double yy) {
		this(-1, xx, yy, 1, 0);
	}
}

public class Drag extends Click {
	public double dx,dy;
	public Drag(int id, double xx, double yy, double dxx, double dyy, int b, int vid) {
		super(id, xx, yy, b, vid);
		dx = dxx; dy = dyy;
		type = DRAG;
	}
	public Drag(int id, double xx, double yy, double dxx, double dyy, int b) {
		this(id, xx, yy, dxx, dyy, b, 0);
	}
	public Drag(double xx, double yy, double dxx, double dyy, int b) {
		this(-1, xx, yy, dxx, dyy, b, 0);
	}
	public Drag(int id, double xx, double yy, double dxx, double dyy) {
		this(id, xx, yy, dxx, dyy, 1, 0);
	}
	public Drag(double xx, double yy, double dxx, double dyy) {
		this(-1, xx, yy, dxx, dyy, 1, 0);
	}
}

public class EndDrag extends Click {
	public EndDrag(int id, double xx, double yy, int b, int vid) {
		super(id, xx, yy, b, vid);
		type = END_DRAG;
	}
	public EndDrag(int id, double xx, double yy, int b) {
		this(id, xx, yy, b, 0);
	}
	public EndDrag(double xx, double yy, int b) {
		this(-1, xx, yy, b, 0);
	}
	public EndDrag(int id, double xx, double yy) {
		super(id, xx, yy, 1, 0);
	}
	public EndDrag(double xx, double yy) {
		super(-1, xx, yy, 1, 0);
	}
}

public class Pinch extends Base {
	public double cx,cy,d,a;
	public Pinch(int id, double cxx, double cyy, double dd, double aa, int vid) {
		super(id, vid);
		cx = cxx; cy = cyy; d = dd; a = aa;
		type = PINCH;
	}
	public Pinch(int id, double cxx, double cyy, double dd, double aa) {
		this(id, cxx, cyy, dd, aa, 0);
	}
	public Pinch(double cxx, double cyy, double dd, double aa) {
		this(-1, cxx, cyy, dd, aa, 0);
	}
}
public class StartPinch extends Pinch {
	public StartPinch(int id, double cxx, double cyy, double dd, double aa, int vid) {
		super(id, cxx,cyy,dd,aa,vid);
		type = START_PINCH;
	}
	public StartPinch(int id, double cxx, double cyy, double dd, double aa) {
		this(id, cxx,cyy,dd,aa,0);
	}
	public StartPinch(double cxx, double cyy, double dd, double aa) {
		this(-1,cxx,cyy,dd,aa,0);
	}
}

public class EndPinch extends Pinch {
	public EndPinch(int id, double cxx, double cyy, double dd, double aa, int vid) {
		super(id, cxx,cyy,dd,aa,vid);
		type = END_PINCH;
	}
	public EndPinch(int id, double cxx, double cyy, double dd, double aa) {
		this(id, cxx,cyy,dd,aa, 0);
	}
	public EndPinch(double cxx, double cyy, double dd, double aa) {
		this(-1,cxx,cyy,dd,aa,0);
	}
}
public class StartRotate extends Base {
	public double cx,cy,a;
	public StartRotate(int id, double cxx, double cyy, double aa, int vid) {
		super(id, vid);
		cx = cxx; cy = cyy; a = aa;
		type = START_ROTATE;
	}
	public StartRotate(int id, double cxx, double cyy, double aa) {
		this(id, cxx, cyy, aa, 0);
	}
	public StartRotate(double cxx, double cyy, double aa) {
		this(-1, cxx, cyy, aa, 0);
	}
}
public class Rotate extends Base {
	public double cx,cy,a;
	public Rotate(int id, double cxx, double cyy, double aa, int vid) {
		super(id, vid);
		cx = cxx; cy = cyy; a = aa;
		type = ROTATE;
	}
	public Rotate(int id, double cxx, double cyy, double aa) {
		this(id, cxx, cyy, aa, 0);
	}
	public Rotate(double cxx, double cyy, double aa) {
		this(-1, cxx, cyy, aa, 0);
	}
}
public class EndRotate extends Base {
	public double cx,cy,a;
	public EndRotate(int id, double cxx, double cyy, double aa, int vid) {
		super(id, vid);
		cx = cxx; cy = cyy; a = aa;
		type = END_ROTATE;
	}
	public EndRotate(int id, double cxx, double cyy, double aa) {
		this(id, cxx, cyy, aa, 0);
	}
	public EndRotate(double cxx, double cyy, double aa) {
		this(-1, cxx, cyy, aa, 0);
	}
}

}