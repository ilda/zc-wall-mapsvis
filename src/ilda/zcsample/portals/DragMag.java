package ilda.zcsample.portals;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.BasicStroke;
import java.util.Vector;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.Location;
import fr.inria.zvtm.engine.portals.CameraPortal;
import fr.inria.zvtm.engine.portals.DraggableCameraPortal;
import fr.inria.zvtm.engine.portals.RoundCameraPortal;

import fr.inria.zvtm.event.PortalListener;
import fr.inria.zvtm.event.CameraListener;

import fr.inria.zvtm.glyphs.VRectangle;
import fr.inria.zvtm.glyphs.VSegment;


// public class DragMag extends DraggableCameraPortal implements CameraListener {
public class DragMag extends CameraPortal implements CameraListener {
//public class DragMag extends RoundCameraPortal implements CameraListener {

    public static final int DM_TYPE_MANATTHAN = 0;
    public static final int DM_TYPE_INDEPENDANT = 1;
    public static final int DM_TYPE_DRAGMAG = 2;
    //
    public static float DM_VIS_LINEWIDTH = 1.0f;
    public static float DM_VIS_ = 1.0f;
    public static Color[] DM_VIS_COLORS = {Color.BLACK, Color.BLUE};
    public static float DM_VISRECT_TRANS = 0.0f;
    public static Color DM_VISRECT_COLOR = Color.BLACK;
    public static int MIN_SIZE = 20;
    private static final int DARK_COLOR_IDX = 0;
    private static final int LIGHT_COLOR_IDX = 1;

    private int type;
    private int min_size;
    private int saveDragBarHeight;
    private double scaleFactor;
 
    public final Camera extCam;
    public final VirtualSpace visSpace;

    private double _displayWidth, _displayHeight;

    private VRectangle visRect = null;
    private VRectangle[] extVisRects = null;
    private VSegment[] jointSegs = null;
    private BasicStroke bs;
    private float lineWidth;
    private Color[] visColors;
    private float visRectTrans;
    private Color visRectColor;

    private boolean visLinked;

    Vector<CameraPortal> magicPortals = new Vector<CameraPortal>();
    int numMagicPortals = 0;

    public DragMag(
        double dw, double dh,
        int x, int y, int width, int height,  Vector<Camera> cvect, Camera extc, VirtualSpace viss, double scaleFactor, int type)
    {
        super(x, y, width, height, cvect);
        this._displayWidth = dw;
        this._displayHeight = dh;
    	this.extCam = extc;
        this.visSpace = viss;
        this.scaleFactor = scaleFactor;
        this.type = type;
        if (type < 0 || type >2) { this.type = DM_TYPE_DRAGMAG; }
        if (type == DM_TYPE_MANATTHAN) {
            //d setDragBarHeight(0);
        }
        if (extCam != null){
            this.extCam.addListener(this);
        }
        lineWidth = DM_VIS_LINEWIDTH;
        this.bs = new BasicStroke(lineWidth);
        visColors = new Color[2];
        visColors[0] = DM_VIS_COLORS[0];
        visColors[1] = DM_VIS_COLORS[1];
        visRectColor = DM_VISRECT_COLOR;
        visRectTrans = DM_VISRECT_TRANS;
        visLinked = false;
        min_size = MIN_SIZE;
        // setupCamera();
    }

    public DragMag(
        double dw, double dh,
        int x, int y, int width, int height,  Camera c, Camera extc, VirtualSpace viss, double scaleFactor, int type)
    {
        //Vector<Camera> cvect = new Vector<Camera>();
        //cvect.add(c);
        this(dw, dh, x, y, width, height, new Vector<Camera>(Collections.singletonList(c)), extc, viss, scaleFactor, type);
    }

    @Override
    public void setBorder(Color bc){
        super.setBorder(bc);
        if (magicPortals == null) return;
        for (CameraPortal cp : magicPortals){
            cp.setBorder(bc);
        }
    }

    @Override
    public void setBorderWidth(float bw){
        super.setBorderWidth(bw);
        if (magicPortals == null) return;
        for (CameraPortal cp : magicPortals){
            cp.setBorderWidth(bw);
        }
    }

    public void setupCamera(){
        setupCamera(scaleFactor);
    }

    public void setupCamera(double scale_factor){
        if (extCam == null) return;
        Location cgl = extCam.getLocation();
            //Location dmloc =  super.getCamera().getLocation();
        double a = (extCam.focal + extCam.altitude) / extCam.focal;
        double pxx = cgl.getX()+ a*(x + w/2 - _displayWidth/2.0);
        double pyy = cgl.getY()+ a*(-y - h/2 + _displayHeight/2.0);
        double newz = extCam.focal * a * (1/scaleFactor) - extCam.focal;
        if (newz < 0) newz = 0.1;  // Fixed a bug by 0 -> 0.1 (do not know why !!) 
        getCamera().setLocation(new Location(pxx,pyy, newz));
        updateVis();
    }

    public void dispose(){
        if (extCam != null){
            this.extCam.removeListener(this);
        }
        if (visRect != null){
            visSpace.removeGlyph(visRect);
            visRect = null;
        }
        if (extVisRects != null){
            for(int i=0; i<2;i++){
                visSpace.removeGlyph(extVisRects[i]);
                extVisRects[i] = null;
            }
        }
        if (jointSegs != null){
            for(int i=0; i<2;i++){
                if (jointSegs[i]!=null){visSpace.removeGlyph(jointSegs[i]);jointSegs[i] = null;}
            }
        }
    }

    // ---
    public CameraPortal addMagic(Vector<Camera> cvect){
        numMagicPortals++;
        CameraPortal magicPortal = new CameraPortal(x+numMagicPortals*w,y,w,h, cvect);
        magicPortal.getCamera().setLocation(getCamera().getLocation());
        getCamera().stick(magicPortal.getCamera());
        magicPortal.setBorder(getBorder());
        magicPortal.setBorderWidth(getBorderWidth());
        //System.out.println("ADDED A DRAGMAGIC!! " + (x+w) +" "+ y +" "+ w +" "+h);
        magicPortals.add(magicPortal);
        updateVis();
        return magicPortal;
    }
    
    public void clearMagic(){
        numMagicPortals = 0;
        magicPortals.clear();
        updateVis();
    }
    public Vector<CameraPortal> getMagicPortals() { return magicPortals; }

    // ---
    public boolean getVisLinked() { return visLinked; }
    public void setVisLinked(boolean vl) { visLinked = vl; }

    public int getType() { return type; }

    public boolean isManatthan() { return (type == DM_TYPE_MANATTHAN); }

    public void setType(int t)
    {
        if (t < 0 || t > 2) t=DM_TYPE_DRAGMAG;
        if (type == t) return;

        if (type == DM_TYPE_MANATTHAN){
            type = t;
            //d setDragBarHeight(saveDragBarHeight);
            updateVis();
            return;
        }
        if (type == DM_TYPE_DRAGMAG){
            if (visRect != null){
                visSpace.removeGlyph(visRect);
                visRect = null;
            }
            if (extVisRects != null){
                for(int i=0; i<2;i++){
                    visSpace.removeGlyph(extVisRects[i]); extVisRects[i]=null;
                }
            }
            if (jointSegs != null){
                for(int i=0; i<2;i++){
                    if (jointSegs[i]!=null){visSpace.removeGlyph(jointSegs[i]);jointSegs[i] = null;}
                }
            }
        }
        if (t == DM_TYPE_MANATTHAN){
            //d saveDragBarHeight = getDragBarHeight();
            //d setDragBarHeight(0);
            type = t;
            cameraMoved(extCam, new Point2D.Double(extCam.vx, extCam.vy), extCam.altitude);
        }
        type = t;
    }

    public void updateDisplaySize(int w, int h){
            _displayWidth = w; _displayHeight = h;
            updateVis();
    }

    public void setVisLinesWidth(float lw){
        lineWidth = lw;
        bs = new BasicStroke(lineWidth);
        if (extVisRects != null){
            for(int i=0; i<2;i++){
                if (extVisRects[i]!=null){ extVisRects[i].setStroke(bs); }
            }   
        }
        if (jointSegs != null){
            for(int i=0; i<2;i++){
                if (jointSegs[i]!=null){jointSegs[i].setStroke(bs); }
            }
        }
        updateVis();
    }

    public void setVisLinesColors(Color c1, Color c2){
        visColors[0] = c1;
        visColors[1] = c2;
        updateVisColors();
    }
    public void setVisLinesLightColor(Color c){
        visColors[LIGHT_COLOR_IDX] = c;
        updateVisColors();
    }
    public void setVisLinesDarkColor(Color c){
        visColors[DARK_COLOR_IDX] = c;
        updateVisColors();
    }

    private void updateVisColors(){
        if (visRect != null){
            visRect.setBorderColor(visColors[0]);
        }
        if (extVisRects != null){
            for(int i=0; i<2;i++){
                if (extVisRects[i]!=null){ extVisRects[i].setBorderColor(visColors[i]); }
            }
        }
        if (jointSegs != null){
            for(int i=0; i<2;i++){
                if (jointSegs[i]!=null){ jointSegs[i].setColor(visColors[i]); }
            }
        }
        updateVis();
    }

    public void setVisRectAttributes(Color c, float trans){
        if (c == null) c = Color.BLACK;
        visRectColor = c;
        if (visRect == null){
            visRectTrans = trans;
        }
        else {
            visRect.setTranslucencyValue(trans);
            visRect.setColor(c);
            if (visRectTrans != 0.0f && trans == 0.0f){
                visSpace.removeGlyph(visRect);
            }
            else if (visRectTrans == 0.0f && trans != 0.0f){
                visSpace.addGlyph(visRect);
            }
            visRectTrans = trans;
            updateVis();
        }
    }

    public void updateVis(){
        if (type == DM_TYPE_MANATTHAN || type == DM_TYPE_INDEPENDANT || extCam == null){
            return;
        }
        Camera cam = super.getCamera();
        double a = (cam.focal + cam.altitude) / cam.focal;
        double exta = (extCam.focal + extCam.altitude) / extCam.focal;
        double rvx = -(extCam.vx - cam.vx)/exta;
        double rvy = -(extCam.vy - cam.vy)/exta;
        double scale = a/exta;
        //System.out.println("scale: "+ a+" "+exta+" "+scale+ " "+ (1/scaleFactor));
        double bw = (super.getBorder() == null)? 0: (borderWidth==0)? 1:borderWidth;
        double rw = (w) * scale + 0*bw/2; 
        double rh = (h) * scale + 0*bw/2;
        double[] rws = new double[2];
        double[] rhs = new double[2];
        rws[0] = rw+(double)lineWidth/2; rhs[0] = rh+(double)lineWidth/2;
        rws[1] = rws[0]+2*(double)lineWidth; rhs[1] = rhs[0]+2*(double)lineWidth;
        // a min size for rw and rh ...
        double cvx = x - _displayWidth/2 + w/2;
        double cvy = -y + _displayHeight/2 - h/2; 
        double jvx = rvx;
        double jvy = rvy;
        double jdx = 1, jdy = 1;
        double d = Math.sqrt(Math.pow(rvx - cvx, 2)+Math.pow(rvy - cvy, 2));
        if (d!=0)   {
            double t1 = Math.abs((cvx-rvx)/d);
            double t2 = Math.abs((cvy-rvy)/d);
            double tt = 1;
            if (t1>Math.sqrt(2)/2) { tt = 1; jdy = 1; } else { tt = t1/t2; jdy = 0; }
            if (rvx < cvx) { jvx = rvx+2*lineWidth+tt*rws[0]/2; }
            else if (rvx > cvx) {jvx = rvx-2*lineWidth-tt*rws[0]/2;}
            //
            if (t2>Math.sqrt(2)/2) { tt = 1; jdx = 1; } else { tt = t2/t1; jdx = 0;}
            if (rvy < cvy) {jvy = rvy+2*lineWidth+tt*rhs[0]/2;}
            else if (rvy > cvy) {jvy = rvy-2*lineWidth-tt*rhs[0]/2; }
        }   
        if (visRect==null){
            visRect = new VRectangle(rvx, rvy, 10,  rw, rh, visRectColor,  null,
                visRectTrans);
            if (visRectTrans>0.0f){
                visSpace.addGlyph(visRect);
            }
            extVisRects = new VRectangle[2];
            extVisRects[0] = new VRectangle(rvx, rvy, 10, rws[0], rhs[0], visRectColor,  visColors[0], 1.0f);
            visSpace.addGlyph(extVisRects[0]);
            extVisRects[0].setFilled(false);
            extVisRects[0].setStroke(bs);
            extVisRects[1] = new VRectangle(rvx, rvy, 10, rws[1], rhs[1], visRectColor,  visColors[1], 1.0f);
            visSpace.addGlyph(extVisRects[1]);
            extVisRects[1].setFilled(false);
            extVisRects[1].setStroke(bs);
            jointSegs = new VSegment[2];
            jointSegs[0] = new VSegment(jvx, jvy, cvx, cvy, 9, visColors[0]);
            jointSegs[1] = new VSegment(
                jvx + jdx*lineWidth, jvy + jdy*lineWidth,
                cvx + jdx*lineWidth, cvy + jdy*lineWidth, 9, visColors[1]);
            for(int i=0; i<2; i++) { jointSegs[i].setStroke(bs); visSpace.addGlyph(jointSegs[i]); }
        }
        else {
            if (visRectTrans>0.0f){
                visRect.moveTo(rvx, rvy);
                visRect.setWidth(rw);
                visRect.setHeight(rh);
            }
            extVisRects[0].moveTo(rvx, rvy);
            if (extVisRects[0].getWidth() != rws[0]) extVisRects[0].setWidth(rws[0]);
            if (extVisRects[0].getHeight() != rhs[0]) extVisRects[0].setHeight(rhs[0]);
            extVisRects[1].moveTo(rvx, rvy);
            if (extVisRects[1].getWidth() != rws[1]) extVisRects[1].setWidth(rws[1]);
            if (extVisRects[1].getHeight() != rhs[1]) extVisRects[1].setHeight(rhs[1]);
            //
            if (true){
                //System.out.println("update vis");
                Point2D.Double[] p = jointSegs[0].getEndPoints();
                if(p[0].x != jvx + jdx || p[0].y != jvy || p[1].x != cvy || p[1].y != cvy){
                    jointSegs[0].setEndPoints(jvx + jdx, jvy, cvx, cvy);
                }
                p = jointSegs[1].getEndPoints();
                jdx = jdx*(lineWidth); jdy = jdy*(lineWidth);
                if(p[0].x != jvx + jdx || p[0].y != jvy + jdy || p[1].x != cvy + jdx || p[1].y != cvy + jdy) {
                    jointSegs[1].setEndPoints(jvx + jdx, jvy + jdy, cvx + jdx, cvy + jdy);
                }
            }
        }
    }

    @Override
    public boolean coordInside(int cx, int cy){
       //return ((cx >= x) && (cx <= x+w) && (cy >= y) && (cy <= y+h));
        double ww = w;
        for (CameraPortal cp : magicPortals) { ww = ww + w; }
        //System.out.println("CI MAG: "+cx+" "+cy+" "+x+" "+y+" "+(x+ww)+" "+(y+ww)+" ");   
        return ((cx >= x) && (cx <= x+ww) &&
            (cy >= y) && (cy <= y+h));
    }

    // Window coordinates  see coordInside(int cx, int cy) for the mag
    public boolean coordInsideVis(int cx, int cy){
        if (type == DM_TYPE_MANATTHAN || type == DM_TYPE_INDEPENDANT || visRect==null) {
            return false;
        }
        double aa = 1;
        double cw = aa*((extVisRects[0].vx - extVisRects[0].vw/2 - 2*lineWidth) + _displayWidth/2.0);
        double cn = aa*(-(extVisRects[0].vy + extVisRects[0].vh/2 + 2*lineWidth) + _displayHeight/2.0);
        double ce = aa*((extVisRects[0].vx + extVisRects[0].vw/2 + 2*lineWidth) + _displayWidth/2.0);
        double cs = aa*(-(extVisRects[0].vy - extVisRects[0].vh/2+ 2*lineWidth) + _displayHeight/2.0);
        //System.out.println("CI VIS: "+cx+" "+cy+" "+cw+" "+ce+" "+cn+" "+cs+" ");
        return (cx >= cw && cx <= ce && cy >= cn && cy <= cs);
    }

    public void moveTo(int mx, int my){
        super.moveTo(mx,my);
        int i = 1; for (CameraPortal cp : magicPortals) { cp.moveTo(mx+i*w,my); i++; }
        updateVis();
    }

    public void moveTo(double mx, double my){
        double dx = mx-x; double dy = my-y;
        moveTo((int)(mx+0.5), (int)(my+0.5));
        int i = 1;
        for (CameraPortal cp : magicPortals) { cp.moveTo((int)(mx+0.5)+i*w, (int)(my+0.5)); i++; }
        if (isManatthan() && extCam != null){
            Camera dmc = super.getCamera();
            double aa = (extCam.focal+Math.abs(extCam.altitude)) / extCam.focal;
            dmc.move(aa*dx, -aa*dy);
        }
    }
    public void move(int dx, int dy){
        //System.out.println("move b: " +x+" "+y+" "+dx+" "+dy);
        super.move(dx,dy);
        int i = 1;
        for (CameraPortal cp : magicPortals) { cp.move(dx,dy); i++; }
        if (isManatthan() && extCam != null){
            Camera dmc = super.getCamera();
            double aa = (extCam.focal+Math.abs(extCam.altitude)) / extCam.focal;
            dmc.move(aa*dx, -aa*dy);
        }
        //System.out.println("move a: " +x+" "+y);
        updateVis();
    }

    public void move(double dx, double dy){
        //System.out.println("move b: " +x+" "+y+" "+dx+" "+dy);
        double xx = dx+x; double yy = dy+y;
        moveTo((int)(xx+0.5), (int)(yy+0.5));
        if (isManatthan() && extCam != null){
            Camera dmc = super.getCamera();
            double aa = (extCam.focal+Math.abs(extCam.altitude)) / extCam.focal;
            dmc.move(aa*dx, -aa*dy);
        }
    }

    public void resize(int dw, int dh){ // FIXME: min size
        if ((dw < 0 && w+dw < min_size) || (dh<0 && h+dh < min_size)) { return; }
    	super.resize(2*dw, 2*dh);
        for (CameraPortal cp : magicPortals) { cp.resize(2*dw, 2*dh); }
        super.moveTo(x-dw,y-dh);
        int i = 1;
        for (CameraPortal cp : magicPortals) { cp.moveTo(x+i*w,y); }
    	//pv.sizeTo(w, h);
        updateVis();
    }

    public double getVisX(){
        if (type == DM_TYPE_MANATTHAN || type == DM_TYPE_INDEPENDANT || extCam == null) return 0;
        Camera dmc = super.getCamera();
        double exta = (extCam.focal + extCam.altitude) / extCam.focal;
        double rvx = -(extCam.vx - dmc.vx)/exta;
        rvx = rvx + _displayWidth/2;
        return rvx; 
    }
    public double getVisY(){
        if (type == DM_TYPE_MANATTHAN || type == DM_TYPE_INDEPENDANT || extCam == null) return 0;
        Camera dmc = super.getCamera();
        double exta = (extCam.focal + extCam.altitude) / extCam.focal;
        double rvy = -(extCam.vy - dmc.vy)/exta;
        rvy = -rvy + _displayHeight/2;
        return rvy; 
    }

    public void moveToVis(double mx, double my){
        if (type == DM_TYPE_MANATTHAN || type == DM_TYPE_INDEPENDANT || extCam == null) return;
        Camera dmc = super.getCamera();
        double exta = (extCam.focal + extCam.altitude) / extCam.focal;
        double rvx = -(extCam.vx - dmc.vx)/exta;
        double rvy = -(extCam.vy - dmc.vy)/exta;
        double cvx = mx - _displayWidth/2;
        double cvy = -my + _displayHeight/2; 
        double dx = rvx - cvx;
        double dy = rvy - cvy;
        //System.out.println(rvx+" "+rvy+" "+cvx+" "+cvy+" "+a+" "+dx+" "+dy);
        moveVis(-dx, -dy);
        //updateVis();
        //if (visLinked && type == DM_TYPE_DRAGMAG){
        //    move((cgl.vx-r[0])/bb, (-cgl.vy+r[1])/bb);
        //}
    }

    public void moveVis(double dx, double dy){
        if (type == DM_TYPE_MANATTHAN || type == DM_TYPE_INDEPENDANT || extCam == null) return;
        Camera dmc = super.getCamera();
        double aa = (extCam.focal+Math.abs(extCam.altitude)) / extCam.focal;
        dmc.move(aa*dx, aa*dy);
        updateVis();
        if (visLinked && type == DM_TYPE_DRAGMAG){
            move(dx, -dy);
        }
    }

    // zoom the drag mag cam at the center of the dm
    public void zoom(double f){
        Camera dmc = super.getCamera();
        Location cgl = dmc.getLocation();
        double a = (dmc.focal + dmc.altitude) / dmc.focal;
        double newz = dmc.focal * a * f - dmc.focal;
        if (newz <= 0.0)
        {   
            newz = 0;
            f = dmc.focal/ (a * dmc.focal);
            // System.out.println("zoom / newz is <= 0, fixing f: " + f);
        }

        double newx, newy;
        newx = cgl.getX(); 
        newy = cgl.getY();
        dmc.setLocation(new Location(newx, newy, newz));
        dmc.setLocation(new Location(newx, newy, newz));
        scaleFactor = scaleFactor/f; // update for manatthan dm
        updateVis();
    }

    // window coordinate
    public void centredZoom(double f, double wx, double wy){
        Camera dmc = super.getCamera();
        Location cgl = dmc.getLocation();
        double a = (dmc.focal + dmc.altitude) / dmc.focal;
        double newz = dmc.focal * a * f - dmc.focal;
        if (newz <= 0.0)
        {   
            newz = 0;
            f = dmc.focal/ (a * dmc.focal);
            // System.out.println("centredZoom newz is <= 0, fixing f: " + f);
        }

        double[] r = _windowToViewCoordinate(dmc, wx, wy);

        double dx = cgl.getX() - r[0];
        double dy = cgl.getY() - r[1];
        double newx, newy;
        newx = cgl.getX() + (f*dx - dx);
        newy = cgl.getY() + (f*dy - dy);
        dmc.setLocation(new Location(newx, newy, newz));
        dmc.setLocation(new Location(newx, newy, newz));
        scaleFactor = scaleFactor/f; // update for manatthan dm
        updateVis();
    }

    private double[] _windowToViewCoordinate(Camera c, double wx, double wy)
    {
        Location cgl = c.getLocation();
        double a = (c.focal + c.getAltitude()) / c.focal;
        // 
        double xx = (long)((double)(wx-x) - ((double)super.w/2.0));
        double yy = (long)(-(double)(wy-y) + ((double)super.h/2.0));
        //
        xx = cgl.getX()+ a*xx;
        yy = cgl.getY()+ a*yy;
    
        double[] r = new double[2];
        r[0] = xx;
        r[1] = yy;

        return r;
    }

    private double[] _windowToViewCoordinate2(Camera c, double wx, double wy)
    {
        Location cgl = c.getLocation();
        double a = (c.focal + c.getAltitude()) / c.focal;
        // 
        double xx = (long)((double)(wx-x) - ((double)_displayWidth/2.0));
        double yy = (long)(-(double)(wy-y) + ((double)_displayHeight/2.0));
        //
        xx = cgl.getX()+ a*xx;
        yy = cgl.getY()+ a*yy;
    
        double[] r = new double[2];
        r[0] = xx;
        r[1] = yy;

        return r;
    }

    public void moveCam(double dx, double dy)
    { 
        Camera dmc = super.getCamera();
        double a = (dmc.focal+Math.abs(dmc.altitude)) / dmc.focal;
        //if (mod == SHIFT_MOD)  a = (c.focal+Math.abs(c.altitude)) / c.focal;
        dmc.move(-a*dx, -a*dy);
        updateVis();
    }

    /* Camera listener (extCam) */
    public void cameraMoved(Camera cam, Point2D.Double coord, double alt)
    {
        if (type == DM_TYPE_MANATTHAN){
            Location cgl = extCam.getLocation();
            //Location dmloc =  super.getCamera().getLocation();
            double a = (extCam.focal + extCam.altitude) / extCam.focal;
            double xx = cgl.getX()+ a*(x + w/2 - _displayWidth/2.0);
            double yy = cgl.getY()+ a*(-y - h/2 + _displayHeight/2.0);
            double newz = extCam.focal * a * (1/scaleFactor) - extCam.focal;
            if (newz < 0) newz = 0;
            getCamera().setLocation(new Location(xx,yy, newz));
        }
        else{
            updateVis();
        }
    }

}