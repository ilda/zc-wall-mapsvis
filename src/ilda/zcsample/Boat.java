package ilda.zcsample;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.List;

import fr.inria.zvtm.glyphs.BoatInfoG;

class Boat {

    // com.vividsolutions.jts.geom.Point lon/lat as a Point
    // java.lang.Double ID
    // java.lang.String Nom
    // java.lang.Double IMO
    // java.lang.Double MMSI
    // java.lang.String Callsign
    // java.lang.Double lat/Y
    // java.lang.Double lon/X
    // java.lang.String Type
    // java.lang.String Statut
    // java.lang.String Longueur
    // java.lang.Double nombre membres d'equipage
    // java.lang.Double nombre passagers
    // java.lang.String Exposition ("oui" / "non") presence dans la zone a evacuer
    // java.lang.Integer TTT_Min temps
    // java.lang.Integer TOT_HUM1
    // java.lang.Double GreenL Amplitude tsunami
    // java.lang.Double SS_GL Amplitude tsuname sans loi de Green

    String name, callsign;
    double lat, lon;
    String type, status;
    double length;
    int nb_crew, nb_passengers;
    boolean exposed;
    int tti;

    // coords in zvtm virtual space for overlay on the map
    double vx, vy;

    BoatInfoG glyph;

    Boat(double vx, double vy, List<Object> attribs){
        this.vx = vx;
        this.vy = vy;
        name = (String)attribs.get(2);
        callsign = (String)attribs.get(5);
        lat = ((Double)attribs.get(6)).doubleValue();
        lon = ((Double)attribs.get(7)).doubleValue();
        type = (String)attribs.get(8);
        status = (String)attribs.get(9);
        nb_crew = ((Double)attribs.get(11)).intValue();
        nb_passengers = ((Double)attribs.get(12)).intValue();
        exposed = ((String)attribs.get(13)).toLowerCase().equals("oui");
        tti = ((Integer)attribs.get(14)).intValue();
        glyph = new BoatInfoG(vx, vy, 11, type, name + " (" + callsign + ")",
                              nb_crew + " / " + nb_passengers, tti + " mn", exposed);
        glyph.setOwner(this);
    }

    public double distanceTo(double x, double y){
        return Math.sqrt((x-vx)*(x-vx)+(y-vy)*(y-vy));
    }

    public String toString(){
        String res = name + " ("+ callsign + ")\n";
        res += "Type: " + type + "\n";
        res += "Status: " + status  + "\n";
        res += "Crew/Passengers: " + nb_crew + " / " + nb_passengers + "\n";
        res += "Position: " + lon + " " + lat + "\n";
        res += "Time to impact (min): " + tti + "\n";
        res += "Exposed: " + exposed + "\n";
        return res;
    }

}
