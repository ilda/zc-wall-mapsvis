package ilda.zcsample.jinput;


class JEvent {
	public static final int JMOUSE_MOTION  = 1;
	public static final int JMOUSE_PRESS   =   2;
	public static final int JMOUSE_RELEASE =   3;
	public static final int JMOUSE_WHEEL   =   4;
	public static final int JMOUSE_TOUCH_DOWN   =   5;
	public static final int JMOUSE_TOUCH_UP   =     6;
	public static final int JMOUSE_TAP   =     7;
	public static final int JMOUSE_UNKOWN = 99;

	public static final int JKEY_PRESS   =       101;
	public static final int JKEY_LONGPRESS   =   102;
	public static final int JKEY_RELEASE =       103;
	public static final int JKEY_UNKOWN =       199;


	public int type;
	// mouse
	public double diff,diffx,diffy;
	public double mdx, mdy;
	public double sx, sy;
	public int button;
	// key
	public int keyCode;
	public String keyName;
	// general
	public int uid;
	public String device_name;

	public JMouse jm;
	public JKeyboard jk;

	JEvent(JMouse jm, int t, double v, int b, double sx, double sy) {
		this.jm = jm;
		uid = jm.uid;
		device_name = jm.name;
		type = t; diff= v; button = b;
		diffx = diffy = 0;
		mdx = mdy = 0; 
		this.sx = sx; this.sy = sy;
	}
	JEvent(JMouse jm, int t, double v, double sx, double sy) {
		this.jm = jm;
		uid = jm.uid;
		device_name = jm.name;
		type = t; diff= v; button = 0;
		diffx = diffy = 0;
		mdx = mdy = 0; 
		this.sx = sx; this.sy = sy;
	}
	JEvent(JMouse jm, int t, double x, double y, double sx, double sy) {
		this.jm = jm;
		uid = jm.uid;
		device_name = jm.name;
		type = t; diff= 0; button = 0;
		diffx = x; diffy = y;
		mdx = jm.mdx; mdy = jm.mdy;
		this.sx = sx; this.sy = sy;
	}
    
    JEvent(JKeyboard jk, int t, int kc, String kn) {
    	this.jk = jk;
		uid = jk.uid;
		device_name = jk.name;
		keyCode = kc;
		keyName = kn;
		type = t; diff= 0; button = 0;
		diffx = 0; diffy = 0;
		mdx = 0; mdy = 0;
		this.sx = 0; this.sy = 0;
	}

    public String toString()
    {
    	String ret = "[" + device_name + "(" + uid+ ")] type:" + type + ", button: " + button + ", diff: " + diff +
    		 ", diff x,y: " + diffx +","+ diffy + ", md x,y: " + mdx  +","+ mdy + ", screen x,y: " + sx  +","+ sy ;
    	return ret;
    }
}
