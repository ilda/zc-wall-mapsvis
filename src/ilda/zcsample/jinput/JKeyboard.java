package ilda.zcsample.jinput;


import java.lang.Thread;

import net.java.games.input.*;

import java.awt.event.KeyEvent;

import java.util.Map;
import java.util.HashMap;

class JKeyboard extends Thread
{

	protected String name;
    protected int index;
    protected int uid;
    protected int vId;
    protected Controller keyboardc;
    protected Component[] comps;
    
    protected Map<Component, String> compsNameMap;


    private JInputDevices _jiDevices;

    protected boolean capLockOn = false; // 57 "Caps Lock"
    protected boolean numLockOn = false; // 68 "Caps Lock"
    protected boolean shiftOn = false;  // 41 "Left Shift", 53 "Right Shift"
    protected boolean ctlOn = false;  // 28 "Left Control", 95 "Right Control"
    protected boolean altOn = false;    // 55 "Left Alt"
    protected boolean altGrOn = false;  // 98 "Right Alt"
    protected boolean winOn = false; // 121

    public JKeyboard(JInputDevices jids, String n, int idx, int id, int vid, Controller kc)
    {
    	_jiDevices = jids; 
	    name = n; index = idx; uid = id; vId = vid;
	    keyboardc = kc;

	    comps = keyboardc.getComponents();
	    compsNameMap =  new HashMap<Component, String>();
	    for  (int i = 0; i < comps.length; i++)
	    {
		    compsNameMap.put(comps[i], new String(comps[i].getName() + i));
	    }
    }

    public String getComponentID(Component c)
    {
	    String ret =compsNameMap.get(c);
	    return ret;
    }

    public void run()
	{
		//System.out.println("RUN: " + name);
		while(true) 
		{
			if (!keyboardc.pollForEvents())
			{
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("fail pool"+ name);
				_jiDevices.reAddKeyboard(this);
				continue;
			}
			EventQueue queue = keyboardc.getEventQueue();
			Event evt = new Event();

			while(queue.getNextEvent(evt))
			{
				int type = -1;
				//evt.getNanos()
				Component comp = evt.getComponent();
				double value = evt.getValue(); 
				int button = 0;
				String superName = getComponentID(comp);
				//System.out.print(superName+" / ");
				//System.out.println("jevt "+ comp.getName() + " "+ comp.getIdentifier() + " " +value);
				String skc = superName.substring(comp.getName().length());
				//System.out.println("skc: "+skc);
				int kc = 0;
				try{
					kc = Integer.parseInt(skc);
				}catch (Exception e) {
					//e.printStackTrace();
					kc = -1;
				}
				// FIXME keyboard mapping ... add +9 to get X Window keycode ???
				//  voitr /usr/share/X11/xkb/*
				// does not work:
				// String s = KeyEvent.getKeyText(kc); System.out.println("to text... "+s);

				type = JEvent.JKEY_RELEASE;
				if (value == 1){
					type = JEvent.JKEY_PRESS;
				}
				else if (value == 2){
					type = JEvent.JKEY_LONGPRESS;
				}
				else if (value > 2){ 
					type = JEvent.JKEY_UNKOWN;
				}
				_jiDevices.handleEvent(new JEvent(this, type, kc, comp.getName()));
			}

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} // while 
	}
}