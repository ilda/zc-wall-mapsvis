package ilda.zcsample.jinput;


import java.lang.Thread;

import net.java.games.input.*;

import java.util.Map;
import java.util.HashMap;

class JMouse extends Thread
{

	// --------------------------------------------
	// Apple magic trackpad 

	// dimension: width: 129.15 mm  height: 107.95 mm   5.125 x 4.25 inches
	// 
	//  superName interpretation one finger:
	// slider8: blob size
	// slider9: pressure/proximity 
	// slider11: x
	// slider12: y

	// slider13: ?? related to several fingers (???)
	// slider10: ?? related to several fingers pinch distance (???)

	// Unknown3: 2 fingers
	// Unknown4: 3 finger
	// Unknown5: 4 fingers

	static long AMTP_TAP_MAX_DOWN_UP_TIME = 100; // ms
	static long AMTP_TAP_Hysteresis = 50;       // humm ... time should be ok !!! pixels
	static double AMTP_TAP_MIN_PRESSURE = 0.01; 
	static double AMTP_TAP_MIN_BLOB = 0.01; 
	static double AMTP_DIFFX_TO_MM_FAC = 64.575;
	static double AMTP_DIFFY_TO_MM_FAC = 53.975;

	static long AMTP_GAINMULT = 200; 
	// --------------------------------------------

	protected String name;
    protected int index;
    protected int uid; 
    protected int vId; 
    protected Controller mousec;
    protected Component[] comps;
    
    protected Map<Component, String> compsNameMap;

    // absolute x, y  for the device (if any, e.g., AMTP)
    protected double px,py,pz;
    // last delta (in mm for absolute device) or in mickey
    protected double mdx, mdy;

    //
    
    // screen x, y
    protected double sx = 0.0, sy = 0.0;
    // one delta in screen cordinate [0,1]x[0,1]
    protected double dw =0.0, dh = 0.0;

    // for AMTP (and absolute device)
    protected int touch_state_x = 0;
    protected int touch_state_y = 0;

    protected int touch_state_tap = 0;
    protected double touch_tap_x, touch_tap_y;
    protected double pressure_tap;
    protected double blob_tap;
    protected long touch_down_time;	

    private JInputDevices _jiDevices;

    public JMouse(JInputDevices jids, String n, int idx, int id, int vid, Controller mc)
    {
    	_jiDevices = jids; 
	    name = n; index = idx; uid = id; vId = vid;
	    mousec = mc;
	    px = py = pz = 0;
	    touch_state_x = 0;
	    touch_state_y = 0;

	    comps = mc.getComponents();
	    compsNameMap =  new HashMap<Component, String>();
	    for  (int i = 0; i < comps.length; i++)
	    {
	    	//System.out.print(comps[i].getName()+", ");
		    compsNameMap.put(comps[i], new String(comps[i].getName() + i));
	    }
	    //System.out.println("");	
	    //Controller[] subConts = mc.getControllers();
	    //for  (int i = 0; i < subConts.length; i++)
	    //{
	    //	 System.out.println("SUBCONTROLERS: "+subConts[i].getName());
	    //}

    }

    public void setScreenPosition(double x, double y){
    	sx = x; sy = y;
    }
    public void setDeltaScreen(double w, double h){
    	dw = w; dh = h;
    }
    
    private void _updateScreenXY(double diffx, double diffy){
    	sx = sx + dw*diffx; sx = (sx > 1.0)? 1.0: sx;  sx = (sx < 0.0)? 0.0: sx; 
    	sy = sy + dh*diffy; sy = (sy > 1.0)? 1.0: sy;  sy = (sy < 0.0)? 0.0: sy; 
    }

    public String getComponentID(Component c)
    {
	    String ret =compsNameMap.get(c);
	    return ret;
    }

	// tunned for AMTP
	private double accel(double d)
	{
		double s = d;
		if (Math.abs(d) >= 16)
		{ 
			d = d*4;
		}
		if (Math.abs(d) >= 8)
		{ 
			d = d*3;
		}
		else if (Math.abs(d) >= 4)
		{ 
			d = d*2;
		}
		//System.out.println("accel: " + s + " -> " + d);
		return d;
	}

	Event evt = new Event();

    public void run()
	{
		//System.out.println("RUN: " + name);
		while(true) 
		{
			if (!mousec.pollForEvents())
			{
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("fail pool"+ name);
				_jiDevices.reAddMouse(this);
				continue;
			}
			else{
				// try {
				// 	Thread.sleep(10);
				// } catch (InterruptedException e) {
				// 	e.printStackTrace();
				// }
			}
			//for  (int i = 0; i < comps.length; i++)
	    	//{
	    		//float data = comps[i].getPollData();
	    		//System.out.println(comps[i].getName()+": "+data);
			//}
			EventQueue queue = mousec.getEventQueue();
			//Event evt = new Event();
			double diffx = 0, diffy = 0;
			mdx = mdy = 0;
			boolean inmotion = false;

			int ce=0;
			while(queue.getNextEvent(evt))
			{
				int type = -1;
				ce++;
				//evt.getNanos()
				Component comp = evt.getComponent();
				double value = evt.getValue(); 
				int button = 0;
				String superName = getComponentID(comp);
				//System.out.print(superName+": ");
				//System.out.println("jevt "+ comp.getName() + " " + comp.isAnalog() + " " + comp.isRelative() + " "+ value +" "+ce);
				// Left,Middle,Right  x,y,z 
				type = JEvent.JMOUSE_UNKOWN;
				if (comp.isAnalog())
				{
					if (comp.getName().equals("x"))
					{
						//System.out.println("jevt("+i+"): "+ comp.getName() + " "+ value);
						if (comp.isRelative())
						{
							mdx = mdx + value;
							if (Math.abs(value) >=3 ) value = 2*value;
							type = JEvent.JMOUSE_MOTION;
							diffx = diffx+value;
							inmotion = true;
						}
						else if (touch_state_x >= 1)
						{
							if (touch_state_x >= 2)
							{
								type = JEvent.JMOUSE_MOTION;
								mdx = mdx+(value-px)*AMTP_DIFFX_TO_MM_FAC;
								double d = (value-px)*AMTP_GAINMULT;
								d = accel(d);
								diffx = diffx + d;
								inmotion = true;
							}
							else
							{
								touch_tap_x = value;
								touch_state_x = 2;
								
							}
							px = value;
							double h = Math.abs(px - touch_tap_x)*AMTP_GAINMULT;
							//System.out.println("\tH: "+ h);
							if (touch_state_tap != 0 && h > AMTP_TAP_Hysteresis)
							{
								touch_state_tap = 0;
							}
						}
						
					}
					else if (comp.getName().equals("y"))
					{
						//System.out.println("jevt("+i+"): "+ comp.getName() + " "+ value);
						if (comp.isRelative())
						{
							mdy = mdy + value;
							if (Math.abs(value) >=3 ) value = 2*value;
							type = JEvent.JMOUSE_MOTION;
							diffy = diffy+value;
							inmotion = true;
						}
						else if (touch_state_y >= 1)
						{
							if (touch_state_y >= 2)
							{
								type = JEvent.JMOUSE_MOTION;
								mdy = (py - value)*AMTP_DIFFY_TO_MM_FAC;
								double d = (py - value)*AMTP_GAINMULT;
								d = accel(d);
								diffy = diffy - d;
								inmotion = true;
							}
							else
							{
								touch_tap_y = value;
								touch_state_y = 2;
							}
							py = value;
							double h = Math.abs(py - touch_tap_y)*AMTP_GAINMULT;
							//System.out.println("\tH: "+ h);
							if (touch_state_tap != 0 && h > AMTP_TAP_Hysteresis)
							{
								touch_state_tap = 0;
							}
							
						}
						
					}
					else if (comp.getName().equals("z"))
					{
						//System.out.println("jevt("+i+"): "+ comp.getName() + " "+ value);
						if (comp.isRelative())
						{
							_jiDevices.handleEvent(new JEvent(this, JEvent.JMOUSE_WHEEL, value, sx, sy));
						}
						else {}
					}
					else if (comp.getName().equals("slider"))
					{
						//System.out.println("jevt("+i+"): "+ superName + " "+ value);
						if (superName.equals("slider8"))
						{
							//System.out.println("jevt("+i+"): "+ superName + " "+ value);
							blob_tap = value;
						}
						if (superName.equals("slider9"))
						{
							//System.out.println("jevt("+i+"): "+ superName + " "+ value);
							pressure_tap = value;
						}
						if (superName.equals("slider13"))
						{
							//System.out.println("jevt("+i+"): "+ superName + " "+ value);
						}
						if (superName.equals("slider10"))
						{
							//System.out.println("jevt("+i+"): "+ superName + " "+ value);
						}
						//if (superName.equals("sliderX"))
						//	System.out.println("jevt("+i+"): "+ superName + " "+ value);						
					}
					else if (inmotion)
					{
						inmotion = false;
						_updateScreenXY(diffx, diffy);
						_jiDevices.handleEvent(new JEvent(this, JEvent.JMOUSE_MOTION, diffx, diffy, sx, sy));
						diffx = diffy = mdx = mdy = 0;
					}
					if (comp.getName().equals("z"))
					{
						type = JEvent.JMOUSE_WHEEL;
					}
				
				}
				else // comp.isAnalog()
				{
					if (inmotion)
					{
						inmotion = false;
						_updateScreenXY(diffx, diffy);
						_jiDevices.handleEvent(new JEvent(this, JEvent.JMOUSE_MOTION, diffx, diffy, sx, sy));
						diffx = diffy = mdx = mdy = 0;
					}
					if (comp.getName().equals("Left"))
					{
						type = (value==1.0f)? JEvent.JMOUSE_PRESS :  JEvent.JMOUSE_RELEASE;
						button = 1 ;
					}
					else if (comp.getName().equals("Right"))
					{
						type = (value==1.0f)? JEvent.JMOUSE_PRESS :  JEvent.JMOUSE_RELEASE;
						button = 3 ;
					}
					else if (comp.getName().equals("Middle"))
					{
						type = (value==1.0f)? JEvent.JMOUSE_PRESS :  JEvent.JMOUSE_RELEASE;
						button = 2 ;
					}
					else if (comp.getName().equals("Touch"))  // Finger
					{
						//System.out.println("jevt("+i+"): "+ superName + " "+ value);
						if (value == 1.0f)
						{
							type = JEvent.JMOUSE_TOUCH_DOWN;
							touch_state_x = touch_state_y = 1;
							touch_state_tap = 0;
							touch_down_time = evt.getNanos();
							if (pressure_tap >= AMTP_TAP_MIN_PRESSURE || blob_tap >= AMTP_TAP_MIN_BLOB)
							{
								touch_state_tap = 1;
								//System.out.println("\t touch state tap 1 " + pressure_tap+" "+ blob_tap);
							}
							else
							{
								//System.out.println("\t touch state tap 0 " + pressure_tap+" "+ blob_tap);
							}
						}
						else
						{
							type = JEvent.JMOUSE_TOUCH_UP;
							touch_state_x = touch_state_y = 0;
							// mightbe inconsistant (no x, y event ...)
							double hx = Math.abs(px - touch_tap_x)*AMTP_GAINMULT;
							double hy = Math.abs(py - touch_tap_y)*AMTP_GAINMULT;
							//
							long difftime = (evt.getNanos() - touch_down_time)/(1000*1000);  // to ms
							//System.out.println("jevt("+i+"): "+ superName + " v:"+ value + " tst:" + touch_state_tap + " dt:" + difftime + " " + evt.getNanos() + " " + touch_down_time + " " + hx + " " + hy);
							if (touch_state_tap >= 1 &&  difftime <= AMTP_TAP_MAX_DOWN_UP_TIME)
							{
							    _jiDevices.handleEvent(new JEvent(this, JEvent.JMOUSE_TAP, 0, 1, sx, sy));
							}
							touch_state_tap = 0;
							pressure_tap = 0;
							blob_tap = 0;
						}
					}
					else if (comp.getName().equals("Unknown"))  // nbr fingers ...
					{
						//System.out.println("jevt("+i+"): "+ superName + " "+ value);
					}
					// Unkown ...
					
				}
				
				// FIXME: add a minimal fequency for motion events ???
				if (type >= 0 && !inmotion)
				{
					//System.out.println("JMOUSE non motion event... "+type);
					_jiDevices.handleEvent(new JEvent(this, type, value, button, sx, sy));
				}
				
			}

			if (inmotion)
			{
				inmotion = false;
				_updateScreenXY(diffx, diffy);
				_jiDevices.handleEvent(new JEvent(this,  JEvent.JMOUSE_MOTION, diffx, diffy, sx, sy)); 
				diffx = diffy = mdx = mdy = 0;
			}
	
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} // while (true)
	}
}