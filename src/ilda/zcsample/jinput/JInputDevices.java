package ilda.zcsample.jinput;

import ilda.zcsample.ildaevent.IldaEvent;

import java.io.*;
import net.java.games.input.*;
import java.util.Vector;
import java.util.Observable;
import java.lang.Thread;

import java.util.Map;
import java.util.HashMap;

//public class jmice extends  Observable implements Runnable
//public class jmice implements Runnable
public class JInputDevices  extends  Observable
{

int _nb = 33;

private class localMouse {
	private boolean[] pressed = new boolean[_nb];
	private boolean[] indrag = new boolean[_nb];
	private double[] pressedX = new double[_nb];
	private double[] pressedY = new double[_nb];
	private double[] prevX = new double[_nb];
	private double[] prevY = new double[_nb];
}

private class localKeyboard {

}

// JinputDashboardInput dashboard;
ControllerEnvironment CE;
Controller[] CS; // = CE.getControllers();
private HashMap<JMouse,localMouse> _allMice;
private HashMap<JKeyboard,localKeyboard> _allKeyboards;

protected double _mickeyToScreenX = 0.0, _mickeyToScreenY = 0.0;
protected double _dragThreshold = 3;

private void printComponants(Controller c)
{
	Component[] comps = c.getComponents();
	System.out.print("\t");
	for  (int i = 0; i < comps.length; i++)
	{
		System.out.print("/C"+i+":"+comps[i].getName());	
	}
	System.out.println("");
}

public Vector<String> getAllDeviceNames()
{
	System.out.println("getAllDeviceNames with jinput...");
	
	Vector<String> ret = new Vector();
	
	//Controller[] cs = CE.getControllers();
	Controller cc = null;

	if (CS.length == 0) {
		System.out.println("No controllers found");
		return ret;
	}
	
	for (int i = 0; i < CS.length; i++)
	{
		System.out.println(i + ". " + CS[i].getName() + ", " + CS[i].getType() + ", " + CS[i].getPortNumber()  );
		if(CS[i].getType()==Controller.Type.MOUSE && CS[i].getName() != null)
		{
			System.out.println("\t It is a mouse!");
			//printComponants(CS[i]);
			ret.add(CS[i].getName());
		}
		else if (CS[i].getType()==Controller.Type.KEYBOARD && CS[i].getName() != null)
		{
			System.out.println("\t It is a keyboard!");
			//printComponants(CS[i]);
			ret.add(CS[i].getName());
		}
	}

	return ret; 
}

public Vector<String> getAllMiceNames()
{
	System.out.println("getAllMiceNames with jinput...");
	
	Vector<String> ret = new Vector();

	
	//Controller[] CS = CE.getControllers();
	Controller mousec = null;

	if (CS.length == 0) {
		System.out.println("No controllers found");
		return ret;
	}
	
	for (int i = 0; i < CS.length; i++)
	{
		System.out.println(i + ". " + CS[i].getName() + ", " + CS[i].getType() + ", " + CS[i].getPortNumber()  );
		if(CS[i].getType()==Controller.Type.MOUSE && CS[i].getName() != null)
		{
			System.out.println("\t It is a mouse!");
			//printComponants(CS[i]);
			ret.add(CS[i].getName());
		}
	}

	return ret; 
}

void reAddMouse(JMouse jm)
{
	CE.clearControllers();
	if (addMouse(jm.name, -1, jm.uid, jm.vId))
	{
		_allMice.remove(jm);
	}

}

void reAddKeyboard(JKeyboard jk)
{
	CE.clearControllers();
	addDevice(jk.name, -1, jk.uid, jk.vId);
	_allKeyboards.remove(jk);

}

public boolean addMouse(String name, int index, int uid, int vId)
{
	System.out.println("try to add mouse " + name);

	//Controller[] CS = CE.getControllers();
	Controller mousec = null;

	if (CS.length == 0) {
		System.out.println("No controllers found");
		return false;
	}
	
	// print the name and type for each controller
	int idx = -1;
	for (int i = 0; i < CS.length; i++)
	{
		idx++;
		if(CS[i].getType() == Controller.Type.MOUSE && CS[i].getName() != null && name.equals(CS[i].getName()))
		{
			if (index == i || index == -1)
			{
				mousec = CS[i];
				break;
			}
		}
	}

	boolean ret = true;

	if (mousec != null)
	{
		System.out.println("jmice find \"" + name + "\" at index " + idx);
		JMouse jm = new JMouse(this, name, idx, uid, vId, mousec);
		jm.setDeltaScreen(_mickeyToScreenX, _mickeyToScreenY);
		// _allMice.add(jm);
		_allMice.put(jm, new localMouse());
	}
	else
	{
		System.out.println("jmouse did not find \"" + name + "\"  at index " + index);
		return false;
	}

	return ret; 
}

public boolean addDevice(String name, int index, int uid, int vId)
{
	System.out.println("try to add device " + name);

	//Controller[] CS = CE.getControllers();
	Controller mkc = null;
	boolean ismouse = false;

	if (CS.length == 0) {
		System.out.println("No controllers found");
		return false;
	}
	
	// print the name and type for each controller
	int idx = -1
;	for (int i = 0; i < CS.length; i++)
	{
		idx++;
		if(CS[i].getType() == Controller.Type.MOUSE && CS[i].getName() != null && name.equals(CS[i].getName()))
		{
			if (index == i || index == -1)
			{
				mkc = CS[i];
				ismouse = true;
				break;
			}
		}
		if(CS[i].getType() == Controller.Type.KEYBOARD && CS[i].getName() != null && name.equals(CS[i].getName()))
		{
			if (index == i || index == -1)
			{
				mkc = CS[i];
				break;
			}
		}
	}

	boolean ret = true;

	if (mkc != null)
	{
		if (ismouse){
			System.out.println("JInputDevices find a mouse \"" + name + "\" at index " + idx);
			JMouse jm = new JMouse(this, name, idx, uid, vId, mkc);
			jm.setDeltaScreen(_mickeyToScreenX, _mickeyToScreenY);
			_allMice.put(jm, new localMouse());
		}
		else{
			System.out.println("JInputDevices find a keyboard \"" + name + "\" at index " + idx);
			JKeyboard jk = new JKeyboard(this, name, idx, uid, vId, mkc);
			//_allKeyboards.add(jk);
			_allKeyboards.put(jk, new localKeyboard());
			ret = false;
		}
	}
	else
	{
		System.out.println("JInputDevices did not find \"" + name + "\"  at index " + index);
		return false;
	}

	return ret; 
}

private IldaEvent ildaEvent = new IldaEvent();

void handleEvent(JEvent e)
{	
	localMouse lm = _allMice.get(e.jm);
	localKeyboard lk = _allKeyboards.get(e.jk);

	switch(e.type)
	{
	case JEvent.JMOUSE_PRESS:{
		if (e.button < _nb) {
			lm.pressed[e.button] = true;
			lm.pressedX[e.button] = e.sx;
			lm.pressedY[e.button] = e.sy;
			// send press ... ?
		}
		else{
			// more than 32 buttons ???
		}
		break;
	}
	case JEvent.JMOUSE_RELEASE:{
		if (e.button >= _nb){
			break;
		}
		// send release
		if (!lm.indrag[e.button]) {
			IldaEvent.Click ie = ildaEvent.new Click(e.uid, e.sx, e.sy, e.button);
			setChanged(); notifyObservers(ie);
		}
		else{
			IldaEvent.EndDrag ie = ildaEvent.new EndDrag(e.uid, e.sx, e.sy, e.button);
			setChanged(); notifyObservers(ie);
		}
		lm.pressed[e.button] = false;
		lm.indrag[e.button] = false;

		break;
	}
	case JEvent.JMOUSE_MOTION:{
		IldaEvent.Motion ie = ildaEvent.new Motion(e.uid, e.sx, e.sy);
		setChanged(); notifyObservers(ie);
		for(int i = 1; i < _nb; i++) {
			if (lm.indrag[i]){
				IldaEvent.Drag iee = ildaEvent.new Drag(
					e.uid, e.sx, e.sy, e.sx - lm.pressedX[i], e.sy - lm.pressedY[i], i);
				setChanged(); notifyObservers(iee);
				lm.pressedX[i] = e.sx;
				lm.pressedY[i] = e.sy;
			}
			else if (lm.pressed[i]){
				if (Math.abs(e.sx - lm.pressedX[i]) > _dragThreshold*_mickeyToScreenX ||
					Math.abs(e.sy - lm.pressedY[i]) > _dragThreshold*_mickeyToScreenY)
				{
					IldaEvent.StartDrag iee = ildaEvent.new StartDrag(e.uid, e.sx, e.sy, i);
					setChanged(); notifyObservers(iee);
					//zcse.dx = pressedX[e.button] - prevX[e.button];
					//zcse.dy = pressedY[e.button] - prevY[e.button];
					lm.indrag[i] = true;
					IldaEvent.Drag ieee = ildaEvent.new Drag(
						e.uid, e.sx, e.sy, e.sx - lm.pressedX[i], e.sy - lm.pressedY[i], i);
					setChanged(); notifyObservers(ieee);
					lm.pressedX[i] = e.sx;
					lm.pressedY[i] = e.sy;
				}
			}

		} 
		break;
	}
	case JEvent.JMOUSE_WHEEL:{
		IldaEvent.Wheel ie = ildaEvent.new Wheel(e.uid, e.sx, e.sy, e.diff);
		setChanged(); notifyObservers(ie);
		break;
	}
	case JEvent.JKEY_PRESS:{
		IldaEvent.KeyPress ie = ildaEvent.new KeyPress(e.uid, e.keyCode, e.keyName);
		setChanged(); notifyObservers(ie);
		break;
	}
	case JEvent.JKEY_RELEASE:{
		IldaEvent.KeyRelease ie = ildaEvent.new KeyRelease(e.uid, e.keyCode, e.keyName);
		System.out.println("[JInputDevices] "+ e.keyCode + " "+ e.keyName);
		setChanged(); notifyObservers(ie);
		break;
	}
	}

	
}

public void startAll()
{
	//System.out.println("allRun " + _allMice.size());
	for(Map.Entry<JMouse, localMouse> entry: _allMice.entrySet()) {
		JMouse jm = entry.getKey();
		jm.start();
	}
		for(Map.Entry<JKeyboard, localKeyboard> entry: _allKeyboards.entrySet()) {
		JKeyboard jk = entry.getKey();
		jk.start();
	}
}

public JInputDevices(double dx, double dy)
{
	super();

	_mickeyToScreenX = dx;
	_mickeyToScreenY = dy;
	_allMice = new HashMap();
	_allKeyboards = new  HashMap();
	CE = ControllerEnvironment.getDefaultEnvironment();
	CS = CE.getControllers();
}


}