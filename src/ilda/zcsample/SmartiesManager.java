package ilda.zcsample;

import java.util.Vector;
import java.util.Map;

import java.util.Observable;
import java.util.Observer; 

import java.awt.geom.Point2D;
import java.awt.Color;

import java.text.DecimalFormat;

import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;

// see:
// http://smarties.lri.fr/
// http://smarties.lri.fr/java-tutorial.html
// http://smarties.lri.fr/javadoc/index.html

import fr.lri.smarties.libserver.Smarties;
import fr.lri.smarties.libserver.SmartiesColors;
import fr.lri.smarties.libserver.SmartiesEvent;
import fr.lri.smarties.libserver.SmartiesPuck;
import fr.lri.smarties.libserver.SmartiesDevice;
import fr.lri.smarties.libserver.SmartiesWidget;
import fr.lri.smarties.libserver.SmartiesWidgetHandler;
import fr.lri.smarties.libserver.SmartiesWidgetHandler;

import ilda.zcsample.portals.DragMag;

class SmartiesManager extends BasicDevice implements Observer
{

private double _displayWidth, _displayHeight;
private ClusterDisplay _display;
private BasicDevice _SSobject;

Smarties m_smarties;

SmartiesWidget _log_widget = null;

SmartiesWidget _attach_widget = null;
SmartiesWidget _link_widget = null;

int _currentMainLayerIndex = 0;
int _currentDMLayerIndex = 0;
int _numLayers = 1;
int _numCreatBaseItems = 0;
int _numAuxLayers = 0;

float cstCDgain = 4f;
float minCDgain = 1f;
float maxCDgainFact = 1f;
int cstCDgainSliderF = 5;
int minCDgainSliderF = 10;
int maxCDgainFactSliderF = 10;
SmartiesWidget _cdgain_slider = null;
SmartiesWidget _maxcdgainfact_slider = null;
boolean accel_on = true;

SmartiesManager(ClusterDisplay cd)
{
	_display = cd;
	_displayWidth = cd.getWidth();
	_displayHeight = cd.getHeight();
	_SSobject = this;
	m_smarties = new Smarties((int)_displayWidth, (int)_displayHeight, cd.getNumCol(), cd.getNumRow());
	//m_smarties.setPureTouchpad(true);
	//m_smarties.createOnePuckByDevice(true);


	m_smarties.setAccelerationParameters(maxCDgainFact, minCDgain);

	// add some Smarties widgets into a 3 x 3 grid
	m_smarties.initWidgets(3,3);

	SmartiesWidget w = m_smarties.addWidget(
		SmartiesWidget.SMARTIES_WIDGET_TYPE_POPUPMENU, "Create a ...", 1, 1, 1, 1);
	w.handler = new createDragMagHandler();
	w.items.add("Drag Mag");
	w.items.add("Manatthan");
	w.items.add("Indep. View");

	_numCreatBaseItems = 3;
	String[] auxsn = _display.getAuxLayerNames();
	if (auxsn != null && auxsn.length >= 1){
		_numAuxLayers = auxsn.length;
		for (int i = 0; i < auxsn.length; i++){
			w.items.add("aux "+auxsn[i]);
		}
	}

	w = m_smarties.addWidget(
		SmartiesWidget.SMARTIES_WIDGET_TYPE_TOGGLE_BUTTON, "attach: off", 2, 1, 1, 1);
	w.labelOn = "attach: on";
	w.on = false;
	w.handler = new toggelAttachHandler();
	_attach_widget = w;

	w = m_smarties.addWidget(
		SmartiesWidget.SMARTIES_WIDGET_TYPE_TOGGLE_BUTTON, "link: off", 3, 1, 1, 1);
	w.labelOn = "link: on";
	w.on = false;
	w.handler = new toggelLinkHandler();
	_link_widget = w;

	String[] sn = _display.getLayerNames();
	if (sn != null && sn.length >= 2){
		_numLayers = sn.length;
		w = m_smarties.addWidget(
			SmartiesWidget.SMARTIES_WIDGET_TYPE_SPINNER, "A Spiner", 1, 2, 1, 1);
		w.handler = new setDMDefaultLayerIndexHandler();
		for (int i = 0; i < sn.length; i++){
			w.items.add(sn[i]);
		}
		w = m_smarties.addWidget(
			SmartiesWidget.SMARTIES_WIDGET_TYPE_SPINNER, "A Spiner", 1, 3, 1, 1);
		w.handler = new setMainLayerIndexHandler();
		for (int i = 0; i < sn.length; i++){
			w.items.add(sn[i]);
		}
	}

	// w = m_smarties.addWidget(
	// 	SmartiesWidget.SMARTIES_WIDGET_TYPE_SPINNER, "A Spiner", 3, 1, 1, 1);
	// w.handler = new spinerHandler();
	// w.items.add("First Item");
	// w.items.add("Second Item");
	// w.items.add("Third Item");

	// a button that map a keyboard
	//w = m_smarties.addWidget(
	//	SmartiesWidget.SMARTIES_WIDGET_TYPE_BUTTON, "Keyboard", 1, 2, 1, 1);
	//w.handler = new  keyboardButtonClicked();

	// destroyDragMagHandler
	w = m_smarties.addWidget(
		SmartiesWidget.SMARTIES_WIDGET_TYPE_BUTTON, "Destroy", 2, 2, 1, 1);
	w.handler = new destroyDragMagHandler();

	w = m_smarties.addWidget(
		SmartiesWidget.SMARTIES_WIDGET_TYPE_BUTTON, "Global View", 3, 2, 1, 1);
	w.handler = new globalViewHandler();

	String[] tsunamiDataSets = _display.getTsunamiDataSetsNames();
	if (tsunamiDataSets != null) {
		w = m_smarties.addWidget(
			SmartiesWidget.SMARTIES_WIDGET_TYPE_MULTICHOICE, "Tsunami Data", 2, 3, 1, 1);
		w.handler = new tsunamiDataHandler();
		for(String s : tsunamiDataSets){
			w.items.add(s);
			w.checked_items.add(false);
		}
	}
	else if (false) {
		w = m_smarties.addWidget(
			SmartiesWidget.SMARTIES_WIDGET_TYPE_TOGGLE_BUTTON, "accel: off", 1, 2, 1, 1);
		w.labelOn = "accel: on";
		w.on = true;
		w.handler = new toggelAccelHandler();
		w = m_smarties.addWidget(
			SmartiesWidget.SMARTIES_WIDGET_TYPE_SLIDER, "CD gain", 1, 3f, 1.5f, 1);
		w.slider_value = (int)minCDgain*minCDgainSliderF;
		_cdgain_slider = w;
		w.handler = new cdgainHandler();
		w = m_smarties.addWidget(
			SmartiesWidget.SMARTIES_WIDGET_TYPE_SLIDER, "Accel Para", 2.5f, 3f, 1.5f, 1);
		w.slider_value = (int)maxCDgainFact*maxCDgainFactSliderF;
		_maxcdgainfact_slider = w;
		w.handler = new maxCDgainFactHandler();
	}
	//if (Sample.tsunami) {
	//	w = m_smarties.addWidget(
	//		SmartiesWidget.SMARTIES_WIDGET_TYPE_TOGGLE_BUTTON, "Tsunami", 3, 3, 1, 1);
	//	w.handler = new tsunamiHandler();
	//}

	// enter inc text
	//w = m_smarties.addWidget(
	//	SmartiesWidget.SMARTIES_WIDGET_TYPE_INCTEXT_BUTTON, "Inc Enter Text", 3, 2, 1, 1);
	//w.handler = new  incEnterText();

	//
	//w = m_smarties.addWidget(
	//	SmartiesWidget.SMARTIES_WIDGET_TYPE_POPUPMENU, "Popup Menu", 1, 3, 1, 1);
	//w.handler = new  popupMenuButtonClicked();
	//w.items.add("Pop 1");
	//w.items.add("Pop 2");
	//w.items.add("Pop 3");

	//w = m_smarties.addWidget(
	//	SmartiesWidget.SMARTIES_WIDGET_TYPE_MULTICHOICE, "multichoice", 2, 3, 1, 1);
	//w.handler = new multiChoiceClicked();
	//w.items.add("Item 0 (v0)");  w.checked_items.add(false);
	//w.items.add("Item 1 (v0)");  w.checked_items.add(false);
	//w.items.add("Item 2 (v0)");  w.checked_items.add(true);
	//w.items.add("Item 3 (v0)");  w.checked_items.add(true);

	//w = m_smarties.addWidget(
	//	SmartiesWidget.SMARTIES_WIDGET_TYPE_CHECKBOX, "fake check box", 3, 2.7f, 1, 1);
	//w.handler = new  fakeCheckboxClicked();

	//w = m_smarties.addWidget(
	//	SmartiesWidget.SMARTIES_WIDGET_TYPE_SLIDER, "fake check box", 3.05f, 3.5f, 0.9f, 0.5f);
	//w.handler = new  fakeSlider();

	//_log_widget = m_smarties.addWidget(
	//	SmartiesWidget.SMARTIES_WIDGET_TYPE_TEXTVIEW, "Vicon Log", 3, 3, 1, 1);

	m_smarties.addObserver(this);
	m_smarties.Run();
	_display.registerDevice(this,"Smarties");
	System.out.println("Smarties running " + _displayWidth + " " + _displayHeight);
}

// ------------------------------------------------------------------------
// local "data" attached to a puck 

class myCursor
{

public int id;
public double x, y;
public double px, py;  // previous x,y
public boolean attached = false;
public boolean linked = false;

public void move(double x, double y)
{
	this.x = x; this.y = y;
	ClusterDisplay.zcsEvent zcse =
		_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, _SSobject, id);
	zcse.x = x; 
	zcse.y = y; 
	zcse.dx = x-px; 
	zcse.dy = y-py;
	this.px = x;
	this.py = y;
	_display.handleEvent(zcse);
}

public myCursor(int id, double x, double y)
{
	this.id = id;
	this.x = x;
	this.y = y;
	this.px = x;
	this.py = y;
	//move(x, y);
}

} // class myCursor

// ------------------------------------------------------------------------
// 

public void moveCursor(int id, double x, double y) {
	x = (x>1)? 1:x; x = (x<0)? 0:x;
    y = (y>1)? 1:y; y = (y<0)? 0:y;
    // System.out.println("Move Puck: " + id);
    m_smarties.movePuck(id, (float)x, (float)y);
}

public void setAttached(int id, boolean b) {
	System.out.println("setAttached: " + id +" "+ b);
	SmartiesPuck p = m_smarties.getPuck(id);
	if (p == null) { // WARN
		return;
	}
	SmartiesDevice dev = null;
	Map<String,SmartiesDevice> 	devmap = m_smarties.getDevicesMapping();
	for(Map.Entry<String,SmartiesDevice> entry : devmap.entrySet()){
		dev = entry.getValue();
		if (p.id == dev.getSelectedPuckID()){
			m_smarties.sendWidgetOnState(_attach_widget.uid, b, dev);
		}
	}
	 ((myCursor)p.app_data).attached = b;
}

public void setLinked(int id, boolean b) {
	System.out.println("setLinked: " + id +" "+ b);
	SmartiesPuck p = m_smarties.getPuck(id);
	if (p == null) { // WARN
		return;
	}
	SmartiesDevice dev = null;
	Map<String,SmartiesDevice> 	devmap = m_smarties.getDevicesMapping();
	for(Map.Entry<String,SmartiesDevice> entry : devmap.entrySet()){
		dev = entry.getValue();
		if (p.id == dev.getSelectedPuckID()){
			m_smarties.sendWidgetOnState(_link_widget.uid, b, dev);
		}
	}
	((myCursor)p.app_data).linked = b;
}

// ------------------------------------------------------------------------
//  check device  sendWidgetState
// sendWidgetState  SmartiesWidget.SMARTIES_WIDGET_STATE_DISABLED SmartiesWidget.SMARTIES_WIDGET_STATE_ENABLED

private void _checkWidgetState(SmartiesDevice dev, SmartiesPuck p){
	if (p==null){
		m_smarties.sendWidgetOnState(_attach_widget.uid, false, dev);
		m_smarties.sendWidgetOnState(_link_widget.uid, false, dev);
		return;
	}
	myCursor c = (myCursor)p.app_data;
	m_smarties.sendWidgetOnState(_attach_widget.uid, c.attached, dev);
	m_smarties.sendWidgetOnState(_link_widget.uid, c.linked, dev);
}

// ------------------------------------------------------------------------
// events handleing

SmartiesPuck _pinchPuck = null;
SmartiesDevice _pinchDevice = null;
SmartiesPuck _dragPuck = null;
SmartiesDevice _dragDevice = null;

float _prevMFPinchD = 0f;
float _prevMFMoveX = 0f;
float _prevMFMoveY = 0f;

public void update(Observable obj, Object arg)
{

	if (!(arg instanceof SmartiesEvent)) 
	{
		return;
	}

	SmartiesEvent e = (SmartiesEvent)arg;

	switch (e.type) {
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_CREATE:
	{
		System.out.println("Create Puck: " + e.id);
		// create the associated cursor
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, _SSobject, e.p.id);
		zcse.color = SmartiesColors.getPuckColorById(e.p.id);
		zcse.x = e.p.x; 
		zcse.y = e.p.y;
		_display.handleEvent(zcse);
		// store it ... 
		e.p.app_data = new myCursor(e.p.id, e.p.x, e.p.y);
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_SELECT:
	{
		System.out.println("Select Puck: " + e.id);
		_checkWidgetState(e.device, e.p);
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_STORE:
	{
		//repaint();
		if (e.p != null)
		{
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(
					ClusterDisplay.zcsEvent.ZCS_HIDE_CURSOR, _SSobject, e.p.id);
			_display.handleEvent(zcse);
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_UNSTORE:	
	{
		if (e.p != null)
		{
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(
					ClusterDisplay.zcsEvent.ZCS_SHOW_CURSOR, _SSobject, e.p.id);
			_display.handleEvent(zcse);
			myCursor c = (myCursor)e.p.app_data;
			c.move(e.p.x, e.p.y);
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_DELETE:
	{
		//System.out.println("Delete Puck: " + e.id);
		// add a zcs delete cursor event !
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DELETE_CURSOR, _SSobject, e.p.id);
		_display.handleEvent(zcse);
		m_smarties.deletePuck(e.p.id);
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_MULTI_TAPS:
	{
		// we support multi finger multi tap
		//System.out.println(
		//	"SMARTIE_EVENTS_TYPE_MULTI_TAPS: " + e.id + " num_fingers: " + e.num_fingers +
		//	" num_taps: " + e.num_taps + " duration: " + e.duration);
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_TAP:
	{
		// and simple tap ...
		ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CURSOR_CLICK, _SSobject, e.id);
		zcse.button = e.num_fingers;
		_display.handleEvent(zcse);
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_START_MOVE:
	{
		//System.out.println("Staring Move Puck: " + e.id + " "+ e.mode);
		if (e.p != null)
		{
			if (e.mode == SmartiesEvent.SMARTIE_GESTUREMOD_DRAG)
			{
				// drag not locked by an other device
				//System.out.println("Staring Move Puck in drag mode");
				_dragDevice = e.device;
				_dragPuck = e.p;
				_prevMFMoveX = e.x;
				_prevMFMoveY = e.y;
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_START_DRAG, _SSobject, e.id);
				zcse.x = e.x; zcse.y = e.y;	
				zcse.button = 1;
				_display.handleEvent(zcse);
			}
			else{
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_START_MOVE_CURSOR, _SSobject, e.id);
				zcse.x = e.x; zcse.y = e.y;
				_display.handleEvent(zcse);
			}
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_MOVE:
	{
		//System.out.println("Move Puck: " + e.id+" "+ e.mode);
		if (e.p != null)
		{
			myCursor c = (myCursor)e.p.app_data;
			c.move(e.p.x, e.p.y);
			if (e.mode == SmartiesEvent.SMARTIES_GESTUREMOD_DRAG)
			{
				float dx = (e.x - _prevMFMoveX);
				float dy = (e.y - _prevMFMoveY);
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DRAG, _SSobject, e.id);
				zcse.x = e.x; zcse.y = e.y;	
				zcse.dx = dx; zcse.dy = dy;
				zcse.button = 1;
				_display.handleEvent(zcse);
				_prevMFMoveX = e.x;
				_prevMFMoveY = e.y;
			}
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_END_MOVE:
	{
		if (e.p != null)
		{
			if (_dragDevice == e.device)
			{
				// this is the device that lock the drag
				_dragDevice = null;
				_dragPuck = null;
			}
			if (e.mode == SmartiesEvent.SMARTIE_GESTUREMOD_DRAG)
			{
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, _SSobject, e.id);
				zcse.x = e.x; zcse.y = e.y;	
				zcse.button = 1;
				_display.handleEvent(zcse);
			}
			else{
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_MOVE_CURSOR, _SSobject, e.id);
				zcse.x = e.x; zcse.y = e.y;
				_display.handleEvent(zcse);
			}
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_START_MFPINCH:
	{
		//System.out.println("Start MF Pinch: " + e.id);
		if (_pinchDevice == null)
		{
			// zoom not locked by an other device
			_pinchDevice = e.device;
			_pinchPuck = e.p;
			_prevMFPinchD = e.d;
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_MFPINCH:
	{
		//System.out.println("MF Pinch: " + e.id);
		if (_pinchDevice == e.device)
		{
			// this is the device that lock the zoom e.p shoul be == _pinchPuck
			// TODO: zoom centred on _pinchPuck (if null center on the center of the pinch)
			if (e.d != 0)
			{
				double x = e.x;
				double y = e.y;
				if (e.p != null)
				{
					x = e.p.x;
					y = e.p.y;
				}
				float f = _prevMFPinchD/e.d;
				ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, _SSobject, e.id);
				zcse.x = x; zcse.y = y; zcse.f = f;
				_display.handleEvent(zcse);
			}
			_prevMFPinchD = e.d;
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_END_MFPINCH:
	{
		//System.out.println("End MF Pinch: " + e.id);
		if (_pinchDevice == e.device)
		{
			// this is the device that lock the zoom
			_pinchDevice = null;
			_pinchPuck = null;
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_START_MFMOVE:
	{
		//System.out.println("Start MF Move: " + e.id);
		if (_dragDevice == null)
		{
			// drag not locked by an other device
			_dragDevice = e.device;
			_dragPuck = e.p;
			_prevMFMoveX = e.x;
			_prevMFMoveY = e.y;
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_MFMOVE:
	{
		//System.out.println("MF Move: " + e.id);
		if (_dragDevice == e.device)
		{
			// this is the device that lock the drag, e.p should be == _dragPuck
			float dx = (e.x - _prevMFMoveX);
			float dy =  (e.y - _prevMFMoveY);
			ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DRAG, _SSobject, e.id);
			zcse.x = e.x; zcse.y = e.y;
			zcse.dx = dx; zcse.dy = dy;
			zcse.button = e.num_fingers;
			zcse.contacts = 0; //e.num_fingers;
			_display.handleEvent(zcse);
			_prevMFMoveX = e.x;
			_prevMFMoveY = e.y;
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_END_MFMOVE:
	{
		//System.out.println("End MF Move: " + e.id);
		if (_dragDevice == e.device)
		{
			// this is the device that lock the drag
			_dragDevice = null;
			_dragPuck = null;
		}
		break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_KEYUP:
	{
	    //System.out.println("SMARTIE_EVENTS_TYPE_KEYUP:  keycode " + e.keycode + " pid " + e.id);
	    break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_KEYDOWN:
	{
	    //System.out.println("SMARTIE_EVENTS_TYPE_KEYDOWN: keycode " + e.keycode + " pid " + e.id);
	    break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_STRING_EDIT:
	{
	    //System.out.println("SMARTIE_EVENTS_TYPE_STRING_EDIT: string " + e.text);
	    break;
	}
	case SmartiesEvent.SMARTIE_EVENTS_TYPE_WIDGET:
	{
		//System.out.println("SMARTIE_EVENTS_TYPE_WIDGET: " + e.widget.uid);
		if (e.widget.handler != null)
		{
			e.widget.handler.callback(e.widget, e, this);
		}
		break;
	}
	default:
	{
		break;
	}
	} // ens switch

}


// ------------------------------------------------------------------------
// widgets handler ...


public class createDragMagHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("createDragMagHandler: " + w.item);
		float x = e.x, y = e.y;
		int aux_idx = -1;
		if (e.p != null){
			x = e.p.x; y = e.p.y;
		}
        else{
            x = 0.5f; y = 0.5f;
        }
		int type = DragMag.DM_TYPE_DRAGMAG; // w.item == 0
		if (w.item == 1){ type =  DragMag.DM_TYPE_MANATTHAN; }
		else if (w.item == 2){ type =  DragMag.DM_TYPE_INDEPENDANT; }
		else if (w.item > 2 && w.item <= _numCreatBaseItems + _numAuxLayers -1){
			aux_idx = w.item - _numCreatBaseItems;
			type = DragMag.DM_TYPE_INDEPENDANT;
		}
		
		if (e.p != null && ((myCursor)e.p.app_data).attached){
			System.out.println("createDragMagHandler: smarties already attached...");
			// DragMagic !!!
			if (aux_idx == -1){
				_display.addMagic(_SSobject, e.p.id);
				return true;
			}
			// FIXME what to do ?
			SmartiesPuck np = m_smarties.createPuck(e.device, x, y, true);
            np.app_data = new myCursor(np.id, x, y);
            ClusterDisplay.zcsEvent zcse =
            	_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, _SSobject, np.id);
			zcse.color = SmartiesColors.getPuckColorById(np.id);
			zcse.x = np.x; zcse.y = np.y;
			_display.handleEvent(zcse);
			e.p = np;
		}
		DragMag dm = null;
		if (aux_idx != -1){
			dm = _display.addAuxDragMagView(aux_idx, x, y);
		}
		else {
			dm = _display.addDragMagAtIndex(_currentDMLayerIndex,type, x, y);
		}
		if (dm != null && e.p != null && !((myCursor)e.p.app_data).attached){
            float sx =x, sy=y;
            if (type ==  DragMag.DM_TYPE_DRAGMAG){
            	x = x - (float)((float)2.3*dm.w/(float)(2*_display.getWidth()));
            	if (x > 0.5) { x = x + (float)((float)2.3*dm.w/(float)(2*_display.getWidth()));}
            }
            m_smarties.movePuck(e.p.id, x, y);
            // -- inputManager.attachCursorToDragMag(SmartiesManager.this, e.p.id, dm);
            ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ATTACH, _SSobject, e.p.id);
			zcse.onoff = true;
			zcse.dm = dm;
			zcse.visor = false;
			_display.handleEvent(zcse);
            // inputManager.moveCursorTo(SmartiesManager.this, e.p.id, x, y);
            myCursor c = (myCursor)e.p.app_data;
			c.move(x, y);
            if (type ==  DragMag.DM_TYPE_DRAGMAG){
                SmartiesPuck np = m_smarties.createPuck(e.device, sx, sy, true);
                np.app_data = new myCursor(np.id, sx, sy);
                // -- inputManager.createCursor(
             	//	SmartiesManager.this, np.id, np.x, np.y,
                //     SmartiesColors.getPuckColorById(np.id));

                zcse = _display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, _SSobject, np.id);
				zcse.color = SmartiesColors.getPuckColorById(np.id);
				zcse.x = np.x; zcse.y = np.y;
				_display.handleEvent(zcse);
                // -- inputManager.attachCursorToDragMag(
                //            SmartiesManager.this, np.id, dm, true);
                zcse = _display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ATTACH, _SSobject, np.id);
				zcse.onoff = true;
				zcse.dm = dm;
				zcse.visor = true;
				_display.handleEvent(zcse);
            }
        }
		return true;
	}
}

public class toggelAttachHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("toggelAttachHandler state: " + w.on);
		ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent( ClusterDisplay.zcsEvent.ZCS_ATTACH, _SSobject, e.id);
		zcse.onoff = w.on;
		_display.handleEvent(zcse);
		return true;
	}
}

public class toggelLinkHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("toggelLinkHandler state: " + w.on);
		ClusterDisplay.zcsEvent zcse =
					_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_LINK, _SSobject, e.id);
		zcse.onoff = w.on;
		_display.handleEvent(zcse);
		return true;
	}
}

public class destroyDragMagHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("destroyDragMagHandler");
		double x = e.x, y = e.y;
		if (e.p != null){
			_display.removeDragMag(e.p.x,e.p.y);
		}
		return true;
	}
}
public class globalViewHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("globalViewHandler");
		_display.globalView();
		return true;
	}
}
public class tsunamiHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("tsunamiHandler");
		_display.startTsunami();
		return true;
	}
}
public class tsunamiDataHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("tsunamiDataHandler " + w.item + " "+ w.on);
		if (w.checked_items != null && w.checked_items.size() >= 3){
			// FIXME in javaSmarties... this is not done correctly
			w.checked_items.set(w.item,  w.on);
			System.out.println("tsunamiDataHandler " + 
				w.checked_items.get(0) +" "+ w.checked_items.get(1)+" "+w.checked_items.get(2));
			_display.displayTsunamiData(
				(w.checked_items.get(0))? 1:0, (w.checked_items.get(1))? 1:0, (w.checked_items.get(2))? 1:0);
		}
		return true;
	}
}
public class setDMDefaultLayerIndexHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("setDMDefaultLayerIndexHandler: " + w.item);
		_currentDMLayerIndex = w.item;
		return true;
	}
}
public class setMainLayerIndexHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("setMainLayerIndexHandler: " + w.item);
		_currentMainLayerIndex = w.item;
		_display.setMainLayerIndex(w.item);
		return true;
	}
}

public class spinerHandler implements SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("Spinner Change items: " + w.item);
		return true;
	}
}

public class keyboardButtonClicked implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("keyboardButtonClicked");
		// map a keyboard for the device that ask ...
		m_smarties.showKeyboard(e.id, e.device); 
		return true;
	}
}

public class enterText implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("enterText get string: " + w.text + " Canceled? " + w.cancel);
		return true;
	}
}

public class incEnterText implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("incEnterText get string: " + w.text + " Canceled? " + w.cancel);
		return true;
	}
}

public class popupMenuButtonClicked implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("popupMenuButtonClicked item: " + w.item);
		return true;
	}
}

public class spinnerButtonClicked implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("testCanvas: spinnerButtonClicked item: " + w.item);
		return true;
	}
}

public class multiChoiceClicked implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println(
			"multiChoiceClicked item: " + w.item + " " + w.checked_items.get(w.item));
		return true;
	}
}

public class fakeCheckboxClicked implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("fakeCheckboxClicked " + w.on);
		return true;
	}
}

public class fakeSlider implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("fakeSlider " + w.slider_value);
		return true;
	}
}
public class cdgainHandler implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("cd gain slider value " + w.slider_value);
		if (accel_on){
			minCDgain = (float)w.slider_value/(float)minCDgainSliderF;
			m_smarties.setAccelerationParameters(maxCDgainFact, minCDgain);
			System.out.println("min cdgain: " + minCDgain + " max cdgain fact " +  maxCDgainFact);
		}
		else{
			cstCDgain = (float)w.slider_value/(float)cstCDgainSliderF;
			m_smarties.setConstantCDgain(cstCDgain);
			System.out.println("cst cd gain: " + cstCDgain);
		}
		return true;
	}
}
public class maxCDgainFactHandler implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("accel range slider values " + w.slider_value);
		if (accel_on){
			maxCDgainFact = (float)w.slider_value/(float)maxCDgainFactSliderF;
			m_smarties.setAccelerationParameters(maxCDgainFact, minCDgain);
			System.out.println("min cdgain: " + minCDgain + " max cdgain fact " +  maxCDgainFact);
		}
		else{
			m_smarties.sendWidgetValue(_maxcdgainfact_slider.uid,  (int)(maxCDgainFact*maxCDgainFactSliderF));
			System.out.println("cst cdgain on slider not usefull");
		}
		return true;
	}
}

public class toggelAccelHandler implements  SmartiesWidgetHandler
{
	public boolean callback(SmartiesWidget w, SmartiesEvent e, Object user_data)
	{
		System.out.println("accel:  " + w.on);
		if (w.on){
			m_smarties.setAccelerationParameters(maxCDgainFact, minCDgain);
			m_smarties.sendWidgetValue(_maxcdgainfact_slider.uid,  (int)(maxCDgainFact*maxCDgainFactSliderF));
			m_smarties.sendWidgetValue(_cdgain_slider.uid,  (int)(minCDgain*minCDgainSliderF));
			System.out.println("min cdgain: " + minCDgain + " max cdgain fact " +  maxCDgainFact);
			accel_on = true;
		}
		else {
			m_smarties.setConstantCDgain(cstCDgain);
			m_smarties.sendWidgetValue(_cdgain_slider.uid,  (int)(cstCDgain*cstCDgainSliderF));
			System.out.println("cst cd gain: " + cstCDgain);
			accel_on = false;
		}
		return true;
	}
}
// -----------------------------------------------------------
// widget logger

private double mypos[] = new double[3];
private String log_name = null;
private long prev_ct = 0;

public void showObjectPosition(String name, double[] pos)
{
	if (_log_widget == null)
	{
		return;
	}
	if (log_name == null) { log_name = name; }
	if (!log_name.equals(name)) { return; }

	DecimalFormat df = new DecimalFormat("0.0000");
	String str = log_name;
	for(int i = 0; i < pos.length && i < 3; i++)
	{
		str = str +" "+ df.format(pos[i]);
		mypos[i] = pos[i];
	}
	// not too often ... 

	//System.out.println(str);
	long ct = System.currentTimeMillis();
	if (ct - prev_ct > 500)
	{	
		prev_ct = ct;
		m_smarties.sendWidgetLabel(_log_widget.uid, str);
	}
}
}
