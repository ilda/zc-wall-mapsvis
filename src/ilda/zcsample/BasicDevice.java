package ilda.zcsample;

public abstract class BasicDevice {
	public abstract void moveCursor(int id, double x, double y); // {}
	public abstract void setAttached(int id, boolean b); // {}
	public abstract void setLinked(int id, boolean b); // {}
	public void inObject(int id, boolean b) {}
}