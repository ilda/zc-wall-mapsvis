package ilda.zcsample;


import ilda.zcsample.jinput.JInputDevices;
import ilda.zcsample.ildaevent.IldaEvent;

import java.util.Observer;
import java.util.Observable;

import java.awt.Color;

import java.util.Vector;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;

class JInputManager implements Observer
{
private ClusterDisplay _display;
protected JInputDevices _devices; 
protected double _mickeyToScreenX, _mickeyToScreenY;
protected double _dragThreshold = 3;

JInputManager(ClusterDisplay cd, String confFileName){
	this(cd, confFileName, false);
}

 JInputManager(ClusterDisplay cd, String confFileName, boolean print_devices)
 {
 	_display = cd;
 	_mickeyToScreenX = 1.0/cd.getWidth();
 	_mickeyToScreenY = 1.0/cd.getHeight();

 	_devices = new JInputDevices(_mickeyToScreenX, _mickeyToScreenY);

 	if (print_devices && confFileName != null){
		_devices.getAllDeviceNames();
	}

 	// "GlidePoint Virtual Touchpad"  
 	// "Logitech USB Laser Mouse"

 	Vector<String> allDeviceNames;
	Vector<Integer> allDeviceVId = null;

	if (confFileName == null)
	{
		allDeviceNames = _devices.getAllDeviceNames();
	}
	else
	{
		allDeviceNames = new Vector<String>();
		allDeviceVId = new Vector<Integer>();
		File f = new File(confFileName);
		if (!f.exists() || !f.canRead() || f.isDirectory()) {
			System.out.println("[JInputDashboard] Error, impossible to read jinput conf file " + confFileName);
			return;
		}
		// parse
		FileReader fr;
		BufferedReader in;
		try {
			fr = new FileReader(f);
			in = new BufferedReader(fr);
			String line;
			while ((line = in.readLine()) != null)
			{
				if (line.startsWith("#")) {continue; }
				String[] sl = line.split(";"); 
				allDeviceNames.add(sl[0]);
				if (sl.length > 1){
					try {
						allDeviceVId.add(Integer.parseInt(sl[1]));
					}
					catch(Exception ex){
						System.out.println("[JInputManager] Error -- invalide conf file");
						allDeviceVId.add(0);
					}
				}
				else{
					allDeviceVId.add(0);
				}
			}
			in.close();
			fr.close();
		} catch (FileNotFoundException e) {
			System.out.println("[JInputManager] Error -- file has disappear");
		} catch (IOException e) {
			System.out.println(
				"[JInputManager] Error -- IOException when parsing ");
		}
	}

	if (allDeviceNames == null || allDeviceNames.size() <= 0){
		System.out.println("[JInputManager] Warning -- No device found");
		return;
	}

	_display.registerDevice(this, "JInputManager");

 	int uid = 0;

 	// here, all keyboards before a mouse have the same id of this mouse
	for (int i= 0; i < allDeviceNames.size(); i++)
	{
		int vId = 0;
		if (allDeviceVId != null && allDeviceVId.size() > i){
			vId = allDeviceVId.get(i);
		}
		boolean r = _devices.addDevice(allDeviceNames.get(i),-1,uid,vId);
		if (r)
		{
			ClusterDisplay.zcsEvent zcse = _display.new zcsEvent(
					ClusterDisplay.zcsEvent.ZCS_CREATE_CURSOR, this, uid);
 			zcse.color = Color.MAGENTA;
 			zcse.vId = vId;
			_display.handleEvent(zcse);
			uid++;
		}
	}

	// listen to the mice
 	_devices.addObserver(this);
 	
 	// start all mice
 	_devices.startAll();

 }

public void update(Observable obj, Object arg)
{

	if (!(arg instanceof IldaEvent.Base)) return;

	IldaEvent.Base e = (IldaEvent.Base)arg;

	// System.out.println(e.toString());
	// FIXME fwd what needed to the vicon

	switch(e.type){
		case IldaEvent.MOTION:{
			IldaEvent.Motion ee = (IldaEvent.Motion)e;
			ClusterDisplay.zcsEvent zcse = _display.new zcsEvent(
					ClusterDisplay.zcsEvent.ZCS_MOVE_CURSOR, this, ee.id);
			// zcse.dx = e.sx - prevX[e.button]; zcse.dy = e.sy - prevY[e.button];
			zcse.x = ee.x; zcse.y = ee.y;
			_display.handleEvent(zcse);
			break;
		}
		case IldaEvent.CLICK: {
			IldaEvent.Click ee = (IldaEvent.Click)e;
			//System.out.println("JInputManager CLICK");
			ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_CURSOR_CLICK, this, ee.id);
			zcse.x = ee.x; zcse.y = ee.y;
			zcse.button = ee.button;
			_display.handleEvent(zcse);
			break;
		}
		case IldaEvent.START_DRAG: {
			IldaEvent.StartDrag ee = (IldaEvent.StartDrag)e;
			//System.out.println("JInputManager START_DRAG");
			ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_START_DRAG, this, ee.id);
			zcse.x = ee.x; zcse.y = ee.y;
			zcse.button = ee.button;
			_display.handleEvent(zcse);
			break;
		}
		case IldaEvent.DRAG: {
			IldaEvent.Drag ee = (IldaEvent.Drag)e;
			//System.out.println("JInputManager DRAG");
			ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_DRAG, this, ee.id);
			zcse.dx = ee.dx; zcse.dy = ee.dy;
			zcse.x = ee.x; zcse.y = ee.y;
			zcse.button = ee.button;
			_display.handleEvent(zcse);
			break;
		}
		case IldaEvent.END_DRAG: {
			IldaEvent.EndDrag ee = (IldaEvent.EndDrag)e;
			//System.out.println("JInputManager END_DRAG");
			ClusterDisplay.zcsEvent zcse =
			_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_END_DRAG, this, ee.id);
			zcse.x = ee.x; zcse.y = ee.y;
			zcse.button = ee.button;
			_display.handleEvent(zcse);
			break;
		}
		case IldaEvent.WHEEL:{
			IldaEvent.Wheel ee = (IldaEvent.Wheel)e;
			ClusterDisplay.zcsEvent zcse =
				_display.new zcsEvent(ClusterDisplay.zcsEvent.ZCS_ZOOM, this, ee.id);
			zcse.x = ee.x; zcse.y = ee.y;
			if (ee.z < 0){
				zcse.f = -ee.z*Dashboard.WHEEL_ZOOMIN_FACTOR;
			}
			else {
				zcse.f = ee.z*Dashboard.WHEEL_ZOOMOUT_FACTOR;
			}
			_display.handleEvent(zcse);
			break;
		}
		case IldaEvent.KEY_RELEASE:{
			IldaEvent.KeyRelease ee = (IldaEvent.KeyRelease)e;
			System.out.println("[JInputManager] KeyRelease:"+ ee.keyCode + " "+ ee.keyName);
			if (ee.keyName.equals("A")){
				_display.stop();
				System.exit(0);
			}
			break;
		}
	}

}

}