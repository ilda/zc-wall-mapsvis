package ilda.zcsample;


import java.util.Vector;

import java.awt.Color;

import fr.inria.zvtm.cluster.ClusterGeometry;
import fr.inria.zvtm.cluster.ClusteredView;
import fr.inria.zvtm.engine.Camera;
import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.VirtualSpaceManager;
import fr.inria.zvtm.engine.Location;

import fr.inria.zvtm.glyphs.Glyph;
import fr.inria.zvtm.glyphs.VCircle;
import fr.inria.zvtm.glyphs.VRectangle;
import fr.inria.zvtm.glyphs.VRing;
import fr.inria.zvtm.glyphs.VRectangleOr;
import fr.inria.zvtm.glyphs.VSegment;

public class CirclesRenderer extends Renderer {

double [] bounds;

public CirclesRenderer(ClusterDisplay cd)
{
	super(cd);
	bounds = super.getBounds();
}

@Override
public void initRendering()
{
	// draw some stuff...
	double rw = _displayWidth/100;
	double rh = rw; //_displayHeight/100;

	// tr
	VRectangle rect = new VRectangle(-_bezWidth + _displayWidth/2, -_bezHeight + _displayHeight/2, 0, rw, rh, Color.GREEN, Color.BLACK);
	rect.setDrawBorder(true);
	_vs.addGlyph(rect);

	// br
	rect = new VRectangle(-_bezWidth + _displayWidth/2, _bezHeight - _displayHeight/2, 0, rw, rh, Color.BLUE, Color.BLACK);
	rect.setDrawBorder(true);
	_vs.addGlyph(rect);

	// bl
	rect = new VRectangle(_bezWidth - _displayWidth/2, _bezHeight - _displayHeight/2, 0, rw, rh, Color.YELLOW, Color.BLACK);
	rect.setDrawBorder(true);
	_vs.addGlyph(rect);

	// tl
	rect = new VRectangle(_bezWidth - _displayWidth/2, -_bezHeight + _displayHeight/2, 0, rw, rh, Color.WHITE, Color.BLACK);
	rect.setDrawBorder(true);
	_vs.addGlyph(rect);

	rect = new VRectangle(0, 0, 0, rw, rh, Color.RED, Color.BLACK);
	rect.setDrawBorder(true);
	_vs.addGlyph(rect);

	VRectangleOr rector = new VRectangleOr(_displayWidth*0.1, _displayHeight*0.05, 1, _displayWidth, _displayHeight*0.05, Color.RED, Color.WHITE, 50.0);
	_vs.addGlyph(rector);

	VSegment testseg = new VSegment(- _displayWidth*0.1, -_displayHeight*0.05,  _displayWidth*0.1, _displayHeight*0.15, 2, Color.BLUE);
	_vs.addGlyph(testseg);
	testseg.setEndPoints(- _displayWidth*0.1, -_displayHeight*0.05,  _displayWidth*0.1, _displayHeight*0.15);

	int numl = 2;
	int numc = 2;
	double step = (_displayWidth-2*_bezWidth)/(2*10);
	double distf = 1.5;
	double diststepW = distf*_displayWidth;
	double diststepH = distf*_displayWidth;
	for (int l=-numl+1; l<numl; l++){
		for (int c=-numc+1; c<numc; c++){
			VRing ring;
			
			for (int i = 0; i < 10; i++)
			{
				//VRing(double x, double y, int z, double vs, double ag, float irr, double or, Color c, Color bc)
				double radius = step*(i+1);
				float p = (float)((radius-rw)/radius);
				ring = new VRing(diststepW*c, diststepH*l, 0, radius, 2*Math.PI, p, 0, Color.YELLOW, Color.BLACK, .5f);
				_vs.addGlyph(ring);
			}
		}
	}
	
	bounds[0] = -(numl-1)*diststepW - step*10; // west
	bounds[1] = (numc-1)*diststepH + step*10; // north
	bounds[2] = - bounds[0]; // east 
	bounds[3] = - bounds[1]; // south

	randomTarget();
}

@Override
public double[] getBounds()
{
	return bounds;
}

@Override
public boolean click(double x, double y)
{

	if (Math.abs(rdX-x) < rdRadius && Math.abs(rdY-y) < rdRadius)
	{
		randomTarget();
		return true;
	}
	return false;
}

private double rdRadius = 0;
private double rdX = 0;
private double rdY = 0;

private VCircle rdCircle = null;

private void randomTarget()
{
	rdX = Math.random()*(_displayWidth*0.8) + _displayWidth*0.1;
	rdY = Math.random()*(_displayHeight*0.8) + _displayHeight*0.1;
	rdX = rdX - _displayWidth/2;
	rdY = - rdY + _displayHeight/2;

	rdRadius =  Math.random()*(_displayHeight*0.01) + _displayHeight*0.01;

	System.out.println("randomTarget: "+ rdX +" "+ rdY +" "+ rdRadius);

	if (rdCircle != null)
	{
		rdCircle.moveTo(rdX, rdY);
		rdCircle.sizeTo( 2*rdRadius);
		//_vs.removeGlyph(rdCircle);
		//rdCircle = null;
	}

	if (rdCircle == null)
	{
		rdCircle = new VCircle(rdX, rdY, 1, 2*rdRadius, Color.GREEN);
		_vs.addGlyph(rdCircle);
	}
}

}
