package ilda.zcsample.filters;

import java.awt.geom.Point2D;

public class OneEuroPointerFilter  extends PointerFilter
{

OneEuroFilter _fx;
OneEuroFilter _fy;

public OneEuroPointerFilter(double freq, double mincutoff, double beta, double dcutoff)
{
	super(1);
	_fx = new OneEuroFilter(freq, mincutoff, beta, dcutoff);
	_fy = new OneEuroFilter(freq, mincutoff, beta, dcutoff);
}

@Override
public Point2D.Double filter(Point2D.Double c, Point3D ipos, long time)
{
	Point2D.Double cur = new Point2D.Double();

	cur.x = _fx.filter(c.x);
	cur.y = _fy.filter(c.y);

	return cur;
}

}