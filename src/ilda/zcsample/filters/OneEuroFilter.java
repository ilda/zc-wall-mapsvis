package ilda.zcsample.filters;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s. conversy from n. roussel c++ version
 */
class LowPassFilter {

    double y, a, s;
    boolean initialized;

    void setAlpha(double alpha) {
        if (alpha <= 0.0 || alpha > 1.0) {
             System.out.println("alpha should be in (0.0., 1.0], set it to 0.5 (it was "+alpha+")");
            //alpha = 0.0;
            alpha = 0.5;
        }
        a = alpha;
    }

    public LowPassFilter(double alpha) {
        init(alpha, 0);
    }

    public LowPassFilter(double alpha, double initval) {
        init(alpha, initval);
    }

    private void init(double alpha, double initval) {
        y = s = initval;
        setAlpha(alpha);
        initialized = false;
    }

    public double filter(double value) {
        double result;
        if (initialized) {
            result = a * value + (1.0 - a) * s;
        } else {
            result = value;
            initialized = true;
        }
        y = value;
        s = result;
        return result;
    }

    public double filterWithAlpha(double value, double alpha) {
        setAlpha(alpha);
        return filter(value);
    }

    public boolean hasLastRawValue() {
        return initialized;
    }

    public double lastRawValue() {
        return y;
    }
};

public class OneEuroFilter {

    double freq;
    double mincutoff;
    double beta_;
    double dcutoff;
    LowPassFilter x;
    LowPassFilter dx;
    double lasttime;
    static double UndefinedTime = -1;

    double alpha(double cutoff) {
        double te = 1.0 / freq;
        double tau = 1.0 / (2 * Math.PI * cutoff);
        return 1.0 / (1.0 + tau / te);
    }

    public void setFrequency(double f) {
        if (f <= 0) {
            System.out.println("freq should be >0, set it to 60");
	       f = 60;
        }
        freq = f;
    }

    public void setMinCutoff(double mc) {
        if (mc <= 0) {
            System.out.println("mincutoff should be >0, set it to 1.0");
	       mc = 1.0;
        }
        System.out.println("setMinCutoff: "+mc);
        mincutoff = mc;
        x = new LowPassFilter(alpha(mincutoff));
        dx = new LowPassFilter(alpha(dcutoff));
        lasttime = UndefinedTime;
    }

    public double getMinCutoff() { return mincutoff; }

    public void setBeta(double b) {
        beta_ = b;
        System.out.println("setBeta: "+ beta_);
        x = new LowPassFilter(alpha(mincutoff));
        dx = new LowPassFilter(alpha(dcutoff));
        lasttime = UndefinedTime;
    }

    public double getBeta() { return beta_; }

    public void setDerivateCutoff(double dc) {
        if (dc <= 0) {
            System.out.println("dcutoff should be >0, set it to 1.0 (it was "+dc+")");
            dc = 1.0;
        }
        dcutoff = dc;
    }

    public OneEuroFilter(double freq) {
        init(freq, 1.0, 0.0, 1.0);
    }

    public OneEuroFilter(double freq, double mincutoff) {
        init(freq, mincutoff, 0.0, 1.0);
    }

    public OneEuroFilter(double freq, double mincutoff, double beta_) {
        init(freq, mincutoff, beta_, 1.0);
    }

    public OneEuroFilter(double f, double mco, double b, double dco) {
        init(f, mco, b, dco);
    }

    private void init(double f, double minco, double b, double dco)
    {
	    this.freq = f; //setFrequency(f);
	    this.mincutoff = minco; //setMinCutoff(minco);
	    beta_ = b; //setBeta(b);
	    setDerivateCutoff(dco);
	    x = new LowPassFilter(alpha(mincutoff));
	    dx = new LowPassFilter(alpha(dcutoff));
	    lasttime = UndefinedTime;
    }

    public double filter(double value) {
        return filter(value, UndefinedTime);
    }

    public double filter(double value, double timestamp) {
        // update the sampling frequency based on timestamps
        if (lasttime != UndefinedTime && timestamp != UndefinedTime) {
            freq = 1.0 / (timestamp - lasttime);
        }
        
        lasttime = timestamp;
        // estimate the current variation per second
        double dvalue = x.hasLastRawValue() ? (value - x.lastRawValue()) * freq : 0.0; // FIXME: 0.0 or value?
        double edvalue = dx.filterWithAlpha(dvalue, alpha(dcutoff));
        // use it to update the cutoff frequency
        double cutoff = mincutoff + beta_ * Math.abs(edvalue);
        // filter the given value
        return x.filterWithAlpha(value, alpha(cutoff));
    }

    public static void main(String[] args) {
        //randSeed();
        double duration = 10.0; // seconds
        double frequency = 120; // Hz
        double mincutoff = 1.0; // FIXME
        double beta = 1.0;      // FIXME
        double dcutoff = 1.0;   // this one should be ok

        System.out.print(
                "#SRC OneEuroFilter.java" + "\n"
                + "#CFG {'beta': " + beta + ", 'freq': " + frequency + ", 'dcutoff': " + dcutoff + ", 'mincutoff': " + mincutoff + "}" + "\n"
                + "#LOG timestamp, signal, noisy, filtered" + "\n");

        OneEuroFilter f = new OneEuroFilter(frequency,
                mincutoff,
                beta,
                dcutoff);
        for (double timestamp = 0.0; timestamp < duration; timestamp += 1.0 / frequency) {
            double signal = Math.sin(timestamp);
            double noisy = signal + (Math.random() - 0.5) / 5.0;
            double filtered = f.filter(noisy, timestamp);
            System.out.println("" + timestamp + ", "
                    + signal + ", "
                    + noisy + ", "
                    + filtered);
        }
    }
}
