package ilda.zcsample.filters;

import java.awt.geom.Point2D;

public class MeanPointerFilter extends PointerFilter
{

public double _radius = 1.0; 

public MeanPointerFilter(int n, double r)
{ 
	super(n);
	_radius = r; 
	System.out.println(" MeanPointerFilter "+ n+" "+ _radius);
}

public MeanPointerFilter(int n)
{ 
	super(n);
	System.out.println(" MeanPointerFilter0 "+n+" "+ _radius);
}

@Override
public Point2D.Double filter(Point2D.Double c, Point3D ipos, long time)
{
	_store(c,ipos,time);

	Point2D.Double cur = new Point2D.Double();
	int div = 0;
	double dist = 0;
	for(int i = 0; i < _num_data; i++)
	{
		//if (i > 0) dist = dist + _prevs[i-1].cur.distance(_prevs[i].cur);
		//if (dist > 0.01*_num_points) break;
		if (i > 0) dist = _prevs[0].cur.distance(_prevs[i].cur);
		if (dist > _radius) break;
		cur.x = cur.x + _prevs[i].cur.x;
		cur.y = cur.y + _prevs[i].cur.y;
		div++;
	}
	cur.x = cur.x/div;
	cur.y = cur.y/div;
	//System.out.print(div+" ");
	return cur;
}

}