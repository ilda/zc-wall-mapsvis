package ilda.zcsample.filters;

import java.awt.geom.Point2D;
import java.text.DecimalFormat;

public class PointerFilter
{

int _num_points = 0;

class TimedValue {
	Point2D.Double cur;
	Point3D ipos;
	long t;
	TimedValue() {cur = new Point2D.Double(); ipos = new Point3D(); t=0;}
}

TimedValue _prevs[];
int _num_data = 0;

public PointerFilter(int num_points)
{
	_num_points = num_points;

	_prevs = new TimedValue[_num_points];
	for(int i = 0 ; i < _num_points; i++)
	{
		_prevs[i] = new TimedValue();
	}
	//System.out.println("PointerFilter num points: "+ _num_points + " "+ _prevs.length);

	_num_data = 0;
}



public void setSmothingNumPoints(int n)
{
	if (n < 2) n=2;

	TimedValue[] p = new TimedValue[n];
	for(int i= 0; i < n; i++)
	{
		if (i < _prevs.length) p[i] = _prevs[i];
	}
	
	for(int i= _prevs.length; i < n; i++) p[i] =  new TimedValue();

	_prevs = p;
	_num_points = n;
	_num_data = Math.min(_num_points, _num_data);
}

public void printPoints()
{
	DecimalFormat df = new DecimalFormat("0.0000");
	System.out.print("Points("+_num_data +"): ");
	for(int i = 0; i < _num_data; i++)
	{
		System.out.print(df.format(_prevs[i].cur.x) +","+df.format(_prevs[i].cur.y) +" ");
	}
	System.out.println("");
}

void _store(Point2D.Double c, Point3D ipos, long time)
{
	for(int i = _num_data-1 ; i > 0; i--)
	{
		_prevs[i].cur.x = _prevs[i-1].cur.x;
		_prevs[i].cur.y = _prevs[i-1].cur.y;
	}

	//System.out.println("LEBGTH" + _prevs.length + " "+ _prevs[0]);
	_prevs[0].cur = new Point2D.Double(c.x,c.y);
	_prevs[0].ipos = ipos;
	_prevs[0].t = time;

	_num_data = Math.min(_num_data+1, _prevs.length);
}

public Point2D.Double filter(Point2D.Double c, Point3D ipos, long time)
{
	_store(c, ipos, time);
	return c;
}

}