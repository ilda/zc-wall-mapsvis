package ilda.zcsample.filters;


class MeanFilter 
{

class TimedValue
{
	double value;
	long t;
	TimedValue(double v, long time) { value =v; t = time; }
}

TimedValue prevs[];
int num_data = 0;

MeanFilter(int n)
{
	if (n < 2) { n=2; }

	prevs = new TimedValue[n];
	num_data = 0;
}

public double filter(double value, long time)
{

	for(int i = num_data-1 ; i > 0; i--)
	{
		prevs[i] = prevs[i-1];
	}

	prevs[0].value = value;
	prevs[0].t = time;

	num_data = Math.min(num_data+1, prevs.length);

	double sum = 0;
	for(int i = 0; i < num_data; i++)
	{
		sum = sum +  prevs[i].value;
	}
	return sum/num_data;
}

}