/*   Copyright (c) INRIA, 2015. All Rights Reserved
 * $Id:  $
 */

package ilda.zcsample;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.PathIterator;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Vector;
import java.net.MalformedURLException;

// GeoTools
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
// import org.geotools.factory.GeoTools;
import org.geotools.util.factory.GeoTools;
import org.geotools.data.shapefile.shp.JTSUtilities;
import org.geotools.geometry.jts.LiteShape;
//
// import com.vividsolutions.jts.geom.Point;
// import com.vividsolutions.jts.geom.Geometry;
// import com.vividsolutions.jts.geom.Coordinate;
// import com.vividsolutions.jts.geom.Polygon;
// import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;
// import com.vividsolutions.jts.geom.util.PointExtracter;
// import com.vividsolutions.jts.geom.util.PolygonExtracter;
// import com.vividsolutions.jts.geom.util.LinearComponentExtracter;
// import com.vividsolutions.jts.geom.LineString;
// import com.vividsolutions.jts.linearref.LinearIterator;
//
// org.locationtech.jts
//
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.simplify.DouglasPeuckerSimplifier;
import org.locationtech.jts.geom.util.PointExtracter;
import org.locationtech.jts.geom.util.PolygonExtracter;
import org.locationtech.jts.geom.util.LinearComponentExtracter;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.linearref.LinearIterator;
//
// import fr.inria.zvtm.glyphs.VPolygon;
import fr.inria.zvtm.glyphs.SICircle;
import fr.inria.zvtm.glyphs.BoatInfoG;
import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.glyphs.DPath;
import fr.inria.zuist.engine.SceneManager;
import fr.inria.zuist.engine.Level;
import fr.inria.zuist.engine.Region;
import fr.inria.zvtm.glyphs.VPolygon;

import ilda.zcsample.gis.GoogleProjection;

public class GisLayer {

	VirtualSpace _vs;

    static final double MIN_DISTANCE_BETWEEN_BOATS = 50;

     // projection from lat/lon to virtual space
    GoogleProjection GP;
    // slippy map transform
    Point2D.Double SMT = new Point2D.Double(0,0);

    private Vector<VPolygon> _securePolys = null; 
    private boolean _secureDisplayed = false;

    public GisLayer(VirtualSpace vs)
    {
    	System.out.println("new GisLayer");
    	_vs = vs;
        _showMartinique();
    }

    private void _showMartinique(){
        Point2D.Double[] zvtmCoords = new Point2D.Double[4];
        zvtmCoords[0] = new Point2D.Double(2230,-2695); // tl
        zvtmCoords[1] = new Point2D.Double(2485,-2695);
        zvtmCoords[2] = new Point2D.Double(2485,-2917); // br
        zvtmCoords[3] = new Point2D.Double(2230,-2917); 
        VPolygon p = new VPolygon(zvtmCoords, 11, Color.GREEN, Color.BLACK);
        _vs.addGlyph(p);
        p.setFilled(false);
        p = new VPolygon(zvtmCoords, 0, Color.GREEN, Color.BLACK, 0.4f);
        _vs.addGlyph(p);
        p.setFilled(true);

    }

    void computeSlippyMapTransform(SceneManager sm, int tileSize){
        Level l = sm.getLevel(sm.getLevelCount()-1);
        double[] wnes = sm.findFarmostRegionCoords();
        Region topLeftR = l.getClosestRegion(new Point2D.Double(wnes[0], wnes[1]));
        // assumes IDs of the form Rz-x-y, like R11-544-736
        String[] zxy = topLeftR.getID().substring(1).split("-");
         System.out.println("GisLayer zxy " + zxy[1] +" "+zxy[2]);
        GP = new GoogleProjection(tileSize, Integer.parseInt(zxy[0]));
        SMT.setLocation(tileSize*Integer.parseInt(zxy[1])-wnes[0],
                        -tileSize*Integer.parseInt(zxy[2])-wnes[1]);
         System.out.println("GisLayer SMT " + SMT.x+" "+ SMT.y);
    }

    Point2D.Double fromLLToPixel(double lon, double lat, Point2D.Double res){
        GP.fromLLToPixel(lon, lat, res);
        res.setLocation(res.x-SMT.x, res.y-SMT.y);
        return res;
    }

    Point2D.Double fromLLToPixel(double lon, double lat){
        return fromLLToPixel(lon, lat, new Point2D.Double());
    }

    Point2D.Double fromPixelToLL(double x, double y, Point2D.Double res){
        GP.fromPixelToLL(x+SMT.x, y+SMT.y, res);
        return res;
    }

    Point2D.Double fromPixelToLL(double x, double y){
        return fromPixelToLL(x, y, new Point2D.Double());
    }

    void loadTsunamiLines(File shapeFile){
    	Map connect = new HashMap();
        try {
            connect.put("url", shapeFile.toURI().toURL());
            try {
                DataStore dataStore = DataStoreFinder.getDataStore(connect);
                String[] typeNames = dataStore.getTypeNames();
                String typeName = typeNames[0];
                FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
                FeatureIterator<SimpleFeature> fi = featureCollection.features();
                int num_lines= 0;
                while(fi.hasNext()){
                    SimpleFeature f = fi.next();
                    Geometry geometry = (Geometry)f.getDefaultGeometry();
                    Object[] lines = LinearComponentExtracter.getLines(geometry).toArray();
                    //System.out.print("num lines: " + lines.length);
                    for (int k=0;k<lines.length;k++){
                    	LinearIterator it = new LinearIterator((Geometry)lines[k]);
                    	while (it.hasNext()){
                    		LineString l =	it.getLine();
                    		int np = l.getNumPoints();
                    		//System.out.print(" "+np);
                    		Coordinate[] coords = l.getCoordinates();
                            DPath path = null;
                            for (int u=0; u < coords.length; u++){
                            	Point2D.Double vp = fromLLToPixel(coords[u].y, coords[u].x);
                    			if (path == null){
                    				path = new DPath(vp.getX(),vp.getY(), 0, Color.RED);
                    			}
                    			else {
                            		path.addSegment(vp.getX(),vp.getY(), true);
                            	} 
                            }
                            _vs.addGlyph(path);
                            it.next();
                        }
                    }
                    //System.out.println("");
                    num_lines++;
                }
                System.out.println("all num lines: " + num_lines);
                try {
                    fi.close();
                }
                catch (IllegalArgumentException iae){
                    System.out.println("Not happy when closing");
                }
            }
            catch(MalformedURLException uex){
                uex.printStackTrace();
            }
        }
        catch (IOException ioex){
            ioex.printStackTrace();
        }
    }

   void loadEpicentre(File shapeFile){
    	//Vector<GisBoat> allBoats = new Vector();
        Map connect = new HashMap();
        try {
            connect.put("url", shapeFile.toURI().toURL());
            try {
                DataStore dataStore = DataStoreFinder.getDataStore(connect);
                String[] typeNames = dataStore.getTypeNames();
                String typeName = typeNames[0];
                FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
                FeatureIterator<SimpleFeature> fi = featureCollection.features();
                while(fi.hasNext()){
                    SimpleFeature f = fi.next();
                    Point p = (Point)f.getAttribute(0);
                    Point2D.Double vp = fromLLToPixel(p.getY(), p.getX());
                    //System.out.println("Epicentre -- vx: "+vx+", vy: "+vy+", x:"+p.getX()+", y:"+p.getY());
                    SICircle epicentre = new SICircle(vp.getX(),vp.getY(), 0, 10, Color.RED);
                    _vs.addGlyph(epicentre);
                }
                try {
                    fi.close();
                }
                catch (IllegalArgumentException iae){
                    System.out.println("Not happy when closing");
                }
            }
            catch(MalformedURLException uex){
                uex.printStackTrace();
            }
        }
        catch (IOException ioex){
            ioex.printStackTrace();
        }
    }

    void loadBoats(File shapeFile){
    	Vector<Boat> allBoats = new Vector();
        Map connect = new HashMap();
        try {
            connect.put("url", shapeFile.toURI().toURL());
            try {
                DataStore dataStore = DataStoreFinder.getDataStore(connect);
                String[] typeNames = dataStore.getTypeNames();
                String typeName = typeNames[0];
                FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
                FeatureIterator<SimpleFeature> fi = featureCollection.features();
                while(fi.hasNext()){
                    SimpleFeature f = fi.next();
                    Point p = (Point)f.getAttribute(0);
                    Point2D.Double vp = fromLLToPixel(p.getY(), p.getX());
                    boolean tooClose = false;
                    for (Boat boat:allBoats){
                        if (boat.distanceTo(vp.getX(),vp.getY()) < MIN_DISTANCE_BETWEEN_BOATS){
                            tooClose = true;
                            break;
                        }
                    }
                    if (!tooClose){
                        Boat boat = new Boat(vp.getX(),vp.getY(), f.getAttributes());
                        //System.out.println("Boat "+f.getAttributes().get(2)+"-- vx: "+vx+", vy: "+vy+", x:"+p.getX()+", y:"+p.getY());
                        _vs.addGlyph(boat.glyph);
                        allBoats.add(boat);
                    }
                }
                try {
                    fi.close();
                }
                catch (IllegalArgumentException iae){
                    System.out.println("Not happy when closing");
                }
            }
            catch(MalformedURLException uex){
                uex.printStackTrace();
            }
        }
        catch (IOException ioex){
            ioex.printStackTrace();
        }
    }

void loadSecure(File shapeFile, Color shapeColor, boolean show)
{
    if (_securePolys == null){
        _securePolys = new Vector<VPolygon>();
    }
    else{
        return;
    }
    _secureDisplayed = show; 
    Map connect = new HashMap();
    try {
        connect.put("url", shapeFile.toURI().toURL());
        try {
            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];
            FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = dataStore.getFeatureSource(typeName).getFeatures();
            FeatureIterator<SimpleFeature> fi = featureCollection.features();
            Vector<Polygon> awtPolygons = new Vector<Polygon>();
            Vector points = new Vector();
            Point2D.Double[] zvtmCoords;
            int i = 0;
            int num_points = 0;
            while(fi.hasNext()){
                SimpleFeature f = fi.next();
                Geometry geometry = (Geometry)f.getDefaultGeometry();
                Object[] polygons = PolygonExtracter.getPolygons(geometry).toArray();
                for (int k=0;k<polygons.length;k++){
                    Geometry simplifiedPolygon = DouglasPeuckerSimplifier.simplify((Geometry)polygons[k], 0.001);
                    PathIterator pi = (new LiteShape(simplifiedPolygon, null, false)).getPathIterator(null);
                    double[] coords = new double[6];
                    int type;
                    Vector shapes = new Vector();
                    boolean  skip = false;
                    while (!pi.isDone()){
                        type = pi.currentSegment(coords);
                        if (type == PathIterator.SEG_LINETO){
                            Point2D.Double vp = fromLLToPixel(coords[1], coords[0]);
                            //System.out.println(x+" "+y);
                            num_points++;
                            // System.out.print(".");
                            points.add(new Point2D.Double(vp.getX(), vp.getY()));
                        }
                        else if (type == PathIterator.SEG_MOVETO){
                            points.clear();
                        }
                        else if (type == PathIterator.SEG_CLOSE && points.size() > 0){
                            zvtmCoords = new Point2D.Double[points.size()];
                            for (int j=0;j<zvtmCoords.length;j++){
                                zvtmCoords[j] = (Point2D.Double)points.elementAt(j);
                            }
                            if (!skip){
                                VPolygon polygon = new VPolygon(zvtmCoords, 8, shapeColor,shapeColor);
                                polygon.setFilled(false);
                                _securePolys.add(polygon);
                                polygon.setFilled(false);
                                if (show) _vs.addGlyph(polygon);
                                //application.sm.createClosedShapeDescription(polygon, "B"+Integer.toString(polygonID++),
                                //    polygon.getZindex(),
                                //    region, false);
                            }
                            skip = false;
                        }
                        else if (type == PathIterator.SEG_CLOSE){
                             System.err.println("Error: not enough points");

                        }
                        else {
                            System.err.println("Error: GeoToolsManager.loadShape: Unsupported path iterator element type:" + type);
                        }
                        pi.next();
                    }
                }
            }
            System.out.println("\n GisLayer secure loaded with "+num_points+" points");
            try {
                fi.close();
            }
            catch (IllegalArgumentException iae){
                System.out.println("Not happy when closing");
            }
        }
        catch(MalformedURLException uex){
            uex.printStackTrace();
        }
    }
    catch (IOException ioex){
        ioex.printStackTrace();
    }
}

}


