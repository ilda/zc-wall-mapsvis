package ilda.zcsample.tuiotouch;

class TouchEvent
{

public static final int DOWN = 1;
public static final int UP = 2;
public static final int MOVE = 3;
public static final int ROTATE = 4;
public static final int START_ROTATE = 5;
public static final int END_ROTATE = 6;

public int action;
public int id;
public double x,y;
public double angle;
public long t;

TouchEvent(int a, int i, double xx, double yy, long tt)
{
	action = a; id = i; x = xx; y = yy; angle = 0; t = tt;
}

TouchEvent(int a, int i, double xx, double yy, double an, long tt)
{
	action = a; id = i; x = xx; y = yy; angle = an; t = tt;
}

}