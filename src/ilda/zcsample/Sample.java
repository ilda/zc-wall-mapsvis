package ilda.zcsample;

import ilda.zcsample.walls.WallCoordinateCalculator;
import ilda.zcsample.walls.wild.WildCoordinateCalculator;
import ilda.zcsample.walls.wilder.WilderCoordinateCalculator;
import ilda.zcsample.walls.wildest.WildestCoordinateCalculator;

import fr.inria.zvtm.engine.VirtualSpaceManager;

import java.lang.Runtime;

public class Sample
{
 
// default to wild with bezel of 100
static private boolean _use_geo_options = false;
static private double _width=2760;
static private double _height=1800;
static private double _bezWidth=100;
static private double _bezHeight=100;
static private double _pixInMM=0.25;
static private int _col = 8;
static private int _row = 4;

static private String _wall = null;

static public boolean zuist = false;
static public boolean desktop = false;
static public String  zuist_file_names = null;
static public String  zuist_aux_file_names = null;
static public boolean sync = false;
static public boolean tsunami = false;
static public boolean boat = false;
static public String[]  consoles = null;

static public int numSlaves=-1;

// only in desktop mode
static public boolean opengl = false;
static public boolean fullscreen = false;
static public int refreshRate = 0; 
static public boolean antialiasing = false;

static private String _vicon_objects = null;
static private String _vicon_host = "192.168.2.3";
// wilder 192.168.2.3
// vicon.wild.lri.fr
static private int _vicon_port = 0;

static int touchPort = 3333;
static boolean useWallToken = true;

static public boolean jinputEnabled = false;
static private String _jinput_conf_file = null;

public	Sample()
{
	VirtualSpaceManager vsm = VirtualSpaceManager.INSTANCE;

  
	System.out.println("WALL: " + _wall +" "+ _use_geo_options +" "+ desktop);
	System.out.println("MaxMemory: "+Runtime.getRuntime().maxMemory()/(1024*1024) + "MB");

     // set master name, should be passed to the slave (cluster)
	if (!desktop){
		System.out.println("Set Master");
    	vsm.setMaster("Sample");
	}

     if (!_use_geo_options && _wall != null)
     {
     	WallCoordinateCalculator wcc = null;
		if (_wall != null && _wall.toLowerCase().equals("wild")) {
			wcc = new WildCoordinateCalculator();
		}
		else if (_wall != null && _wall.toLowerCase().equals("wilder")){
			wcc = new WilderCoordinateCalculator();
			System.out.println("WALL: WILDER "+ _wall);
		}
		else if (_wall != null && _wall.toLowerCase().equals("wildest")){
			wcc = new WildestCoordinateCalculator();
			System.out.println("WALL: WILDEST "+ _wall);
		}
		else {
			System.out.println("WARNING UNKOW WALL "+ _wall);
		}
		if (wcc != null)
		{
			_width =  wcc.getOneScreenWidth();
			_height =  	wcc.getOneScreenHeight();
			_pixInMM = wcc.getPixInMM();
			_bezWidth = wcc.getBezelWidth();
			_bezHeight = wcc.getBezelHeight();
			_col = wcc.getGridCol();
			_row = wcc.getGridRow();
			_vicon_host= wcc.getViconHost();
		}
    }

	ClusterDisplay cd = new ClusterDisplay(
		_width, _height, _col, _row,  _bezWidth, _bezHeight, _pixInMM);

	SmartiesManager sm = new SmartiesManager(cd);
	//new DashboardTestFilter(cd,null);

	ViconManager vm = null;

	if (_vicon_objects != null)
	{
		vm = new ViconManager(
	 		cd, sm, _vicon_objects, _vicon_host, _vicon_port, _wall, true, false);
	}

	//new TuioManager();
	if (useWallToken){
		new TuioWallTokenManager(cd, touchPort);
	}
	else {
		// deprecated (nul, zero)
		new TuioTouchManager(cd, touchPort);
	}

	if (jinputEnabled) {
		new JInputManager(cd, _jinput_conf_file);
	}
	//else {
		new Dashboard(cd);
	//}

	if (!desktop){
		cd.initConsoleListener();
	}
}

public static void main(String[] args)
{
    for (int i = 0; i < args.length; i++)
	{
		// parse options...
		String arg = args[i];
		if (arg.equals("-w")) {
			i++;
			_width = Double.parseDouble(args[i]);
			_use_geo_options = true;
		}
		else if (arg.equals("-h")) {
			i++;
			_height = Double.parseDouble(args[i]);
			_use_geo_options = true;
		}
		else if (arg.equals("-psmm")) {
			i++;
			_pixInMM = Double.parseDouble(args[i]);
		}
		else if (arg.equals("-c")) {
			i++;
			_col = Integer.parseInt(args[i]);
			_use_geo_options = true;
		}
		else if (arg.equals("-r")) {
			i++;
			_row = Integer.parseInt(args[i]);
			_use_geo_options = true;
		}
		else if (arg.equals("-bw")) {
			i++;
			_bezWidth = Double.parseDouble(args[i]);
			_use_geo_options = true;
		}
		else if (arg.equals("-bh")) {
			i++;
			_bezHeight = Double.parseDouble(args[i]);
			_use_geo_options = true;
		}
		else if (arg.equals("-wall"))
		{
			i++;
			_wall = args[i];
			_use_geo_options = false;
		}
		else if (arg.equals("-z"))
		{
			zuist = true;
		}
		else if (arg.equals("-t"))
		{
			tsunami = true;
		}
		else if (arg.equals("-b"))
		{
			boat = true;
		}
		else if (arg.equals("-d"))
		{
			desktop = true;
		}
		else if (arg.equals("-f"))
		{
			fullscreen = true;
		}
		else if (arg.equals("-o"))
		{
			opengl = true;
		}
		else if (arg.equals("-a"))
		{
			antialiasing = true;
		}
		else if (arg.equals("-rr"))
		{
			i++;
			refreshRate = Integer.parseInt(args[i]);
		}
		else if (arg.equals("-zf"))
		{
			i++;
			zuist_file_names = args[i];
			zuist = true;
		}
		else if (arg.equals("-za"))
		{
			i++;
			zuist_aux_file_names = args[i];
			//zuist = true;
		}
		else if (arg.equals("-vicon-objects"))
		{
			i++;
			_vicon_objects = args[i];
		}
		else if (arg.equals("-vicon-host"))
		{
			i++;
			_vicon_host = args[i];
		}
		else if (arg.equals("-j"))
		{
			jinputEnabled = true;
		}
		else if (arg.equals("-jc"))
		{
			i++;
			_jinput_conf_file = args[i];
			jinputEnabled = true;
		}
		else if (arg.equals("-co"))
		{
			i++;
			String co_arg = args[i];
			consoles = co_arg.split(";");
		}
		else if (arg.equals("-sync"))
		{
			sync = true;
		}
		else if (arg.equals("-ns"))
		{
			i++;
			numSlaves = Integer.parseInt(args[i]);
		}
		else if (arg.equals("-tp"))
		{
			i++;
			touchPort = Integer.parseInt(args[i]);
		}
	}

	new Sample();
}

}
