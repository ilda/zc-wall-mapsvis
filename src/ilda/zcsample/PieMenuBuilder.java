package ilda.zcsample;


import java.awt.Color;
import java.awt.geom.Point2D;

import java.util.Vector;
import java.util.HashMap;

import fr.inria.zvtm.engine.VirtualSpace;
import fr.inria.zvtm.engine.View;
import fr.inria.zvtm.engine.ViewPanel;
import fr.inria.zvtm.event.ViewListener;
import fr.inria.zvtm.event.PickerListener;
import fr.inria.zvtm.glyphs.Glyph;
import fr.inria.zvtm.glyphs.VText;
import fr.inria.zvtm.glyphs.PRectangle;
import fr.inria.zvtm.glyphs.VRectangle;
import fr.inria.zvtm.glyphs.ClosedShape;
import fr.inria.zvtm.widgets.PieMenuFactory;
import fr.inria.zvtm.widgets.PieMenu;
import java.awt.Font;


public class PieMenuBuilder {
	
    public static final String T_PM = "pm";

    public static final Font WALL_MENU_FONT = new Font("Dialog", Font.PLAIN, 32);
    public static final Font DESKTOP_MENU_FONT = new Font("Dialog", Font.PLAIN, 12);

    public static final Font PIEMENU_FONT = WALL_MENU_FONT;

    public static Color PIEMENU_FILL_COLOR = Color.BLACK;
    public static Color PIEMENU_BORDER_COLOR = Color.WHITE;
    public static Color PIEMENU_INSIDE_COLOR = Color.DARK_GRAY;

    static {
        PieMenuFactory.setItemFillColor(PIEMENU_FILL_COLOR);
        PieMenuFactory.setItemBorderColor(PIEMENU_BORDER_COLOR);
        PieMenuFactory.setSelectedItemFillColor(PIEMENU_INSIDE_COLOR);
        PieMenuFactory.setSelectedItemBorderColor(null);
        PieMenuFactory.setLabelColor(PIEMENU_BORDER_COLOR);
        PieMenuFactory.setFont(PIEMENU_FONT);
        PieMenuFactory.setAngle(0);
    }

    private VirtualSpace _vs;
    private ClusterDisplay _display;
    private Font _font;

	public PieMenuBuilder(VirtualSpace vs, ClusterDisplay d){
		_vs = vs;
        _display = d;
        _font = WALL_MENU_FONT;
        if (_display.getWidth() < 4000){
            _font = DESKTOP_MENU_FONT;
        }
	}

    private void setup(Point2D.Double coords, float iray){
        float radius = (float)_display.getHeight()/10f + iray;
        float inner = iray/radius; 
        PieMenuFactory.setInnerRatio(inner);
        PieMenuFactory.setSensitivityRadius(1.0);
        PieMenuFactory.setRadius((long)radius);
        PieMenuFactory.setTranslucency(0.7f);
        PieMenuFactory.setFont(_font);
    }
	
    static final String BGM_GLOBAL_VIEW = "Global";
    static final String BGM_DRAGMAG = "DragMag";
    static final String BGM_DATA = "Data";
    static final String BGM_LAYER = "Layer";
    static final String BGM_OVERVIEW = "OverView";

    static final String[] BGM_COMMANDS = {BGM_DATA, BGM_DRAGMAG, BGM_LAYER, BGM_GLOBAL_VIEW, BGM_OVERVIEW};
    static final Point2D.Double[] BGM_OFFSETS = {
        new Point2D.Double(0,0), new Point2D.Double(-10,0),
        new Point2D.Double(0,-10), new Point2D.Double(10,0), new Point2D.Double(0,0)};

    public  PieMenu createBgPieMenu(Point2D.Double coords, float iray)
    {
        setup(coords, iray);
        PieMenuFactory.setAngle(Math.PI/12);

        PieMenu pm = PieMenuFactory.createPieMenu(BGM_COMMANDS, BGM_OFFSETS, 0, _vs, coords);
        pm.setSensitivity(true);
        
        Glyph[] items = pm.getItems();
        for (Glyph item:items){
            item.setType(T_PM);
        }

        return pm;
    }

    static final String DMM_MAGIC = "Magic";
    static final String DMM_DELETE = "Close";
    static final String DMM_ATTACH = "Attach";
    //static final String DMM_ = "Layer";
    //static final String DMM_ = "OverView";

    static final String[] DMM_COMMANDS = {DMM_MAGIC, DMM_DELETE, DMM_ATTACH};
    static final Point2D.Double[] DMM_OFFSETS = {
        new Point2D.Double(0,0), new Point2D.Double(0,0),
        new Point2D.Double(0,-0)};

    public  PieMenu createDMPieMenu(Point2D.Double coords, float iray)
    {
        setup(coords, iray);
        // PieMenuFactory.setAngle(Math.PI/12);

        PieMenu pm = PieMenuFactory.createPieMenu(DMM_COMMANDS, DMM_OFFSETS, 0, _vs, coords);
        pm.setSensitivity(true);
        
        Glyph[] items = pm.getItems();
        for (Glyph item:items){
            item.setType(T_PM);
        }

        return pm;
    }
}