#!/bin/bash

LOGIN="wild"
NAME="Sample"
CLASS="ilda.zcsample.Sample"
LIP="2"
accum_rest=0
REST=""
CONSOLES="no"

#  /home2/wild/workspace/zvtm/zuist-cluster/trunk
#ZVTMHOME="/media/data/Demos/zvtm/lib"
#ZVTMHOME="/home2/wild/workspace/zvtm/zuist-cluster/trunk"
#ZVTMHOME="/home2/wild/workspace/zc-sample"
ZVTMHOME="/home/wild/Demos/zc-sample"
IMAGE_PATH="/home/wild/Demos/data/zvtm-images"

WHATCLUSTER="zvtm-cluster-0.2.10.oc-SNAPSHOT.jar"

## FIXME !
export LD_LIBRARY_PATH="/usr/lib/jvm/default-java/jre/lib/amd64/"

for i in "$@"
do
  case $i in
    -n=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
    	 NAME="${i#*=}"
      fi
    ;;
    -c=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
    	   CLASS="${i#*=}"
      fi
    ;;
    -z)
        WHATCLUSTER="zuist-cluster-0.3.0.oc-SNAPSHOT.jar"
        REST="$REST $i"
    ;;
    -ip=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
    	   #LIP="192.168.0.${i#*=}"
        LIP="${i#*=}"
      fi
    ;;
    -l=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
        LOGIN="${i#*=}"
      fi
    ;;
    -co=*)
        if [ $accum_rest == 1 ]; then
            REST="$REST -co \"${i#*=}\"";
        else
            CONSOLES="${i#*=}"
        fi
    ;;
    --)
  		accum_rest=1
    ;;
    *)
		if [ $accum_rest == 1 ]; 
			then REST="$REST $i";
		fi
    ;;
  esac
done

echo "LOGIN: " $LOGIN
echo "NAME: " $NAME
echo "CLASS: " $CLASS
echo "REST: " $REST
echo "LIP: " $LIP
echo "CONSOLES: " $CONSOLES
echo "WHATCLUSTER: " $WHATCLUSTER

GTV="26.0"
JARS_GT=":target/gt-api-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-main-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-opengis-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-shapefile-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-data-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-metadata-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-referencing-$GTV.jar"
JARS_GT=$JARS_GT":target/jai_core-1.1.3.jar"
JARS_GT=$JARS_GT":target/jdom-1.1.3.jar"
JARS_GT=$JARS_GT":target/jsr-275-1.0-beta-2.jar"
JARS_GT=$JARS_GT":target/jts-1.13.jar"
JARS_GT=$JARS_GT":target/vecmath-1.3.2.jar"
JARS_GT=$JARS_GT":target/commons-codec-1.9.jar"
JARS_GT=$JARS_GT":target/commons-pool-1.5.4.jar"

##echo "JARS_GT:"$JARS_GT
##exit

TARGET="target/"
JARS=$TARGET"commons-logging-1.1.jar"
JARS=$JARS":"$TARGET"args4j-2.0.29.jar"
JARS=$JARS":"$TARGET"aspectjrt-1.6.5.jar"
JARS=$JARS":"$TARGET"jgroups-3.6.8.Final.jar"
JARS=$JARS":"$TARGET"log4j-1.2.17.jar"
JARS=$JARS":"$TARGET"slf4j-api-1.7.10.jar"
JARS=$JARS":"$TARGET"slf4j-log4j12-1.7.10.jar"
JARS=$JARS":"$TARGET"timingframework-1.0.jar"
JARS=$JARS":"$TARGET$WHATCLUSTER


function colNum {
  case "$1" in 
    "a" ) return 0;;
    "b" ) return 1;;
    "c" ) return 2;;
    "d" ) return 3;;
  esac
}


NUMBER_OF_SLAVES=32

for col in {a..d}
do
  for row in {1..4}
      do
      colNum $col 
      SLAVENUM1=`expr $? \* 8 + $row - 1`
      SLAVENUM2=`expr $SLAVENUM1 + 4`
      colNum $col
      startIp=`expr 32 + $? \* 4 + $row - 1`
      
        echo " "
        echo " "$col$row.wild.lri.fr
        chost="192.168.0.$startIp"
        echo "$LOGIN@$col$row"
        echo "start on \"$chost\" $SLAVENUM1  $SLAVENUM2"
      
      ## -x -126 -y -10 FIXME in cluster geometry... 
      ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "cd $ZVTMHOME ; DISPLAY=:0.0 java  -Xmx2g -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.0.$startIp" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM1  -x -63 -y -0 -u -n $NAME" &
      sleep 1
      ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "cd $ZVTMHOME ; DISPLAY=:0.0 java  -Xmx2g -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.0.$startIp" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM2  -x 3840 -y 0 -u -n $NAME" &
      
        sleep 1
        #break
      done
      #break
done

#exit
if [ "x$CONSOLES" != "xno" ]; then
  ## FIXME: WILDER !!!
    echo ""
    echo "Start the console...."
    echo ""
    ## frontal 1920,1080
    startIp=2 
    ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 1 -u -a -n $NAME" &
    sleep 1
    # console1 1920,1200
    #startIp=163
    #ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java  $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 2 -u -f -a -n $NAME" &
    # console2 1920,1200
    startIp=159
    ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java  $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 2 -u -f -a -n $NAME" &
fi

echo ""
echo "Now start master...."
echo ""

sleep 3
JARS="target/aspectjrt-1.6.5.jar"
#JARS=$JARS":target/jgroups-2.7.0.GA.jar"
JARS=$JARS":target/jgroups-3.6.8.Final.jar"
JARS=$JARS":target/log4j-1.2.17.jar"
JARS=$JARS":target/slf4j-api-1.7.10.jar"
JARS=$JARS":target/slf4j-log4j12-1.7.10.jar"
JARS=$JARS":target/timingframework-1.0.jar"
JARS=$JARS":target/xercesImpl-2.8.1.jar"
JARS=$JARS":target/xml-apis-1.3.03.jar"
JARS=$JARS":target/xmlParserAPIs-2.6.2.jar"
#JARS=$JARS":target/zvtm-svg-0.2.1.jar"
JARS=$JARS":target/zvtm-svg-0.2.2-SNAPSHOT.jar"
JARS=$JARS":target/commons-logging-1.1.jar"
JARS=$JARS":target/args4j-2.0.29.jar"
JARS=$JARS":target/vrpn-1.0.jar"
JARS=$JARS":target/jinput-1.0.jar"
#
JARS=$JARS":"$JARS_GT
#
JARS=$JARS":target/javaSmarties-1.4.0-SNAPSHOT.jar"
JARS=$JARS":target/"$WHATCLUSTER 
JARS=$JARS":target/zcsample-0.1.0-SNAPSHOT.jar"

echo ""
echo "cp  "$JARS
echo ""

java -Xmx8g -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.0.$LIP" -Djava.library.path=".:lib:/usr/lib/jvm/default-java/jre/lib/amd64/" -cp $JARS $CLASS -ns $NUMBER_OF_SLAVES -wall wildest $REST

#wildo killall java
#WALL=WILDER 
WALL=WILD  walldo -l $LOGIN killall java
#walldo killall java
