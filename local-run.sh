#!/bin/bash

# usage example: 
# ./local-run.sh -n=Sample -c=ilda.zcsample.Sample -- -w 1000 -h 500 -c 1 -r 1 -bw 10 -bh 20 
# ./local-run.sh -n=Sample -z -c=ilda.zcsample.Sample -- -w 1000 -h 500 -c 1 -r 1 -bw 10 -bh 20 -z -zf /home/olivier/bigimages/zuist/NASA-Curiosity/ZVTM/scene.xml
NAME="Sample"
CLASS="ilda.zcsample.Sample"
accum_rest=0
zuist=0
desktop=0
REST=""
SLAVEOPTS=""
SEVERAL="no"
CONSOLES="no"
DSUNOPENGL=""
SLAVE_DSUNOPENGL=""

WHATCLUSTER="zvtm-cluster-0.2.10.oc-SNAPSHOT.jar"

export LD_LIBRARY_PATH="/usr/lib/jvm/default-java/lib/amd64/"

for i in "$@"
do
case $i in
    -n=*)
        if [ $accum_rest == 1 ]; 
            then REST="$REST -n ${i#*=}";
        else
    	   NAME="${i#*=}"
        fi
    ;;
    -c=*)
        if [ $accum_rest == 1 ]; 
            then REST="$REST -c ${i#*=}";
        else
    	   CLASS="${i#*=}"
        fi
    ;;
    -z)
        REST="$REST $i";
        zuist=1
        if [ $desktop == 1 ];
            then WHATCLUSTER="zuist-core-0.5.0.oc-SNAPSHOT.jar"
        else
            WHATCLUSTER="zuist-cluster-0.3.0.oc-SNAPSHOT.jar"
        fi
    ;;
    -d)
        REST="$REST $i";
        desktop=1
        if [ $zuist == 1 ];
            then WHATCLUSTER="zuist-core-0.5.0.oc-SNAPSHOT.jar"
        else
            WHATCLUSTER="zvtm-core-0.12.0.oc-SNAPSHOT.jar"
        fi
    ;;
    -a)
        REST="$REST $i";
        SLAVEOPTS="$SLAVEOPTS $i"
    ;;
    -o)
        DSUNOPENGL="-Dsun.java2d.opengl=true"
        REST="$REST $i";
    ;;
    -so)
        ## far slower
        SLAVE_DSUNOPENGL="-Dsun.java2d.opengl=True"
        ### GLView does not support portals and overlay cam ...
        SLAVEOPTS="$SLAVEOPTS -o"
        ##SLAVEOPTS="$SLAVEOPTS"
    ;;
    -s)
        if [ $accum_rest == 1 ]; 
            then REST="$REST $i";
        else
            SEVERAL="yes"
        fi
    ;;
    -r=*)
        if [ $accum_rest == 1 ]; then
            REST="$REST -r ${i#*=}";
        else
            SLAVEOPTS="$SLAVEOPTS -r ${i#*=}"
        fi
    ;;
    -co=*)
        if [ $accum_rest == 1 ]; then
            REST="$REST -co \"${i#*=}\"";
        else
            CONSOLES="${i#*=}"
        fi
    ;;
    --)
  		accum_rest=1
    ;;
    *)
		if [ $accum_rest == 1 ]; 
			then REST="$REST $i";
		fi
    ;;
esac
#shift
done

echo "NAME:"$NAME":"
echo "CLASS:"$CLASS":"
echo "SLAVEOPTS:"$SLAVEOPTS":"
echo "SEVERAL:"$SEVERAL":"
echo "CONSOLES:"$CONSOLES":"
echo "REST:"$REST":"
echo "CLUSTER:"$WHATCLUSTER":"


GTV="26.0"
JARS_GT=":target/gt-api-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-main-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-opengis-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-shapefile-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-data-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-metadata-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-referencing-$GTV.jar"
JARS_GT=$JARS_GT":target/jai_core-1.1.3.jar"
JARS_GT=$JARS_GT":target/jdom-1.1.3.jar"
JARS_GT=$JARS_GT":target/jsr-275-1.0-beta-2.jar"
JARS_GT=$JARS_GT":target/jts-1.13.jar"
JARS_GT=$JARS_GT":target/vecmath-1.3.2.jar"
JARS_GT=$JARS_GT":target/commons-codec-1.9.jar"
JARS_GT=$JARS_GT":target/commons-pool-1.5.4.jar"

##echo "JARS_GT:"$JARS_GT
##exit

if [ $desktop == 1 ]; then
    JARS="target/aspectjrt-1.6.5.jar"
    JARS=$JARS":target/log4j-1.2.17.jar"
    JARS=$JARS":target/slf4j-api-1.7.10.jar"
    JARS=$JARS":target/slf4j-log4j12-1.7.10.jar"
    JARS=$JARS":target/timingframework-1.0.jar"
    JARS=$JARS":target/xercesImpl-2.8.1.jar"
    JARS=$JARS":target/xml-apis-1.3.03.jar"
    JARS=$JARS":target/xmlParserAPIs-2.6.2.jar"
    JARS=$JARS":target/zvtm-svg-0.2.2-SNAPSHOT.jar"
    #JARS=$JARS":target/zvtm-svg-0.2.1.jar"
    JARS=$JARS":target/commons-logging-1.1.jar"
    JARS=$JARS":target/args4j-2.0.29.jar"
    #
    JARS=$JARS":"$JARS_GT
    #
    JARS=$JARS":target/vrpn-1.0.jar"
    JARS=$JARS":target/jinput-1.0.jar"
    JARS=$JARS":target/javaSmarties-1.4.0.jar"
    JARS=$JARS":target/"$WHATCLUSTER 
    JARS=$JARS":target/zcsample-0.1.0-SNAPSHOT.jar"

    java  $SLAVE_DSUNOPENGL -Djava.library.path=".:lib" -cp $JARS $CLASS $REST
    exit
fi

JARS="target/commons-logging-1.1.jar"
JARS=$JARS":target/args4j-2.0.29.jar"
JARS=$JARS":target/aspectjrt-1.6.5.jar"
JARS=$JARS":target/jgroups-3.6.8.Final.jar"
JARS=$JARS":target/log4j-1.2.17.jar"
JARS=$JARS":target/slf4j-api-1.7.10.jar"
JARS=$JARS":target/slf4j-log4j12-1.7.10.jar"
JARS=$JARS":target/timingframework-1.0.jar"
JARS=$JARS":target/"$WHATCLUSTER

#JARS=$JARS":target/zuist-cluster-0.3.0-SNAPSHOT.jar"

#java -XX:+DoEscapeAnalysis -XX:+UseConcMarkSweepGC -Xmx1g -Djava.net.preferIPv4Stack=true -cp $JARS fr.inria.zvtm.cluster.SlaveApp -n ZuistCluster -b 0  &

java  -Xmx2048m $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr="localhost" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 0  -x -0 -y 0 -n $NAME &
if [ "x$SEVERAL" = "xyes" ]; then
    sleep 2
    java  -Xmx2048m $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr="localhost" -cp $JARS  fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 1 -i 0 -x 500 -y 0 -n $NAME &
fi

if [ "x$CONSOLES" != "xno" ]; then
    sleep 2
    java  -Xmx2048m $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr="localhost" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 1 -x 0 -y 710 -n $NAME &
    sleep 2
    java  -Xmx2048m $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr="localhost" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 2 -x 650 -y 710 -n $NAME &

fi

sleep 2

JARS="target/aspectjrt-1.6.5.jar"
JARS=$JARS":target/jgroups-3.6.8.Final.jar"
JARS=$JARS":target/log4j-1.2.17.jar"
JARS=$JARS":target/slf4j-api-1.7.10.jar"
JARS=$JARS":target/slf4j-log4j12-1.7.10.jar"
JARS=$JARS":target/timingframework-1.0.jar"
JARS=$JARS":target/xercesImpl-2.8.1.jar"
JARS=$JARS":target/xml-apis-1.3.03.jar"
JARS=$JARS":target/xmlParserAPIs-2.6.2.jar"
JARS=$JARS":target/zvtm-svg-0.2.2-SNAPSHOT.jar"
JARS=$JARS":target/commons-logging-1.1.jar"
JARS=$JARS":target/args4j-2.0.29.jar"
JARS=$JARS":target/vrpn-1.0.jar"
JARS=$JARS":target/jinput-1.0.jar"
#
JARS=$JARS":"$JARS_GT
#
JARS=$JARS":target/javaSmarties-1.4.0.jar"
JARS=$JARS":target/"$WHATCLUSTER 
JARS=$JARS":target/zcsample-0.1.0-SNAPSHOT.jar"

echo ""
echo java -Xmx2G -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="localhost" -Djava.library.path=".:lib" -cp $JARS $CLASS $REST

echo ""

java -Xmx2G -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="localhost" -Djava.library.path=".:lib" -cp $JARS $CLASS $REST

killall java