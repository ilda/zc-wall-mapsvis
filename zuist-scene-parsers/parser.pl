#!/usr/bin/perl -w

use Getopt::Long;
use XML::Simple;
use Data::Dumper;
use File::Basename;
use File::Path qw(make_path);

my $Help = "\n" .
"Usage:   -d indir -i infile -outdir outdir[]=test -outfile outfile[=scene.xml]\n" .
"\n";

my $dir = "";
my $infile = "";
my $outfile = "scene.xml";
my $outdir = "test/";
my $convert;
my $help;


GetOptions (
	"dir=s" => \$dir,
	"infile=s" => \$infile,
	"outfile=s" => \$outfile,
	"outdir=s" => \$outdir,
	"convert" => \$convert,
	"help" => \$help,
) or die("Error in command line arguments\n");

if ($help) {
	print $Help;
	exit(0);
}


$infile = $dir . $infile;


if (not -f $infile) {
	print STDERR "ERROR: File \"$infile\" could not be found!\n\n";
	exit 100;
}

my $xml = new XML::Simple;

# read XML file
my $data = $xml->XMLin($infile, KeyAttr => {  });

# my $nxml = XMLout($data, RootName => 'scene', KeyAttr => {  }); # NoIndent => 1);
# $outfile = $outdir.$outfile;
# open FILE, ">$outfile" or die "ERROR: Could not write to $outfile: $!";
# print FILE $nxml;
# close FILE;
# exit(0);

# we assume that the first region describe the scene !
my $fullW;
my $fullH;
my $mainX;
my $mainY;
my $getFirstRegion = 0;

foreach my $k  (keys(%$data)) {
	print $k . "\n";
	next if (! ($k eq "region"));
	print $data->{$k} . "\n";
	my $i;
	for($i = 0; $i < @{$data->{$k}}; $i++) { ##  (@{$data->{$k}}) {
		print "\t" . $data->{$k}->[$i]->{"id"} . "\n";
		if ($getFirstRegion == 0 &&  $data->{$k}->[$i]->{"id"} eq "R-1-0-0-0-0-0-0"){
			$getFirstRegion = 1;
			$fullW = $data->{$k}->[$i]->{"w"};
			$fullH = $data->{$k}->[$i]->{"h"};
			$mainX = $data->{$k}->[$i]->{"x"};
			$mainY = $data->{$k}->[$i]->{"y"};
			print "sd, w: ". $fullW . " h: " . $fullH . " x: ". $mainX . " y: " . $mainY. "\n";
			last;
		}
	}
}

foreach my $k  (keys(%$data)) {
	print $k . "\n";
	next if (! ($k eq "region"));
	print $data->{$k} . "\n";
	my $i;
	for($i = 0; $i < @{$data->{$k}}; $i++)
	{
		my $sw =  $data->{$k}->[$i]->{"w"};
		$data->{$k}->[$i]->{"w"} = $data->{$k}->[$i]->{"h"};
		$data->{$k}->[$i]->{"h"} = $sw;
		my $sx =   $data->{$k}->[$i]->{"x"};
		my $sy =   $data->{$k}->[$i]->{"y"};
		$data->{$k}->[$i]->{"x"} = -$sy;
		$data->{$k}->[$i]->{"y"} = $sx;

		foreach my $kkk  (keys($data->{$k}->[$i])) {
			#print "\t" . $kkk . "\n";
			next if (! ($kkk eq "resource"));
			my $img = $data->{$k}->[$i]->{$kkk}->{"src"};
			my $dirname  = dirname($img);
			#print $outdir.$dirname. "\n";
			make_path($outdir.$dirname);
			my $imgf = $dir . $img;
			my $outimgf = $outdir . $img;
			if (not -f $imgf){
				print "WARN: could not found " . $imgf . "\n";
				#print "convert $imgf \n"
			}
			elsif ($convert) {
				#print "convert $imgf -rotate 90 $outimgf\n";
				system("convert $imgf -rotate -90 $outimgf");
			}
			$sw =   $data->{$k}->[$i]->{$kkk}->{"w"};
			$data->{$k}->[$i]->{$kkk}->{"w"} = $data->{$k}->[$i]->{$kkk}->{"h"};
			$data->{$k}->[$i]->{$kkk}->{"h"} = $sw;
			$sx =   $data->{$k}->[$i]->{$kkk}->{"x"};
			$sy =   $data->{$k}->[$i]->{$kkk}->{"y"};
			$data->{$k}->[$i]->{$kkk}->{"x"} = -$sy;
			$data->{$k}->[$i]->{$kkk}->{"y"} = $sx;
		}
	}
}

#print Dumper($data);
my $nxml = XMLout($data, RootName => 'scene', KeyAttr => {  }); #NoIndent => 1, NoSort => 1);

$outfile = $outdir.$outfile;

open FILE, ">$outfile" or die "ERROR: Could not write to $outfile: $!";
print FILE $nxml;
close FILE;
