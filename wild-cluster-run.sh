#!/bin/bash

# usage example: 
#		./wild-cluster-run.sh -ip=145 -n=Sample -c=ilda.zcsample.Sample  -- other option

# will run slaves for each cluster screen, joining the application named 'Sample'
# and then run the master application 

NAME=""
CLASS=""
LIP="192.168.0.2"
accum_rest=0
REST=""

for i in "$@"
do
case $i in
    -n=*)
    	NAME="${i#*=}"
    ;;
    -c=*)
    	CLASS="${i#*=}"
    ;;
    -ip=*)
    	#LIP="192.168.0.${i#*=}"
        LIP="${i#*=}"
    ;;
    --)
  		accum_rest=1
    ;;
    *)
		if [ $accum_rest == 1 ]; 
			then REST="$REST $i";
		fi
    ;;
esac
#shift
done

echo "NAME: " $NAME
echo "CLASS: " $CLASS
echo "REST: " $REST
echo "LIP: " $LIP

#exit

function colNum {
  case "$1" in 
	  "a" ) return 0;;
	  "b" ) return 1;;
	  "c" ) return 2;;
	  "d" ) return 3;;
  esac
}

#start client nodes
for col in {a..d}
do
	for row in {1..4}
      do
		  colNum $col 
		  SLAVENUM1=`expr $? \* 8 + $row - 1`
		  SLAVENUM2=`expr $SLAVENUM1 + 4`
                  colNum $col
                  IP=`expr 32 + $? \* 4 + $row - 1`


		ssh wild@$col$row.wild.lri.fr -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "cd /opt/zvtm-cluster/ && DISPLAY=:0.0 java  -Xmx2g -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.0.$IP" -cp 'target/*' fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM1  -r 5  -x -100 -y -100 -d :0.0 -n $NAME" &
		sleep 1
		ssh wild@$col$row.wild.lri.fr -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "cd /opt/zvtm-cluster/ && DISPLAY=:0.1 java  -Xmx2g -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.0.$IP" -cp 'target/*' fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM2  -r 5  -x -100 -y -100 -d :0.1 -n $NAME" &
      done
done

sleep 2

#java -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.0.56" -Djava.library.path=".:lib" -cp "target/*" $CLASS $REST
java -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.0.$LIP" -Djava.library.path=".:lib" -cp "target/*" $CLASS $REST

wildo killall java -9
