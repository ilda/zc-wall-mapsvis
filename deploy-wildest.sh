#!/bin/bash

####
# first time:
#
# cluster:
# ./deploy-wildest.sh -nc -i
#
# frontal:
# ./deploy-wildest.sh -nc -d -f -nocluster
# scp cmdlines wild@192.168.0.2:/home/wild/Demos/zc-sample/

### to update the frontal:
# ./deploy-wildest.sh -f -nocluster

### to update the cluster (but only when SlaveApp change .... rarely!)
# ./deploy-wildest.sh


PROGHOME="/home/wild/Demos/zc-sample"
TARGET="/home/wild/Demos/zc-sample/target"
DATACARAIBE="Caribe_Wave_2015"
DATAMARTINIQUE_DIR="Martinique-data"
IMAGESDIR="/media/ssd/Demos/zvtm"

CLEAN="yes"
CPDATA="no"
CLEANDATA="no"
CPIMAGES="no"
FRONTAL="no"
CLUSTER="yes"
LOGIN="wild"

for i in "$@"
do
  case $i in
    -nc)
	     CLEAN="no"
	   ;;
	   -d)
      CPDATA="yes"
      CLEANDATA="no"
	   ;;
    -cd)
      CLEANDATA="yes"
    ;;
    -i)
      CPIMAGES="yes"
    ;;
    -f)
      FRONTAL="yes"
    ;;
    -nocluster)
      CLUSTER="no"
    ;;
esac
done

function colIP {
  case "$1" in
          "a" ) return 0;;
          "b" ) return 1;;
  esac
}

echo "CLEAN:" $CLEAN
echo "CPDATA:" $CPDATA
echo "CLEANDATA:" $CLEANDATA


function colNum {
  case "$1" in 
    "a" ) return 0;;
    "b" ) return 1;;
    "c" ) return 2;;
    "d" ) return 3;;
  esac
}

if [ $CLUSTER == "yes" ]; then
  for col in {a..d}
  do
    for row in {1..4}
        do
          colNum $col
          startIp=`expr 32 + $? \* 4 + $row - 1`
        
          echo " "
          echo " "$LOGIN@$col$row.wild.lri.fr
          chost="192.168.0.$startIp"
          echo "copy on \"$chost\" "

          if [ $CLEAN == "yes" ]; then
            # clean 
          	echo "ssh $LOGIN@$chost ; rm -rf $TARGET"
          	ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "rm -rf $TARGET"
          fi
          echo "ssh $LOGIN@$chost; mkdir -p $PROGHOME"
          ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "mkdir -p $PROGHOME"
          echo "scp -r target/ $LOGIN@$chost:$PROGHOME"
          scp -r target/ $LOGIN@$chost:$PROGHOME
          if [ $CPIMAGES == "yes" ]; then
              echo "ssh $LOGIN@$chost ; mkdir -p $IMAGESDIR"
              ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "mkdir -p $IMAGESDIR"
              echo "scp -r /media/ssd/Demos/zvtm/martinique_IGN_ortho/ $LOGIN@$chost:$IMAGESDIR"
              scp -r /media/ssd/Demos/zvtm/martinique_IGN_ortho/ $LOGIN@$chost:$IMAGESDIR
              echo "scp -r /media/ssd/Demos/zvtm/martinique_IGN_scan25/ $LOGIN@$chost:$IMAGESDIR"
              scp -r /media/ssd/Demos/zvtm/martinique_IGN_scan25/ $LOGIN@$chost:$IMAGESDIR
              echo "scp -r /media/ssd/Demos/zvtm/carribean_osm/ $LOGIN@$chost:$IMAGESDIR"
              scp -r /media/ssd/Demos/zvtm/carribean_osm/ $LOGIN@$chost:$IMAGESDIR
              scp -r /media/ssd/Demos/zvtm/P $LOGIN@$chost:$IMAGESDIR
          fi
        done
  done
fi

if [ $FRONTAL == "yes" ]; then
    startIp=2 
    echo " "
    echo "FRONTAL"
    chost="192.168.0.$startIp"

    if [ $CLEAN == "yes" ]; then
      # clean 
      echo "ssh $LOGIN@$chost ; rm -rf $TARGET"
      ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "rm -rf $TARGET"
    fi
    echo "ssh $LOGIN@$chost; mkdir -p $PROGHOME"
    ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "mkdir -p $PROGHOME"
    echo "scp -r target/ $LOGIN@$chost:$PROGHOME"
    scp -r target/ $LOGIN@$chost:$PROGHOME
    if [ $CPIMAGES == "yes" ]; then
        echo "ssh $LOGIN@$chost ; mkdir -p $IMAGESDIR"
        ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "mkdir -p $IMAGESDIR"
        echo "scp -r /media/ssd/Demos/zvtm/martinique_IGN_ortho/ $LOGIN@$chost:$IMAGESDIR"
        scp -r /media/ssd/Demos/zvtm/martinique_IGN_ortho/ $LOGIN@$chost:$IMAGESDIR
        echo "scp -r /media/ssd/Demos/zvtm/martinique_IGN_scan25/ $LOGIN@$chost:$IMAGESDIR"
        scp -r /media/ssd/Demos/zvtm/martinique_IGN_scan25/ $LOGIN@$chost:$IMAGESDIR
        echo "scp -r /media/ssd/Demos/zvtm/carribean_osm/ $LOGIN@$chost:$IMAGESDIR"
        scp -r /media/ssd/Demos/zvtm/carribean_osm/ $LOGIN@$chost:$IMAGESDIR
    fi
    ## data only for frontal ...
    if [ $CPDATA == "yes" ]; then
        echo "ssh $LOGIN@$chost ; mkdir -p $PROGHOME/$DATAMARTINIQUE_DIR"
        ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "mkdir -p $PROGHOME/$DATAMARTINIQUE_DIR"
        echo "scp -r Martinique-data/1_Evacuation  $LOGIN@$chost:$PROGHOME/$DATAMARTINIQUE_DIR"
        scp -r Martinique-data/1_Evacuation  $LOGIN@$chost:$PROGHOME/$DATAMARTINIQUE_DIR
        echo "scp -r Caribe_Wave_2015/ $LOGIN@$chost:$PROGHOMECaribe_Wave_2015"
        scp -r Caribe_Wave_2015/ $LOGIN@$chost:$PROGHOMECaribe_Wave_2015
    fi
fi