#!/bin/bash

# -vicon-objects list of ":" separated vicon object description:  a "," separated
# list: object_name[,pointer|tracker[,filter_name[,a/b/c..]]] 
#
# filter: Mean[,numsmooth,radius] 
#         OneEuro[,mincutoff,beta,freq]
#

./wilder-cluster-run.sh -ip=2 -n=Sample -c=ilda.zcsample.Sample -l=wild -- -vicon-objects "Lego,pointer,mean:hand" -wall wilder
