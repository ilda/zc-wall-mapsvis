# zc-wall-mapsvis

An images/maps visualisation and navigation application for wall displays driven by a rendering cluster of computers. Based on zvtm and zvtm-cluster. 

## Install

You need maven ... TBD ... email me olivier.chapuis@lisn.upsaclay.fr

-> zvtm-core: 



-> zvtm-cluster:
-> zuist:
-> zuist-cluster:

## Run

See HELP.txt and cmdline.txt

## License

GPL

## Project status

TBD
