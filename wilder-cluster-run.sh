#!/bin/bash

# usage example: 
#		./wilder-cluster-run.sh -ip=144 -n=Sample -c=ilda.zcsample.Sample -l=chapuis -- other option
# will run slaves for each cluster screen, joining the application named 'Sample'
# and then run the master application 

#echo "$@"

## ./wilder_all_run.sh /media/ssd/Demos/zvtm/paris26GP/scene_fullL0.xml

LOGIN="wild"
NAME="Sample"
CLASS="ilda.zcsample.Sample"
LIP="2"
accum_rest=0
REST=""
CONSOLES="no"

#  /home2/wild/workspace/zvtm/zuist-cluster/trunk
#ZVTMHOME="/media/data/Demos/zvtm/lib"
#ZVTMHOME="/home2/wild/workspace/zvtm/zuist-cluster/trunk"
#ZVTMHOME="/home2/wild/workspace/zc-wall-mapsvis"
ZVTMHOME="/home2/wild/workspace/zc-sample"

WHATCLUSTER="zvtm-cluster-0.2.10.oc-SNAPSHOT.jar"

## export LD_LIBRARY_PATH="/usr/lib/jvm/default-java/lib/"
## export LD_LIBRARY_PATH=/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/
## java-1.11.0-openjdk-amd64

for i in "$@"
do
case $i in
    -n=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
    	 NAME="${i#*=}"
      fi
    ;;
    -c=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
    	   CLASS="${i#*=}"
      fi
    ;;
    -z)
        WHATCLUSTER="zuist-cluster-0.3.0.oc-SNAPSHOT.jar"
        REST="$REST $i"
    ;;
    -ip=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
    	   #LIP="192.168.0.${i#*=}"
        LIP="${i#*=}"
      fi
    ;;
    -l=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
        LOGIN="${i#*=}"
      fi
    ;;
    -co=*)
        if [ $accum_rest == 1 ]; then
            REST="$REST -co \"${i#*=}\"";
        else
            CONSOLES="${i#*=}"
        fi
    ;;
    --)
  		accum_rest=1
    ;;
    *)
		if [ $accum_rest == 1 ]; 
			then REST="$REST $i";
		fi
    ;;
esac
#shift
done

echo "LOGIN: " $LOGIN
echo "NAME: " $NAME
echo "CLASS: " $CLASS
echo "REST: " $REST
echo "LIP: " $LIP
echo "CONSOLES: " $CONSOLES
echo "WHATCLUSTER: " $WHATCLUSTER

GTV="26.0"
JARS_GT=":target/gt-api-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-main-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-opengis-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-shapefile-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-data-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-metadata-$GTV.jar"
JARS_GT=$JARS_GT":target/gt-referencing-$GTV.jar"
JARS_GT=$JARS_GT":target/jai_core-1.1.3.jar"
JARS_GT=$JARS_GT":target/jdom-1.1.3.jar"
JARS_GT=$JARS_GT":target/jsr-275-1.0-beta-2.jar"
JARS_GT=$JARS_GT":target/jts-1.13.jar"
JARS_GT=$JARS_GT":target/vecmath-1.3.2.jar"
JARS_GT=$JARS_GT":target/commons-codec-1.9.jar"
JARS_GT=$JARS_GT":target/commons-pool-1.5.4.jar"

##echo "JARS_GT:"$JARS_GT
##exit

TARGET="target/"
JARS=$TARGET"commons-logging-1.1.jar"
JARS=$JARS":"$TARGET"args4j-2.0.29.jar"
JARS=$JARS":"$TARGET"aspectjrt-1.6.5.jar"
JARS=$JARS":"$TARGET"jgroups-3.6.8.Final.jar"
JARS=$JARS":"$TARGET"log4j-1.2.17.jar"
JARS=$JARS":"$TARGET"slf4j-api-1.7.10.jar"
JARS=$JARS":"$TARGET"slf4j-log4j12-1.7.10.jar"
JARS=$JARS":"$TARGET"timingframework-1.0.jar"
JARS=$JARS":"$TARGET$WHATCLUSTER

function colNum {
  case "$1" in
          "a" ) return 0;;
          "b" ) return 2;;
  esac
}

function colIP {
  case "$1" in
          "a" ) return 0;;
          "b" ) return 1;;
  esac
}

function startId {
  case "$1" in 
      "a" ) return 0;;
      "b" ) return 40;;
  esac
}


function blockNum {
  case "$1" in 
      "a" ) return 8;;
      "b" ) return 7;;
  esac
}

NUMBER_OF_SLAVES=10

for col in {a..b}
do
    for row in {1..5}
    do
        SLAVENUM=`expr $row - 1`
        startId $col
        SLAVENUM=`expr $? + $SLAVENUM`
        colIP $col
        startIp=`expr $? + 1`
        startIp=`expr $startIp \* 10`
        startIp=`expr $startIp + $row` 
        blockNum $col
        BLOCKNB=$?
        echo "$LOGIN@$col$row"
        echo "-Djgroups.bind_addr=\"192.168.2.$startIp\" Slavenum: $SLAVENUM $BLOCKNB"
        
        ### 1 slave
        ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java -Xmx1024m -server -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM -wb $BLOCKNB -u -f -a -n $NAME" &
        ###

        ### 2 slaves
        #ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java -Xmx1024m -server -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM -wb 4 -x 0 -u -n $NAME" &
        #sleep 1
        #SLAVENUM=`expr 20 + $SLAVENUM`
        #ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java -server -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM  -wb 4 -x 3840 -u -n $NAME" &
        #NUMBER_OF_SLAVES=20

        #### 4 slaves
        #ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java -server -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM  -wb 2 -u -n $NAME" &
        #SLAVENUM=`expr 10 + $SLAVENUM`
        #echo "-Djgroups.bind_addr=\"192.168.2.$startIp\" Slavenum: $SLAVENUM $BLOCKNB"
        #ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java -server -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM  -wb 2 -x 1920 -u -n $NAME" &
        #sleep 1
        #SLAVENUM=`expr 10 + $SLAVENUM`
        #ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java -server -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM  -wb 2 -x 3840 -u -n $NAME" &
        #sleep 1
        #SLAVENUM=`expr 10 + $SLAVENUM`
        #ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java -server -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp -b $SLAVENUM  -wb 2 -x 5760 -u -n $NAME" &
        ####

        sleep 1
        #break
      done
      #break
done

#exit
if [ "x$CONSOLES" != "xno" ]; then
    echo ""
    echo "Start the console...."
    echo ""
    ## frontal 1920,1080
    startIp=2 
    ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 1 -u -a -n $NAME" &
    sleep 1
    # console1 1920,1200
    #startIp=163
    #ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java  $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 2 -u -f -a -n $NAME" &
    # console2 1920,1200
    startIp=159
    ssh $LOGIN@192.168.2.$startIp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "export DISPLAY=:0 ; cd $ZVTMHOME ; java  $SLAVE_DSUNOPENGL -server -XX:+DoEscapeAnalysis -Djava.net.preferIPv4Stack=true $SLAVE_DSUNOPENGL -Djgroups.bind_addr=\"192.168.2.$startIp\" -cp $JARS fr.inria.zvtm.cluster.SlaveApp $SLAVEOPTS -u -b 0 -i 2 -u -f -a -n $NAME" &
fi

echo ""
echo "Now start master...."
echo ""

sleep 3
JARS="target/aspectjrt-1.6.5.jar"
#JARS=$JARS":target/jgroups-2.7.0.GA.jar"
JARS=$JARS":target/jgroups-3.6.8.Final.jar"
JARS=$JARS":target/log4j-1.2.17.jar"
JARS=$JARS":target/slf4j-api-1.7.10.jar"
JARS=$JARS":target/slf4j-log4j12-1.7.10.jar"
JARS=$JARS":target/timingframework-1.0.jar"
JARS=$JARS":target/xercesImpl-2.8.1.jar"
JARS=$JARS":target/xml-apis-1.3.03.jar"
JARS=$JARS":target/xmlParserAPIs-2.6.2.jar"
#JARS=$JARS":target/zvtm-svg-0.2.1.jar"
JARS=$JARS":target/zvtm-svg-0.2.2-SNAPSHOT.jar"
JARS=$JARS":target/commons-logging-1.1.jar"
JARS=$JARS":target/args4j-2.0.29.jar"
JARS=$JARS":target/vrpn-1.0.jar"
JARS=$JARS":target/jinput-1.0.jar"
#
JARS=$JARS":"$JARS_GT
#
JARS=$JARS":target/javaSmarties-1.4.0.jar"
JARS=$JARS":target/"$WHATCLUSTER 
JARS=$JARS":target/zcsample-0.1.0-SNAPSHOT.jar"

echo ""
echo "cp  "$JARS
echo ""

#java -Xmx8g -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.2.$LIP" -Djava.library.path=".:lib:/usr/lib/jvm/java-1.7.0-openjdk-amd64/jre/lib/amd64" -cp $JARS $CLASS -r 5 -c 15 -bw 960 -bh 960 $REST
##java -Xmx8g -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.2.$LIP" -Djava.library.path=".:lib:/usr/lib/jvm/java-1.8.0-oracle/jre/lib/amd64" -cp $JARS $CLASS -r 5 -c 16  -bw 960 -bh 960 -ns $NUMBER_OF_SLAVES $REST
#java -Xmx8g -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.2.$LIP" -Djava.library.path=".:lib:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/" -cp $JARS $CLASS -ns $NUMBER_OF_SLAVES -wall wilder $REST
LD_LIBRARY_PATH=/usr/lib/jvm/java-11-openjdk-amd64/lib java -Xmx8g -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr="192.168.2.$LIP" -Djava.library.path=".:lib:/usr/lib/jvm/java-11-openjdk-amd64/lib" -cp $JARS $CLASS -ns $NUMBER_OF_SLAVES -wall wilder $REST

#wildo killall java
#WALL=WILDER 
WALL=WILDER  walldo -l $LOGIN killall java
#walldo killall java
